package com.peachy.web;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.*;
import java.util.stream.Collectors;

public class MutableHeadersHttpServletRequest extends HttpServletRequestWrapper {
    private final Set<Pair<String,String>> headers;

    public MutableHeadersHttpServletRequest(HttpServletRequest request) {
        super(request);
        this.headers =
                Collections.list(request.getHeaderNames()).stream()
                        .map(headerName -> Pair.of(headerName,request.getHeader(headerName)))
                        .collect(Collectors.toSet());
    }

    @Override
    public String getHeader(String name) {
        return headerByName(name)
                .map(Pair::getRight)
                .orElse(null);
    }

    public void addHeader(String headerName,
                          String headerValue) {
        Optional<Pair<String,String>> optionalHeader = headerByName(headerName);

        if (optionalHeader.isPresent()) {
            removeHeader(headerName);
            addHeader(headerName,headerValue);
        } else {
            headers.add(Pair.of(headerName,headerValue));
        }
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        return Collections.enumeration(
                headers.stream()
                        .filter(header -> StringUtils.equalsIgnoreCase(header.getLeft(),name))
                        .map(Pair::getRight)
                        .collect(Collectors.toSet()));
    }

    public void removeHeader(String headerName) {
        headerByName(headerName)
                .ifPresent(headers::remove);
    }

    private Optional<Pair<String,String>> headerByName(String name) {
        return headers.stream()
                .filter(header -> StringUtils.equalsIgnoreCase(header.getLeft(),name))
                .findAny();
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        return Collections.enumeration(headers.stream()
                .map(Pair::getLeft)
                .collect(Collectors.toList()));
    }
}
