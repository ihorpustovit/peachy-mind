package com.peachy.web;


import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(assignableTypes = PeachyMindController.class)
@RequiredArgsConstructor
public class ExceptionTranslator {

    @ExceptionHandler(PeachyMindException.class)
    public ResponseEntity<String> peachyMindExceptionHandler(PeachyMindException peachyMindException) {
        return ResponseEntity.status(peachyMindException.getHttpStatus())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(peachyMindException.toString());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<String> nullPointerExceptionHandler(NullPointerException e) {
        e.printStackTrace();
        return peachyMindExceptionHandler(ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR))
                .withMessage("Internal exception occurred")
                .withReason(e.getStackTrace()[0].toString())
                .build());
    }
}
