package com.peachy.web.config;

import com.peachy.web.filter.ExceptionTranslatingFilter;
import com.peachy.web.filter.exception.translator.FilterExceptionTranslator;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

@Configuration
@RequiredArgsConstructor
public class WebConfig {
    private final FilterExceptionTranslator filterExceptionTranslatorProvider;

    @Bean
    public FilterRegistrationBean<ExceptionTranslatingFilter> peachyMindExceptionHandlingFilter() {
        FilterRegistrationBean<ExceptionTranslatingFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        ExceptionTranslatingFilter exceptionTranslatingFilter = new ExceptionTranslatingFilter(filterExceptionTranslatorProvider);
        filterRegistrationBean.setFilter(exceptionTranslatingFilter);
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE + 1);
        return filterRegistrationBean;
    }
}
