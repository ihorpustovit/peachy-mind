package com.peachy.web.filter.exception.translator;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class FilterExceptionTranslatorProvider implements FilterExceptionTranslator {
    private final List<AbstractFilterExceptionTranslator> filterExceptionTranslators;


    @Override
    public PeachyMindException translate(Throwable e) {
        return filterExceptionTranslators.stream()
                .filter(filterExceptionTranslator -> filterExceptionTranslator.canTranslate(e))
                .findAny()
                .orElseThrow(() ->
                        ExceptionBuilder.buildFrom(new BadArgumentException("Unable to handle a request"))
                                .withReason(String.format("Unable to translate an filter thrown exception into %s", PeachyMindException.class.getSimpleName()))
                                .withSolution(String.format("Add a new implementation of %s that would translate, %s into %s",
                                        AbstractFilterExceptionTranslator.class.getSimpleName(),
                                        e.getClass().getSimpleName(),
                                        PeachyMindException.class.getSimpleName()))
                                .build())
                .translate(e);
    }

    @Override
    public boolean canTranslate(Throwable e) {
        return false;
    }
}
