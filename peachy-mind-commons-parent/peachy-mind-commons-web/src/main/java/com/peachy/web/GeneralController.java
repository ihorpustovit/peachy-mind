package com.peachy.web;

import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class GeneralController implements PeachyMindController {

    private final Environment environment;

    @GetMapping("/")
    public ResponseEntity<String> health() {
        return ResponseEntity.ok("UP");
    }

    @GetMapping("/general/port/defined")
    public ResponseEntity<String> definedPort() {
        return ResponseEntity.ok(Optional.ofNullable(environment.getProperty("self.port")).orElse("undefined"));
    }

    @GetMapping("/general/port/actual")
    public ResponseEntity<String> actualPort() {
        return ResponseEntity.ok(Optional.ofNullable(environment.getProperty("local.server.port")).orElse("undefined"));
    }
}
