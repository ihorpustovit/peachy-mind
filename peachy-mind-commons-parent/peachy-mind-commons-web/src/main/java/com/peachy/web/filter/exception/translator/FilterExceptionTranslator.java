package com.peachy.web.filter.exception.translator;

import com.peachy.commons.exception.PeachyMindException;

public interface FilterExceptionTranslator {
    PeachyMindException translate(Throwable e);

    boolean canTranslate(Throwable e);
}
