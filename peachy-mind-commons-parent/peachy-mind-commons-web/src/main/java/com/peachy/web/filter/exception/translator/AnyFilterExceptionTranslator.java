package com.peachy.web.filter.exception.translator;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
@Order
public class AnyFilterExceptionTranslator extends AbstractFilterExceptionTranslator {
    @Override
    public PeachyMindException translate(Throwable e) {
        return ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()))
                .withReason(String.format("Exception is not of %s type, hence is strange exception", PeachyMindException.class.getSimpleName()))
                .withSolution(String.format("It is not a good practice to throw exceptions of any type but %s anywhere in this application. Consider finding a place from where an exception was thrown and convert it to eligible type there", PeachyMindException.class.getSimpleName()))
                .withFault("Developer's fault")
                .build();
    }

    @Override
    public boolean canTranslate(Throwable e) {
        return true;
    }
}
