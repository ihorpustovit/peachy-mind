package com.peachy.web.filter.exception.translator;

import com.peachy.commons.exception.PeachyMindException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class PeachyMindFilterExceptionTranslator extends AbstractFilterExceptionTranslator {
    @Override
    public PeachyMindException translate(Throwable e) {
        return (PeachyMindException) e;
    }

    @Override
    public boolean canTranslate(Throwable e) {
        return PeachyMindException.class.isAssignableFrom(e.getClass());
    }
}
