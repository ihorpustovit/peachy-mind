package com.peachy.web.filter;

import com.peachy.commons.exception.PeachyMindException;
import com.peachy.web.PeachyMindHttpHeaders;
import com.peachy.web.filter.exception.translator.FilterExceptionTranslator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.NestedServletException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequiredArgsConstructor
public class ExceptionTranslatingFilter extends OncePerRequestFilter {

    private final FilterExceptionTranslator filterExceptionTranslatorProvider;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (NestedServletException e) {
            writeExceptionToResponse(filterExceptionTranslatorProvider.translate(e.getCause()), response);
        } catch (Exception e) {
            writeExceptionToResponse(filterExceptionTranslatorProvider.translate(e), response);
        }
    }

    private void writeExceptionToResponse(PeachyMindException e, HttpServletResponse response) throws IOException {
        response.setStatus(e.getHttpStatus().value());
        response.setHeader(PeachyMindHttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(e.toString());
    }
}
