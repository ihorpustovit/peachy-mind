package com.peachy.jpa.converter;

import com.peachy.commons.utils.Converter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface EntityDtoConverter<E, D> extends Converter<E, D> {
    default D toDto(E entity) {
        return to(entity);
    }

    default E toEntity(D dto) {
        return from(dto);
    }

    default List<D> toDto(List<E> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

    default List<E> toEntity(List<D> dtos) {
        return dtos.stream().map(this::toEntity).collect(Collectors.toList());
    }

    default Optional<D> toDto(Optional<E> entity) {
        return entity.map(this::toDto);
    }

    default Optional<E> toEntity(Optional<D> dto) {
        return dto.map(this::toEntity);
    }
}
