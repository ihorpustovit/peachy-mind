package com.peachy.jpa.config.datasource.peachymindauz;

import com.peachy.commons.condition.property.present.ConditionalOnPropertyPresent;
import com.peachy.jpa.config.datasource.AbstractDatasourceConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = {"com.peachy.security.repository", "com.peachy.gateway.repository"},
        entityManagerFactoryRef = "peachyMindAuzEntityManagerFactory",
        transactionManagerRef = "peachyMindAuzTransactionManager")
@EnableTransactionManagement
@EnableJpaAuditing
@ConditionalOnPropertyPresent("db.peachy-mind-auz.name")
@RequiredArgsConstructor
public class PeachyMindAuzDatasourceConfig extends AbstractDatasourceConfig {
    protected final Environment environment;

    @Override
    protected String getDbHost() {
        return environment.getProperty("db.peachy-mind-auz.host");
    }

    @Override
    protected long getDbPort() {
        return Integer.parseInt(environment.getProperty("db.peachy-mind-auz.port"));
    }

    @Override
    protected String getDbName() {
        return environment.getProperty("db.peachy-mind-auz.name");
    }

    @Override
    protected String getUsername() {
        return environment.getProperty("db.peachy-mind-auz.credentials.username");
    }

    @Override
    protected String getPassword() {
        return environment.getProperty("db.peachy-mind-auz.credentials.password");
    }

    @Override
    protected String[] getPackagesToScan() {
        return new String[]{"com.peachy.security.entity", "com.peachy.gateway.entity"};
    }

    @Bean
    public DataSource peachyMindAuzDataSource() {
        return createDataSource();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean peachyMindAuzEntityManagerFactory(@Qualifier("peachyMindAuzDataSource") DataSource peachyMindAuzDataSource) {
        return createEntityManagerFactory(peachyMindAuzDataSource);
    }

    @Bean
    public TransactionManager peachyMindAuzTransactionManager(@Qualifier("peachyMindAuzEntityManagerFactory") EntityManagerFactory peachyMindAuzEntityManagerFactory) {
        return createTransactionManager(peachyMindAuzEntityManagerFactory);
    }

    @Bean
    public JdbcTemplate peachyMindAuzJdbcTemplate(@Qualifier("peachyMindAuzDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
