package com.peachy.jpa.config.datasource;

import com.peachy.jpa.config.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

@Configuration
public class GeneralDatasourceConfig {
    @Bean
    public PrimaryDataSourceDecisionMaker primaryDataSourceDecisionMaker(ConfigurableEnvironment configurableEnvironment) {
        return new PrimaryDataSourceDecisionMaker(new EarlyPropertyReader(configurableEnvironment));
    }

    @Bean
    public PrimaryEntityManagerFactoryDecisionMaker primaryEntityManagerFactoryDecisionMaker(ConfigurableEnvironment configurableEnvironment) {
        return new PrimaryEntityManagerFactoryDecisionMaker(new EarlyPropertyReader(configurableEnvironment));
    }

    @Bean
    public PrimaryTransactionManagerDecisionMaker primaryTransactionManagerDecisionMaker(ConfigurableEnvironment configurableEnvironment) {
        return new PrimaryTransactionManagerDecisionMaker(new EarlyPropertyReader(configurableEnvironment));
    }

    @Bean
    public PrimaryJdbcTemplateDecisionMaker primaryJdbcTemplateDecisionMaker(ConfigurableEnvironment configurableEnvironment) {
        return new PrimaryJdbcTemplateDecisionMaker(new EarlyPropertyReader(configurableEnvironment));
    }
}
