package com.peachy.jpa.config;

import javax.sql.DataSource;

public class PrimaryDataSourceDecisionMaker extends PrimaryJpaBeanDecisionMaker {
    public PrimaryDataSourceDecisionMaker(EarlyPropertyReader earlyPropertyReader) {
        super(earlyPropertyReader, DataSource.class, "dataSource");
    }
}
