package com.peachy.jpa.config;

import org.springframework.jdbc.core.JdbcTemplate;

public class PrimaryJdbcTemplateDecisionMaker extends PrimaryJpaBeanDecisionMaker {
    public PrimaryJdbcTemplateDecisionMaker(EarlyPropertyReader earlyPropertyReader) {
        super(earlyPropertyReader, JdbcTemplate.class, "jdbcTemplate");
    }
}
