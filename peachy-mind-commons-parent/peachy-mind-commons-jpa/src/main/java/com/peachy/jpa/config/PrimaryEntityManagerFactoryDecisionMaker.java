package com.peachy.jpa.config;

import javax.persistence.EntityManagerFactory;

public class PrimaryEntityManagerFactoryDecisionMaker extends PrimaryJpaBeanDecisionMaker {
    public PrimaryEntityManagerFactoryDecisionMaker(EarlyPropertyReader earlyPropertyReader) {
        super(earlyPropertyReader, EntityManagerFactory.class, "entityManagerFactory");
    }
}
