package com.peachy.jpa.config.datasource.peachymind;

import com.peachy.commons.condition.property.present.ConditionalOnPropertyPresent;
import com.peachy.jpa.config.datasource.AbstractDatasourceConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.peachy.repository",
        entityManagerFactoryRef = "peachyMindEntityManagerFactory",
        transactionManagerRef = "peachyMindTransactionManager")
@EnableTransactionManagement
@EnableJpaAuditing
@ConditionalOnPropertyPresent("db.peachy-mind.name")
@RequiredArgsConstructor
public class PeachyMindDatasourceConfig extends AbstractDatasourceConfig {
    protected final Environment environment;

    @Override
    protected String getDbHost() {
        return environment.getProperty("db.peachy-mind.host");
    }

    @Override
    protected long getDbPort() {
        return Integer.parseInt(environment.getProperty("db.peachy-mind.port"));
    }

    @Override
    protected String getDbName() {
        return environment.getProperty("db.peachy-mind.name");
    }

    @Override
    protected String getUsername() {
        return environment.getProperty("db.peachy-mind.credentials.username");
    }

    @Override
    protected String getPassword() {
        return environment.getProperty("db.peachy-mind.credentials.password");
    }

    @Override
    protected String[] getPackagesToScan() {
        return new String[]{"com.peachy.entity", "com.peachy.security.entity"};
    }

    @Bean
    public DataSource peachyMindDataSource() {
        return createDataSource();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean peachyMindEntityManagerFactory(@Qualifier("peachyMindDataSource") DataSource dataSource) {
        return createEntityManagerFactory(dataSource);
    }

    @Bean
    public TransactionManager peachyMindTransactionManager(@Qualifier("peachyMindEntityManagerFactory") EntityManagerFactory peachyMindEntityManagerFactory) {
        return createTransactionManager(peachyMindEntityManagerFactory);
    }

    @Bean
    public JdbcTemplate peachyMindJdbcTemplate(@Qualifier("peachyMindDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
