package com.peachy.jpa.config;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.utils.assertion.Assert;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class PrimaryJpaBeanDecisionMaker implements BeanDefinitionRegistryPostProcessor {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final EarlyPropertyReader earlyPropertyReader;
    private final Class<?> beanType;
    private final String beanNameCommonPostfix;

    public PrimaryJpaBeanDecisionMaker(EarlyPropertyReader earlyPropertyReader,
                                       Class<?> beanType,
                                       String beanNameCommonPostfix) {
        this.earlyPropertyReader = earlyPropertyReader;
        this.beanType = beanType;
        this.beanNameCommonPostfix = beanNameCommonPostfix;
        ensureState();
    }

    private void ensureState() {
        Assert.that(beanType)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException(String.format("Failed to instantiate %s", PrimaryJpaBeanDecisionMaker.class.getSimpleName())))
                        .withReason("beanTypeName is null")
                        .build());

        Assert.that(beanNameCommonPostfix)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException(String.format("Failed to instantiate %s", PrimaryJpaBeanDecisionMaker.class.getSimpleName())))
                        .withReason("beanNameCommonPostfix is blank")
                        .build());
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {

    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        String primaryDatabaseName = earlyPropertyReader.readProperty("db.primary");
        boolean primaryDatabaseSpecified = StringUtils.isNotBlank(primaryDatabaseName);

        if (primaryDatabaseSpecified) {
            markTransactionManagerAsPrimaryForDB(primaryDatabaseName, beanFactory);
        }
    }

    private void markTransactionManagerAsPrimaryForDB(String dbName,
                                                      ConfigurableListableBeanFactory beanFactory) {
        String[] jpaBeanNames = beanFactory.getBeanNamesForType(beanType);
        String simplifiedDbName = dbName.replaceAll("[^a-zA-Z0-9]", StringUtils.EMPTY);

        List<String> primaryJpaBeanCandidatesNames = Stream.of(jpaBeanNames)
                .filter(beanName -> StringUtils.equalsIgnoreCase(simplifiedDbName, simplifyBeanName(beanName)))
                .collect(Collectors.toList());

        List<BeanDefinition> primaryJpaBeanCandidates = primaryJpaBeanCandidatesNames.stream()
                .map(beanFactory::getBeanDefinition)
                .collect(Collectors.toList());

        Assert.that(primaryJpaBeanCandidates)
                .isFalse(candidatesList -> candidatesList.size() > 1)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException(String.format("Failed to mark primary %s as primary", beanType.getSimpleName())))
                        .withReason(String.format("Several candidates found to be marked as primary: %s", primaryJpaBeanCandidatesNames))
                        .build())
                .isFalse(candidatesList -> candidatesList.size() < 1)
                .otherwise(() -> LOGGER.warn(String.format("Failed to mark primary %s as primary. No candidates found", beanType.getSimpleName())));

        if (primaryJpaBeanCandidates.size() == 1) {
            primaryJpaBeanCandidates.get(0).setPrimary(true);
        }
    }

    private String simplifyBeanName(String transactionManagerName) {
        return StringUtils.replaceIgnoreCase(transactionManagerName, beanNameCommonPostfix, StringUtils.EMPTY);
    }

}
