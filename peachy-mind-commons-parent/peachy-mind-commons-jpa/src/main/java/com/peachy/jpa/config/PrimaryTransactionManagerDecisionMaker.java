package com.peachy.jpa.config;

import org.springframework.transaction.TransactionManager;

public class PrimaryTransactionManagerDecisionMaker extends PrimaryJpaBeanDecisionMaker {
    public PrimaryTransactionManagerDecisionMaker(EarlyPropertyReader earlyPropertyReader) {
        super(earlyPropertyReader, TransactionManager.class, "transactionManager");
    }
}
