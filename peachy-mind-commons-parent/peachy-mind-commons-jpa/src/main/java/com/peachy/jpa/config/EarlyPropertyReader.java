package com.peachy.jpa.config;

import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Component;

@Component
public class EarlyPropertyReader {

    private final ConfigurableEnvironment configurableEnvironment;

    public EarlyPropertyReader(ConfigurableEnvironment configurableEnvironment) {
        this.configurableEnvironment = configurableEnvironment;
    }

    public String readProperty(String propertyName) {
        for (PropertySource<?> source : configurableEnvironment.getPropertySources()) {
            if (source instanceof EnumerablePropertySource) {
                EnumerablePropertySource<?> propertySource = (EnumerablePropertySource<?>) source;
                for (String property : propertySource.getPropertyNames()) {
                    if (propertyName.equals(property)) {
                        return (String) propertySource.getProperty(propertyName);
                    }
                }
            }
        }
        throw new IllegalStateException("Unable to determine value of property " + propertyName);
    }

}
