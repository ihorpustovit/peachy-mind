package com.peachy.security;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public enum TokenPrefix {
    BEARER("Bearer"),
    REFRESH("Refresh"),
    INTERNAL("Internal"),
    NONE(StringUtils.EMPTY);

    private final String stringyPrefix;

    TokenPrefix(String stringyPrefix) {
        this.stringyPrefix = stringyPrefix;
    }

    public String getStringyPrefix() {
        return stringyPrefix;
    }

    public static Optional<TokenPrefix> valueOfStringyPrefix(String stringyPrefix) {
        return Arrays.stream(values())
                .filter(tokenPrefix -> StringUtils.equals(tokenPrefix.getStringyPrefix(),stringyPrefix))
                .findFirst();
    }

    public static List<String> stringyTokenPrefixes() {
        return Arrays.stream(values())
                .map(TokenPrefix::getStringyPrefix)
                .collect(Collectors.toList());
    }
}
