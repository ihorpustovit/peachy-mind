package com.peachy.security.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.peachy.jpa.dto.BaseDTO;
import com.peachy.security.UserAuthority;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO
        extends BaseDTO
        implements UserDetails {
    private String id;

    private String username;

    private String plainPassword;

    private String hashedPassword;

    private UserAuthority authority;

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority(getAuthority().getStringyAuthority()));
    }

    @Override
    public String getPassword() {
        return hashedPassword;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String getPlainPassword() {
        return plainPassword;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }

    public UserAuthority getAuthority() {
        return Objects.isNull(authority) ? UserAuthority.NONE : authority;
    }
}
