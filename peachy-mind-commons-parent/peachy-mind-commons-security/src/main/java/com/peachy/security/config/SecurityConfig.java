package com.peachy.security.config;

import com.peachy.security.SecurityUtils;
import com.peachy.security.UserAuthority;
import com.peachy.security.filter.InternalAuthenticationFilter;
import com.peachy.security.service.auth.provider.InternalAuthenticationProvider;
import com.peachy.security.service.auth.token.InternalAuthTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final InternalAuthenticationProvider internalAuthenticationProvider;

    private final InternalAuthTokenService internalAuthTokenService;

    @Bean
    public RoleHierarchy roleHierarchy() {
        StringBuilder hierarchyExpressionsBuilder = new StringBuilder();

        Arrays.stream(UserAuthority.values())
                .map(SecurityUtils::buildRoleHierarchyExpressionFor)
                .forEach(hierarchyExpressionsBuilder::append);

        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy(hierarchyExpressionsBuilder.toString());

        return roleHierarchy;
    }

    @Bean
    public InternalAuthenticationFilter peachyMindAuthenticationFilter() {
        return new InternalAuthenticationFilter(
                internalAuthenticationProvider,
                internalAuthTokenService);
    }

    @Bean
    public PasswordEncoder defaultPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
