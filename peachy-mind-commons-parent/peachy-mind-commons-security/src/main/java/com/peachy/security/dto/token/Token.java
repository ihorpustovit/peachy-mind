package com.peachy.security.dto.token;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.notfound.PropertyNotFoundException;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.security.TokenPrefix;
import com.peachy.security.UserAuthority;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public interface Token {
    String getPureToken();

    default String getPrefixedToken() {
        Assert.that(getPrefix())
                .isNotNull()
                .otherwiseThrow(new PropertyNotFoundException("Unable to build prefixed token due to missing prefix property"));

        return Objects.equals(getPrefix(), TokenPrefix.NONE) ?
                getPureToken() :
                getPrefix()
                        .getStringyPrefix()
                        .concat(StringUtils.SPACE)
                        .concat(getPureToken());
    }

    @JsonIgnore
    TokenPrefix getPrefix();

    default String getStringyPrefix() {
        return Objects.nonNull(getPrefix()) ?
                getPrefix().getStringyPrefix() :
                StringUtils.EMPTY;
    }

    Long getIssuedAtUnix();

    Long getExpiresAtUnix();

    @JsonIgnore
    UserAuthority getAuthority();

    default String getStringyAuthority() {
        return Objects.nonNull(getAuthority()) ?
                getAuthority().getStringyAuthority() :
                StringUtils.EMPTY;
    }

    Map<String, Object> getAdditionalClaims();

    default Optional<Object> getAdditionalClaimForName(String claimName) {
        return getAdditionalClaimForName(claimName, Object.class);
    }

    default <T> Optional<T> getAdditionalClaimForName(String claimName, Class<T> claimValueType) {
        Optional<Object> claimValue = Optional.ofNullable(getAdditionalClaims().get(claimName));

        claimValue.ifPresent(cv -> {
            if (!claimValueType.isAssignableFrom(cv.getClass())) {
                throw ExceptionBuilder.buildFrom(new BadArgumentException(String.format("Failed to obtain the %s claim of the right type", claimName)))
                        .withReason(String.format("%s claim of %s was found, but is of a different type than requested. " +
                                "Requested type was %s and the claim is of type %s", claimName, this.getClass().getSimpleName(), claimValueType.getSimpleName(), cv.getClass().getSimpleName()))
                        .withSolution(String.format("Make logic request the claim of type %s, or convert the %s %s claim to %s", cv.getClass().getSimpleName(), this.getClass().getSimpleName(), claimName, claimValueType.getSimpleName()))
                        .build();
            }
        });

        return claimValue
                .filter(cv -> claimValueType.isAssignableFrom(cv.getClass()))
                .map(cv -> (T) cv);
    }
}
