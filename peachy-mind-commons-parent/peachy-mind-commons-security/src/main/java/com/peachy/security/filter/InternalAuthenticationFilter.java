package com.peachy.security.filter;

import com.peachy.security.TokenPrefix;
import com.peachy.security.dto.token.JwtToken;
import com.peachy.security.service.auth.provider.InternalAuthenticationProvider;
import com.peachy.security.service.auth.token.InternalAuthTokenService;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InternalAuthenticationFilter extends AbstractAuthenticationFilter {

    private final InternalAuthenticationProvider internalAuthenticationProvider;

    private final InternalAuthTokenService internalAuthTokenService;

    public InternalAuthenticationFilter(InternalAuthenticationProvider internalAuthenticationProvider,
                                        InternalAuthTokenService internalAuthTokenService) {
        super(TokenPrefix.INTERNAL.getStringyPrefix());
        this.internalAuthenticationProvider = internalAuthenticationProvider;
        this.internalAuthTokenService = internalAuthTokenService;
    }

    @Override
    protected void doAuthentication(HttpServletRequest request, HttpServletResponse response, String authToken) throws CredentialsExpiredException {
        JwtToken internalAuthToken = internalAuthTokenService.parseToken(authToken);
        Authentication authentication = internalAuthenticationProvider.authenticate(internalAuthToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
