package com.peachy.security.service.auth;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.commons.utils.assertion.PartyAssert;
import com.peachy.security.UserAuthority;
import com.peachy.security.converter.UserConverter;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DefaultUserService implements UserService {
    private final UserRepository userRepository;

    private final UserConverter userConverter;

    private PasswordEncoder passwordEncoder;

    public DefaultUserService(UserRepository userRepository,
                              UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    @Lazy
    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public UserDTO save(UserDTO user) {
        Assert.that(user)

                .isNotEmpty(UserDTO::getUsername)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to create user"))
                        .withReason("Username must not be empty")
                        .build())

                .isFalse(u -> existsByName(u.getUsername()))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to create user"))
                        .withReason(String.format("Username %s has already been taken", user.getUsername()))
                        .withSolution("Try think up other username")
                        .build())

                .isNotEmpty(UserDTO::getPlainPassword)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to create user"))
                        .withReason("Password must not be empty")
                        .build())

                .isFalse(u -> Objects.equals(u.getAuthority(), UserAuthority.NONE))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to create user"))
                        .withReason("Authority must not be empty")
                        .withSolution(String.format("Try specify one of the following authorities: %s",
                                Arrays.stream(UserAuthority.values())
                                        .map(UserAuthority::name)
                                        .filter(authority -> !StringUtils.equals(authority, UserAuthority.NONE.name()))
                                        .collect(Collectors.joining(", "))))
                        .build());

        user.setHashedPassword(passwordEncoder.encode(user.getPlainPassword()));

        userRepository.save(userConverter.toEntity(user));

        UserDTO savedUser = findByUsername(user.getUsername())
                .orElseThrow(() ->
                        ExceptionBuilder.buildFrom(new NotFoundException("Unable to create user"))
                                    .withReason("User was actually saved, but the system failed to find it persisting operation. It may be due to save/fetch operations broken synchronization")
                                    .withFault("Developer's fault")
                                    .withSolution("Synchronize save/fetch operations so that fetch would occur after the user was stored at database and guarangeed satisfiable response back")
                                    .build());
        savedUser.setPlainPassword(user.getPlainPassword());

        return savedUser;
    }

    @Override
    public UserDTO updateUserAuthority(String id, UserAuthority authority) {
        UserDTO user = findById(id)
                .orElseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException("Unable to update user authority"))
                                        .withReason(String.format("No user found for id: %s",id))
                                        .build());
        user.setAuthority(authority);

        return userConverter.toDto(userRepository.save(userConverter.toEntity(user)));
    }

    @Override
    public Optional<UserDTO> findById(String id) {
        return userConverter.toDto(userRepository.findById(id));
    }

    @Override
    public Optional<UserDTO> findByUsername(String username) {
        return userConverter.toDto(userRepository.findByUsername(username));
    }

    @Override
    public List<UserDTO> findAllByIds(List<String> userIds) {
        return userRepository.findAllById(userIds).stream()
                .map(userConverter::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existAllByIds(List<String> userIds) {
        Assert.that(userIds)
                .isNotNull()
                .and(PartyAssert::hasNoNullElements)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Failed to check if there is users for given ids"))
                        .withReason("User ids should not be null")
                        .build());
        return userRepository.existsAllByIdIn(userIds);
    }

    @Override
    public boolean existAllByIdsAndAuthority(List<String> userIds, UserAuthority authority) {
        Assert.that(userIds)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Failed to check if there is users with such authority for given ids"))
                        .withReason("User ids should not be null")
                        .build());

        Assert.that(authority)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Failed to check if there is users with such authority for given ids"))
                        .withReason("Authority should not be null")
                        .build());

        return userRepository.existsAllByIdInAndAuthority(userIds, authority);
    }
}
