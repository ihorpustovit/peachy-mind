package com.peachy.security.converter;

import com.peachy.jpa.converter.EntityDtoConverter;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserConverter extends EntityDtoConverter<UserEntity, UserDTO> {

}
