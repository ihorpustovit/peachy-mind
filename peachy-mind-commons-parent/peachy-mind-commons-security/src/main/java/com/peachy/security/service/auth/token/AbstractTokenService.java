package com.peachy.security.service.auth.token;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.security.dto.token.Token;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@RefreshScope
public abstract class AbstractTokenService<T extends Token> implements TokenService<T>, InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        checkState();
    }

    private void checkState() {
        Assert.that(this)
                .isNotEmpty(AbstractTokenService::getSignature)
                .otherwiseThrow(new BadArgumentException("Signature must not be empty"))

                .isNotNull(AbstractTokenService::getSignatureAlgorithm)
                .otherwiseThrow(new BadArgumentException("Signature algorithm must not be empty"))

                .isNotNull(AbstractTokenService::getPrefix)
                .otherwiseThrow(new BadArgumentException("Prefix must not be null"));

        try {
            getSignatureAlgorithm();
        } catch (SignatureException e) {
            throw ExceptionBuilder.buildFrom(new BadArgumentException(getSignatureAlgorithmName() + " is not supported signature algorithm"))
                    .withSolution("try use supportable signature algorithm")
                    .withFault("Developer's fault")
                    .build();
        }
    }

    protected SignatureAlgorithm getSignatureAlgorithm() {
        return SignatureAlgorithm.forName(getSignatureAlgorithmName());
    }

    public abstract String getSignatureAlgorithmName();

    public abstract String getSignature();
}
