package com.peachy.security.entity;

import com.peachy.jpa.entity.BaseEntity;
import com.peachy.security.UserAuthority;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Setter
@Getter
@Table(name = "user")
@Entity
@NoArgsConstructor
public class UserEntity extends BaseEntity {

    @Id
    @GenericGenerator(name = "default_id_generation_seq", strategy = "com.peachy.jpa.DefaultIdGenerator")
    @GeneratedValue(generator = "default_id_generation_seq")
    private String id;

    @Column(name = "username")
    private String username;

    @Column(name = "hashed_password")
    private String hashedPassword;

    @Column(name = "authority")
    @Enumerated(EnumType.STRING)
    private UserAuthority authority;

}
