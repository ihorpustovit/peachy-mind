package com.peachy.security.service.auth;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.notfound.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

//TODO add user existence check while authenticating the user
@Component
@RequiredArgsConstructor
public class DefaultUserDetailsService implements UserDetailsService {
    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userService.findByUsername(s)
                .orElseThrow(() ->
                                ExceptionBuilder.buildFrom(new NotFoundException("Unable to fetch user details"))
                                        .withReason(String.format("There's no user for username: %s",s))
                                        .build());
    }
}
