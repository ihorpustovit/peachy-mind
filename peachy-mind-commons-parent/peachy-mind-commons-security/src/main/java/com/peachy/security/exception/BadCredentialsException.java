package com.peachy.security.exception;

import com.peachy.commons.exception.PeachyMindException;
import org.springframework.http.HttpStatus;

public class BadCredentialsException extends PeachyMindException {
    public BadCredentialsException() {
        super(HttpStatus.UNAUTHORIZED);
    }

    public BadCredentialsException(String message) {
        super(HttpStatus.UNAUTHORIZED, message);
    }
}
