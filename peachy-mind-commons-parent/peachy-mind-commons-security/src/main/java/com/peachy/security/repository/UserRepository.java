package com.peachy.security.repository;

import com.peachy.security.UserAuthority;
import com.peachy.security.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
    Optional<UserEntity> findByUsername(String username);

    boolean existsAllByIdIn(List<String> userIds);

    boolean existsAllByIdInAndAuthority(List<String> userIds,
                                        UserAuthority userAuthority);
}
