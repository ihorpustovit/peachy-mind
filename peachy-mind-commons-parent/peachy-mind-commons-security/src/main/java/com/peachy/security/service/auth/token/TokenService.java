package com.peachy.security.service.auth.token;


import com.peachy.security.TokenPrefix;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.Token;

public interface TokenService<T extends Token> {
    T generateToken(UserDTO user);

    T parseToken(String token);

    TokenPrefix getPrefix();
}
