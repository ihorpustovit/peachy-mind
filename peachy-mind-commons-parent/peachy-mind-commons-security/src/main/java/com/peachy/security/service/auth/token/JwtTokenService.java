package com.peachy.security.service.auth.token;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenPrefix;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.JwtToken;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RefreshScope
public abstract class JwtTokenService<T extends JwtToken> extends AbstractTokenService<T> {
    @Value("${token.jwt.signature.algorithm}")
    protected String signatureAlgorithmName;

    @Value("${token.jwt.signature.secret}")
    protected String signature;

    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        TokenPrefix.valueOfStringyPrefix(getStringyPrefix())
                .orElseThrow(() ->
                        ExceptionBuilder.buildFrom(new BadArgumentException(String.format("Unable to build %s",this.getClass().getSimpleName())))
                        .withReason(String.format("Bad token prefix type: %s",getStringyPrefix()))
                        .build());
    }

    private final SigningKeyResolver signingKeyResolver = new SigningKeyResolverAdapter() {
        @Override
        public byte[] resolveSigningKeyBytes(JwsHeader header, Claims claims) {
            return TextCodec.BASE64.decode(getSignature());
        }
    };

    @Override
    public TokenPrefix getPrefix() {
        return TokenPrefix.valueOfStringyPrefix(getStringyPrefix())
                .orElseThrow(() -> ExceptionBuilder
                        .buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to obtain token prefix"))
                        .withReason(String.format("Unable to resolve token prefix for string: %s",getStringyPrefix()))
                        .withFault("Developer's fault")
                        .build());
    }

    protected abstract T convertGeneratedToken(JwtToken token, UserDTO user);

    protected abstract T convertParsedToken(JwtToken token);

    protected abstract Set<String> getTokenSpecificClaimsNames();

    protected abstract Map<String,Object> getTokenSpecificClaims(UserDTO user);

    public abstract String getIssuer();

    public abstract String getStringyPrefix();

    public abstract Long getLifetime();

    @Override
    public String getSignatureAlgorithmName() {
        return signatureAlgorithmName;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    public T generateTokenWithClaims(UserDTO user, Map<String, Object> additionalClaims) {
        Map<String, Object> claims = new HashMap<>();
        Map<String,Object> specificAndAdditionalClaims = new HashMap<>();
        specificAndAdditionalClaims.putAll(getTokenSpecificClaims(user));
        specificAndAdditionalClaims.putAll(additionalClaims);

        Instant issuedAt = Instant.now();

        String principal = user.getId();
        Instant expiresAt = issuedAt.plus(Duration.ofSeconds(getLifetime()));

        claims.put(PeachyMindJwtClaim.ISSUER, getIssuer());
        claims.put(PeachyMindJwtClaim.ISSUED_AT_UNIX, issuedAt.getEpochSecond());
        claims.put(PeachyMindJwtClaim.EXPIRATION_UNIX, expiresAt.getEpochSecond());
        claims.put(PeachyMindJwtClaim.AUTHORITY, user.getAuthority().name());
        claims.putAll(specificAndAdditionalClaims);

        String pureToken = Jwts.builder()
                .addClaims(claims)
                .signWith(getSignatureAlgorithm(), TextCodec.BASE64.decode(getSignature()))
                .compact();

        JwtToken jwtAuthToken = new JwtToken(
                pureToken,
                user.getAuthority(),
                issuedAt,
                expiresAt,
                getPrefix(),
                principal,
                specificAndAdditionalClaims);

        jwtAuthToken.setIssuer(getIssuer());

        return convertGeneratedToken(jwtAuthToken, user);
    }

    @Override
    public T generateToken(UserDTO user) {
        return generateTokenWithClaims(user, Collections.emptyMap());
    }

    @Override
    public T parseToken(String token) {
        Jws<Claims> jws = Jwts.parser()
                .setSigningKeyResolver(signingKeyResolver)
                .parseClaimsJws(token);

        Map<String,Object> additionalClaims = getTokenSpecificClaimsNames().stream()
                .filter(claimName -> Objects.nonNull(jws.getBody().get(claimName)))
                .collect(Collectors.toMap(Function.identity(), claimName -> jws.getBody().get(claimName)));

        JwtToken parsedJwtToken = new JwtToken(
                token,
                Optional.ofNullable(jws.getBody().get(PeachyMindJwtClaim.AUTHORITY, String.class)).map(UserAuthority::valueOf).orElse(UserAuthority.NONE),
                Optional.ofNullable(jws.getBody().get(PeachyMindJwtClaim.ISSUED_AT_UNIX, Long.class)).map(Instant::ofEpochSecond).orElse(null),
                Optional.ofNullable(jws.getBody().get(PeachyMindJwtClaim.EXPIRATION_UNIX, Long.class)).map(Instant::ofEpochSecond).orElse(null),
                getPrefix(),
                Optional.ofNullable(jws.getBody().get(PeachyMindJwtClaim.SUBJECT, String.class)).orElse(StringUtils.EMPTY),
                additionalClaims);

        Optional.ofNullable(jws.getBody().get(PeachyMindJwtClaim.ISSUER, String.class))
                .ifPresent(parsedJwtToken::setIssuer);

        return convertParsedToken(parsedJwtToken);
    }
}
