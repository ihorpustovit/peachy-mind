package com.peachy.security.filter;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.security.SecurityUtils;
import com.peachy.security.exception.UnauthorizedException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractAuthenticationFilter extends OncePerRequestFilter {
    private final String authenticationPrefix;

    public AbstractAuthenticationFilter(String authenticationPrefix) {
        this.authenticationPrefix = authenticationPrefix;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        SecurityUtils.authTokenFrom(request, authenticationPrefix)
                .ifPresent(authToken -> {
                    try {
                        doAuthentication(request, response, authToken);
                    } catch (CredentialsExpiredException e) {
                        throw ExceptionBuilder.buildFrom(new UnauthorizedException("Unable to perform authentication"))
                                .withReason("Given token has expired")
                                .withSolution("Refresh the token by hitting PUT /auth/refresh or issue a new one by hitting POST /auth/sign/in")
                                .build();
                    }
                });

        filterChain.doFilter(request, response);
    }

    protected abstract void doAuthentication(HttpServletRequest request, HttpServletResponse response, String authToken) throws CredentialsExpiredException;
}
