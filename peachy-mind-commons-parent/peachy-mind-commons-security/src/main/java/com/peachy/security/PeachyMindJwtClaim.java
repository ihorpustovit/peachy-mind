package com.peachy.security;

import io.jsonwebtoken.Claims;

public interface PeachyMindJwtClaim extends Claims {
    String AUTHORITY = "auz";

    String TOKEN_ID = "tid";

    String TOKEN_TYPE = "tpe";

    String ISSUED_AT_UNIX = "iux";

    String EXPIRATION_UNIX = "eux";

    String INTERNAL_TOKEN = "itn";
}
