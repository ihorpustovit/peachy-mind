package com.peachy.security.exception;

import com.peachy.commons.exception.PeachyMindException;
import org.springframework.http.HttpStatus;

public class UnauthorizedException extends PeachyMindException {
    public UnauthorizedException(String message) {
        super(HttpStatus.UNAUTHORIZED, message);
    }
}
