package com.peachy.security.dto.token;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.peachy.commons.utils.Utils;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenPrefix;
import com.peachy.security.TokenType;
import com.peachy.security.UserAuthority;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.time.Instant;
import java.util.*;

@Getter
public class JwtToken
        extends UsernamePasswordAuthenticationToken
        implements Token {

    protected final String pureToken;

    protected final UserAuthority authority;

    protected final Instant issuedAt;

    protected final Instant expiresAt;

    protected final TokenPrefix prefix;

    protected final Map<String, Object> additionalClaims;

    protected final String principal;

    @Setter
    protected String issuer;


    public JwtToken(String pureToken,
                    UserAuthority authority,
                    Instant issuedAt,
                    Instant expiresAt,
                    TokenPrefix prefix,
                    String principal,
                    Map<String, Object> additionalClaims) {
        super(principal, StringUtils.EMPTY,
                Optional.ofNullable(authority)
                        .map(a -> Collections.singletonList(new SimpleGrantedAuthority(a.name())))
                        .orElse(Collections.emptyList()));
        this.pureToken = pureToken;
        this.authority = authority;
        this.issuedAt = issuedAt;
        this.expiresAt = expiresAt;
        this.prefix = prefix;
        this.principal = principal;
        this.additionalClaims = additionalClaims;
    }

    public JwtToken(String pureToken,
                    UserAuthority authority,
                    Instant issuedAt,
                    Instant expiresAt,
                    TokenPrefix prefix,
                    String principal,
                    List<Pair<String, Object>> additionalClaims) {
        this(pureToken,
                authority,
                issuedAt,
                expiresAt,
                prefix,
                principal,
                Utils.asMap(additionalClaims));
    }

    @Override
    @JsonIgnore
    public Object getCredentials() {
        return StringUtils.EMPTY;
    }

    @Override
    @JsonIgnore
    public String getPrincipal() {
        return principal;
    }

    @Override
    public Long getIssuedAtUnix() {
        return getIssuedAt().getEpochSecond();
    }

    @Override
    public Long getExpiresAtUnix() {
        return getExpiresAt().getEpochSecond();
    }

    @Override
    @JsonIgnore
    public boolean isAuthenticated() {
        return super.isAuthenticated();
    }

    @Override
    @JsonIgnore
    public Object getDetails() {
        return super.getDetails();
    }


    public boolean isExpired() {
        return Instant.now().isAfter(getExpiresAt());
    }

    @JsonIgnore
    public Instant getIssuedAt() {
        return issuedAt;
    }

    @JsonIgnore
    public Instant getExpiresAt() {
        return expiresAt;
    }

    @JsonIgnore
    public Map<String, Object> getAdditionalClaims() {
        return additionalClaims;
    }

    public boolean isOfType(TokenType type) {
        return Objects.equals(type, getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_TYPE, String.class)
                .map(TokenType::valueOf)
                .orElse(TokenType.NONE));
    }
}
