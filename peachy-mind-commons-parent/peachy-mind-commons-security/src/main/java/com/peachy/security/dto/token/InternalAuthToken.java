package com.peachy.security.dto.token;

import com.peachy.security.TokenPrefix;
import com.peachy.security.UserAuthority;
import org.apache.commons.lang3.tuple.Pair;

import java.time.Instant;
import java.util.List;
import java.util.Map;

public class InternalAuthToken extends JwtToken {
    public InternalAuthToken(String pureToken,
                             UserAuthority authority,
                             Instant issuedAt,
                             Instant expiresAt,
                             String principal,
                             Map<String, Object> additionalClaims) {
        super(pureToken,
                authority,
                issuedAt,
                expiresAt,
                TokenPrefix.INTERNAL,
                principal,
                additionalClaims);
    }


    public InternalAuthToken(String pureToken,
                             UserAuthority authority,
                             Instant issuedAt,
                             Instant expiresAt,
                             String principal,
                             List<Pair<String, Object>> additionalClaims) {
        super(pureToken,
                authority,
                issuedAt,
                expiresAt,
                TokenPrefix.INTERNAL,
                principal,
                additionalClaims);
    }
}
