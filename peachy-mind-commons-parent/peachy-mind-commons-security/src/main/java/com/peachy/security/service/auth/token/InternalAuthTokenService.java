package com.peachy.security.service.auth.token;

import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.SecurityUtils;
import com.peachy.security.TokenPrefix;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.InternalAuthToken;
import com.peachy.security.dto.token.JwtToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.*;

@RefreshScope
@Service
public class InternalAuthTokenService extends JwtTokenService<InternalAuthToken> {
    @Value("${token.internal.claim.iss}")
    private String issuer;

    @Value("${token.internal.claim.lifetime}")
    private Long lifetime;

    @Value("${token.internal.signature.algorithm}")
    protected String signatureAlgorithmName;

    @Value("${token.internal.signature.secret}")
    protected String signature;

    @Override
    public String getSignatureAlgorithmName() {
        return signatureAlgorithmName;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    @Override
    protected InternalAuthToken convertGeneratedToken(JwtToken token, UserDTO user) {
        return internalTokenOf(token);
    }

    @Override
    protected InternalAuthToken convertParsedToken(JwtToken token) {
        return internalTokenOf(token);
    }

    private InternalAuthToken internalTokenOf(JwtToken token) {
        InternalAuthToken internalAuthToken = new InternalAuthToken(
                token.getPureToken(),
                token.getAuthority(),
                token.getIssuedAt(),
                token.getExpiresAt(),
                token.getPrincipal(),
                token.getAdditionalClaims());

        internalAuthToken.setIssuer(token.getIssuer());

        return internalAuthToken;
    }

    @Override
    protected Set<String> getTokenSpecificClaimsNames() {
        return new HashSet<>(
                Arrays.asList(
                        PeachyMindJwtClaim.SUBJECT,
                        PeachyMindJwtClaim.TOKEN_ID,
                        PeachyMindJwtClaim.TOKEN_TYPE));
    }

    @Override
    protected Map<String, Object> getTokenSpecificClaims(UserDTO user) {
        Map<String, Object> tokenSpecificClaims = new HashMap<>();
        tokenSpecificClaims.put(PeachyMindJwtClaim.SUBJECT, user.getId());
        tokenSpecificClaims.put(PeachyMindJwtClaim.TOKEN_ID, SecurityUtils.generateTokenId());
        tokenSpecificClaims.put(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.INTERNAL.name());

        return tokenSpecificClaims;
    }

    @Override
    public String getIssuer() {
        return issuer;
    }

    @Override
    public String getStringyPrefix() {
        return TokenPrefix.INTERNAL.getStringyPrefix();
    }

    @Override
    public Long getLifetime() {
        return lifetime;
    }
}
