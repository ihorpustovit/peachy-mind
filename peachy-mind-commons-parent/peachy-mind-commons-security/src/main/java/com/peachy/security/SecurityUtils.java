package com.peachy.security;

import com.peachy.security.dto.UserDTO;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Optional;

public final class SecurityUtils {
    public static Optional<UserDTO> getUserPrincipal() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .filter(Objects::nonNull)
                .filter(userPrincipal -> UserDTO.class.isAssignableFrom(userPrincipal.getClass()))
                .map(userPrincipal -> (UserDTO) userPrincipal);
    }

    public static String generateTokenId() {
        return RandomStringUtils.random(12, true, true);
    }

    public static Optional<String> authTokenFrom(HttpServletRequest request,
                                                 String withStringyPrefix) {
        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (StringUtils.isNotBlank(authorizationHeader) &&
                authorizationHeader.matches(withStringyPrefix + StringUtils.SPACE + "[^\\s]+")) {
            return Optional.of(authorizationHeader.replace(withStringyPrefix.concat(StringUtils.SPACE), StringUtils.EMPTY));
        } else {
            return Optional.empty();
        }
    }

    public static String buildRoleHierarchyExpressionFor(UserAuthority authority) {
        StringBuilder hierarchyExpressionBuilder = new StringBuilder();

        for (UserAuthority impliedAuthority : authority.impliedAuthorities()) {
            hierarchyExpressionBuilder
                    .append(authority.getStringyAuthority())
                    .append(" > ")
                    .append(impliedAuthority.getStringyAuthority())
                    .append("\n");
        }

        return hierarchyExpressionBuilder.toString();
    }
}
