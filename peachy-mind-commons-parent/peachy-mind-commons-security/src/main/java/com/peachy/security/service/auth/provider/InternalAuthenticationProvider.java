package com.peachy.security.service.auth.provider;

import com.peachy.security.dto.token.InternalAuthToken;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class InternalAuthenticationProvider extends PeachyMindAuthenticationProvider<InternalAuthToken> {
    public InternalAuthenticationProvider(Environment environment) {
        super(InternalAuthToken.class, environment);
    }
}
