package com.peachy.security;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public enum UserAuthority {
    NONE("None", Integer.MIN_VALUE),
    ANY("Any", Integer.MIN_VALUE + 1),
    ADMIN("Admin",Integer.MAX_VALUE),
    INSTRUCTOR("Instructor", Integer.MAX_VALUE - 1),
    STUDENT("Student",Integer.MAX_VALUE - 2);

    private final String stringyAuthority;

    private final int authorityLevel;


    UserAuthority(String stringyAuthority, int authorityLevel) {
        this.stringyAuthority = stringyAuthority;
        this.authorityLevel = authorityLevel;
    }

    public String getStringyAuthority() {
        return stringyAuthority;
    }

    public int getAuthorityLevel() {
        return authorityLevel;
    }

    public static List<String> stringyAuthorities() {
        return Arrays.stream(UserAuthority.values())
                .map(UserAuthority::getStringyAuthority)
                .collect(Collectors.toList());
    }


    private static List<UserAuthority> authoritiesOfLevelLowerThanOrEqual(int authorityLevel) {
        return Arrays.stream(UserAuthority.values())
                .filter(userAuthority -> userAuthority.getAuthorityLevel() <= authorityLevel)
                .collect(Collectors.toList());
    }

    public List<UserAuthority> impliedAuthorities() {
        return authoritiesOfLevelLowerThanOrEqual(getAuthorityLevel())
                .stream()
                .filter(impliedAuthority -> !Objects.equals(impliedAuthority, this))
                .collect(Collectors.toList());
    }

    public static boolean existsByName(String authorityName) {
        return Arrays.stream(values())
                .map(UserAuthority::name)
                .collect(Collectors.toList())
                .contains(authorityName);
    }

    public static String[] names() {
        return Arrays.stream(values())
                .map(UserAuthority::name)
                .collect(Collectors.toList())
                .toArray(new String[]{});
    }
}
