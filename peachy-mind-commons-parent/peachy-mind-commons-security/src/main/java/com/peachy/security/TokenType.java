package com.peachy.security;

public enum TokenType {
    GATEWAY("Gateway"),
    REFRESH("Refresh"),
    INTERNAL("Internal"),
    NONE("None");

    private final String stringyType;

    TokenType(String stringyType) {
        this.stringyType = stringyType;
    }

    public String getStringyType() {
        return stringyType;
    }
}
