package com.peachy.security.dto.token;

public interface RefreshableToken extends Token {
    String getPrefixedRefreshToken();
}
