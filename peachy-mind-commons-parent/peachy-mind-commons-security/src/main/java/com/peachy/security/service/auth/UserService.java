package com.peachy.security.service.auth;

import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public interface UserService {
    UserDTO save(UserDTO user);

    UserDTO updateUserAuthority(String id, UserAuthority authority);

    Optional<UserDTO> findById(String id);

    Optional<UserDTO> findByUsername(String username);

    List<UserDTO> findAllByIds(List<String> userIds);

    default boolean existsByName(String username) {
        return findByUsername(username).isPresent();
    }

    boolean existAllByIds(List<String> userIds);

    default boolean existsById(String userId){
        return existAllByIds(Collections.singletonList(userId));
    }

    boolean existAllByIdsAndAuthority(List<String> userIds,
                                      UserAuthority authority);
}
