package com.peachy.security.service.auth.provider;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.notfound.PropertyNotFoundException;
import com.peachy.commons.utils.RunningMode;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.JwtToken;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Objects;
import java.util.Optional;

public abstract class PeachyMindAuthenticationProvider<T extends JwtToken> extends AbstractUserDetailsAuthenticationProvider {

    private final Class<T> supportableTokenType;

    private final RunningMode runningMode;

    public PeachyMindAuthenticationProvider(Class<T> supportableTokenType, Environment environment) {
        Assert.that(supportableTokenType)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new PropertyNotFoundException(String.format("Failed to create %s object", this.getClass().getSimpleName())))
                        .withReason("supportableTokenType property should not be empty")
                        .build());

        Assert.that(environment)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new PropertyNotFoundException(String.format("Failed to create %s object", this.getClass().getSimpleName())))
                        .withReason("environment property should not be empty")
                        .build());
        this.supportableTokenType = supportableTokenType;
        this.runningMode = Optional.ofNullable(environment.getProperty("self.running-mode"))
                .map(RunningMode::valueOf)
                .orElse(RunningMode.REGULAR);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.getClass().isAssignableFrom(supportableTokenType);
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        boolean isJwtToken = Objects.nonNull(authentication) && JwtToken.class.isAssignableFrom(authentication.getClass());
        boolean isNotDebugMode = !Objects.equals(RunningMode.DEBUG, runningMode);

        if (isJwtToken) {
            JwtToken jwtToken = (JwtToken) authentication;

            if (isNotDebugMode && jwtToken.isExpired()) {
                throw new CredentialsExpiredException("Given token has expired");
            }
        }
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        T gatewayAuthToken = (T) authentication;
        UserDTO user = new UserDTO();
        user.setId(gatewayAuthToken.getPrincipal());
        user.setAuthority(gatewayAuthToken.getAuthority());
        return user;
    }
}
