package com.peachy.commons.exception.badrequest;

public class BadArgumentException extends BadRequestException {
    public BadArgumentException(String message) {
        super(message);
    }
}
