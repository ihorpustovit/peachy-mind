package com.peachy.commons.utils.assertion;

import com.peachy.commons.action.Action;

import java.util.concurrent.CompletableFuture;

public class AsyncAssertionResult<T> extends AssertionResult<T, SingleAsyncAssert<T>> {
    protected final CompletableFuture<Boolean> futureCondition;

    protected final SingleAsyncAssert<T> wrappedAssert;

    protected final String assertionId;

    public AsyncAssertionResult(SingleAsyncAssert<T> wrappedAssert,
                                String assertionId,
                                CompletableFuture<Boolean> futureCondition) {
        super(wrappedAssert, false);
        this.futureCondition = futureCondition;
        this.wrappedAssert = wrappedAssert;
        this.assertionId = assertionId;
    }

    public SingleAsyncAssert<T> otherwise(Action actionIfAssertionIsNotMet) {
        wrappedAssert.addOtherwise(assertionId, actionIfAssertionIsNotMet);
        return wrappedAssert;
    }

    public AsyncAssertionResult<T> then(Action actionIfAssertionIsMet) {
        wrappedAssert.addThen(assertionId, actionIfAssertionIsMet);
        return this;
    }
}
