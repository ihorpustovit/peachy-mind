package com.peachy.commons.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.util.Objects;

@Getter
public class PeachyMindException extends RuntimeException {
    private final HttpStatus httpStatus;

    @Getter(AccessLevel.PACKAGE)
    private ObjectNode responseJson;

    public PeachyMindException(HttpStatus httpStatus, String message) {
        this(httpStatus, message, new ObjectMapper().createObjectNode());
        this.responseJson = ExceptionBuilder.buildFrom(this)
                .withMessage(message)
                .build()
                .getResponseJson();
    }

    private PeachyMindException(HttpStatus httpStatus, String message, ObjectNode responseJson) {
        super(message);
        this.httpStatus = httpStatus;
        this.responseJson = responseJson;
    }

    public PeachyMindException(HttpStatus httpStatus) {
        this(httpStatus,StringUtils.EMPTY);
    }

    @Override
    public String toString() {
        return Objects.nonNull(responseJson) ? responseJson.toPrettyString() : StringUtils.EMPTY;
    }
}
