package com.peachy.commons.condition.property;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.utils.assertion.Assert;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class OnPropertyContainsValueCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        boolean matches;

        MultiValueMap<String, Object> annotationAttributes = metadata.getAllAnnotationAttributes(ConditionalOnPropertyContainsValues.class.getName());
        String propertyName = elementsToString(annotationAttributes.get("property")).get(0);

        if (Objects.isNull(context.getEnvironment().getProperty(propertyName))) {
            matches = false;
        } else {
            List<String> expectedPropertyValues = eraseSpaces(elementsToString(Arrays.asList((String[]) annotationAttributes.get("propertyValues").get(0))));
            List<String> actualPropertyValues = eraseSpaces(elementsToString(context.getEnvironment().getProperty(propertyName, List.class)));

            matches = actualPropertyValues.containsAll(expectedPropertyValues);
        }


        return matches;
    }

    private List<String> elementsToString(List<Object> objects) {
        Assert.that(objects)
                .isTrue(objectsList -> objectsList.stream().allMatch(element -> String.class.isAssignableFrom(element.getClass())))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to perform PropertyContainsValue condition"))
                        .withReason("Some of the matching participants are not actually a string")
                        .build());
        return objects.stream()
                .map(element -> (String) element)
                .collect(Collectors.toList());
    }

    private List<String> eraseSpaces(List<String> strings) {
        return strings.stream()
                .map(value -> value.replace(StringUtils.SPACE, StringUtils.EMPTY))
                .collect(Collectors.toList());
    }
}
