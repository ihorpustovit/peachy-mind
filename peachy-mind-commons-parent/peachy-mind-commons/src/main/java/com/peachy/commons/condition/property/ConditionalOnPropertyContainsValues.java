package com.peachy.commons.condition.property;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Conditional(OnPropertyContainsValueCondition.class)
public @interface ConditionalOnPropertyContainsValues {
    String property();

    String[] propertyValues();
}
