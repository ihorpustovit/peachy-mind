package com.peachy.commons.callback.executor;


import com.peachy.commons.callback.Callback;
import com.peachy.commons.callback.ManualCallback;

import java.util.List;
import java.util.stream.Collectors;

public abstract class ManualCallbackExecutor implements CallbackExecutor {

    protected final List<ManualCallback> callbacks;

    public ManualCallbackExecutor() {
        this.callbacks = initializeCallbacks();
    }

    @Override
    public void executeCallback() {
        executeCallbacks(callbacks);
    }

    public void executeWithExclusion(List<Class<? extends Callback>> exclusionCallbacksList) {
        executeCallbacks(
                callbacks.stream()
                        .filter(applicationCallback -> !exclusionCallbacksList.contains(applicationCallback.getClass()))
                        .collect(Collectors.toList()));
    }

    protected void executeCallbacks(List<ManualCallback> callbacks) {
        callbacks.forEach(ManualCallback::performCallback);
    }

    protected abstract List<ManualCallback> initializeCallbacks();
}
