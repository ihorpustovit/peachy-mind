package com.peachy.commons.callback.executor;


public interface CallbackExecutor {
    void executeCallback();
}
