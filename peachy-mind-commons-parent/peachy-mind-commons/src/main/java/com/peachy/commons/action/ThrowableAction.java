package com.peachy.commons.action;

@FunctionalInterface
public interface ThrowableAction {
    void act() throws Exception;
}
