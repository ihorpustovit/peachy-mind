package com.peachy.commons.exception.notfound;

import com.peachy.commons.exception.PeachyMindException;
import org.springframework.http.HttpStatus;

public class NotFoundException extends PeachyMindException {
    public NotFoundException() {
        super(HttpStatus.NOT_FOUND);
    }

    public NotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
