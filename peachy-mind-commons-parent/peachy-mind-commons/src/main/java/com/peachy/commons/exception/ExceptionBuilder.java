package com.peachy.commons.exception;

import com.peachy.commons.utils.assertion.Assert;
import org.apache.commons.lang3.StringUtils;

public class ExceptionBuilder<T extends PeachyMindException> {
    private final T buildingException;

    ExceptionBuilder(T buildingException) {
        this.buildingException = buildingException;
    }

    public static <T extends PeachyMindException> ExceptionBuilder<T> buildFrom(T easeApiException) {
        return new ExceptionBuilder<>(easeApiException);
    }

    public ExceptionBuilder<T> withSolution(String solution) {
        setProperty("solution", solution);
        return this;
    }

    public ExceptionBuilder<T> withFault(String fault) {
        setProperty("fault", fault);
        return this;
    }

    public ExceptionBuilder<T> withMessage(String message) {
        setProperty("message",message);
        return this;
    }

    public ExceptionBuilder<T> withReason(String reason) {
        setProperty("reason",reason);
        return this;
    }

    private void setProperty(String propertyName, String propertyValue) {
        Assert.that(propertyValue)
                .isNotBlank()
                .otherwise(() -> buildingException.getResponseJson().put(propertyName,String.format("No %s available",propertyName)));
        buildingException.getResponseJson().put(propertyName,propertyValue);
    }

    public T build() {
        return buildingException;
    }

}
