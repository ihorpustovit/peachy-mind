package com.peachy.commons.utils;

import com.peachy.commons.action.ThrowableAction;
import com.peachy.commons.action.ThrowableReturnableAction;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.exception.UnexpectedException;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class Utils {
    public static <K, V> Map<K, V> asMap(Pair<K, V>... pairs) {
        return asMap(Arrays.asList(pairs));
    }

    public static <K, V> Map<K, V> asMap(List<Pair<K, V>> pairs) {
        return pairs.stream()
                .collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
    }

    public static <T> T unmarshallFuture(Future<T> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            boolean isPeachyMindException = Objects.nonNull(e.getCause()) &&
                    PeachyMindException.class.isAssignableFrom(e.getCause().getClass());

            if (isPeachyMindException) {
                throw (PeachyMindException) e.getCause();
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    public static <T> T replaceException(ThrowableReturnableAction<T> action,
                                         Function<Exception, PeachyMindException> replacement,
                                         Class<? extends Exception> replacementFor) {
        try {
            return action.throwableAct();
        } catch (Exception e) {
            if (replacementFor.isAssignableFrom(e.getClass())) {
                throw replacement.apply(e);
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    public static void replaceException(ThrowableAction action,
                                        Function<Exception, PeachyMindException> replacement,
                                        Class<? extends Exception> replacementFor) {
        try {
            action.act();
        } catch (Exception e) {
            if (replacementFor.isAssignableFrom(e.getClass())) {
                throw replacement.apply(e);
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    public static <T> T replaceIOException(ThrowableReturnableAction<T> action, String situationalMessage) {
        Function<Exception, PeachyMindException> ioExceptionReplacement = rootException ->
                ExceptionBuilder.buildFrom(new UnexpectedException(rootException))
                        .withReason(situationalMessage)
                        .build();
        return replaceException(action, ioExceptionReplacement, IOException.class);
    }

    public static void replaceIOException(ThrowableAction action, String situationalMessage) {
        Function<Exception, PeachyMindException> ioExceptionReplacement = rootException ->
                ExceptionBuilder.buildFrom(new UnexpectedException(rootException))
                        .withReason(situationalMessage)
                        .build();
        replaceException(action, ioExceptionReplacement, IOException.class);
    }
}
