package com.peachy.commons.callback.prestartup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.SocketUtils;

public class RandomPortInitializationCallback implements PreStartupCallback {
    private final int randomPortLowerBound;
    private final int randomPortUpperBound;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public RandomPortInitializationCallback() {
        this.randomPortLowerBound = 1;
        this.randomPortUpperBound = 50000;
    }


    @Override
    public void performCallback() {
        try {
            int port = SocketUtils.findAvailableTcpPort(randomPortLowerBound, randomPortUpperBound);
            System.setProperty("self.port", String.valueOf(port));
            LOGGER.info("Application port has been randomly generated and set to {}", port);
        } catch (IllegalStateException e) {
            LOGGER.warn(String.format("No port available in range %s-%s. Default embedded server configuration will be used.", randomPortLowerBound, randomPortUpperBound));
        }
    }
}
