package com.peachy.commons.exception.badrequest;

import com.peachy.commons.exception.PeachyMindException;
import org.springframework.http.HttpStatus;

public class BadRequestException extends PeachyMindException {
    public BadRequestException() {
        super(HttpStatus.BAD_REQUEST);
    }

    public BadRequestException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}
