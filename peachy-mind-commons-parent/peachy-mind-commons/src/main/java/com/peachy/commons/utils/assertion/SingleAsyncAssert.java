package com.peachy.commons.utils.assertion;

import com.peachy.commons.action.Action;
import com.peachy.commons.exception.ConflictException;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.utils.Utils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.StopWatch;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SingleAsyncAssert<T> extends Assert<T, AsyncAssertionResult> {
    private final Map<String, CompletableFuture<Boolean>> assertionsFuture = new HashMap<>();

    private final Map<String, Action> then = new HashMap<>();

    private final Map<String, Action> otherwise = new HashMap<>();

    public SingleAsyncAssert(T assertedObject) {
        super(assertedObject);
    }

    public AsyncAssertionResult<T> isTrue(Predicate<T> assertedObjectPredicate) {
        String assertionId = RandomStringUtils.random(12, true, true);
        CompletableFuture<Boolean> assertionFuture = CompletableFuture.supplyAsync(() -> assertedObjectPredicate.test(assertedObject));
        AsyncAssertionResult<T> asyncAssertionResult =
                new AsyncAssertionResult<>(this, assertionId, assertionFuture);
        assertionsFuture.put(assertionId, assertionFuture);
        return asyncAssertionResult;
    }

    public void waitForAll() {
        Utils.unmarshallFuture(CompletableFuture.allOf(assertionsFuture.values().toArray(new CompletableFuture[]{})));
        Map<String, Boolean> finishedAssertions = assertionsFuture.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> Utils.unmarshallFuture(entry.getValue())));

        finishedAssertions.forEach((assertionId, conditionMet) -> {
            if (conditionMet) {
                Optional.ofNullable(then.get(assertionId))
                        .ifPresent(Action::act);
            } else {
                Optional.ofNullable(otherwise.get(assertionId))
                        .ifPresent(Action::act);
            }
        });
    }

    void addThen(String assertionId, Action action) {
        then.put(assertionId, action);
    }

    void addOtherwise(String assertionId, Action action) {
        otherwise.put(assertionId, action);
    }

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Assert.thatAsync("aa")
                .isTrue(s -> {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return StringUtils.isNotBlank(s);
                })
                .otherwise(() -> System.out.println("Is blank"))
                .isTrue(s -> {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return Objects.equals(s, "aa");
                })
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Conflict"))
                        .withReason("Reason")
                        .build())
                .waitForAll();
        stopWatch.stop();
        System.out.println(stopWatch.getLastTaskTimeMillis());
    }
}
