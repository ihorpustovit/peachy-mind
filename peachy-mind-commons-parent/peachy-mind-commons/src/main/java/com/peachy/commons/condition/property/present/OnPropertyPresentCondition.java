package com.peachy.commons.condition.property.present;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.MultiValueMap;

import java.util.Objects;
import java.util.Optional;

public class OnPropertyPresentCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        MultiValueMap<String, Object> annotationAttributes = metadata.getAllAnnotationAttributes(ConditionalOnPropertyPresent.class.getName());
        String propertyName = Optional.ofNullable(annotationAttributes.get("value"))
                .map(attribute -> attribute.get(0))
                .map(attributeValue -> (String) attributeValue)
                .orElse(StringUtils.EMPTY);

        return StringUtils.isEmpty(propertyName) || Objects.nonNull(context.getEnvironment().getProperty(propertyName));
    }
}
