package com.peachy.commons.utils.assertion;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

public abstract class Assert<T, AR extends AssertionResult> {
    protected final T assertedObject;

    protected Assert(T assertedObject) {

        this.assertedObject = assertedObject;
    }

    public static <T> SingleAssert<T> that(T assertedObject) {
        return new SingleAssert<>(assertedObject);
    }

    public static <T> PartyAssert<T> that(List<T> assertedObject) {

        return new PartyAssert<>(assertedObject);
    }

    public static <T> SingleAsyncAssert<T> thatAsync(T assertedObject) {
        return new SingleAsyncAssert<>(assertedObject);
    }

    public abstract AR isTrue(Predicate<T> assertedObjectPredicate);

    public static SingleAssert.SingleAssertionResult<Boolean> isTrue(boolean statement) {
        return new SingleAssert.SingleAssertionResult<>(new SingleAssert<>(statement), statement);
    }

    public AR isFalse(Predicate<T> assertedObjectPredicate) {
        return isTrue(assertedObjectPredicate.negate());
    }

    public AR isNotNull() {
        return isNotNull(ao -> ao);
    }

    public AR isNotNull(Function<T, ?> toObjectMapper) {
        return isTrue(ao -> {
            return Objects.nonNull(toObjectMapper.apply(ao));
        });
    }
}
