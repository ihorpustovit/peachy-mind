package com.peachy.commons.callback;


import org.springframework.context.ApplicationListener;

public interface Callback {
    void performCallback();
}
