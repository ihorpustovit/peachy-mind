package com.peachy.commons.callback.executor;

import com.peachy.commons.callback.ManualCallback;
import com.peachy.commons.callback.prestartup.ProfileDependentCallback;
import com.peachy.commons.callback.prestartup.RandomPortInitializationCallback;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PreStartupCallbackExecutor
        extends ManualCallbackExecutor {
    public static final PreStartupCallbackExecutor INSTANCE = new PreStartupCallbackExecutor();

    @Override
    protected List<ManualCallback> initializeCallbacks() {
        return Collections.singletonList(
                ProfileDependentCallback.ofNegated(
                        Arrays.asList("standalone", "standalone-test"),
                        Collections.singletonList(new RandomPortInitializationCallback())));
    }
}
