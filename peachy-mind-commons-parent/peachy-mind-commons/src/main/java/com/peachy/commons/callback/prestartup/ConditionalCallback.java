package com.peachy.commons.callback.prestartup;

import com.peachy.commons.callback.Callback;

public interface ConditionalCallback extends Callback {
    @Override
    default void performCallback() {
        if (conditionMet()) {
            callbackFlow();
        }
    }

    void callbackFlow();

    boolean conditionMet();
}
