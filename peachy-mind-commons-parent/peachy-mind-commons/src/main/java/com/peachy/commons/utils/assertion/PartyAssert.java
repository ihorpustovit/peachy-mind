package com.peachy.commons.utils.assertion;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PartyAssert<T> extends Assert<List<T>, PartyAssert.PartyAssertionResult<T>> {

    protected PartyAssert(List<T> list) {
        super(list);
    }

    public PartyAssertionResult<T> matchesAnyOf(Predicate<T> matcher) {
        return isTrue(party -> party.stream().anyMatch(matcher));
    }

    public PartyAssertionResult<T> are(Class<?> clazz) {
        return isTrue(party -> party.stream().allMatch(part -> Objects.nonNull(part) && clazz.isAssignableFrom(part.getClass())));
    }

    public PartyAssertionResult<T> areNotBlank() {
        if (Objects.nonNull(assertedObject)) {
            Assert.that(assertedObject)
                    .are(String.class)
                    .otherwiseThrow(ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to perform assertion correctly"))
                            .withReason("Unable to check if non-string object is blank")
                            .withFault("Developer's fault")
                            .build());
            return areNotBlank(ao -> (String) ao);
        } else {
            return new PartyAssertionResult<>(this, false);
        }
    }

    public PartyAssertionResult<T> areNotBlank(Function<T, String> toStringMapper) {
        return isTrue(ao -> ao.stream().map(toStringMapper).allMatch(StringUtils::isNotBlank));
    }

    public PartyAssertionResult<T> areNotNull(Function<T, ?> toObjectMapper) {
        return isTrue(ao -> {
            return ao.stream().allMatch(o -> Objects.nonNull(toObjectMapper.apply(o)));
        });
    }

    @Override
    public PartyAssertionResult<T> isTrue(Predicate<List<T>> assertedObjectPredicate) {
        return new PartyAssertionResult<>(this, assertedObjectPredicate.test(assertedObject));
    }

    public <N> PartyAssertionResult<T> isTrueNested(Predicate<List<N>> assertedObjectPredicate, Function<T, N> toNestedPropertyMapper) {
        return isTrue(ao -> assertedObjectPredicate.test(ao.stream().map(toNestedPropertyMapper).collect(Collectors.toList())));
    }


    public PartyAssertionResult<T> hasNoNullElements() {
        return Objects.isNull(assertedObject) ?
                new PartyAssertionResult<>(this, false) :
                isTrue(ao -> ao.stream().noneMatch(Objects::isNull));
    }

    public static class PartyAssertionResult<T> extends AssertionResult<T, PartyAssert<T>> {
        public PartyAssertionResult(PartyAssert<T> wrappedAssert, boolean conditionMet) {
            super(wrappedAssert, conditionMet);
        }
    }
}
