package com.peachy.commons.utils.assertion;

import com.peachy.commons.action.Action;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

public class SingleAssert<T> extends Assert<T, SingleAssert.SingleAssertionResult<T>> {

    protected SingleAssert(T assertedObject) {
        super(assertedObject);
    }

    public SingleAssertionResult<T> is(Class<?> clazz) {
        return isTrue(ao -> clazz.isAssignableFrom(ao.getClass()));
    }

    public SingleAssertionResult<T> isNotEmpty(Function<T, String> toStringMapper) {
        return isTrue(ao -> StringUtils.isNotEmpty(toStringMapper.apply(ao)));
    }

    public SingleAssertionResult<T> isNotBlank(Function<T, String> toStringMapper) {
        return isTrue(ao -> StringUtils.isNotBlank(toStringMapper.apply(ao)));
    }

    public SingleAssertionResult<T> isNotBlank() {
        if (Objects.nonNull(assertedObject)) {
            Assert.that(assertedObject)
                    .is(String.class)
                    .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to perform assertion correctly"))
                            .withReason("Unable to check if non-string object is blank")
                            .withFault("Developer's fault")
                            .build());
            return isTrue(ao -> StringUtils.isNotBlank((String) ao));
        } else {
            return new SingleAssertionResult<>(this, false);
        }
    }

    @Override
    public SingleAssertionResult<T> isTrue(Predicate<T> assertedObjectPredicate) {
        return new SingleAssertionResult<>(this, assertedObjectPredicate.test(assertedObject));
    }

    public static void ifTrueElse(boolean condition, Action actionIfTrue, Action actionIfFalse) {
        if (condition) {
            actionIfTrue.act();
        } else {
            actionIfFalse.act();
        }
    }

    public SingleAssertionResult<T> isGreaterThan(Function<T, Double> toDoubleMapper, Double greaterThan) {
        Assert.that(assertedObject)
                .is(Comparable.class)
                .otherwiseThrow(
                        ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to perform assertion correctly"))
                                .withReason(String.format("Object passed to comparison is not of %s type, hence cannot be compared", Comparable.class.getSimpleName()))
                                .withFault("Developer's fault")
                                .withSolution(String.format("Make asserted object implement %s, or remove comparison assertion", Comparable.class.getSimpleName()))
                                .build());

        return isTrue(ao -> toDoubleMapper.apply(ao) > greaterThan);
    }

    public SingleAssertionResult<T> isGreaterThanZero(Function<T, Double> toDoubleMapper) {
        return isGreaterThan(toDoubleMapper, 0.0);
    }

    public SingleAssertionResult<T> isEqualTo(T equalTo) {
        return isTrue(ao -> Objects.equals(ao, equalTo));
    }

    public SingleAssertionResult<T> isEqualToString(String equalTo) {
        Assert.that(assertedObject)
                .is(String.class)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to perform assertion correctly"))
                        .withReason("Unable to check if non-string object is blank")
                        .withFault("Developer's fault")
                        .build());

        return isTrue(ao -> StringUtils.equals((String) ao, equalTo));
    }

    public static class SingleAssertionResult<T> extends AssertionResult<T, SingleAssert<T>> {
        public SingleAssertionResult(SingleAssert<T> wrappedAssert, boolean conditionMet) {
            super(wrappedAssert, conditionMet);
        }
    }
}
