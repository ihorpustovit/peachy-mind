package com.peachy.commons.exception;

import org.springframework.http.HttpStatus;

public class UnexpectedException extends PeachyMindException{
    public UnexpectedException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }

    public UnexpectedException(Exception cause, String message) {
        this(HttpStatus.INTERNAL_SERVER_ERROR, message);
        initCause(cause);
    }

    public UnexpectedException(Exception cause) {
        this(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected exception occurred");
        initCause(cause);
    }
}
