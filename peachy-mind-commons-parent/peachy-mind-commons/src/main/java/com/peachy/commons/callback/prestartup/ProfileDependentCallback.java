package com.peachy.commons.callback.prestartup;

import com.peachy.commons.callback.ManualCallback;

import java.util.List;
import java.util.function.Predicate;

public class ProfileDependentCallback implements ConditionalCallback, PreStartupCallback {

    private final Predicate<String> profilePredicate;

    private final List<ManualCallback> manualCallbacks;

    private ProfileDependentCallback(Predicate<String> profilePredicate,
                                     List<ManualCallback> manualCallbacks) {
        this.profilePredicate = profilePredicate;
        this.manualCallbacks = manualCallbacks;
    }

    public static ProfileDependentCallback of(List<String> ifProfileIsIn,
                                              List<ManualCallback> manualCallbacks) {
        return new ProfileDependentCallback(ifProfileIsIn::contains, manualCallbacks);
    }

    public static ProfileDependentCallback ofNegated(List<String> ifProfileIsNotIn,
                                                     List<ManualCallback> manualCallbacks) {
        return new ProfileDependentCallback(profileName -> !ifProfileIsNotIn.contains(profileName), manualCallbacks);
    }


    @Override
    public void callbackFlow() {
        manualCallbacks.forEach(ManualCallback::performCallback);
    }

    @Override
    public boolean conditionMet() {
        return profilePredicate.test(System.getProperty("spring.profiles.active"));
    }
}
