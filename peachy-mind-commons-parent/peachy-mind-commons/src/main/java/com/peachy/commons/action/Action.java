package com.peachy.commons.action;

@FunctionalInterface
public interface Action {
    void act();
}
