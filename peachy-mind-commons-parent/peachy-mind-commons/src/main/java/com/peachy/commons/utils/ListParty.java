package com.peachy.commons.utils;

import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ListParty<T> {
    private final List<T> members;

    protected ListParty(List<T> members) {
        this.members = members;
    }

    public static <T> ListParty<T> of(List<T> members) {
        return new ListParty<>(members);
    }

    public <ST> Optional<T> select(ST selectTarget,
                                   Function<T, ST> selectTargetMapper) {
        return members.stream()
                .filter(member -> Objects.equals(selectTargetMapper.apply(member), selectTarget))
                .findAny();
    }

    public <ST> List<T> selectSubParty(Predicate<ST> selectionPredicate,
                                       Function<T, ST> selectTargetMapper) {
        return members.stream()
                .filter(member -> selectionPredicate.test(selectTargetMapper.apply(member)))
                .collect(Collectors.toList());
    }

    public <N, NP> MapParty<N, List<T>> groupByNestedProperty(Function<T, N> nestedPropertyMapper,
                                                              Function<N, NP> nestedPropertyFieldMapper) {
        List<N> nestedProperties = members.stream().map(nestedPropertyMapper).collect(Collectors.toList());

        Map<NP, List<T>> groupedByNestedProperty = members.stream()
                .collect(Collectors.groupingBy(member -> nestedPropertyFieldMapper.apply(nestedPropertyMapper.apply(member))));

        Map<N, List<T>> groups = groupedByNestedProperty.entrySet().stream()
                .map(entry -> Pair.of(ListParty.of(nestedProperties).select(entry.getKey(), nestedPropertyFieldMapper), entry.getValue()))
                .filter(pair -> pair.getKey().isPresent())
                .map(pair -> Pair.of(pair.getKey().get(), pair.getValue()))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

        return MapParty.of(groups);
    }
}
