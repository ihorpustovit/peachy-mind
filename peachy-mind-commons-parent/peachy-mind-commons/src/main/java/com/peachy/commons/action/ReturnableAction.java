package com.peachy.commons.action;

@FunctionalInterface
public interface ReturnableAction<R> {
    R act();
}
