package com.peachy.commons.utils.assertion;

import com.peachy.commons.action.Action;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import org.springframework.http.HttpStatus;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

public class AssertionResult<T, A extends Assert> {
    protected final boolean conditionMet;

    protected final A wrappedAssert;

    protected AssertionResult(A wrappedAssert,
                              boolean conditionMet) {
        this.conditionMet = conditionMet;
        this.wrappedAssert = wrappedAssert;
    }

    public A otherwise(Action actionIfAssertionIsNotMet) {
        if (!conditionMet) {
            actionIfAssertionIsNotMet.act();
        }

        return wrappedAssert;
    }

    public AssertionResult<T, A> then(Action actionIfAssertionIsMet) {
        if (conditionMet) {
            actionIfAssertionIsMet.act();
        }

        return this;
    }

    public A otherwiseIgnore() {
        return otherwise(() -> {
        });
    }

    public A otherwiseThrow(PeachyMindException exceptionToThrow) {
        return otherwiseThrow(() -> exceptionToThrow);
    }

    public A otherwiseThrow(Supplier<PeachyMindException> exceptionSupplier) {
        return otherwise(() -> {

            if (Objects.nonNull(exceptionSupplier)) {
                PeachyMindException ex = exceptionSupplier.get();
                if (Objects.nonNull(ex)) {
                    throw ex;
                }
            }

            throw ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to perform assertion correctly"))
                    .withReason("Condition was not met, but an exception you supplied to throw appears to be empty")
                    .withFault("Developer's fault")
                    .build();
        });

    }

    public AssertionResult<T, A> and(Function<A, AssertionResult<T,A>> otherAssertionResult) {
        return new AssertionResult<>(wrappedAssert, conditionMet && otherAssertionResult.apply(wrappedAssert).conditionMet);
    }

    public boolean conditionMet() {
        return conditionMet;
    }
}

