package com.peachy.commons.exception;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends PeachyMindException {
    public ForbiddenException(String message) {
        super(HttpStatus.FORBIDDEN, message);
    }
}
