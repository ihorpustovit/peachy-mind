package com.peachy.commons.utils;

import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MapParty<K,V> {
    private final Map<K,V> members;

    protected MapParty(Map<K,V> members) {
        this.members = members;
    }

    public static <K,V> MapParty<K,V> of(Map<K,V> members) {
        return new MapParty<>(members);
    }

    public <T> List<T> turnInto(Function<Map.Entry<K,V>, T> converter) {
        return members.entrySet().stream()
                .map(converter)
                .collect(Collectors.toList());
    }

    public <K1,V1> MapParty<K1,V1> turnIntoMap(Function<Map.Entry<K,V>, Pair<K1,V1>> converter) {
        return MapParty.of(members.entrySet().stream()
                .map(converter)
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue)));
    }

    public Map<K,V> endParty() {
        return members;
    }
}
