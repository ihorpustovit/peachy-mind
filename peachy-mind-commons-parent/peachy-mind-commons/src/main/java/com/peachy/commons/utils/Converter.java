package com.peachy.commons.utils;

import org.springframework.stereotype.Component;

@Component
public interface Converter<S, D> {
    D to(S e);

    S from(D d);
}
