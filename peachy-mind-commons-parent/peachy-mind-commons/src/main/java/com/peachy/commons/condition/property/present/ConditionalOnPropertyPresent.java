package com.peachy.commons.condition.property.present;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Conditional(OnPropertyPresentCondition.class)
public @interface ConditionalOnPropertyPresent {
    @AliasFor("propertyName")
    String value() default StringUtils.EMPTY;

    @AliasFor("value")
    String propertyName() default StringUtils.EMPTY;
}
