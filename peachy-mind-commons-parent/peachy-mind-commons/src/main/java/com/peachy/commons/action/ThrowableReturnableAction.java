package com.peachy.commons.action;

@FunctionalInterface
public interface ThrowableReturnableAction<R> {
    R throwableAct() throws Exception;
}
