package com.peachy.commons.exception.notfound;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PropertyNotFoundException extends NotFoundException {
    public PropertyNotFoundException(String message) {
        super(message);
    }
}
