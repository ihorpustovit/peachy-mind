package com.peachy.commons.exception;

import org.springframework.http.HttpStatus;

public class ConflictException extends PeachyMindException {
    public ConflictException(String message) {
        super(HttpStatus.CONFLICT, message);
    }
}
