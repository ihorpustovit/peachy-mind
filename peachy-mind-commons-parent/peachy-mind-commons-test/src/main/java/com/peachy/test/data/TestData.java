package com.peachy.test.data;

import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenType;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.InternalAuthToken;
import com.peachy.security.entity.UserEntity;
import org.apache.commons.lang3.tuple.Pair;

import java.time.Instant;
import java.util.Arrays;

public class TestData {
    public static UserEntity getUserEntity() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(TestProperties.getUsername());
        userEntity.setAuthority(UserAuthority.STUDENT);
        userEntity.setId(TestProperties.getUserId());
        userEntity.setHashedPassword(TestProperties.getPassword());
        return userEntity;
    }

    public static UserDTO getUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setAuthority(UserAuthority.STUDENT);
        userDTO.setId(TestProperties.getUserId());
        userDTO.setUsername(TestProperties.getUsername());
        userDTO.setPlainPassword(TestProperties.getPassword());

        return userDTO;
    }

    public static InternalAuthToken getInternalAuthToken() {
        UserDTO userDTO = getUserDTO();
        InternalAuthToken internalAuthToken =
                new InternalAuthToken(
                        TestProperties.getPureInternalToken(),
                        userDTO.getAuthority(),
                        Instant.ofEpochSecond(TestProperties.getInternalTokenIssuedAtUnix()),
                        Instant.ofEpochSecond(TestProperties.getInternalTokenExpiresAtUnix()),
                        userDTO.getId(),
                        Arrays.asList(
                                Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.INTERNAL.name()),
                                Pair.of(PeachyMindJwtClaim.TOKEN_ID, TestProperties.getTokenId()),
                                Pair.of(PeachyMindJwtClaim.SUBJECT, userDTO.getId()),
                                Pair.of(PeachyMindJwtClaim.ISSUER, TestProperties.getIssuer()),
                                Pair.of(PeachyMindJwtClaim.ISSUED_AT_UNIX, TestProperties.getInternalTokenIssuedAtUnix()),
                                Pair.of(PeachyMindJwtClaim.EXPIRATION_UNIX, TestProperties.getInternalTokenExpiresAtUnix()))
                );

        internalAuthToken.setIssuer(TestProperties.getIssuer());

        return internalAuthToken;
    }
}
