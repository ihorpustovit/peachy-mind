package com.peachy.test.data;

import com.peachy.security.entity.UserEntity;
import org.springframework.jdbc.core.JdbcTemplate;

public final class UserServiceTestUtils {
    public static void save(UserEntity user, JdbcTemplate jdbcTemplate) {
        if (!existsById(user.getId(), jdbcTemplate)) {
            jdbcTemplate.update("insert into user(id,username,hashed_password,authority,created_at,updated_at) value (?,?,?,?,now(),now())",
                    user.getId(),
                    user.getUsername(),
                    user.getHashedPassword(),
                    user.getAuthority().name());
        }
    }

    public static boolean existsById(String userId, JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("select count(*) from user where id=?", Integer.class, userId) > 0;
    }

    public static boolean existsByUsername(String username, JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("select count(*) from user where username=?", Integer.class, username) > 0;
    }

    public static void truncateUsers(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("delete from user where username <> 'admin'");
    }

    public static void deleteById(String userId, JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("DELETE FROM user WHERE id = ?", userId);
    }
}
