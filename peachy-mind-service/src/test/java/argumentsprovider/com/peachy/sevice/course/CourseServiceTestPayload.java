package argumentsprovider.com.peachy.sevice.course;

import com.peachy.dto.course.CourseCreationDTO;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonInCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.CourseEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class CourseServiceTestPayload {
    private final CourseCreationDTO courseCreationDTO;

    private final ExpandedCourseDTO expandedCourseDTO;

    private final CourseEntity courseEntity;

    private final CourseDTO courseDTO;

    private final List<LessonCreationDTO> lessonCreationsDTOs;

    private final List<LessonDTO> lessonsDTOs;

    private final List<LessonInCourseDTO> lessonInCourseDTOs;

    private final UserCourseDTO instructorUserCourseDTO;

    private final UserCourseDTO studentUserCourseDTO;
}
