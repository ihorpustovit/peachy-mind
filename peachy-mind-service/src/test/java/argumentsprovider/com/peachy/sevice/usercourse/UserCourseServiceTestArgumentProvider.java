package argumentsprovider.com.peachy.sevice.usercourse;

import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import testdata.course.CourseServiceTestData;
import testdata.usercourse.UserCourseServiceTestData;

import java.util.stream.Stream;

public class UserCourseServiceTestArgumentProvider implements ArgumentsProvider {
    /**
     * {@link UserCourseServiceTestPayload} is built according to the following rules:
     * - contains randomly generated {@link ExpandedCourseDTO}
     * - contains {@link UserDTO} of {@link UserAuthority#INSTRUCTOR} authority
     * - contains {@link UserCourseEntity} that represents relation between previously generated {@link UserDTO} and {@link ExpandedCourseDTO}
     * - contains {@link UserCourseDTO} that represents relation between previously generated {@link UserDTO} and {@link ExpandedCourseDTO}
     */
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        UserCourseDTO instructorUserCourseDTO = UserCourseServiceTestData.getInstructorUserCourseDTO();
        UserCourseEntity instructorUserCourseEntity = UserCourseServiceTestData.convertToUserCourseEntity(instructorUserCourseDTO);

        UserCourseDTO studentUserCourseDTO = UserCourseServiceTestData.getStudentUserCourseDTO();
        UserCourseEntity studentUserCourseEntity = UserCourseServiceTestData.convertToUserCourseEntity(studentUserCourseDTO);


        return Stream.of(Arguments.of(
                new UserCourseServiceTestPayload(
                        instructorUserCourseDTO.getCourse(),
                        CourseServiceTestData.convertToCourseDTOFrom(instructorUserCourseDTO.getCourse()),
                        instructorUserCourseDTO.getUser(),
                        instructorUserCourseEntity,
                        instructorUserCourseDTO,
                        studentUserCourseDTO.getUser(),
                        studentUserCourseEntity,
                        studentUserCourseDTO)));
    }
}
