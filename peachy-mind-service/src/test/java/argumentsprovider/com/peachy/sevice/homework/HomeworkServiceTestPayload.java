package argumentsprovider.com.peachy.sevice.homework;

import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkCreationDTO;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.entity.homework.HomeworkEntity;
import com.peachy.security.dto.UserDTO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class HomeworkServiceTestPayload {
    private final HomeworkCreationDTO homeworkCreation;

    private final HomeworkDTO homeworkDTO;

    private final HomeworkEntity homeworkEntity;

    private final ExpandedHomeworkDTO expandedHomeworkDTO;

    private final UserDTO userDTO;

    private final LessonDTO lessonDTO;

    private final UserCourseDTO userCourseDTO;

    private final UserCourseEntity userCourseEntity;

}
