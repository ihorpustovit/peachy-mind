package argumentsprovider.com.peachy.sevice.lesson;

import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.entity.LessonEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
@Getter
public class LessonServiceTestPayload {
    private final List<LessonCreationDTO> lessonCreationsDTOForDifferentCourses;

    private final List<LessonEntity> lessonEntities;

    private final List<LessonDTO> lessonDTOS;

    private final List<ExpandedCourseDTO> expandedCourseDTOS;

    private final List<CourseDTO> courseDTOS;
}
