package argumentsprovider.com.peachy.sevice.lesson;

import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.entity.LessonEntity;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import testdata.course.CourseServiceTestData;
import testdata.lesson.LessonServiceTestData;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LessonServiceTestArgumentProvider implements ArgumentsProvider {

    /**
     * {@link LessonServiceTestPayload} is built according to the following rules:
     * - {@link LessonServiceTestPayload} cares within a list of 2 lessons that belong to the different projects
     * - {@link LessonServiceTestPayload} has a list of {@link ExpandedCourseDTO} that lessons belong to
     * - {@link LessonServiceTestPayload} has a list of {@link LessonEntity}. Each has a {@link CourseEntity} within that is a
     * representation of one of the {@link ExpandedCourseDTO}s that lies in {@link LessonServiceTestPayload}
     * - {@link LessonServiceTestPayload} has a list of {@link LessonCreationDTO}. Each has a {@link ExpandedCourseDTO#getId()} within that is an id
     * of one of the {@link ExpandedCourseDTO}s that lies in {@link LessonServiceTestPayload}
     */
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        List<LessonDTO> lessonDTOs = LessonServiceTestData.generateLessonDTOsOfCount(2);
        List<ExpandedCourseDTO> expandedCourseDTOS = CourseServiceTestData.generateExpandedCourseDTOsOfCount(2);
        List<CourseDTO> courseDTOS = expandedCourseDTOS.stream().map(CourseServiceTestData::convertToCourseDTOFrom).collect(Collectors.toList());
        List<CourseEntity> courseEntities = expandedCourseDTOS.stream().map(CourseServiceTestData::convertToCourseEntityFrom).collect(Collectors.toList());
        List<LessonEntity> lessonEntities = lessonDTOs.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .collect(Collectors.toList());
        List<LessonCreationDTO> lessonCreationDTOs = lessonDTOs.stream()
                .map(LessonServiceTestData::convertToLessonCreationFrom)
                .collect(Collectors.toList());

        lessonEntities.get(0).setCourse(courseEntities.get(0));
        lessonEntities.get(1).setCourse(courseEntities.get(1));

        lessonCreationDTOs.get(0).setCourseId(courseEntities.get(0).getId());
        lessonCreationDTOs.get(1).setCourseId(courseEntities.get(1).getId());

        lessonDTOs.get(0).setCourse(CourseServiceTestData.convertToCourseDTOFrom(expandedCourseDTOS.get(0)));
        lessonDTOs.get(1).setCourse(CourseServiceTestData.convertToCourseDTOFrom(expandedCourseDTOS.get(1)));

        expandedCourseDTOS.get(0).setLessons(Collections.singletonList(LessonServiceTestData.convertToLessonInCourseDTOFrom(lessonDTOs.get(0))));
        expandedCourseDTOS.get(1).setLessons(Collections.singletonList(LessonServiceTestData.convertToLessonInCourseDTOFrom(lessonDTOs.get(1))));


        return Stream.of(Arguments.of(new LessonServiceTestPayload(lessonCreationDTOs, lessonEntities, lessonDTOs, expandedCourseDTOS, courseDTOS)));
    }
}
