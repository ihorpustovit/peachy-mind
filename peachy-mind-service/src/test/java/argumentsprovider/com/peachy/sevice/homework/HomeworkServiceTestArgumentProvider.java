package argumentsprovider.com.peachy.sevice.homework;

import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkCreationDTO;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.homework.HomeworkEntity;
import com.peachy.security.dto.UserDTO;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import testdata.course.CourseServiceTestData;
import testdata.homework.HomeworkServiceTestData;
import testdata.usercourse.UserCourseServiceTestData;

import java.util.stream.Stream;

public class HomeworkServiceTestArgumentProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        HomeworkDTO homeworkDTO = HomeworkServiceTestData.getHomeworkDTO();
        UserDTO userDTO = homeworkDTO.getUser();
        LessonDTO lessonDTO = homeworkDTO.getLesson();

        HomeworkEntity homeworkEntity = HomeworkServiceTestData.convertToHomeworkEntityFrom(homeworkDTO);
        HomeworkCreationDTO homeworkCreationDTO = HomeworkServiceTestData.convertToHomeworkDTOFrom(homeworkDTO);
        ExpandedHomeworkDTO expandedHomeworkDTO = HomeworkServiceTestData.convertToExpandedHomeworkDTOFrom(homeworkDTO);
        UserCourseDTO userCourseDTO = UserCourseServiceTestData.getStudentUserCourseDTO();
        userCourseDTO.setUser(userDTO);
        lessonDTO.setCourse(CourseServiceTestData.convertToCourseDTOFrom(userCourseDTO.getCourse()));

        return Stream.of(Arguments.of(new HomeworkServiceTestPayload(
                homeworkCreationDTO,
                homeworkDTO,
                homeworkEntity,
                expandedHomeworkDTO,
                userDTO,
                lessonDTO,
                userCourseDTO,
                UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO))));
    }
}
