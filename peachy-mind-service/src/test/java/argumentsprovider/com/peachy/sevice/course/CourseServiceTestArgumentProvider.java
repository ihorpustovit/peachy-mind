package argumentsprovider.com.peachy.sevice.course;

import com.peachy.dto.course.CourseCreationDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonInCourseDTO;
import com.peachy.security.dto.UserDTO;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import testdata.TestProperties;
import testdata.course.CourseServiceTestData;
import testdata.lesson.LessonServiceTestData;
import testdata.usercourse.UserCourseServiceTestData;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CourseServiceTestArgumentProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        List<LessonDTO> lessonDTOs = LessonServiceTestData.generateLessonDTOsOfCount(5);
        List<LessonInCourseDTO> lessonInCourseDTOS = lessonDTOs.stream().map(LessonServiceTestData::convertToLessonInCourseDTOFrom).collect(Collectors.toList());
        ExpandedCourseDTO expandedCourseDTO = CourseServiceTestData.getExpandedCourseDTO();
        lessonDTOs.forEach(lessonDTO -> lessonDTO.setCourse(CourseServiceTestData.convertToCourseDTOFrom(expandedCourseDTO)));
        expandedCourseDTO.setLessons(lessonInCourseDTOS);

        CourseCreationDTO courseCreationDTO = new CourseCreationDTO(
                TestProperties.getCourseTitle(),
                TestProperties.getCourseShortDescription(),
                expandedCourseDTO.getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList()),
                lessonDTOs.stream().map(LessonServiceTestData::convertToLessonInCourseCreationDTOFrom).collect(Collectors.toList()));

        CourseServiceTestPayload courseServiceTestPayload = new CourseServiceTestPayload(
                courseCreationDTO,
                expandedCourseDTO,
                CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO),
                CourseServiceTestData.convertToCourseDTOFrom(expandedCourseDTO),
                lessonDTOs.stream().map(LessonServiceTestData::convertToLessonCreationFrom).collect(Collectors.toList()),
                lessonDTOs,
                lessonInCourseDTOS,
                UserCourseServiceTestData.getInstructorUserCourseDTO(),
                UserCourseServiceTestData.getStudentUserCourseDTO());

        return Stream.of(Arguments.of(courseServiceTestPayload));
    }
}
