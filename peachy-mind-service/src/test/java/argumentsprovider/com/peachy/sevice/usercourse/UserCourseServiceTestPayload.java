package argumentsprovider.com.peachy.sevice.usercourse;

import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.security.dto.UserDTO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class UserCourseServiceTestPayload {
    private final ExpandedCourseDTO expandedCourseDTO;

    private final CourseDTO courseDTO;

    private final UserDTO instructor;

    private final UserCourseEntity instructorUserCourseEntity;

    private final UserCourseDTO instructorUserCourseDTO;

    private final UserDTO student;

    private final UserCourseEntity studentUserCourseEntity;

    private final UserCourseDTO studentUserCourseDTO;

}
