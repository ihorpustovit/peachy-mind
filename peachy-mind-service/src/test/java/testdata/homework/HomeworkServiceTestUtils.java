package testdata.homework;

import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.entity.homework.HomeworkEntity;
import com.peachy.security.dto.UserDTO;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDateTime;

public class HomeworkServiceTestUtils {
    public static boolean existsByLessonIdAndUserId(String lessonId,
                                                    String userId,
                                                    JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM homework WHERE lesson_id = ? AND user_id = ? ",
                Integer.class,
                lessonId,
                userId) > 0;
    }

    public static int countByLessonIdAndUserId(String lessonId,
                                               String userId,
                                               JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM homework WHERE lesson_id = ? AND user_id = ? ",
                Integer.class,
                lessonId,
                userId);
    }

    public static HomeworkDTO findByLessonIdAndUserId(String lessonId,
                                                      String userId,
                                                      JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("SELECT lesson_id, user_id, s3Key, s3Bucket, file_name, created_at, updated_at FROM homework WHERE lesson_id = ? AND user_id = ? ",
                (rs, rowNum) -> {
                    LessonDTO lessonDTO = new LessonDTO();
                    lessonDTO.setId(rs.getObject(1, String.class));
                    UserDTO userDTO = new UserDTO();
                    userDTO.setId(rs.getObject(2, String.class));
                    HomeworkDTO homeworkDTO = new HomeworkDTO(
                            lessonDTO,
                            userDTO,
                            rs.getObject(3, String.class),
                            rs.getObject(4, String.class),
                            rs.getObject(5, String.class));
                    homeworkDTO.setCreatedAt(rs.getObject(6, LocalDateTime.class));
                    homeworkDTO.setUpdatedAt(rs.getObject(7, LocalDateTime.class));
                    return homeworkDTO;
                },
                lessonId,
                userId);
    }

    public static void save(HomeworkEntity homeworkEntity, JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("insert into homework(user_id, lesson_id, s3Key,s3Bucket, file_name, created_at, updated_at) value (?,?,?,?,?,now(),now())",
                homeworkEntity.getId().getUser().getId(),
                homeworkEntity.getId().getLesson().getId(),
                homeworkEntity.getS3Key(),
                homeworkEntity.getS3Bucket(),
                homeworkEntity.getFileName());
    }

    public static void truncateHomeworks(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("DELETE FROM homework WHERE 1=1");
    }
}
