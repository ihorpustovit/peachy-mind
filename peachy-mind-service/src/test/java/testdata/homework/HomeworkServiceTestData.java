package testdata.homework;

import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkCreationDTO;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.entity.homework.HomeworkCompositeId;
import com.peachy.entity.homework.HomeworkEntity;
import com.peachy.security.dto.UserDTO;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import testdata.TestProperties;
import testdata.UserServiceTestData;
import testdata.lesson.LessonServiceTestData;

import java.time.LocalDateTime;

public class HomeworkServiceTestData {
    public static HomeworkDTO getHomeworkDTO() {
        HomeworkDTO homeworkDTO = new HomeworkDTO();
        LessonDTO lessonDTO = LessonServiceTestData.getLessonDTO();
        UserDTO userDTO = UserServiceTestData.getStudent();
        homeworkDTO.setLesson(lessonDTO);
        homeworkDTO.setFileName(TestProperties.randomString(12));
        homeworkDTO.setS3Key(TestProperties.randomString(12));
        homeworkDTO.setS3Bucket(TestProperties.randomString(12));
        homeworkDTO.setUser(userDTO);
        homeworkDTO.setCreatedAt(LocalDateTime.now());
        homeworkDTO.setUpdatedAt(LocalDateTime.now());
        return homeworkDTO;
    }

    public static HomeworkCreationDTO convertToHomeworkDTOFrom(HomeworkDTO homeworkDTO) {
        HomeworkCreationDTO homeworkCreationDTO = new HomeworkCreationDTO();
        homeworkCreationDTO.setLessonId(homeworkDTO.getLessonId());
        homeworkCreationDTO.setUser(homeworkDTO.getUser());

        MockMultipartFile mockHomework = new MockMultipartFile("homeworkTest.txt", "homeworkTest", MediaType.TEXT_PLAIN_VALUE, "homeworkTest".getBytes());
        homeworkCreationDTO.setHomeworkFile(mockHomework);
        return homeworkCreationDTO;
    }

    public static HomeworkEntity convertToHomeworkEntityFrom(HomeworkDTO homeworkDTO) {
        HomeworkEntity homeworkEntity = new HomeworkEntity();
        HomeworkCompositeId homeworkCompositeId = new HomeworkCompositeId();
        homeworkCompositeId.setLesson(LessonServiceTestData.convertToLessonEntityFrom(homeworkDTO.getLesson()));
        homeworkCompositeId.setUser(UserServiceTestData.convertToUserEntity(homeworkDTO.getUser()));
        homeworkEntity.setId(homeworkCompositeId);
        homeworkEntity.setFileName(homeworkDTO.getFileName());
        homeworkEntity.setS3Key(homeworkDTO.getS3Key());
        homeworkEntity.setS3Bucket(homeworkDTO.getS3Bucket());
        homeworkEntity.setCreatedAt(LocalDateTime.now());
        homeworkEntity.setUpdatedAt(LocalDateTime.now());
        return homeworkEntity;
    }

    public static ExpandedHomeworkDTO convertToExpandedHomeworkDTOFrom(HomeworkDTO homeworkDTO) {
        ExpandedHomeworkDTO expandedHomeworkDTO = new ExpandedHomeworkDTO();
        expandedHomeworkDTO.setLesson(homeworkDTO.getLesson());
        expandedHomeworkDTO.setUser(homeworkDTO.getUser());
        expandedHomeworkDTO.setFileName(homeworkDTO.getFileName());
        expandedHomeworkDTO.setS3Key(homeworkDTO.getS3Key());
        expandedHomeworkDTO.setS3Bucket(homeworkDTO.getS3Bucket());
        expandedHomeworkDTO.setCreatedAt(homeworkDTO.getCreatedAt());
        expandedHomeworkDTO.setUpdatedAt(homeworkDTO.getUpdatedAt());
        return expandedHomeworkDTO;
    }

    public static HomeworkDTO convertToHomeworkDTOFrom(HomeworkEntity homeworkEntity) {
        HomeworkDTO homeworkDTO = new HomeworkDTO();
        homeworkDTO.setLesson(LessonServiceTestData.convertToLessonDTOFrom(homeworkEntity.getId().getLesson()));
        homeworkDTO.setUser(UserServiceTestData.convertToUserDTOFrom(homeworkEntity.getId().getUser()));
        homeworkDTO.setFileName(homeworkEntity.getFileName());
        homeworkDTO.setS3Key(homeworkEntity.getS3Key());
        homeworkDTO.setS3Bucket(homeworkEntity.getS3Bucket());
        homeworkDTO.setCreatedAt(homeworkEntity.getCreatedAt());
        homeworkDTO.setUpdatedAt(homeworkEntity.getUpdatedAt());
        return homeworkDTO;
    }
}
