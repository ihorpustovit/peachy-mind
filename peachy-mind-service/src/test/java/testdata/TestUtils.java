package testdata;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.TextCodec;

public final class TestUtils {
    public static Object getJwtClaim(String token, String jwtClaim) {
        Jws<Claims> jws = Jwts.parser()
                .setSigningKey(TextCodec.BASE64.decode(TestProperties.getInternalTokenSecret()))
                .parseClaimsJws(token.substring(9));

        return jws.getBody().get(jwtClaim);
    }
}
