package testdata.course;

import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.entity.CourseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public final class CourseServiceTestUtils {
    public static void truncateCourses(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("DELETE FROM course WHERE 1=1");
    }

    public static boolean existsById(String courseId,
                                     JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM course WHERE id = ?", Integer.class, courseId) > 0;
    }

    public static void save(JdbcTemplate jdbcTemplate, CourseEntity courseEntity) {
        jdbcTemplate.update("insert into course(id, title, short_description, created_at, updated_at) value (?,?,?,now(),now())",
                courseEntity.getId(),
                courseEntity.getTitle(),
                courseEntity.getShortDescription());
    }

    public static List<ExpandedCourseDTO> findAll(JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.query("select * from course", new BeanPropertyRowMapper<>(ExpandedCourseDTO.class));
    }
}
