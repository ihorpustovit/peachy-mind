package testdata.course;

import com.peachy.dto.course.CourseCreationDTO;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonInCourseDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import org.springframework.util.Assert;
import testdata.TestProperties;
import testdata.UserServiceTestData;
import testdata.lesson.LessonServiceTestData;

import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CourseServiceTestData {
    public static CourseCreationDTO getCourseCreationDTO() {
        return new CourseCreationDTO(
                TestProperties.getCourseTitle(),
                TestProperties.getCourseShortDescription(),
                Collections.singletonList(UserServiceTestData.getInstructor().getId()),
                LessonServiceTestData.generateLessonInCourseCreationDTOsOfCount(5));
    }

    public static CourseDTO getCourseDTO() {
        return convertToCourseDTOFrom(getExpandedCourseDTOWithoutLessons());
    }

    public static ExpandedCourseDTO getExpandedCourseDTO() {
        ExpandedCourseDTO expandedCourseDTOWithoutLessons = getExpandedCourseDTOWithoutLessons();
        List<LessonInCourseDTO> lessonDTOWithoutCourse = LessonServiceTestData.generateLessonInCourseDTOsOfCount(5);
        expandedCourseDTOWithoutLessons.setLessons(lessonDTOWithoutCourse);
        lessonDTOWithoutCourse.forEach(lessonInCourseDTO -> lessonInCourseDTO.setCourse(convertToCourseDTOFrom(expandedCourseDTOWithoutLessons)));
        return expandedCourseDTOWithoutLessons;
    }

    public static ExpandedCourseDTO getExpandedCourseDTOWithoutLessons() {
        return new ExpandedCourseDTO(
                TestProperties.getCourseId(),
                TestProperties.getCourseTitle(),
                TestProperties.getCourseShortDescription(),
                Collections.singletonList(UserServiceTestData.getInstructor()),
                null,
                UserCourseStatus.STARTED,
                0);
    }


    public static CourseEntity getCourseEntity() {
        CourseEntity courseEntity = new CourseEntity();
        courseEntity.setId(TestProperties.getCourseId());
        courseEntity.setTitle(TestProperties.getCourseTitle());
        courseEntity.setShortDescription(TestProperties.getCourseShortDescription());
        return courseEntity;
    }

    public static List<ExpandedCourseDTO> generateExpandedCourseDTOsOfCount(int count) {
        Supplier<ExpandedCourseDTO> generateCourseEntity = () -> {
            ExpandedCourseDTO expandedCourseDTO = new ExpandedCourseDTO();
            expandedCourseDTO.setId(TestProperties.randomString(12));
            expandedCourseDTO.setTitle(TestProperties.randomString(12));
            expandedCourseDTO.setShortDescription(TestProperties.randomString(12));
            expandedCourseDTO.setLessons(Collections.emptyList());
            expandedCourseDTO.setInstructors(Collections.emptyList());
            return expandedCourseDTO;
        };

        return Stream.generate(generateCourseEntity).limit(count).collect(Collectors.toList());
    }

    public static List<CourseEntity> generateCourseEntitiesOfCount(int count) {
        return generateExpandedCourseDTOsOfCount(count).stream()
                .map(CourseServiceTestData::convertToCourseEntityFrom)
                .collect(Collectors.toList());
    }

    public static CourseEntity convertToCourseEntityFrom(ExpandedCourseDTO expandedCourseDTO) {
        Assert.notNull(expandedCourseDTO, "Null course dto passed to convert to course entity");
        CourseEntity courseEntity = new CourseEntity();
        courseEntity.setShortDescription(expandedCourseDTO.getShortDescription());
        courseEntity.setId(expandedCourseDTO.getId());
        courseEntity.setTitle(expandedCourseDTO.getTitle());
        courseEntity.setCreatedAt(expandedCourseDTO.getCreatedAt());
        courseEntity.setUpdatedAt(expandedCourseDTO.getUpdatedAt());
        return courseEntity;
    }

    public static CourseEntity convertToCourseEntityFrom(CourseDTO courseDTO) {
        CourseEntity courseEntity = new CourseEntity();
        courseEntity.setId(courseDTO.getId());
        courseEntity.setTitle(courseDTO.getTitle());
        courseEntity.setShortDescription(courseDTO.getShortDescription());
        courseEntity.setUpdatedAt(courseDTO.getUpdatedAt());
        courseEntity.setCreatedAt(courseDTO.getCreatedAt());
        return courseEntity;
    }

    public static CourseDTO convertToCourseDTOFrom(CourseEntity courseEntity) {
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(courseEntity.getId());
        courseDTO.setTitle(courseEntity.getTitle());
        courseDTO.setShortDescription(courseEntity.getShortDescription());
        courseDTO.setUpdatedAt(courseEntity.getUpdatedAt());
        courseDTO.setCreatedAt(courseEntity.getCreatedAt());
        return courseDTO;
    }

    public static CourseDTO convertToCourseDTOFrom(ExpandedCourseDTO expandedCourseDTO) {
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(expandedCourseDTO.getId());
        courseDTO.setTitle(expandedCourseDTO.getTitle());
        courseDTO.setShortDescription(expandedCourseDTO.getShortDescription());
        courseDTO.setUpdatedAt(expandedCourseDTO.getUpdatedAt());
        courseDTO.setCreatedAt(expandedCourseDTO.getCreatedAt());
        return courseDTO;
    }

    public static ExpandedCourseDTO convertToCourseDTOFrom(CourseDTO courseDTO) {
        ExpandedCourseDTO expandedCourseDTO = new ExpandedCourseDTO();
        expandedCourseDTO.setId(courseDTO.getId());
        expandedCourseDTO.setTitle(courseDTO.getTitle());
        expandedCourseDTO.setShortDescription(courseDTO.getShortDescription());
        expandedCourseDTO.setUpdatedAt(courseDTO.getUpdatedAt());
        expandedCourseDTO.setCreatedAt(courseDTO.getCreatedAt());
        return expandedCourseDTO;
    }
}
