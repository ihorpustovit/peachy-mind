package testdata;

import org.apache.commons.lang3.RandomStringUtils;

public final class TestProperties {

    public static String getCourseTitle() {
        return "Stub course title";
    }

    public static String getCourseShortDescription() {
        return "Stub course short description";
    }

    public static String getCourseId() {
        return "rpBvQOXmxk5b";
    }

    public static String getLessonTitle() {
        return "Stub lesson title";
    }

    public static String getLessonShortDescription() {
        return "Stub lesson short description";
    }

    public static String getLessonContent() {
        return "Stub lesson content";
    }

    public static String getLessonId() {
        return "rpBvQOXmxk2r";
    }

    public static String getUserId() {
        return "rpbVqOXmxk2r";
    }

    public static String randomString(int ofLength) {
        return RandomStringUtils.random(ofLength,true,true);
    }

    public static String getAdminInternalToken() {
        return "Internal eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJLcnI5M3hkcXZsNFIiLCJpdG4iOiJJbnRlcm5hbCBleUpoYkdjaU9pSklVelV4TWlKOS5leUp6ZFdJaU9pSkxjbkk1TTNoa2NYWnNORklpTENKaGRYb2lPaUpCUkUxSlRpSXNJbWx6Y3lJNkluZDNkeTV3WldGamFIa3RiV2x1WkM1amIyMGlMQ0owY0dVaU9pSkpUbFJGVWs1QlRDSXNJbVYxZUNJNk1UWXlPVGt3TnpZMk15d2lhWFY0SWpveE5qSTVNekF5T0RZekxDSjBhV1FpT2lKVlRHNXZiWFJvVEVOclEySWlmUS54cmV1bk1YcE8xMzVlRTd2Zm9IWXYzOEFmMkdRQ1BmbkR3ai1mUFlXcHdvNV9EWVpnWE9wNDFmQ21semFlTF9mTkozU2o5cGtVZ1IxdWNGMG4wdDhTdyIsImF1eiI6IkFETUlOIiwiaXNzIjoid3d3LnBlYWNoeS1taW5kLmNvbSIsInRwZSI6IkdBVEVXQVkiLCJldXgiOjE2Mjk5MDcwNjMsIml1eCI6MTYyOTMwMjg2MywidGlkIjoiVUxub210aExDa0NiIn0.wbyUzPIxHCF4Tn5raVQkiO5RERboqTCYpvoSxGKYL_x2n6NI3AXcaN1ASxvTeC7LzTz1odOJNSlN4SlF6iemjQ";
    }

    public static String getInstructorInternalToken() {
        return "Internal eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJUWEJ4RVlQejVxSUoiLCJpdG4iOiJJbnRlcm5hbCBleUpoYkdjaU9pSklVelV4TWlKOS5leUp6ZFdJaU9pSlVXRUo0UlZsUWVqVnhTVW9pTENKaGRYb2lPaUpKVGxOVVVsVkRWRTlTSWl3aWFYTnpJam9pZDNkM0xuQmxZV05vZVMxdGFXNWtMbU52YlNJc0luUndaU0k2SWtsT1ZFVlNUa0ZNSWl3aVpYVjRJam94TmpJNU9UQTNORGt4TENKcGRYZ2lPakUyTWprek1ESTJPVEVzSW5ScFpDSTZJa2h3YnpOdFFVUktTMnhRUXlKOS5TaFAtQ2FFTkpnTnc2MVJIQ1NhU3NaOHN2UHE2blZlM0F5c3VmRkd2M2ZmekdVTXczMTAzY3EwdTZaVG93V0haRnlnTTBSaWktdHljV2RROVA4U3pJZyIsImF1eiI6IklOU1RSVUNUT1IiLCJpc3MiOiJ3d3cucGVhY2h5LW1pbmQuY29tIiwidHBlIjoiR0FURVdBWSIsImV1eCI6MTYyOTkwNjg5MSwiaXV4IjoxNjI5MzAyNjkxLCJ0aWQiOiJIcG8zbUFESktsUEMifQ.qQEprhfwWLVC35R2uk937tBnPwqpbiibNXqLos9cRlXUq-oMVB9u2rOUucbACrpmur0qXeKtDnpWmOeYLxUwtw";
    }

    public static String getStudentInternalToken() {
        return "Internal eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJEY0lycldtUmVTQmEiLCJpdG4iOiJJbnRlcm5hbCBleUpoYkdjaU9pSklVelV4TWlKOS5leUp6ZFdJaU9pSkVZMGx5Y2xkdFVtVlRRbUVpTENKaGRYb2lPaUpUVkZWRVJVNVVJaXdpYVhOeklqb2lkM2QzTG5CbFlXTm9lUzF0YVc1a0xtTnZiU0lzSW5Sd1pTSTZJa2xPVkVWU1RrRk1JaXdpWlhWNElqb3hOakk1T1RBNE56TTFMQ0pwZFhnaU9qRTJNamt6TURNNU16VXNJblJwWkNJNklrNWlhVFZCY21kb1luVXdhQ0o5LmlOMm96aVdESV9XcDBXRUVMdVNYRkpaeXl6TDRlaEtDS0ZXYUtXZXJGSW5xQmVtRGswR21hQmV4OWdidUdNaUY5Z2hYTHA4WUNvWEpXSFdweDJjSHpBIiwiYXV6IjoiU1RVREVOVCIsImlzcyI6Ind3dy5wZWFjaHktbWluZC5jb20iLCJ0cGUiOiJHQVRFV0FZIiwiZXV4IjoxNjI5OTA4MTM1LCJpdXgiOjE2MjkzMDM5MzUsInRpZCI6Ik5iaTVBcmdoYnUwaCJ9.F6oIuazQh9TqIu_Hfy8SqZMt9tRCteQEFXnaem4RYvH0FJLPGXj8nOs_oUnQ5_9UpzV9R_w0e6xejkM7W2iK0g";
    }

    public static String getInternalTokenSecret() {
        return "ybh3+N9auwFTcqOGl0AEE1GbPf27ivTTmgzGK3P8oS+vcY1PXV6feI2gz1dX3a3yayVvdE5ONiIwDsLZf7rOwg==";
    }
}
