package testdata;

import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;

public class UserServiceTestData {
    public static UserDTO getInstructor() {
        UserDTO instructor = new UserDTO();
        instructor.setId(TestProperties.randomString(12));
        instructor.setUsername(TestProperties.randomString(12));
        instructor.setPlainPassword(TestProperties.randomString(12));
        instructor.setHashedPassword(TestProperties.randomString(12));
        instructor.setAuthority(UserAuthority.INSTRUCTOR);
        return instructor;
    }

    public static UserDTO getStudent() {
        UserDTO instructor = new UserDTO();
        instructor.setId(TestProperties.randomString(12));
        instructor.setUsername(TestProperties.randomString(12));
        instructor.setPlainPassword(TestProperties.randomString(12));
        instructor.setHashedPassword(TestProperties.randomString(12));
        instructor.setAuthority(UserAuthority.STUDENT);
        return instructor;
    }

    public static UserEntity convertToUserEntity(UserDTO userDTO) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userDTO.getId());
        userEntity.setUsername(userDTO.getUsername());
        userEntity.setAuthority(userDTO.getAuthority());
        userEntity.setHashedPassword(userDTO.getHashedPassword());
        userEntity.setCreatedAt(userDTO.getCreatedAt());
        userEntity.setUpdatedAt(userDTO.getUpdatedAt());
        return userEntity;
    }

    public static UserDTO convertToUserDTOFrom(UserEntity userEntity) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userEntity.getId());
        userDTO.setUsername(userEntity.getUsername());
        userDTO.setAuthority(userEntity.getAuthority());
        userDTO.setHashedPassword(userEntity.getHashedPassword());
        userDTO.setCreatedAt(userEntity.getCreatedAt());
        userDTO.setUpdatedAt(userEntity.getUpdatedAt());
        return userDTO;
    }
}
