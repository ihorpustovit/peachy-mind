package testdata.usercourse;

import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.usercourse.UserCourseCompositeId;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import org.springframework.util.Assert;
import testdata.UserServiceTestData;
import testdata.course.CourseServiceTestData;

public class UserCourseServiceTestData {
    public static UserCourseDTO getInstructorUserCourseDTO() {
        return new UserCourseDTO(
                UserServiceTestData.getInstructor(),
                CourseServiceTestData.getExpandedCourseDTO(),
                UserCourseStatus.STARTED);
    }

    public static UserCourseDTO getStudentUserCourseDTO() {
        return new UserCourseDTO(
                UserServiceTestData.getStudent(),
                CourseServiceTestData.getExpandedCourseDTO(),
                UserCourseStatus.STARTED);
    }

    public static UserCourseEntity convertToUserCourseEntity(UserCourseDTO userCourseDTO) {
        Assert.notNull(userCourseDTO, "Null user course dto passed to convert to user entity");
        UserCourseEntity userCourseEntity = new UserCourseEntity();
        userCourseEntity.setId(new UserCourseCompositeId(CourseServiceTestData.convertToCourseEntityFrom(userCourseDTO.getCourse()), UserServiceTestData.convertToUserEntity(userCourseDTO.getUser())));
        userCourseEntity.setStatus(userCourseDTO.getStatus());
        return userCourseEntity;
    }
}
