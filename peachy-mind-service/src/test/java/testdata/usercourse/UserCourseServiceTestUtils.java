package testdata.usercourse;

import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.usercourse.UserCourseCompositeId;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.security.dto.UserDTO;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class UserCourseServiceTestUtils {
    public static boolean existAllByIds(List<UserCourseCompositeId> userCourseIds,
                                        JdbcTemplate jdbcTemplate) {
        String collapsedUserCourseIds = userCourseIds.stream()
                .map(userCourseId -> "\'" + userCourseId.getCollapsedId() + "\'")
                .collect(Collectors.joining(","));
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM user_course WHERE CONCAT(user_id, user_course.course_id) in (" + collapsedUserCourseIds + ")", Integer.class) >= userCourseIds.size();
    }

    public static void save(UserCourseEntity userCourseEntity, JdbcTemplate jdbcTemplate) {
        if (!existAllByIds(Collections.singletonList(userCourseEntity.getId()), jdbcTemplate)) {
            jdbcTemplate.update("INSERT INTO user_course(user_id, course_id, created_at, updated_at, status) VALUE (?,?,now(),now(),?)",
                    userCourseEntity.getId().getUser().getId(),
                    userCourseEntity.getId().getCourse().getId(),
                    Optional.ofNullable(userCourseEntity.getStatus())
                            .map(UserCourseStatus::name)
                            .orElse(null));
        }
    }

    public static List<UserCourseDTO> findAll(JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.query("select user_id,course_id from user_course", (rs, rowNum) -> {
            UserCourseDTO userCourseDTO = new UserCourseDTO();
            UserDTO userDTO = new UserDTO();
            userDTO.setId(rs.getString("user_id"));
            ExpandedCourseDTO expandedCourseDTO = new ExpandedCourseDTO();
            expandedCourseDTO.setId(rs.getString("course_id"));
            userCourseDTO.setCourse(expandedCourseDTO);
            userCourseDTO.setUser(userDTO);
            return userCourseDTO;
        });
    }

    public static List<UserCourseDTO> findAllByCourseId(String courseId,
                                                        JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.query("select user_id,course_id from user_course where course_id=?", (rs, rowNum) -> {
            UserCourseDTO userCourseDTO = new UserCourseDTO();
            UserDTO userDTO = new UserDTO();
            userDTO.setId(rs.getString("user_id"));
            ExpandedCourseDTO expandedCourseDTO = new ExpandedCourseDTO();
            expandedCourseDTO.setId(rs.getString("course_id"));
            userCourseDTO.setCourse(expandedCourseDTO);
            userCourseDTO.setUser(userDTO);
            return userCourseDTO;
        }, courseId);
    }

    public static Optional<UserCourseDTO> findById(UserCourseCompositeId userCourseId,
                                                   JdbcTemplate jdbcTemplate) {
        return Optional.ofNullable(jdbcTemplate.queryForObject("select user_id,course_id,status from user_course where CONCAT(user_id,course_id)=?", (rs, rowNum) -> {
            UserCourseDTO userCourseDTO = new UserCourseDTO();
            UserDTO userDTO = new UserDTO();
            userDTO.setId(rs.getString("user_id"));
            ExpandedCourseDTO expandedCourseDTO = new ExpandedCourseDTO();
            expandedCourseDTO.setId(rs.getString("course_id"));
            userCourseDTO.setCourse(expandedCourseDTO);
            userCourseDTO.setUser(userDTO);
            userCourseDTO.setStatus(UserCourseStatus.valueOf(rs.getString("status")));
            return userCourseDTO;
        }, userCourseId.getCollapsedId()));
    }

    public static void truncateUserCourse(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("DELETE FROM user_course WHERE 1=1");
    }
}
