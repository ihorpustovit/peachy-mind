package testdata.lesson;

import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonInCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.lesson.creation.LessonInCourseCreationDTO;
import com.peachy.entity.LessonEntity;
import testdata.TestProperties;
import testdata.course.CourseServiceTestData;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LessonServiceTestData {
    private static final Integer DEFAULT_LESSON_MAX_GRADE = 10;

    public static LessonInCourseCreationDTO getLessonInCourseCreationDTO() {
        return new LessonInCourseCreationDTO(
                TestProperties.getLessonTitle(),
                TestProperties.getLessonShortDescription(),
                TestProperties.getLessonContent(),
                DEFAULT_LESSON_MAX_GRADE);
    }

    public static LessonInCourseDTO getLessonInCourseDTO() {
        ExpandedCourseDTO expandedCourseDTOWithoutLessons = CourseServiceTestData.getExpandedCourseDTOWithoutLessons();
        LessonInCourseDTO lessonDTOWithoutCourse = LessonServiceTestData.getLessonInCourseDTOWithoutCourse();
        expandedCourseDTOWithoutLessons.setLessons(Collections.singletonList(lessonDTOWithoutCourse));
        lessonDTOWithoutCourse.setCourse(CourseServiceTestData.convertToCourseDTOFrom(expandedCourseDTOWithoutLessons));
        return lessonDTOWithoutCourse;
    }

    static LessonInCourseDTO getLessonInCourseDTOWithoutCourse() {
        return new LessonInCourseDTO(
                TestProperties.getLessonId(),
                TestProperties.getCourseTitle(),
                TestProperties.getLessonShortDescription(),
                TestProperties.getLessonContent(),
                DEFAULT_LESSON_MAX_GRADE,
                null);
    }

    public static LessonCreationDTO getLessonCreationDTO() {
        return new LessonCreationDTO(
                TestProperties.getLessonTitle(),
                TestProperties.getLessonShortDescription(),
                TestProperties.getLessonContent(),
                TestProperties.getCourseId(),
                DEFAULT_LESSON_MAX_GRADE);
    }

    public static LessonDTO getLessonDTO() {
        LessonDTO lessonDTO = getLessonDTOWithoutCourse();
        LessonInCourseDTO lessonInCourseDTO = new LessonInCourseDTO(
                lessonDTO.getId(),
                lessonDTO.getTitle(),
                lessonDTO.getShortDescription(),
                lessonDTO.getContent(),
                DEFAULT_LESSON_MAX_GRADE,
                null);
        ExpandedCourseDTO expandedCourseDTO = CourseServiceTestData.getExpandedCourseDTOWithoutLessons();
        expandedCourseDTO.setLessons(Collections.singletonList(lessonInCourseDTO));
        lessonInCourseDTO.setCourse(CourseServiceTestData.convertToCourseDTOFrom(expandedCourseDTO));
        lessonDTO.setCourse(CourseServiceTestData.convertToCourseDTOFrom(expandedCourseDTO));
        return lessonDTO;
    }

    static LessonDTO getLessonDTOWithoutCourse() {
        return new LessonDTO(
                TestProperties.getLessonId(),
                TestProperties.getLessonTitle(),
                TestProperties.getLessonShortDescription(),
                TestProperties.getLessonContent(),
                DEFAULT_LESSON_MAX_GRADE,
                null);
    }

    public static List<LessonDTO> generateLessonDTOsOfCount(int count) {
        Supplier<LessonDTO> generateLesson = () ->
                new LessonDTO(
                        TestProperties.randomString(12),
                        TestProperties.randomString(12),
                        TestProperties.randomString(12),
                        TestProperties.randomString(12),
                        DEFAULT_LESSON_MAX_GRADE,
                        null);
        return Stream.generate(generateLesson).limit(count).collect(Collectors.toList());
    }

    public static List<LessonInCourseCreationDTO> generateLessonInCourseCreationDTOsOfCount(int count) {
        return generateLessonDTOsOfCount(count)
                .stream()
                .map(LessonServiceTestData::convertToLessonInCourseCreationDTOFrom)
                .collect(Collectors.toList());
    }

    public static List<LessonInCourseDTO> generateLessonInCourseDTOsOfCount(int count) {
        return generateLessonDTOsOfCount(count)
                .stream()
                .map(LessonServiceTestData::convertToLessonInCourseDTOFrom)
                .collect(Collectors.toList());
    }

    public static List<LessonEntity> generateLessonEntitiesOfCount(int count) {
        return generateLessonDTOsOfCount(count)
                .stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .collect(Collectors.toList());
    }

    public static List<LessonCreationDTO> generateLessonCreationDTOsOfCount(int count) {
        return generateLessonDTOsOfCount(count)
                .stream()
                .map(LessonServiceTestData::convertToLessonCreationFrom)
                .collect(Collectors.toList());
    }

    public static LessonInCourseDTO convertToLessonInCourseDTOFrom(LessonDTO lessonDTO) {
        return new LessonInCourseDTO(
                lessonDTO.getId(),
                lessonDTO.getTitle(),
                lessonDTO.getShortDescription(),
                lessonDTO.getContent(),
                DEFAULT_LESSON_MAX_GRADE,
                null);
    }

    public static LessonInCourseCreationDTO convertToLessonInCourseCreationDTOFrom(LessonDTO lessonDTO) {
        return new LessonInCourseCreationDTO(
                lessonDTO.getTitle(),
                lessonDTO.getShortDescription(),
                lessonDTO.getContent(),
                DEFAULT_LESSON_MAX_GRADE);
    }

    public static LessonEntity convertToLessonEntityFrom(LessonDTO lessonDTO) {
        LessonEntity lessonEntity = new LessonEntity();
        lessonEntity.setId(lessonDTO.getId());
        lessonEntity.setTitle(lessonDTO.getTitle());
        lessonEntity.setShortDescription(lessonDTO.getShortDescription());
        lessonEntity.setContent(lessonDTO.getContent());

        if (Objects.nonNull(lessonDTO.getCourse())) {
            lessonEntity.setCourse(CourseServiceTestData.convertToCourseEntityFrom(lessonDTO.getCourse()));
        }
        return lessonEntity;
    }

    public static LessonDTO convertToLessonDTOFrom(LessonEntity lessonEntity) {
        LessonDTO lessonDTO = new LessonDTO();
        lessonDTO.setId(lessonEntity.getId());
        lessonDTO.setTitle(lessonEntity.getTitle());
        lessonDTO.setShortDescription(lessonEntity.getShortDescription());
        lessonDTO.setContent(lessonEntity.getContent());

        if (Objects.nonNull(lessonEntity.getCourse())) {
            lessonDTO.setCourse(CourseServiceTestData.convertToCourseDTOFrom(lessonEntity.getCourse()));
        }

        return lessonDTO;
    }

    public static LessonCreationDTO convertToLessonCreationFrom(LessonDTO lessonDTO) {
        LessonCreationDTO lessonCreationDTO = new LessonCreationDTO();
        lessonCreationDTO.setTitle(lessonDTO.getTitle());
        lessonCreationDTO.setShortDescription(lessonDTO.getShortDescription());
        lessonCreationDTO.setContent(lessonDTO.getContent());
        lessonCreationDTO.setMaxGrade(5);

        if (Objects.nonNull(lessonDTO.getCourse())) {
            lessonCreationDTO.setCourseId(lessonDTO.getCourse().getId());
        }
        return lessonCreationDTO;
    }
}
