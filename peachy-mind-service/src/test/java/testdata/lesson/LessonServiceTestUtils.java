package testdata.lesson;

import com.peachy.dto.lesson.LessonDTO;
import com.peachy.entity.LessonEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.course.CourseServiceTestUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class LessonServiceTestUtils {
    public static boolean existsById(String lessonId,
                                     JdbcTemplate jdbcTemplate) {
        return existAllByIds(Collections.singletonList(lessonId), jdbcTemplate);
    }

    public static boolean existAllByIds(List<String> lessonIds,
                                        JdbcTemplate jdbcTemplate) {
        String collapsedLessonIds = lessonIds.stream().map(lessonId -> "\'" + lessonId + "\'").collect(Collectors.joining(","));
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM lesson WHERE id in (" + collapsedLessonIds + ")", Integer.class) >= lessonIds.size();
    }

    public static void truncateLessons(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("DELETE FROM lesson WHERE 1=1");
    }

    public static void save(LessonEntity lessonEntity, JdbcTemplate jdbcTemplate) {
        if (!CourseServiceTestUtils.existsById(lessonEntity.getCourse().getId(), jdbcTemplate)) {
            CourseServiceTestUtils.save(jdbcTemplate, lessonEntity.getCourse());
        }

        jdbcTemplate.update("insert into lesson (id, title, short_description, content, course_id, created_at, updated_at) value (?,?,?,?,?,now(),now())",
                lessonEntity.getId(),
                lessonEntity.getTitle(),
                lessonEntity.getShortDescription(),
                lessonEntity.getContent(),
                lessonEntity.getCourse().getId());
    }

    public static List<LessonDTO> findAll(JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.query("select * from lesson", new BeanPropertyRowMapper<>(LessonDTO.class));
    }

    public static List<LessonDTO> findAllByCourseId(String courseId,
                                                    JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.query("select * from lesson where course_id=?", new BeanPropertyRowMapper<>(LessonDTO.class), courseId);
    }
}
