package webmvc.com.peachy.controller;

import argumentsprovider.com.peachy.sevice.homework.HomeworkServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.homework.HomeworkServiceTestPayload;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.fasterxml.jackson.databind.JsonNode;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.entity.homework.HomeworkEntity;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import com.peachy.test.data.UserServiceTestUtils;
import com.peachy.web.PeachyMindHttpHeaders;
import com.squareup.okhttp.*;
import io.restassured.RestAssured;
import lombok.SneakyThrows;
import okio.BufferedSink;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import testdata.TestProperties;
import testdata.TestUtils;
import testdata.UserServiceTestData;
import testdata.homework.HomeworkServiceTestUtils;
import testdata.lesson.LessonServiceTestData;
import testdata.lesson.LessonServiceTestUtils;
import testdata.usercourse.UserCourseServiceTestUtils;
import webmvc.com.peachy.WebMvcTest;

import java.io.IOException;

import static org.hamcrest.Matchers.is;

public class HomeworkControllerTest extends WebMvcTest {
    @Value("${aws.s3.access-key}")
    private String s3AccessKey;

    @Value("${aws.s3.secret-access-key}")
    private String s3SecretAccessKey;

    @Value("${aws.s3.region}")
    private String regionName;

    @Autowired
    @Qualifier("peachyMindAuzJdbcTemplate")
    private JdbcTemplate peachyMindAuzJdbcTemplate;

    private AmazonS3 s3Client;

    @Value("${aws.s3.bucket.base}")
    private String s3BaseBucketName;

    private final String HOMEWORK_CONTROLLER_PATH = "/homework";

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        s3Client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(s3AccessKey, s3SecretAccessKey)))
                .withRegion(Regions.fromName(regionName))
                .build();

        HomeworkServiceTestUtils.truncateHomeworks(jdbcTemplate);
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
    }

    private RequestBody okHttpMultipartRequestBody(MultipartFile multipartFile) {
        return new RequestBody() {
            @Override
            public MediaType contentType() {
                return MediaType.parse(org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE);
            }

            @Override
            public void writeTo(BufferedSink bufferedSink) throws IOException {
                bufferedSink.write(multipartFile.getBytes());
            }

            @Override
            public long contentLength() {
                return multipartFile.getSize();
            }
        };
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testUploadHomeworkReturnsBadRequestIfLessonDoesntExistForGivenId(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        LessonDTO lesson = homeworkServiceTestPayload.getLessonDTO();
        String studentInternalToken = TestProperties.getStudentInternalToken();
        String userId = TestUtils.getJwtClaim(studentInternalToken, PeachyMindJwtClaim.SUBJECT).toString();
        UserEntity userEntity = UserServiceTestData.convertToUserEntity(homeworkServiceTestPayload.getUserDTO());
        userEntity.setId(userId);

        UserServiceTestUtils.save(userEntity, peachyMindAuzJdbcTemplate);

        MultipartFile multipartFile = homeworkServiceTestPayload.getHomeworkCreation().getHomeworkFile();
        //TEST
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody body = okHttpMultipartRequestBody(multipartFile);

        RequestBody multipartRequest = new MultipartBuilder()
                .addFormDataPart("homework", multipartFile.getName(), body)
                .build();

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(String.format("http://localhost:%s/homework/upload",port))
                .queryParam("lessonId", lesson.getId());

        Request request = new Request.Builder()
                .url(uriBuilder.toUriString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, studentInternalToken)
                .post(multipartRequest)
                .build();

        Response response = okHttpClient.newCall(request).execute();

        //ASSERT
        JsonNode uploadHomeworkResponse = objectMapper.readTree(response.body().string());
        Assertions.assertTrue(uploadHomeworkResponse.has("message"));
        Assertions.assertTrue(uploadHomeworkResponse.has("reason"));
        Assertions.assertEquals("Unable to upload homework", uploadHomeworkResponse.get("message").asText());
        Assertions.assertEquals(String.format("Lesson doesn't exist for id: %s",lesson.getId()), uploadHomeworkResponse.get("reason").asText());
        Assertions.assertFalse(HomeworkServiceTestUtils.existsByLessonIdAndUserId(lesson.getId(), userId, jdbcTemplate));
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, HttpStatus.valueOf(response.code()));
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testUploadHomeworkReturnsConflictIfHomeworkWasAlreadyUploaded(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        LessonDTO lesson = homeworkServiceTestPayload.getLessonDTO();
        String studentInternalToken = TestProperties.getStudentInternalToken();
        String userId = TestUtils.getJwtClaim(studentInternalToken, PeachyMindJwtClaim.SUBJECT).toString();

        UserEntity userEntity = UserServiceTestData.convertToUserEntity(homeworkServiceTestPayload.getUserDTO());
        userEntity.setId(userId);

        HomeworkEntity homeworkEntity = homeworkServiceTestPayload.getHomeworkEntity();
        homeworkEntity.getId().setUser(userEntity);

        UserServiceTestUtils.save(userEntity, peachyMindAuzJdbcTemplate);
        LessonServiceTestUtils.save(LessonServiceTestData.convertToLessonEntityFrom(lesson), jdbcTemplate);
        HomeworkServiceTestUtils.save(homeworkEntity, jdbcTemplate);

        MultipartFile multipartFile = homeworkServiceTestPayload.getHomeworkCreation().getHomeworkFile();
        //TEST
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody body = okHttpMultipartRequestBody(multipartFile);

        RequestBody multipartRequest = new MultipartBuilder()
                .addFormDataPart("homework", multipartFile.getName(), body)
                .build();

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(String.format("http://localhost:%s/homework/upload",port))
                .queryParam("lessonId", lesson.getId());

        Request request = new Request.Builder()
                .url(uriBuilder.toUriString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, studentInternalToken)
                .post(multipartRequest)
                .build();

        Response response = okHttpClient.newCall(request).execute();

        //ASSERT
        JsonNode uploadHomeworkResponse = objectMapper.readTree(response.body().string());
        Assertions.assertTrue(uploadHomeworkResponse.has("message"));
        Assertions.assertTrue(uploadHomeworkResponse.has("reason"));
        Assertions.assertEquals("Unable to upload homework", uploadHomeworkResponse.get("message").asText());
        Assertions.assertEquals("Homework has been uploaded already", uploadHomeworkResponse.get("reason").asText());
        Assertions.assertEquals(1, HomeworkServiceTestUtils.countByLessonIdAndUserId(lesson.getId(), userId, jdbcTemplate));
        Assertions.assertEquals(HttpStatus.CONFLICT, HttpStatus.valueOf(response.code()));
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testUploadHomeworkReturnsConflictIfUserIsNotAssignedToACourse(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        LessonDTO lesson = homeworkServiceTestPayload.getLessonDTO();
        String studentInternalToken = TestProperties.getStudentInternalToken();
        String userId = TestUtils.getJwtClaim(studentInternalToken, PeachyMindJwtClaim.SUBJECT).toString();

        UserEntity userEntity = UserServiceTestData.convertToUserEntity(homeworkServiceTestPayload.getUserDTO());
        userEntity.setId(userId);

        HomeworkEntity homeworkEntity = homeworkServiceTestPayload.getHomeworkEntity();
        homeworkEntity.getId().setUser(userEntity);

        UserServiceTestUtils.save(userEntity, peachyMindAuzJdbcTemplate);
        LessonServiceTestUtils.save(LessonServiceTestData.convertToLessonEntityFrom(lesson), jdbcTemplate);

        MultipartFile multipartFile = homeworkServiceTestPayload.getHomeworkCreation().getHomeworkFile();
        //TEST
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody body = okHttpMultipartRequestBody(multipartFile);

        RequestBody multipartRequest = new MultipartBuilder()
                .addFormDataPart("homework", multipartFile.getName(), body)
                .build();

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(String.format("http://localhost:%s/homework/upload", port))
                .queryParam("lessonId", lesson.getId());

        Request request = new Request.Builder()
                .url(uriBuilder.toUriString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, studentInternalToken)
                .post(multipartRequest)
                .build();

        Response response = okHttpClient.newCall(request).execute();

        //ASSERT
        JsonNode uploadHomeworkResponse = objectMapper.readTree(response.body().string());
        Assertions.assertTrue(uploadHomeworkResponse.has("message"));
        Assertions.assertTrue(uploadHomeworkResponse.has("reason"));
        Assertions.assertEquals("Unable to upload homework", uploadHomeworkResponse.get("message").asText());
        Assertions.assertEquals("User is not assigned to a course", uploadHomeworkResponse.get("reason").asText());
        Assertions.assertEquals("User should get assigned to the course first", uploadHomeworkResponse.get("solution").asText());
        Assertions.assertEquals(0, HomeworkServiceTestUtils.countByLessonIdAndUserId(lesson.getId(), userId, jdbcTemplate));
        Assertions.assertEquals(HttpStatus.CONFLICT, HttpStatus.valueOf(response.code()));
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        HomeworkServiceTestUtils.truncateHomeworks(jdbcTemplate);
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
    }
}
