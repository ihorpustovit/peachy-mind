package webmvc.com.peachy.controller;

import argumentsprovider.com.peachy.sevice.course.CourseServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.course.CourseServiceTestPayload;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.test.data.UserServiceTestUtils;
import com.peachy.web.PeachyMindHttpHeaders;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.TestProperties;
import testdata.UserServiceTestData;
import testdata.course.CourseServiceTestData;
import testdata.course.CourseServiceTestUtils;
import testdata.lesson.LessonServiceTestData;
import testdata.lesson.LessonServiceTestUtils;
import testdata.usercourse.UserCourseServiceTestData;
import testdata.usercourse.UserCourseServiceTestUtils;
import webmvc.com.peachy.WebMvcTest;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;

public class LessonControllerTest extends WebMvcTest {

    protected final String LESSON_CONTROLLER_PATH = "/lesson";

    @Autowired
    @Qualifier("peachyMindAuzJdbcTemplate")
    private JdbcTemplate peachyMindAuzJdbcTemplate;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    @SneakyThrows
    public void testAdminCreateLesson(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = Collections.singletonList(courseServiceTestPayload.getLessonsDTOs().get(0));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        LessonCreationDTO additionalLesson = LessonServiceTestData.convertToLessonCreationFrom(courseServiceTestPayload.getLessonsDTOs().get(1));
        String lessonResource = objectMapper.writeValueAsString(additionalLesson);

        //TEST AND ASSERT
        ValidatableResponse validatableResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .body(lessonResource)
                .post(LESSON_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.CREATED.value());

        validatableResponse
                .body("id", not(StringUtils.EMPTY))
                .body("id", notNullValue())
                .body("title", is(additionalLesson.getTitle()))
                .body("shortDescription", is(additionalLesson.getShortDescription()))
                .body("content", is(additionalLesson.getContent()))
                .body("course.id", is(additionalLesson.getCourseId()))
                .body("course.title", is(expandedCourseDTO.getTitle()))
                .body("course.shortDescription", is(expandedCourseDTO.getShortDescription()));

        String additionalCourseId = validatableResponse.extract().path("id");

        List<LessonDTO> lessonsByCourseId = LessonServiceTestUtils.findAllByCourseId(additionalLesson.getCourseId(), jdbcTemplate);
        Assertions.assertNotNull(lessonsByCourseId);
        Assertions.assertEquals(2, lessonsByCourseId.size());
        Assertions.assertTrue(lessonsByCourseId.stream().map(LessonDTO::getId).collect(Collectors.toList()).contains(additionalCourseId));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    @SneakyThrows
    public void testInstructorCreateLesson(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = Collections.singletonList(courseServiceTestPayload.getLessonsDTOs().get(0));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        LessonCreationDTO additionalLesson = LessonServiceTestData.convertToLessonCreationFrom(courseServiceTestPayload.getLessonsDTOs().get(1));
        String lessonResource = objectMapper.writeValueAsString(additionalLesson);

        //TEST AND ASSERT
        ValidatableResponse validatableResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getInstructorInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .body(lessonResource)
                .post(LESSON_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.CREATED.value());

        validatableResponse
                .body("id", not(StringUtils.EMPTY))
                .body("id", notNullValue())
                .body("title", is(additionalLesson.getTitle()))
                .body("shortDescription", is(additionalLesson.getShortDescription()))
                .body("content", is(additionalLesson.getContent()))
                .body("course.id", is(additionalLesson.getCourseId()))
                .body("course.title", is(expandedCourseDTO.getTitle()))
                .body("course.shortDescription", is(expandedCourseDTO.getShortDescription()));

        String additionalCourseId = validatableResponse.extract().path("id");

        List<LessonDTO> lessonsByCourseId = LessonServiceTestUtils.findAllByCourseId(additionalLesson.getCourseId(), jdbcTemplate);
        Assertions.assertNotNull(lessonsByCourseId);
        Assertions.assertEquals(2, lessonsByCourseId.size());
        Assertions.assertTrue(lessonsByCourseId.stream().map(LessonDTO::getId).collect(Collectors.toList()).contains(additionalCourseId));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    @SneakyThrows
    public void testStudentCreateLesson(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = Collections.singletonList(courseServiceTestPayload.getLessonsDTOs().get(0));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        LessonCreationDTO additionalLesson = LessonServiceTestData.convertToLessonCreationFrom(courseServiceTestPayload.getLessonsDTOs().get(1));
        String lessonResource = objectMapper.writeValueAsString(additionalLesson);

        //TEST AND ASSERT
        ValidatableResponse validatableResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .body(lessonResource)
                .post(LESSON_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }


    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    @SneakyThrows
    public void testCreateLessonReturnsBadRequestIfLessonTitleIsEmpty(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = Collections.singletonList(courseServiceTestPayload.getLessonsDTOs().get(0));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        LessonCreationDTO additionalLesson = LessonServiceTestData.convertToLessonCreationFrom(courseServiceTestPayload.getLessonsDTOs().get(1));
        additionalLesson.setTitle(StringUtils.EMPTY);
        String lessonResource = objectMapper.writeValueAsString(additionalLesson);

        //TEST AND ASSERT
        ValidatableResponse validatableResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .body(lessonResource)
                .post(LESSON_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        validatableResponse
                .body("message", is("Unable to create lesson"))
                .body("reason", is("Lesson title should not be empty"));

        List<LessonDTO> lessonsByCourseId = LessonServiceTestUtils.findAllByCourseId(additionalLesson.getCourseId(), jdbcTemplate);
        Assertions.assertNotNull(lessonsByCourseId);
        Assertions.assertEquals(1, lessonsByCourseId.size());
        Assertions.assertTrue(lessonsByCourseId.stream()
                .map(LessonDTO::getId)
                .collect(Collectors.toList()).containsAll(lessonDTOS.stream()
                        .map(LessonDTO::getId)
                        .collect(Collectors.toList())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    @SneakyThrows
    public void testCreateLessonReturnsBadRequestIfLessonTitleIsTooLong(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = Collections.singletonList(courseServiceTestPayload.getLessonsDTOs().get(0));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        LessonCreationDTO additionalLesson = LessonServiceTestData.convertToLessonCreationFrom(courseServiceTestPayload.getLessonsDTOs().get(1));
        additionalLesson.setTitle(TestProperties.randomString(150));
        String lessonResource = objectMapper.writeValueAsString(additionalLesson);

        //TEST AND ASSERT
        ValidatableResponse validatableResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .body(lessonResource)
                .post(LESSON_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        validatableResponse
                .body("message", is("Unable to create lesson"))
                .body("reason", matchesPattern("Course title should not exceed [0-9]+ characters"));

        List<LessonDTO> lessonsByCourseId = LessonServiceTestUtils.findAllByCourseId(additionalLesson.getCourseId(), jdbcTemplate);
        Assertions.assertNotNull(lessonsByCourseId);
        Assertions.assertEquals(1, lessonsByCourseId.size());
        Assertions.assertTrue(lessonsByCourseId.stream()
                .map(LessonDTO::getId)
                .collect(Collectors.toList()).containsAll(lessonDTOS.stream()
                        .map(LessonDTO::getId)
                        .collect(Collectors.toList())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    @SneakyThrows
    public void testCreateLessonReturnsBadRequestIfLessonShortDescriptionIsTooLong(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = Collections.singletonList(courseServiceTestPayload.getLessonsDTOs().get(0));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        LessonCreationDTO additionalLesson = LessonServiceTestData.convertToLessonCreationFrom(courseServiceTestPayload.getLessonsDTOs().get(1));
        additionalLesson.setShortDescription(TestProperties.randomString(600));
        String lessonResource = objectMapper.writeValueAsString(additionalLesson);

        //TEST AND ASSERT
        ValidatableResponse validatableResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .body(lessonResource)
                .post(LESSON_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        validatableResponse
                .body("message", is("Unable to create lesson"))
                .body("reason", matchesPattern("Course description should not exceed [0-9]+ characters"));

        List<LessonDTO> lessonsByCourseId = LessonServiceTestUtils.findAllByCourseId(additionalLesson.getCourseId(), jdbcTemplate);
        Assertions.assertNotNull(lessonsByCourseId);
        Assertions.assertEquals(1, lessonsByCourseId.size());
        Assertions.assertTrue(lessonsByCourseId.stream()
                .map(LessonDTO::getId)
                .collect(Collectors.toList()).containsAll(lessonDTOS.stream()
                        .map(LessonDTO::getId)
                        .collect(Collectors.toList())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    @SneakyThrows
    public void testCreateLessonReturnsBadRequestIfCourseThatLessonIsAssignedToDoesntExist(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        LessonCreationDTO additionalLesson = LessonServiceTestData.convertToLessonCreationFrom(courseServiceTestPayload.getLessonsDTOs().get(1));
        String lessonResource = objectMapper.writeValueAsString(additionalLesson);

        //TEST AND ASSERT
        ValidatableResponse validatableResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .body(lessonResource)
                .post(LESSON_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        validatableResponse
                .body("message", is("Unable to create lesson"))
                .body("reason", matchesPattern("Course that lesson's being assigned to doesn't exist"))
                .body("solution", is("Assign lesson to course that actually exists"));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    @SneakyThrows
    public void testCreateLessonReturnsBadRequestIfLessonHasEmptyCourseId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = Collections.singletonList(courseServiceTestPayload.getLessonsDTOs().get(0));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        LessonCreationDTO additionalLesson = LessonServiceTestData.convertToLessonCreationFrom(courseServiceTestPayload.getLessonsDTOs().get(1));
        additionalLesson.setCourseId(StringUtils.EMPTY);
        String lessonResource = objectMapper.writeValueAsString(additionalLesson);

        //TEST AND ASSERT
        ValidatableResponse validatableResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .body(lessonResource)
                .post(LESSON_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        validatableResponse
                .body("message", is("Unable to create lesson"))
                .body("reason", is("Course id that lesson is assigned to should not be blank or empty"));

        List<LessonDTO> lessonsByCourseId = LessonServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(lessonsByCourseId);
        Assertions.assertEquals(1, lessonsByCourseId.size());
        Assertions.assertTrue(lessonsByCourseId.stream()
                .map(LessonDTO::getId)
                .collect(Collectors.toList()).containsAll(lessonDTOS.stream()
                        .map(LessonDTO::getId)
                        .collect(Collectors.toList())));
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
    }
}
