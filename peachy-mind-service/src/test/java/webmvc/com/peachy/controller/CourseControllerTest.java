package webmvc.com.peachy.controller;

import argumentsprovider.com.peachy.sevice.course.CourseServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.course.CourseServiceTestPayload;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.dto.course.CourseCreationDTO;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.creation.LessonInCourseCreationDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.entity.usercourse.UserCourseCompositeId;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import com.peachy.test.data.UserServiceTestUtils;
import com.peachy.web.PeachyMindHttpHeaders;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.TextCodec;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.core.IsNot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.TestProperties;
import testdata.TestUtils;
import testdata.UserServiceTestData;
import testdata.course.CourseServiceTestData;
import testdata.course.CourseServiceTestUtils;
import testdata.lesson.LessonServiceTestData;
import testdata.lesson.LessonServiceTestUtils;
import testdata.usercourse.UserCourseServiceTestData;
import testdata.usercourse.UserCourseServiceTestUtils;
import webmvc.com.peachy.WebMvcTest;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;

public class CourseControllerTest extends WebMvcTest {

    protected final String COURSE_CONTROLLER_PATH = "/course";

    @Autowired
    @Qualifier("peachyMindAuzJdbcTemplate")
    private JdbcTemplate peachyMindAuzJdbcTemplate;

    private  Function<LessonInCourseCreationDTO, ObjectNode> lessonToObjectNode = lesson -> {
        ObjectNode lessonNode = objectMapper.createObjectNode();
        lessonNode.put("title", lesson.getTitle());
        lessonNode.put("shortDescription", lesson.getShortDescription());
        lessonNode.put("content", lesson.getContent());
        lessonNode.put("maxGrade", lesson.getMaxGrade());
        return lessonNode;
    };

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
    }


    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testStudentCreateCourseReturnsForbidden(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", courseCreationDTO.getTitle());
        courseCreationResource.put("shortDescription", courseCreationDTO.getShortDescription());

        ArrayNode instructorIds = courseCreationResource.putArray("instructorIds");
        courseCreationDTO.getInstructorIds().forEach(instructorIds::add);

        ArrayNode lessons = courseCreationResource.putArray("lessons");
        courseCreationDTO.getLessons().stream()
                .map(lessonToObjectNode)
                .forEach(lessons::addPOJO);

        courseServiceTestPayload.getExpandedCourseDTO().getInstructors().forEach(userDTO -> UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate));
        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testCreateCourseReturnsBadRequestIfCourseTitleIsEmpty(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", StringUtils.EMPTY);
        courseCreationResource.put("shortDescription", courseCreationDTO.getShortDescription());

        ArrayNode instructorIds = courseCreationResource.putArray("instructorIds");
        courseCreationDTO.getInstructorIds().forEach(instructorIds::add);

        ArrayNode lessons = courseCreationResource.putArray("lessons");
        courseCreationDTO.getLessons().stream()
                .map(lessonToObjectNode)
                .forEach(lessons::addPOJO);

        courseServiceTestPayload.getExpandedCourseDTO().getInstructors().forEach(userDTO -> UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate));
        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        createCourseResponse
                .body("message", is("Unable to create course"))
                .body("reason", is("Course title should not be empty"));

        Assertions.assertTrue(CourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(courseDTO -> StringUtils.equals(courseDTO.getTitle(), courseCreationDTO.getTitle()) &&
                        StringUtils.equals(courseDTO.getShortDescription(), courseCreationDTO.getShortDescription())));
        Assertions.assertTrue(LessonServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(lessonDTO -> Assert.that(courseCreationDTO.getLessons())
                        .matchesAnyOf(lessonInCourseCreationDTO ->
                                StringUtils.equals(lessonInCourseCreationDTO.getTitle(), lessonDTO.getTitle()) &&
                                        StringUtils.equals(lessonInCourseCreationDTO.getShortDescription(), lessonDTO.getShortDescription()))
                        .conditionMet()));

        Assertions.assertTrue(UserCourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(userCourseDTO -> Assert.that(courseCreationDTO.getInstructorIds())
                        .matchesAnyOf(instructorId -> StringUtils.equals(instructorId, userCourseDTO.getUserId()))
                        .conditionMet()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testCreateCourseReturnsBadRequestIfCourseTitleTooLong(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", TestProperties.randomString(100));
        courseCreationResource.put("shortDescription", courseCreationDTO.getShortDescription());

        ArrayNode instructorIds = courseCreationResource.putArray("instructorIds");
        courseCreationDTO.getInstructorIds().forEach(instructorIds::add);

        ArrayNode lessons = courseCreationResource.putArray("lessons");
        courseCreationDTO.getLessons().stream()
                .map(lessonToObjectNode)
                .forEach(lessons::addPOJO);

        courseServiceTestPayload.getExpandedCourseDTO().getInstructors().forEach(userDTO -> UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate));
        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        createCourseResponse
                .body("message", is("Unable to create course"))
                .body("reason", is(matchesPattern("Course title should not exceed [0-9]+ characters")));

        Assertions.assertTrue(CourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(courseDTO -> StringUtils.equals(courseDTO.getTitle(), courseCreationDTO.getTitle()) &&
                        StringUtils.equals(courseDTO.getShortDescription(), courseCreationDTO.getShortDescription())));
        Assertions.assertTrue(LessonServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(lessonDTO -> Assert.that(courseCreationDTO.getLessons())
                        .matchesAnyOf(lessonInCourseCreationDTO ->
                                StringUtils.equals(lessonInCourseCreationDTO.getTitle(), lessonDTO.getTitle()) &&
                                        StringUtils.equals(lessonInCourseCreationDTO.getShortDescription(), lessonDTO.getShortDescription()))
                        .conditionMet()));

        Assertions.assertTrue(UserCourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(userCourseDTO -> Assert.that(courseCreationDTO.getInstructorIds())
                        .matchesAnyOf(instructorId -> StringUtils.equals(instructorId, userCourseDTO.getUserId()))
                        .conditionMet()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testCreateCourseReturnsBadRequestIfCourseShortDescriptionTooLong(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", courseCreationDTO.getTitle());
        courseCreationResource.put("shortDescription", TestProperties.randomString(600));

        ArrayNode instructorIds = courseCreationResource.putArray("instructorIds");
        courseCreationDTO.getInstructorIds().forEach(instructorIds::add);

        ArrayNode lessons = courseCreationResource.putArray("lessons");
        courseCreationDTO.getLessons().stream()
                .map(lessonToObjectNode)
                .forEach(lessons::addPOJO);

        courseServiceTestPayload.getExpandedCourseDTO().getInstructors().forEach(userDTO -> UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate));
        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        createCourseResponse
                .body("message", is("Unable to create course"))
                .body("reason", is(matchesPattern("Course description should not exceed [0-9]+ characters")));

        Assertions.assertTrue(CourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(courseDTO -> StringUtils.equals(courseDTO.getTitle(), courseCreationDTO.getTitle()) &&
                        StringUtils.equals(courseDTO.getShortDescription(), courseCreationDTO.getShortDescription())));
        Assertions.assertTrue(LessonServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(lessonDTO -> Assert.that(courseCreationDTO.getLessons())
                        .matchesAnyOf(lessonInCourseCreationDTO ->
                                StringUtils.equals(lessonInCourseCreationDTO.getTitle(), lessonDTO.getTitle()) &&
                                        StringUtils.equals(lessonInCourseCreationDTO.getShortDescription(), lessonDTO.getShortDescription()))
                        .conditionMet()));

        Assertions.assertTrue(UserCourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(userCourseDTO -> Assert.that(courseCreationDTO.getInstructorIds())
                        .matchesAnyOf(instructorId -> StringUtils.equals(instructorId, userCourseDTO.getUserId()))
                        .conditionMet()));
    }


    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testCreateCourseReturnsBadRequestIfCourseHasNoLessons(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", courseCreationDTO.getTitle());
        courseCreationResource.put("shortDescription", courseCreationDTO.getShortDescription());

        ArrayNode instructorIds = courseCreationResource.putArray("instructorIds");
        courseCreationDTO.getInstructorIds().forEach(instructorIds::add);

        courseServiceTestPayload.getExpandedCourseDTO().getInstructors().forEach(userDTO -> UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate));
        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        createCourseResponse
                .body("message", is("Unable to create course"))
                .body("reason", is(matchesPattern("Course should have at least [0-9]+ lessons within")));

        Assertions.assertTrue(CourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(courseDTO -> StringUtils.equals(courseDTO.getTitle(), courseCreationDTO.getTitle()) &&
                        StringUtils.equals(courseDTO.getShortDescription(), courseCreationDTO.getShortDescription())));
        Assertions.assertTrue(LessonServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(lessonDTO -> Assert.that(courseCreationDTO.getLessons())
                        .matchesAnyOf(lessonInCourseCreationDTO ->
                                StringUtils.equals(lessonInCourseCreationDTO.getTitle(), lessonDTO.getTitle()) &&
                                        StringUtils.equals(lessonInCourseCreationDTO.getShortDescription(), lessonDTO.getShortDescription()))
                        .conditionMet()));

        Assertions.assertTrue(UserCourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(userCourseDTO -> Assert.that(courseCreationDTO.getInstructorIds())
                        .matchesAnyOf(instructorId -> StringUtils.equals(instructorId, userCourseDTO.getUserId()))
                        .conditionMet()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testCreateCourseReturnsBadRequestIfCourseIsLowOnLessons(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", courseCreationDTO.getTitle());
        courseCreationResource.put("shortDescription", TestProperties.randomString(600));

        ArrayNode instructorIds = courseCreationResource.putArray("instructorIds");
        courseCreationDTO.getInstructorIds().forEach(instructorIds::add);

        ArrayNode lessons = courseCreationResource.putArray("lessons");
        lessons.addPOJO(lessonToObjectNode.apply(courseCreationDTO.getLessons().get(0)));

        courseServiceTestPayload.getExpandedCourseDTO().getInstructors().forEach(userDTO -> UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate));
        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        createCourseResponse
                .body("message", is("Unable to create course"))
                .body("reason", is(matchesPattern("Course description should not exceed [0-9]+ characters")));

        Assertions.assertTrue(CourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(courseDTO -> StringUtils.equals(courseDTO.getTitle(), courseCreationDTO.getTitle()) &&
                        StringUtils.equals(courseDTO.getShortDescription(), courseCreationDTO.getShortDescription())));
        Assertions.assertTrue(LessonServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(lessonDTO -> Assert.that(courseCreationDTO.getLessons())
                        .matchesAnyOf(lessonInCourseCreationDTO ->
                                StringUtils.equals(lessonInCourseCreationDTO.getTitle(), lessonDTO.getTitle()) &&
                                        StringUtils.equals(lessonInCourseCreationDTO.getShortDescription(), lessonDTO.getShortDescription()))
                        .conditionMet()));

        Assertions.assertTrue(UserCourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(userCourseDTO -> Assert.that(courseCreationDTO.getInstructorIds())
                        .matchesAnyOf(instructorId -> StringUtils.equals(instructorId, userCourseDTO.getUserId()))
                        .conditionMet()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testCreateCourseReturnsBadRequestIfCourseHasNonExistentInstructors(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", courseCreationDTO.getTitle());
        courseCreationResource.put("shortDescription", courseCreationDTO.getShortDescription());
        ArrayNode lessons = courseCreationResource.putArray("lessons");
        courseCreationDTO.getLessons().stream()
                .map(lessonToObjectNode)
                .forEach(lessons::addPOJO);

        ArrayNode instructorIds = courseCreationResource.putArray("instructorIds");
        courseCreationDTO.getInstructorIds().forEach(instructorIds::add);

        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        createCourseResponse
                .body("message", is("Unable to create course"))
                .body("reason", is("Non-existent user specified as instructor"));

        Assertions.assertTrue(CourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(courseDTO -> StringUtils.equals(courseDTO.getTitle(), courseCreationDTO.getTitle()) &&
                        StringUtils.equals(courseDTO.getShortDescription(), courseCreationDTO.getShortDescription())));
        Assertions.assertTrue(LessonServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(lessonDTO -> Assert.that(courseCreationDTO.getLessons())
                        .matchesAnyOf(lessonInCourseCreationDTO ->
                                StringUtils.equals(lessonInCourseCreationDTO.getTitle(), lessonDTO.getTitle()) &&
                                        StringUtils.equals(lessonInCourseCreationDTO.getShortDescription(), lessonDTO.getShortDescription()))
                        .conditionMet()));

        Assertions.assertTrue(UserCourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(userCourseDTO -> Assert.that(courseCreationDTO.getInstructorIds())
                        .matchesAnyOf(instructorId -> StringUtils.equals(instructorId, userCourseDTO.getUserId()))
                        .conditionMet()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testCreateCourseReturnsBadRequestIfCourseHasNoInstructors(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", courseCreationDTO.getTitle());
        courseCreationResource.put("shortDescription", courseCreationDTO.getShortDescription());
        ArrayNode lessons = courseCreationResource.putArray("lessons");
        courseCreationDTO.getLessons().stream()
                .map(lessonToObjectNode)
                .forEach(lessons::addPOJO);

        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        createCourseResponse
                .body("message", is("Unable to create course"))
                .body("reason", is(String.format("At least %s instructors should be assigned per course", 1)));

        Assertions.assertTrue(CourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(courseDTO -> StringUtils.equals(courseDTO.getTitle(), courseCreationDTO.getTitle()) &&
                        StringUtils.equals(courseDTO.getShortDescription(), courseCreationDTO.getShortDescription())));
        Assertions.assertTrue(LessonServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(lessonDTO -> Assert.that(courseCreationDTO.getLessons())
                        .matchesAnyOf(lessonInCourseCreationDTO ->
                                StringUtils.equals(lessonInCourseCreationDTO.getTitle(), lessonDTO.getTitle()) &&
                                        StringUtils.equals(lessonInCourseCreationDTO.getShortDescription(), lessonDTO.getShortDescription()))
                        .conditionMet()));

        Assertions.assertTrue(UserCourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(userCourseDTO -> Assert.that(courseCreationDTO.getInstructorIds())
                        .matchesAnyOf(instructorId -> StringUtils.equals(instructorId, userCourseDTO.getUserId()))
                        .conditionMet()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testCreateCourseReturnsBadRequestIfUserAssignedToCourseAsInstructorIsNotActuallyAnInstructor(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseCreationDTO courseCreationDTO = courseServiceTestPayload.getCourseCreationDTO();
        Assertions.assertTrue(courseCreationDTO.getInstructorIds().containsAll(courseServiceTestPayload.getExpandedCourseDTO().getInstructors().stream().map(UserDTO::getId).collect(Collectors.toList())));
        ObjectNode courseCreationResource = objectMapper.createObjectNode();
        courseCreationResource.put("title", courseCreationDTO.getTitle());
        courseCreationResource.put("shortDescription", courseCreationDTO.getShortDescription());
        ArrayNode lessons = courseCreationResource.putArray("lessons");
        courseCreationDTO.getLessons().stream()
                .map(lessonToObjectNode)
                .forEach(lessons::addPOJO);

        ArrayNode instructorIds = courseCreationResource.putArray("instructorIds");
        courseCreationDTO.getInstructorIds().forEach(instructorIds::add);

        courseServiceTestPayload.getExpandedCourseDTO().getInstructors().forEach(userDTO -> {
            userDTO.setAuthority(UserAuthority.STUDENT);
            UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate);
        });

        //TEST AND ASSERT
        ValidatableResponse createCourseResponse = RestAssured
                .with()
                .body(courseCreationResource.toPrettyString())
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(COURSE_CONTROLLER_PATH)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        createCourseResponse
                .body("message", is("Unable to assign instructor to course"))
                .body("reason", is(String.format("User that you are trying to assign to course as %s is actually not of %s authority or doesn't exist at all",
                        UserAuthority.INSTRUCTOR.getStringyAuthority(),
                        UserAuthority.INSTRUCTOR.getStringyAuthority())));

        Assertions.assertTrue(CourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(courseDTO -> StringUtils.equals(courseDTO.getTitle(), courseCreationDTO.getTitle()) &&
                        StringUtils.equals(courseDTO.getShortDescription(), courseCreationDTO.getShortDescription())));
        Assertions.assertTrue(LessonServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(lessonDTO -> Assert.that(courseCreationDTO.getLessons())
                        .matchesAnyOf(lessonInCourseCreationDTO ->
                                StringUtils.equals(lessonInCourseCreationDTO.getTitle(), lessonDTO.getTitle()) &&
                                        StringUtils.equals(lessonInCourseCreationDTO.getShortDescription(), lessonDTO.getShortDescription()))
                        .conditionMet()));

        Assertions.assertTrue(UserCourseServiceTestUtils.findAll(jdbcTemplate).stream()
                .noneMatch(userCourseDTO -> Assert.that(courseCreationDTO.getInstructorIds())
                        .matchesAnyOf(instructorId -> StringUtils.equals(instructorId, userCourseDTO.getUserId()))
                        .conditionMet()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAdminAssignInstructorToCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewInstructor = new UserEntity();
        brandNewInstructor.setId(TestProperties.randomString(12));
        brandNewInstructor.setUsername(TestProperties.randomString(12));
        brandNewInstructor.setHashedPassword(TestProperties.randomString(12));
        brandNewInstructor.setAuthority(UserAuthority.INSTRUCTOR);
        UserServiceTestUtils.save(brandNewInstructor, peachyMindAuzJdbcTemplate);

        //TEST
        RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("instructorId", brandNewInstructor.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/instructor"))
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(2, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.containsAll(Arrays.asList(courseServiceTestPayload.getInstructorUserCourseDTO().getUserId(), brandNewInstructor.getId())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testInstructorAssignInstructorToCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewInstructor = new UserEntity();
        brandNewInstructor.setId(TestProperties.randomString(12));
        brandNewInstructor.setUsername(TestProperties.randomString(12));
        brandNewInstructor.setHashedPassword(TestProperties.randomString(12));
        brandNewInstructor.setAuthority(UserAuthority.INSTRUCTOR);
        UserServiceTestUtils.save(brandNewInstructor, peachyMindAuzJdbcTemplate);

        //TEST
        RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getInstructorInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("instructorId", brandNewInstructor.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/instructor"))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testStudentAssignInstructorToCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewInstructor = new UserEntity();
        brandNewInstructor.setId(TestProperties.randomString(12));
        brandNewInstructor.setUsername(TestProperties.randomString(12));
        brandNewInstructor.setHashedPassword(TestProperties.randomString(12));
        brandNewInstructor.setAuthority(UserAuthority.INSTRUCTOR);
        UserServiceTestUtils.save(brandNewInstructor, peachyMindAuzJdbcTemplate);

        //TEST
        RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("instructorId", brandNewInstructor.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/instructor"))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }


    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignInstructorToCourseReturnsNotFoundIfCourseDoesntExist(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        UserEntity brandNewInstructor = new UserEntity();
        brandNewInstructor.setId(TestProperties.randomString(12));
        brandNewInstructor.setUsername(TestProperties.randomString(12));
        brandNewInstructor.setHashedPassword(TestProperties.randomString(12));
        brandNewInstructor.setAuthority(UserAuthority.INSTRUCTOR);
        UserServiceTestUtils.save(brandNewInstructor, peachyMindAuzJdbcTemplate);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("instructorId", brandNewInstructor.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/instructor"))
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());

        response
                .body("message", is("Unable to assign user to course"))
                .body("reason", is(String.format("No course found for id: %s", expandedCourseDTO.getId())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignInstructorToCourseReturnsBadRequestIfNoInstructorIdSpecified(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewInstructor = new UserEntity();
        brandNewInstructor.setId(TestProperties.randomString(12));
        brandNewInstructor.setUsername(TestProperties.randomString(12));
        brandNewInstructor.setHashedPassword(TestProperties.randomString(12));
        brandNewInstructor.setAuthority(UserAuthority.INSTRUCTOR);
        UserServiceTestUtils.save(brandNewInstructor, peachyMindAuzJdbcTemplate);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("instructorId", StringUtils.EMPTY)
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/instructor"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        response
                .body("message", is("Unable to assign instructor to course"))
                .body("reason", is("Some instructor ids are empty"));

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getInstructorUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignInstructorToCourseReturnsBadRequestIfGivenInstructorDoesntExist(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewInstructor = new UserEntity();
        brandNewInstructor.setId(TestProperties.randomString(12));
        brandNewInstructor.setUsername(TestProperties.randomString(12));
        brandNewInstructor.setHashedPassword(TestProperties.randomString(12));
        brandNewInstructor.setAuthority(UserAuthority.INSTRUCTOR);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("instructorId", brandNewInstructor.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/instructor"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        response
                .body("message", is("Unable to assign instructor to course"))
                .body("reason", is(String.format("User that you are trying to assign to course as %s is actually not of %s authority or doesn't exist at all",
                        UserAuthority.INSTRUCTOR.getStringyAuthority(),
                        UserAuthority.INSTRUCTOR.getStringyAuthority())));
        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getInstructorUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignInstructorToCourseReturnsBadRequestIfGivenUserIsNotAnInstructor(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewInstructor = new UserEntity();
        brandNewInstructor.setId(TestProperties.randomString(12));
        brandNewInstructor.setUsername(TestProperties.randomString(12));
        brandNewInstructor.setHashedPassword(TestProperties.randomString(12));
        brandNewInstructor.setAuthority(UserAuthority.STUDENT);
        UserServiceTestUtils.save(brandNewInstructor, peachyMindAuzJdbcTemplate);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("instructorId", brandNewInstructor.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/instructor"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        response
                .body("message", is("Unable to assign instructor to course"))
                .body("reason", is(String.format("User that you are trying to assign to course as %s is actually not of %s authority or doesn't exist at all",
                        UserAuthority.INSTRUCTOR.getStringyAuthority(),
                        UserAuthority.INSTRUCTOR.getStringyAuthority())));

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getInstructorUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourseReturnsConflictInCaseOfRepetitiveAssignment(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("instructorId", userCourseDTO.getUserId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/instructor"))
                .then()
                .statusCode(HttpStatus.CONFLICT.value());

        response
                .body("message", is("Unable to assign instructor to course"))
                .body("reason", is("Some instructors are already assigned for given course"));

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getInstructorUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAdminAssignStudentToCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO studentUserCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(studentUserCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(studentUserCourseDTO), jdbcTemplate);
        UserEntity brandNewStudent = new UserEntity();
        brandNewStudent.setId(TestProperties.randomString(12));
        brandNewStudent.setUsername(TestProperties.randomString(12));
        brandNewStudent.setHashedPassword(TestProperties.randomString(12));
        brandNewStudent.setAuthority(UserAuthority.STUDENT);
        UserServiceTestUtils.save(brandNewStudent, peachyMindAuzJdbcTemplate);

        //TEST
        RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", brandNewStudent.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(2, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.containsAll(Arrays.asList(courseServiceTestPayload.getStudentUserCourseDTO().getUserId(), brandNewStudent.getId())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testInstructorAssignStudentToCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO studentUserCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(studentUserCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(studentUserCourseDTO), jdbcTemplate);
        UserEntity brandNewStudent = new UserEntity();
        brandNewStudent.setId(TestProperties.randomString(12));
        brandNewStudent.setUsername(TestProperties.randomString(12));
        brandNewStudent.setHashedPassword(TestProperties.randomString(12));
        brandNewStudent.setAuthority(UserAuthority.STUDENT);
        UserServiceTestUtils.save(brandNewStudent, peachyMindAuzJdbcTemplate);

        //TEST
        RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getInstructorInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", brandNewStudent.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(2, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.containsAll(Arrays.asList(courseServiceTestPayload.getStudentUserCourseDTO().getUserId(), brandNewStudent.getId())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testStudentAssignToCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO studentUserCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();
        studentUserCourseDTO.getUser().setId(TestUtils.getJwtClaim(TestProperties.getStudentInternalToken(), PeachyMindJwtClaim.SUBJECT).toString());

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(studentUserCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        //TEST
        RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", studentUserCourseDTO.getUserId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getStudentUserCourseDTO().getUserId()));
    }


    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testStudentAssignStudentToCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO studentUserCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(studentUserCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(studentUserCourseDTO), jdbcTemplate);
        UserEntity brandNewStudent = new UserEntity();
        brandNewStudent.setId(TestProperties.randomString(12));
        brandNewStudent.setUsername(TestProperties.randomString(12));
        brandNewStudent.setHashedPassword(TestProperties.randomString(12));
        brandNewStudent.setAuthority(UserAuthority.STUDENT);
        UserServiceTestUtils.save(brandNewStudent, peachyMindAuzJdbcTemplate);

        //TEST
        RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", brandNewStudent.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getStudentUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignStudentToCourseReturnsNotFoundIfCourseDoesntExist(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        UserEntity brandNewStudent = new UserEntity();
        brandNewStudent.setId(TestProperties.randomString(12));
        brandNewStudent.setUsername(TestProperties.randomString(12));
        brandNewStudent.setHashedPassword(TestProperties.randomString(12));
        brandNewStudent.setAuthority(UserAuthority.STUDENT);
        UserServiceTestUtils.save(brandNewStudent, peachyMindAuzJdbcTemplate);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", brandNewStudent.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());

        response
                .body("message", is("Unable to assign user to course"))
                .body("reason", is(String.format("No course found for id: %s", expandedCourseDTO.getId())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignStudentToCourseReturnsBadRequestIfNoStudentIdSpecified(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewStudent = new UserEntity();
        brandNewStudent.setId(TestProperties.randomString(12));
        brandNewStudent.setUsername(TestProperties.randomString(12));
        brandNewStudent.setHashedPassword(TestProperties.randomString(12));
        brandNewStudent.setAuthority(UserAuthority.STUDENT);
        UserServiceTestUtils.save(brandNewStudent, peachyMindAuzJdbcTemplate);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", StringUtils.EMPTY)
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        response
                .body("message", is("Unable to assign student to course"))
                .body("reason", is("Some student ids are empty"));

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getStudentUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignStudentToCourseReturnsBadRequestIfGivenStudentDoesntExist(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewStudent = new UserEntity();
        brandNewStudent.setId(TestProperties.randomString(12));
        brandNewStudent.setUsername(TestProperties.randomString(12));
        brandNewStudent.setHashedPassword(TestProperties.randomString(12));
        brandNewStudent.setAuthority(UserAuthority.STUDENT);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", brandNewStudent.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        response
                .body("message", is("Unable to assign student to course"))
                .body("reason", is(String.format("User that you are trying to assign to course as %s is actually not of %s authority or doesn't exist at all",
                        UserAuthority.STUDENT.getStringyAuthority(),
                        UserAuthority.STUDENT.getStringyAuthority())));
        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getStudentUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignStudentToCourseReturnsBadRequestIfGivenUserIsNotAStudent(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);
        UserEntity brandNewStudent = new UserEntity();
        brandNewStudent.setId(TestProperties.randomString(12));
        brandNewStudent.setUsername(TestProperties.randomString(12));
        brandNewStudent.setHashedPassword(TestProperties.randomString(12));
        brandNewStudent.setAuthority(UserAuthority.INSTRUCTOR);
        UserServiceTestUtils.save(brandNewStudent, peachyMindAuzJdbcTemplate);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", brandNewStudent.getId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        response
                .body("message", is("Unable to assign student to course"))
                .body("reason", is(String.format("User that you are trying to assign to course as %s is actually not of %s authority or doesn't exist at all",
                        UserAuthority.STUDENT.getStringyAuthority(),
                        UserAuthority.STUDENT.getStringyAuthority())));

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getStudentUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignStudentToCourseReturnsConflictInCaseOfRepetitiveAssignment(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", userCourseDTO.getUserId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.CONFLICT.value());

        response
                .body("message", is("Unable to assign student to course"))
                .body("reason", is("Some students are already assigned for given course"));

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAllByCourseId(expandedCourseDTO.getId(), jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(1, usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getStudentUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testAssignStudentToCourseReturnsConflictIfUserReachedCoursesLimit(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        List<CourseEntity> randomCourses = CourseServiceTestData.generateCourseEntitiesOfCount(5);

        randomCourses.forEach(courseEntity -> CourseServiceTestUtils.save(jdbcTemplate, courseEntity));
        List<UserCourseEntity> studentCourses = randomCourses.stream().map(courseEntity -> {
            UserCourseEntity userCourseEntity = new UserCourseEntity();
            userCourseEntity.setId(new UserCourseCompositeId(courseEntity, UserServiceTestData.convertToUserEntity(courseServiceTestPayload.getStudentUserCourseDTO().getUser())));
            userCourseEntity.setStatus(UserCourseStatus.STARTED);
            return userCourseEntity;
        })
                .collect(Collectors.toList());

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        studentCourses.forEach(userCourseEntity -> UserCourseServiceTestUtils.save(userCourseEntity, jdbcTemplate));

        //TEST
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .queryParam("studentId", userCourseDTO.getUserId())
                .post(COURSE_CONTROLLER_PATH.concat("/" + expandedCourseDTO.getId()).concat("/assign/student"))
                .then()
                .statusCode(HttpStatus.CONFLICT.value());

        response
                .body("message", is("Unable to assign student to course"))
                .body("reason", is("Some students are already enrolled for 5 courses, and can't take anymore"));

        //ASSERT
        List<UserCourseDTO> usersByCourseId = UserCourseServiceTestUtils.findAll(jdbcTemplate);
        Assertions.assertNotNull(usersByCourseId);
        Assertions.assertEquals(randomCourses.size(), usersByCourseId.size());
        List<String> userIds = usersByCourseId.stream()
                .map(UserCourseDTO::getUserId)
                .collect(Collectors.toList());
        Assertions.assertTrue(userIds.contains(courseServiceTestPayload.getStudentUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testFindCourseById(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getExpandedCourseDTO();
        List<LessonDTO> lessonDTOS = courseServiceTestPayload.getLessonsDTOs();
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getInstructorUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        lessonDTOS.stream()
                .map(LessonServiceTestData::convertToLessonEntityFrom)
                .forEach(lessonEntity -> LessonServiceTestUtils.save(lessonEntity, jdbcTemplate));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        //TEST
        ValidatableResponse courseResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/").concat(expandedCourseDTO.getId()))
                .then()
                .statusCode(HttpStatus.OK.value());

        courseResponse
                .body("id", is(expandedCourseDTO.getId()))
                .body("title", is(expandedCourseDTO.getTitle()))
                .body("shortDescription", is(expandedCourseDTO.getShortDescription()))
                .body("instructors", hasItem(allOf(hasEntry("id", userCourseDTO.getUserId()))));

        lessonDTOS.stream().map(LessonDTO::getId).forEach(lessonId -> {
            courseResponse.body("lessons", hasItem(allOf(hasEntry("id", lessonId))));
        });
    }

    @Test
    public void testFindCourseByIdReturnsNotFoundIfNoCourseFoundForGivenId() {
        //SETUP
        String courseId = TestProperties.randomString(12);

        //TEST
        ValidatableResponse courseResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getAdminInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/").concat(courseId))
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());

        courseResponse
                .body("message", is("Unable to retrieve course"))
                .body("reason", is(String.format("No course found for id: %s", courseId)));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testGetLessonsForCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseDTO course = courseServiceTestPayload.getLessonsDTOs().get(0).getCourse();
        LessonDTO lesson = courseServiceTestPayload.getLessonsDTOs().get(0);
        LessonServiceTestUtils.save(LessonServiceTestData.convertToLessonEntityFrom(lesson), jdbcTemplate);

        //TEST
        ValidatableResponse lessonsPerCourseResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/").concat(course.getId()).concat("/lessons"))
                .then()
                .statusCode(HttpStatus.OK.value());

        lessonsPerCourseResponse
                .body("course.id", is(course.getId()))
                .body("course.title", is(course.getTitle()))
                .body("course.shortDescription", is(course.getShortDescription()))
                .body("lessons[0].id", is(lesson.getId()))
                .body("lessons[0].title", is(lesson.getTitle()))
                .body("lessons[0].shortDescription", is(lesson.getShortDescription()))
                .body("lessons[0].content", is(lesson.getContent()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testGetLessonsForCourseReturnsEmptyArrayIfThereIsNoLessonsForGivenCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseDTO course = courseServiceTestPayload.getLessonsDTOs().get(0).getCourse();
        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(course));

        //TEST
        ValidatableResponse lessonsPerCourseResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/").concat(course.getId()).concat("/lessons"))
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());

        lessonsPerCourseResponse
                .body("message", is(String.format("Unable to retrieve lessons for course id: %s", course.getId())))
                .body("reason", is(String.format("No lessons found for course id: %s", course.getId())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testGetStudentsForCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO().getCourse();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(courseServiceTestPayload.getStudentUserCourseDTO().getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(courseServiceTestPayload.getStudentUserCourseDTO()), jdbcTemplate);

        //TEST
        ValidatableResponse usersPerCourseResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getInstructorInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/").concat(expandedCourseDTO.getId()).concat("/students"))
                .then()
                .statusCode(HttpStatus.OK.value());

        usersPerCourseResponse
                .body("course.id", is(expandedCourseDTO.getId()))
                .body("course.title", is(expandedCourseDTO.getTitle()))
                .body("course.shortDescription", is(expandedCourseDTO.getShortDescription()))
                .body("users.size", is(1))
                .body("users.id", hasItems(courseServiceTestPayload.getStudentUserCourseDTO().getUserId()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testGetStudentsForCourseReturnsNotFoundStatusIfThereAreNoUsersForGivenCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO().getCourse();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));

        //TEST
        ValidatableResponse usersPerCourseResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getInstructorInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/").concat(expandedCourseDTO.getId()).concat("/students"))
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());

        usersPerCourseResponse
                .body("message", is(String.format("Unable to retrieve students for course id: %s", expandedCourseDTO.getId())))
                .body("reason", is(String.format("No students found for course id: %s", expandedCourseDTO.getId())));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testStudentGetStudentsForCourse(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        ExpandedCourseDTO expandedCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO().getCourse();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(expandedCourseDTO));

        //TEST
        ValidatableResponse usersPerCourseResponse = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/").concat(expandedCourseDTO.getId()).concat("/students"))
                .then()
                .statusCode(HttpStatus.FORBIDDEN.value());

    }


    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testGetCoursesForUser(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        Jws<Claims> jws = Jwts.parser()
                .setSigningKey(TextCodec.BASE64.decode(TestProperties.getInternalTokenSecret()))
                .parseClaimsJws(TestProperties.getStudentInternalToken().substring(9));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();
        userCourseDTO.getUser().setId(jws.getBody().get(PeachyMindJwtClaim.SUBJECT, String.class));

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(userCourseDTO.getCourse()));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        //TEST
        ValidatableResponse coursesForUser = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/self"))
                .then()
                .statusCode(HttpStatus.OK.value());

        coursesForUser
                .body("size", is(1))
                .body("[0].id", is(userCourseDTO.getCourse().getId()))
                .body("[0].title", is(userCourseDTO.getCourse().getTitle()))
                .body("[0].shortDescription", is(userCourseDTO.getCourse().getShortDescription()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testGetCoursesForUserReturnsEmptyArrayIfThereAreNoCoursesForGivenUser(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP

        Jws<Claims> jws = Jwts.parser()
                .setSigningKey(TextCodec.BASE64.decode(TestProperties.getInternalTokenSecret()))
                .parseClaimsJws(TestProperties.getStudentInternalToken().substring(9));
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();
        userCourseDTO.getUser().setId(jws.getBody().get(PeachyMindJwtClaim.SUBJECT, String.class));

        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        //TEST
        ValidatableResponse coursesForUser = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TestProperties.getStudentInternalToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .get(COURSE_CONTROLLER_PATH.concat("/self"))
                .then()
                .statusCode(HttpStatus.OK.value());

        coursesForUser
                .body("size", is(0));
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
    }
}
