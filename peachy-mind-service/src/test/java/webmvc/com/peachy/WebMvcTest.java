package webmvc.com.peachy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.peachy.PeachyMindService;
import com.peachy.test.AbstractJpaTest;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = PeachyMindService.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("standalone-test")
public abstract class WebMvcTest extends AbstractJpaTest {

    @LocalServerPort
    protected int port;

    protected final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setPort() {
        RestAssured.port = port;
    }
}
