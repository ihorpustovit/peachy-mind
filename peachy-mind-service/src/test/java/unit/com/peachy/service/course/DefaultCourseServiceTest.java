package unit.com.peachy.service.course;

import argumentsprovider.com.peachy.sevice.course.CourseServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.course.CourseServiceTestPayload;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.converter.course.CourseConverter;
import com.peachy.converter.course.CourseFromBaseDTOConverter;
import com.peachy.converter.course.CourseToBaseDTOConverter;
import com.peachy.converter.lesson.LessonFromBaseDTOConverter;
import com.peachy.converter.lesson.LessonToBaseDTOConverter;
import com.peachy.dto.course.CourseCreationDTO;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.lesson.creation.LessonInCourseCreationDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.dto.usercourse.UsersPerCourseDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.repository.CourseRepository;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.course.DefaultCourseService;
import com.peachy.service.lesson.LessonService;
import com.peachy.service.usercourse.UserCourseService;
import com.peachy.service.userlessongrade.UserLessonGradeService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import testdata.UserServiceTestData;
import testdata.course.CourseServiceTestData;

import java.util.*;
import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class DefaultCourseServiceTest {
    @Mock
    private CourseRepository courseRepository;

    @Mock
    private LessonService defaultLessonService;

    @Mock
    private CourseConverter courseConverter;

    @Mock
    private CourseToBaseDTOConverter toBaseDTOConverter;

    @Mock
    private CourseFromBaseDTOConverter fromBaseDTOConverter;

    @Mock
    private LessonFromBaseDTOConverter fromLessonBaseDTOConverter;

    @Mock
    private LessonToBaseDTOConverter toLessonBaseDTOConverter;

    @Mock
    private UserService defaultUserService;

    @Mock
    private UserCourseService defaultUserCourseService;

    @Mock
    private UserLessonGradeService userLessonGradeService;

    private DefaultCourseService defaultCourseService;

    @Captor
    private ArgumentCaptor<List<String>> stringsCaptor;

    @Captor
    private ArgumentCaptor<CourseEntity> courseEntityCaptor;

    @Captor
    private ArgumentCaptor<List<LessonCreationDTO>> lessonCreationsCaptor;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @BeforeEach
    public void beforeEach() {
        defaultCourseService = new DefaultCourseService(
                courseRepository,
                defaultLessonService,
                courseConverter,
                toBaseDTOConverter,
                fromBaseDTOConverter,
                fromLessonBaseDTOConverter,
                toLessonBaseDTOConverter,
                defaultUserService);
        defaultCourseService.setUserLessonGradeService(userLessonGradeService);
        defaultCourseService.setUserCourseService(defaultUserCourseService);
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSave(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        when(toBaseDTOConverter.fromCreationToBase(Mockito.any(CourseCreationDTO.class)))
                .thenReturn(courseServiceTestPayload.getCourseDTO());
        when(defaultUserService.findAllByIds(Mockito.anyList()))
                .thenReturn(courseServiceTestPayload.getExpandedCourseDTO().getInstructors());
        when(fromBaseDTOConverter.fromBaseToExpanded(any(CourseDTO.class)))
                .thenReturn(courseServiceTestPayload.getExpandedCourseDTO());
        when(defaultUserService.existAllByIds(Mockito.anyList()))
                .thenReturn(true);
        when(courseConverter.toEntity(Mockito.any(CourseDTO.class)))
                .thenReturn(courseServiceTestPayload.getCourseEntity());
        when(courseRepository.save(Mockito.any(CourseEntity.class)))
                .thenReturn(courseServiceTestPayload.getCourseEntity());
        when(courseConverter.toDto(Mockito.any(CourseEntity.class)))
                .thenReturn(courseServiceTestPayload.getCourseDTO());
        when(toLessonBaseDTOConverter.fromInCourseCreationToBase(any(LessonInCourseCreationDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonsDTOs().get(0));
        when(fromLessonBaseDTOConverter.toLessonCreationFromBase(Mockito.any(LessonDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonCreationsDTOs().get(0));
        when(defaultLessonService.saveAll(Mockito.anyList()))
                .thenReturn(courseServiceTestPayload.getLessonsDTOs());
        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(Mockito.any(LessonDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));
        when(defaultUserCourseService.assignInstructorsToCourse(Mockito.anyList(), anyString()))
                .thenReturn(Collections.singletonList(courseServiceTestPayload.getInstructorUserCourseDTO()));

        //TEST
        defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO());

        //VERIFY
        verify(toBaseDTOConverter, atMostOnce())
                .fromCreationToBase(Mockito.any(CourseCreationDTO.class));
        verify(defaultUserService, atMostOnce())
                .findAllByIds(Mockito.anyList());
        verify(defaultUserService).findAllByIds(stringsCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .existAllByIds(Mockito.anyList());
        verify(courseConverter, atMostOnce())
                .toEntity(Mockito.any(CourseDTO.class));
        verify(courseRepository, atMostOnce())
                .save(Mockito.any(CourseEntity.class));
        verify(courseRepository).save(courseEntityCaptor.capture());
        verify(courseConverter, atMostOnce())
                .toDto(Mockito.any(CourseEntity.class));
        verify(fromLessonBaseDTOConverter, Mockito.times(courseServiceTestPayload.getExpandedCourseDTO().getLessons().size()))
                .toLessonCreationFromBase(Mockito.any(LessonDTO.class));
        verify(fromLessonBaseDTOConverter, Mockito.times(courseServiceTestPayload.getExpandedCourseDTO().getLessons().size()))
                .toLessonInCourseFromBase(Mockito.any(LessonDTO.class));
        verify(defaultLessonService, atMostOnce())
                .saveAll(Mockito.anyList());
        verify(defaultLessonService).saveAll(lessonCreationsCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .assignInstructorsToCourse(Mockito.anyList(), anyString());
        verify(defaultUserCourseService)
                .assignInstructorsToCourse(stringsCaptor.capture(), stringCaptor.capture());
        verify(toLessonBaseDTOConverter,times(courseServiceTestPayload.getCourseCreationDTO().getLessons().size()))
                .fromInCourseCreationToBase(any(LessonInCourseCreationDTO.class));
        verify(fromBaseDTOConverter, atMostOnce())
                .fromBaseToExpanded(any(CourseDTO.class));

        //ASSERT
        Assertions.assertEquals(courseServiceTestPayload.getCourseCreationDTO().getInstructorIds(), stringsCaptor.getAllValues().get(0));
        Assertions.assertEquals(courseServiceTestPayload.getCourseEntity(), courseEntityCaptor.getValue());
        Assertions.assertTrue(lessonCreationsCaptor.getValue().stream().allMatch(lessonCreationDTO ->
                Objects.equals(lessonCreationDTO, courseServiceTestPayload.getLessonCreationsDTOs().get(0))));
        Assertions.assertEquals(courseServiceTestPayload.getCourseCreationDTO().getInstructorIds(), stringsCaptor.getAllValues().get(1));
        Assertions.assertEquals(courseServiceTestPayload.getCourseEntity().getId(), stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfCourseHasEmptyTitle(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        courseServiceTestPayload.getExpandedCourseDTO().setTitle(StringUtils.EMPTY);


        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfCourseHasTitleTooLong(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        courseServiceTestPayload.getExpandedCourseDTO().setTitle(RandomStringUtils.random(81));


        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfCourseHasShortDescriptionTooLong(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        courseServiceTestPayload.getExpandedCourseDTO().setShortDescription(RandomStringUtils.random(501));


        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfCourseHasNoLessons(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        courseServiceTestPayload.getExpandedCourseDTO().setLessons(null);


        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO()));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfCourseIsLowOnLessons(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        courseServiceTestPayload.getExpandedCourseDTO().setLessons(Collections.singletonList(courseServiceTestPayload.getExpandedCourseDTO().getLessons().get(0)));
        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO()));

    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfNonExistentInstructorsForCourseSpecified(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        when(defaultUserService.existAllByIds(Mockito.anyList()))
                .thenReturn(false);

        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO()));


        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existAllByIds(Mockito.anyList());
        verify(defaultUserService)
                .existAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertEquals(courseServiceTestPayload.getCourseCreationDTO().getInstructorIds(), stringsCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfNoInstructorsSpecified(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        courseServiceTestPayload.getExpandedCourseDTO().setInstructors(Collections.emptyList());

        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO()));


        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existAllByIds(Mockito.anyList());
        verify(defaultUserService)
                .existAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertEquals(courseServiceTestPayload.getCourseCreationDTO().getInstructorIds(), stringsCaptor.getValue());
    }

//    @ParameterizedTest
//    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
//    public void testFindExpandedById(CourseServiceTestPayload courseServiceTestPayload) {
//        List<UsersPerCourseDTO> instructorsPerCourse =
//                Collections.singletonList(new UsersPerCourseDTO(courseServiceTestPayload.getCourseDTO(), Collections.singletonList(courseServiceTestPayload.getInstructorUserCourseDTO().getUser())));
//        List<LessonsPerCourseDTO> lessonsPerCourseDTOS =
//                Collections.singletonList(new LessonsPerCourseDTO(courseServiceTestPayload.getCourseDTO(), courseServiceTestPayload.getLessonInCourseDTOs()));
//
//        //SETUP
//        String courseId = courseServiceTestPayload.getCourseEntity().getId();
//        when(courseRepository.findAllByIdAsync(anyList()))
//                .thenReturn(CompletableFuture.completedFuture(Collections.singletonList(courseServiceTestPayload.getCourseEntity())));
//        when(courseConverter.toDto(anyList()))
//                .thenReturn(Collections.singletonList(courseServiceTestPayload.getCourseDTO()));
//        when(defaultLessonService.findAllByCourseIds(anyList()))
//                .thenReturn(lessonsPerCourseDTOS);
//        when(defaultUserCourseService.findAllInstructorsByCourseIds(anyList()))
//                .thenReturn(instructorsPerCourse);
//        when(fromBaseDTOConverter.fromBaseToExpanded(any(CourseDTO.class)))
//                .thenReturn(courseServiceTestPayload.getExpandedCourseDTO());
//        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
//                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));
//
//        //TEST
//        Optional<ExpandedCourseDTO> courseDTO = defaultCourseService.findExpandedById(courseId);
//
//        //VERIFY
//        verify(courseRepository, atMostOnce())
//                .findAllByIdAsync(anyList());
//        verify(courseRepository)
//                .findAllByIdAsync(stringsCaptor.capture());
//
//        verify(courseConverter, atMostOnce())
//                .toDto(any(CourseEntity.class));
//
//        verify(defaultLessonService, atMostOnce())
//                .findAllByCourseIds(anyList());
//        verify(defaultLessonService)
//                .findAllByCourseIds(stringsCaptor.capture());
//
//        verify(defaultUserCourseService, atMostOnce())
//                .findAllInstructorsByCourseIds(anyList());
//        verify(defaultUserCourseService)
//                .findAllInstructorsByCourseIds(stringsCaptor.capture());
//
//        verify(fromLessonBaseDTOConverter, times(courseServiceTestPayload.getLessonsDTOs().size()))
//                .toLessonInCourseFromBase(any(LessonDTO.class));
//
//        //ASSERT
//        Assertions.assertTrue(courseDTO.isPresent());
//        Assertions.assertTrue(Objects.nonNull(courseDTO.get().getInstructors()) && !courseDTO.get().getInstructors().isEmpty());
//        Assertions.assertTrue(Objects.nonNull(courseDTO.get().getLessons()) && !courseDTO.get().getLessons().isEmpty());
//        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(capturedCourseIds -> capturedCourseIds.contains(courseId)));
//    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testFindByIdReturnsEmptyOptionalIfThereIsNoCourseForGivenId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        List<UsersPerCourseDTO> instructorsPerCourse =
                Collections.singletonList(new UsersPerCourseDTO(courseServiceTestPayload.getCourseDTO(), Collections.singletonList(courseServiceTestPayload.getInstructorUserCourseDTO().getUser())));
        List<LessonsPerCourseDTO> lessonsPerCourseDTOS =
                Collections.singletonList(new LessonsPerCourseDTO(courseServiceTestPayload.getCourseDTO(), courseServiceTestPayload.getLessonInCourseDTOs()));


        String courseId = courseServiceTestPayload.getCourseEntity().getId();
        when(courseRepository.findAllByIdAsync(anyList()))
                .thenReturn(CompletableFuture.completedFuture(Collections.emptyList()));
        when(courseConverter.toDto(anyList()))
                .thenReturn(Collections.emptyList());
        when(defaultLessonService.findAllByCourseIds(anyList()))
                .thenReturn(lessonsPerCourseDTOS);
        when(defaultUserCourseService.findAllInstructorsByCourseIds(anyList()))
                .thenReturn(instructorsPerCourse);

        //TEST
        Optional<ExpandedCourseDTO> courseDTO = defaultCourseService.findExpandedById(courseId);

        //VERIFY
        verify(courseRepository, atMostOnce())
                .findAllByIdAsync(anyList());
        verify(courseRepository)
                .findAllByIdAsync(stringsCaptor.capture());

        verify(courseConverter, atMostOnce())
                .toDto(any(CourseEntity.class));

        verify(defaultLessonService, atMostOnce())
                .findAllByCourseIds(anyList());
        verify(defaultLessonService)
                .findAllByCourseIds(stringsCaptor.capture());

        verify(defaultUserCourseService, atMostOnce())
                .findAllInstructorsByCourseIds(anyList());
        verify(defaultUserCourseService)
                .findAllInstructorsByCourseIds(stringsCaptor.capture());
        verifyNoInteractions(fromBaseDTOConverter);

        //ASSERT
        Assertions.assertFalse(courseDTO.isPresent());
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(capturedCourseIds -> capturedCourseIds.contains(courseId)));
    }

//    @ParameterizedTest
//    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
//    public void testFindAllByIds(CourseServiceTestPayload courseServiceTestPayload) {
//        //SETUP
//        List<String> courseIds = Collections.singletonList(courseServiceTestPayload.getCourseEntity().getId());
//        List<LessonsPerCourseDTO> lessonsPerCourseDTOS =
//                Collections.singletonList(new LessonsPerCourseDTO(courseServiceTestPayload.getCourseDTO(), courseServiceTestPayload.getLessonInCourseDTOs()));
//        List<UsersPerCourseDTO> instructorsPerCourse =
//                Collections.singletonList(new UsersPerCourseDTO(courseServiceTestPayload.getCourseDTO(), Collections.singletonList(courseServiceTestPayload.getInstructorUserCourseDTO().getUser())));
//
//
//        when(courseRepository.findAllByIdAsync(anyList()))
//                .thenReturn(CompletableFuture.completedFuture(Collections.singletonList(courseServiceTestPayload.getCourseEntity())));
//        when(courseConverter.toDto(anyList()))
//                .thenReturn(Collections.singletonList(courseServiceTestPayload.getCourseDTO()));
//        when(fromBaseDTOConverter.fromBaseToExpanded(any(CourseDTO.class)))
//                .thenReturn(courseServiceTestPayload.getExpandedCourseDTO());
//        when(defaultLessonService.findAllByCourseIds(anyList()))
//                .thenReturn(lessonsPerCourseDTOS);
//        when(defaultUserCourseService.findAllInstructorsByCourseIds(anyList()))
//                .thenReturn(instructorsPerCourse);
//        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
//                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));
//
//        //TEST
//        List<ExpandedCourseDTO> foundExpandedCourseDTOS = defaultCourseService.findExpandedAllByIds(courseIds);
//
//        //VERIFY
//        verify(courseRepository, atMostOnce())
//                .findAllByIdAsync(anyList());
//        verify(courseRepository)
//                .findAllByIdAsync(stringsCaptor.capture());
//        verify(courseConverter, atMostOnce())
//                .toDto(any(CourseEntity.class));
//        verify(defaultLessonService, atMostOnce())
//                .findAllByCourseIds(anyList());
//        verify(defaultLessonService)
//                .findAllByCourseIds(stringsCaptor.capture());
//        verify(defaultUserCourseService, atMostOnce())
//                .findAllInstructorsByCourseIds(anyList());
//        verify(defaultUserCourseService)
//                .findAllInstructorsByCourseIds(stringsCaptor.capture());
//        verify(fromLessonBaseDTOConverter, times(courseServiceTestPayload.getLessonsDTOs().size()))
//                .toLessonInCourseFromBase(any(LessonDTO.class));
//
//        //ASSERT
//        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(stringsList -> stringsList.containsAll(courseIds)));
//        Assertions.assertEquals(1, foundExpandedCourseDTOS.size());
//        Assertions.assertEquals(courseIds.get(0), courseServiceTestPayload.getExpandedCourseDTO().getId());
//    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testFindAllByIdsReturnsEmptyListIfThereIsNoCoursesForGivenIds(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        List<String> courseIds = Collections.singletonList(courseServiceTestPayload.getCourseEntity().getId());
        List<LessonsPerCourseDTO> lessonsPerCourseDTOS =
                Collections.singletonList(new LessonsPerCourseDTO(courseServiceTestPayload.getCourseDTO(), courseServiceTestPayload.getLessonInCourseDTOs()));
        List<UsersPerCourseDTO> instructorsPerCourse =
                Collections.singletonList(new UsersPerCourseDTO(courseServiceTestPayload.getCourseDTO(), Collections.singletonList(courseServiceTestPayload.getInstructorUserCourseDTO().getUser())));

        when(courseRepository.findAllByIdAsync(anyList()))
                .thenReturn(CompletableFuture.completedFuture(Collections.emptyList()));
        when(courseConverter.toDto(any(CourseEntity.class)))
                .thenReturn(courseServiceTestPayload.getCourseDTO());
        when(fromBaseDTOConverter.fromBaseToExpanded(any(CourseDTO.class)))
                .thenReturn(courseServiceTestPayload.getExpandedCourseDTO());
        when(defaultLessonService.findAllByCourseIds(anyList()))
                .thenReturn(lessonsPerCourseDTOS);
        when(defaultUserCourseService.findAllInstructorsByCourseIds(anyList()))
                .thenReturn(instructorsPerCourse);
        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));

        //TEST
        List<ExpandedCourseDTO> foundExpandedCourseDTOS = defaultCourseService.findExpandedAllByIds(courseIds);

        //VERIFY
        verify(courseRepository, atMostOnce())
                .findAllByIdAsync(anyList());
        verify(courseRepository)
                .findAllByIdAsync(stringsCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .findAllByCourseIds(anyList());
        verify(defaultLessonService)
                .findAllByCourseIds(stringsCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .findAllInstructorsByCourseIds(anyList());
        verify(defaultUserCourseService)
                .findAllInstructorsByCourseIds(stringsCaptor.capture());

        verifyNoInteractions(fromLessonBaseDTOConverter);

        //ASSERT
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(stringsList -> stringsList.containsAll(courseIds)));
        Assertions.assertTrue(foundExpandedCourseDTOS.isEmpty());
    }

//    @ParameterizedTest
//    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
//    public void testFindAllByIdsReturnsEmptyLessonsAndInstructorsIfThereAreNoOfThem(CourseServiceTestPayload courseServiceTestPayload) {
//        //SETUP
//        List<String> courseIds = Collections.singletonList(courseServiceTestPayload.getCourseEntity().getId());
//
//        when(courseRepository.findAllByIdAsync(anyList()))
//                .thenReturn(CompletableFuture.completedFuture(Collections.singletonList(courseServiceTestPayload.getCourseEntity())));
//        when(courseConverter.toDto(anyList()))
//                .thenReturn(Collections.singletonList(courseServiceTestPayload.getCourseDTO()));
//        when(fromBaseDTOConverter.fromBaseToExpanded(any(CourseDTO.class)))
//                .thenReturn(courseServiceTestPayload.getExpandedCourseDTO());
//        when(defaultLessonService.findAllByCourseIds(anyList()))
//                .thenReturn(Collections.emptyList());
//        when(defaultUserCourseService.findAllInstructorsByCourseIds(anyList()))
//                .thenReturn(Collections.emptyList());
//        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
//                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));
//
//        //TEST
//        List<ExpandedCourseDTO> foundExpandedCourseDTOS = defaultCourseService.findExpandedAllByIds(courseIds);
//
//        //VERIFY
//        verify(courseRepository, atMostOnce())
//                .findAllByIdAsync(anyList());
//        verify(courseRepository)
//                .findAllByIdAsync(stringsCaptor.capture());
//        verify(courseConverter, atMostOnce())
//                .toDto(any(CourseEntity.class));
//        verify(defaultLessonService, atMostOnce())
//                .findAllByCourseIds(anyList());
//        verify(defaultLessonService)
//                .findAllByCourseIds(stringsCaptor.capture());
//        verify(defaultUserCourseService, atMostOnce())
//                .findAllInstructorsByCourseIds(anyList());
//        verify(defaultUserCourseService)
//                .findAllInstructorsByCourseIds(stringsCaptor.capture());
//
//        verifyNoInteractions(fromLessonBaseDTOConverter);
//        //ASSERT
//        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(stringsList -> stringsList.containsAll(courseIds)));
//        Assertions.assertEquals(1, foundExpandedCourseDTOS.size());
//        Assertions.assertEquals(courseIds.get(0), courseServiceTestPayload.getExpandedCourseDTO().getId());
//        Assertions.assertTrue(foundExpandedCourseDTOS.get(0).getLessons().isEmpty());
//        Assertions.assertTrue(foundExpandedCourseDTOS.get(0).getInstructors().isEmpty());
//    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testExistsByIdReturnsTrueIfThereIsACourseForGivenId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        String courseId = courseServiceTestPayload.getCourseEntity().getId();
        when(courseRepository.countByIdIn(anyList()))
                .thenReturn(1L);

        //TEST AND ASSERT
        Assertions.assertTrue(defaultCourseService.existsById(courseId));

        //VERIFY
        verify(courseRepository, atMostOnce())
                .countByIdIn(anyList());
        verify(courseRepository)
                .countByIdIn(stringsCaptor.capture());

        Assertions.assertEquals(1, stringsCaptor.getValue().size());
        Assertions.assertEquals(courseId, stringsCaptor.getValue().get(0));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testExistsByIdReturnsFalseIfThereIsNoCourseForGivenId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        String courseId = courseServiceTestPayload.getCourseEntity().getId();
        when(courseRepository.countByIdIn(anyList()))
                .thenReturn(0L);

        //TEST AND ASSERT
        Assertions.assertFalse(defaultCourseService.existsById(courseId));

        //VERIFY
        verify(courseRepository, atMostOnce())
                .countByIdIn(anyList());
        verify(courseRepository)
                .countByIdIn(stringsCaptor.capture());

        Assertions.assertEquals(1, stringsCaptor.getValue().size());
        Assertions.assertEquals(courseId, stringsCaptor.getValue().get(0));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testExistAllByIdsReturnsTrueIfThereIsACourseForGivenId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        String courseId = courseServiceTestPayload.getCourseEntity().getId();
        when(courseRepository.countByIdIn(anyList()))
                .thenReturn(1L);

        //TEST AND ASSERT
        Assertions.assertTrue(defaultCourseService.existAllByIds(Collections.singletonList(courseId)));

        //VERIFY
        verify(courseRepository, atMostOnce())
                .countByIdIn(anyList());
        verify(courseRepository)
                .countByIdIn(stringsCaptor.capture());

        Assertions.assertEquals(1, stringsCaptor.getValue().size());
        Assertions.assertEquals(courseId, stringsCaptor.getValue().get(0));
    }


    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testExistAllByIdsReturnsFalseIfThereIsNoCourseForGivenId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        String courseId = courseServiceTestPayload.getCourseEntity().getId();
        when(courseRepository.countByIdIn(anyList()))
                .thenReturn(0L);

        //TEST AND ASSERT
        Assertions.assertFalse(defaultCourseService.existAllByIds(Collections.singletonList(courseId)));

        //VERIFY
        verify(courseRepository, atMostOnce())
                .countByIdIn(anyList());
        verify(courseRepository)
                .countByIdIn(stringsCaptor.capture());

        Assertions.assertEquals(1, stringsCaptor.getValue().size());
        Assertions.assertEquals(courseId, stringsCaptor.getValue().get(0));
    }

    @Test
    public void testExistAllByIdsThrowsBadArgumentExceptionIfInappropriateIdIsPassed() {
        //TEST AND ASSERT
        Assertions.assertThrows(BadArgumentException.class, () -> defaultCourseService.existAllByIds(null));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultCourseService.existAllByIds(Collections.singletonList(StringUtils.EMPTY)));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultCourseService.existAllByIds(Collections.singletonList(StringUtils.SPACE)));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void findAllByUserId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        when(courseRepository.findAllByUserId(anyString()))
                .thenReturn(Collections.singletonList(CourseServiceTestData.convertToCourseEntityFrom(userCourseDTO.getCourse())));
        when(courseConverter.toDto(anyList()))
                .thenReturn(Collections.singletonList(CourseServiceTestData.convertToCourseDTOFrom(userCourseDTO.getCourse())));

        //TEST
        List<CourseDTO> courseDTOS = defaultCourseService.findAllByUserId(userCourseDTO.getUserId());

        //VERIFY
        verify(courseRepository,atMostOnce())
                .findAllByUserId(anyString());
        verify(courseRepository)
                .findAllByUserId(stringCaptor.capture());
        verify(courseConverter, atMostOnce())
                .toDto(anyList());

        //ASSERT
        Assertions.assertNotNull(courseDTOS);
        Assertions.assertEquals(1, courseDTOS.size());
        Assertions.assertEquals(userCourseDTO.getCourse().getId(), courseDTOS.get(0).getId());
        Assertions.assertEquals(userCourseDTO.getUser().getId(), stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void findAllByUserIdReturnsEmptyListIfThereIsNoCoursesForGiveUser(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        when(courseRepository.findAllByUserId(anyString()))
                .thenReturn(Collections.emptyList());
        when(courseConverter.toDto(anyList()))
                .thenReturn(Collections.emptyList());

        //TEST
        Assertions.assertTrue(defaultCourseService.findAllByUserId(userCourseDTO.getUserId()).isEmpty());

        //VERIFY
        verify(courseRepository,atMostOnce())
                .findAllByUserId(anyString());
        verify(courseRepository)
                .findAllByUserId(stringCaptor.capture());
        verify(courseConverter, atMostOnce())
                .toDto(anyList());

        //ASSERT
        Assertions.assertEquals(userCourseDTO.getUser().getId(), stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void findAllByUserIdThrowsBadArgumentExceptionIfGivenUserIdIsEmpty(CourseServiceTestPayload courseServiceTestPayload) {
        //ASSERT
        Assertions.assertThrows(BadArgumentException.class, () -> defaultCourseService.findAllByUserId(null));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultCourseService.findAllByUserId(StringUtils.EMPTY));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultCourseService.findAllByUserId(StringUtils.SPACE));
    }


}
