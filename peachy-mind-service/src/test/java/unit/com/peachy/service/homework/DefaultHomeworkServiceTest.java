package unit.com.peachy.service.homework;

import argumentsprovider.com.peachy.sevice.homework.HomeworkServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.homework.HomeworkServiceTestPayload;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.peachy.commons.exception.ConflictException;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.converter.homework.HomeworkConverter;
import com.peachy.converter.homework.HomeworkFromBaseDTOConverter;
import com.peachy.converter.homework.HomeworkToBaseDTOConverter;
import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkCreationDTO;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.homework.HomeworkProjection;
import com.peachy.entity.homework.HomeworkCompositeId;
import com.peachy.entity.homework.HomeworkEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.repository.HomeworkRepository;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.aws.s3.S3Service;
import com.peachy.service.homework.DefaultHomeworkService;
import com.peachy.service.homework.HomeworkService;
import com.peachy.service.lesson.LessonService;
import com.peachy.service.usercourse.UserCourseService;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class DefaultHomeworkServiceTest {
    @Mock
    private S3Service s3Service;

    @Mock
    private LessonService defaultLessonService;

    @Mock
    private UserService defaultUserService;

    @Mock
    private HomeworkConverter homeworkConverter;

    @Mock
    private HomeworkToBaseDTOConverter toBaseDTOConverter;

    @Mock
    private HomeworkFromBaseDTOConverter fromBaseDTOConverter;

    @Mock
    private HomeworkRepository homeworkRepository;

    @Mock
    private UserCourseService defaultUserCourseService;

    private HomeworkService defaultHomeworkService;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Captor
    private ArgumentCaptor<HomeworkCompositeId> homeworkCompositeIdCaptor;

    @BeforeEach
    public void beforeEach() {
        defaultHomeworkService = new DefaultHomeworkService(
                s3Service,
                defaultLessonService,
                defaultUserService,
                defaultUserCourseService,
                homeworkConverter,
                toBaseDTOConverter,
                fromBaseDTOConverter,
                homeworkRepository);
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSave(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        when(defaultUserService.existsById(anyString()))
                .thenReturn(true);
        when(defaultLessonService.existsById(anyString()))
                .thenReturn(true);
        when(homeworkRepository.existsById(any(HomeworkCompositeId.class)))
                .thenReturn(false);
        when(toBaseDTOConverter.fromCreationToBase(any(HomeworkCreationDTO.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkDTO());
        when(defaultLessonService.findById(anyString()))
                .thenReturn(Optional.of(homeworkServiceTestPayload.getLessonDTO()));
        when(homeworkConverter.toEntity(any(HomeworkDTO.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkEntity());
        when(homeworkRepository.save(any(HomeworkEntity.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkEntity());
        when(homeworkConverter.toDto(any(HomeworkEntity.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkDTO());
        when(defaultUserCourseService.existsByLessonIdAndUserId(anyString(), anyString()))
                .thenReturn(true);
        when(defaultUserCourseService.findStatusByCourseIdAndUserId(anyString(),anyString()))
                .thenReturn(Optional.of(UserCourseStatus.STARTED));
        doNothing().when(defaultUserCourseService).updateStatusByCourseIdAndUserId(anyString(),anyString(),any(UserCourseStatus.class));
        doNothing().when(s3Service).upload(anyString(), anyString(), any(MultipartFile.class));

        //TEST
        HomeworkDTO savedHomework = defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation());

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existsById(anyString());
        verify(defaultUserService)
                .existsById(stringCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .existsById(anyString());
        verify(defaultLessonService)
                .existsById(stringCaptor.capture());
        verify(homeworkRepository, atMostOnce())
                .existsById(any(HomeworkCompositeId.class));
        verify(homeworkRepository)
                .existsById(homeworkCompositeIdCaptor.capture());
        verify(toBaseDTOConverter, atMostOnce())
                .fromCreationToBase(any(HomeworkCreationDTO.class));
        verify(defaultLessonService, atMostOnce())
                .findById(anyString());
        verify(defaultLessonService)
                .findById(stringCaptor.capture());
        verify(homeworkConverter, atMostOnce())
                .toEntity(any(HomeworkDTO.class));
        verify(homeworkRepository, atMostOnce())
                .save(any(HomeworkEntity.class));
        verify(homeworkConverter, atMostOnce())
                .toDto(any(HomeworkEntity.class));
        verify(s3Service, atMostOnce())
                .upload(anyString(), anyString(), any(MultipartFile.class));
        verify(defaultUserCourseService, atMostOnce())
                .existsByLessonIdAndUserId(anyString(),anyString());
        verify(defaultUserCourseService)
                .existsByLessonIdAndUserId(stringCaptor.capture(),stringCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .findStatusByCourseIdAndUserId(anyString(), anyString());
        verify(defaultUserCourseService)
                .findStatusByCourseIdAndUserId(stringCaptor.capture(), stringCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .updateStatusByCourseIdAndUserId(anyString(), anyString(), any(UserCourseStatus.class));
        verify(defaultUserCourseService)
                .updateStatusByCourseIdAndUserId(stringCaptor.capture(), stringCaptor.capture(), any(UserCourseStatus.class));

        //ASSERT
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(1));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), homeworkCompositeIdCaptor.getValue().getUser().getId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), homeworkCompositeIdCaptor.getValue().getLesson().getId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(2));
        Assertions.assertNotNull(savedHomework);
        Assertions.assertNotNull(savedHomework.getLessonId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), savedHomework.getLessonId());
        Assertions.assertNotNull(savedHomework.getUserId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), savedHomework.getUserId());
        Assertions.assertNotNull(savedHomework.getFileName());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getHomeworkFile().getName(), savedHomework.getFileName());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(3));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(4));
        Assertions.assertEquals(savedHomework.getLesson().getCourse().getId(), stringCaptor.getAllValues().get(5));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(6));
        Assertions.assertEquals(savedHomework.getLesson().getCourse().getId(), stringCaptor.getAllValues().get(7));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(8));
    }

    @Test
    public void testSaveThrowsBadArgumentExceptionIfHomeworkCreationDTOIsEmpty() {
        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> defaultHomeworkService.save(null));
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsBadArgumentExceptionIfHomeworkFileIsEmpty(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        homeworkServiceTestPayload.getHomeworkCreation().setHomeworkFile(null);
        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfUserIsEmpty(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        homeworkServiceTestPayload.getHomeworkCreation().setUser(null);
        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfLessonIdIsBlank(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        homeworkServiceTestPayload.getHomeworkCreation().setLessonId(null);
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));

        homeworkServiceTestPayload.getHomeworkCreation().setLessonId(StringUtils.EMPTY);
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));

        homeworkServiceTestPayload.getHomeworkCreation().setLessonId(StringUtils.SPACE);
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfUserDoesntExistForGivenId(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        when(defaultUserService.existsById(anyString()))
                .thenReturn(true);

        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existsById(anyString());
        verify(defaultUserService)
                .existsById(stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsBadRequestExceptionIfLessonDoesntExistForGivenId(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        when(defaultUserService.existsById(anyString()))
                .thenReturn(true);
        when(defaultLessonService.existsById(anyString()))
                .thenReturn(false);

        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existsById(anyString());
        verify(defaultUserService)
                .existsById(stringCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .existsById(anyString());
        verify(defaultLessonService)
                .existsById(stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(1));
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsConflictExceptionIfHomeworkAlreadyExists(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        when(defaultUserService.existsById(anyString()))
                .thenReturn(true);
        when(defaultLessonService.existsById(anyString()))
                .thenReturn(true);
        when(homeworkRepository.existsById(any(HomeworkCompositeId.class)))
                .thenReturn(true);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existsById(anyString());
        verify(defaultUserService)
                .existsById(stringCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .existsById(anyString());
        verify(defaultLessonService)
                .existsById(stringCaptor.capture());
        verify(homeworkRepository, atMostOnce())
                .existsById(any(HomeworkCompositeId.class));
        verify(homeworkRepository)
                .existsById(homeworkCompositeIdCaptor.capture());

        //ASSERT
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(1));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), homeworkCompositeIdCaptor.getValue().getUser().getId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), homeworkCompositeIdCaptor.getValue().getLesson().getId());
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsConflictExceptionIfUserIsNotAssignedToACourse(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        when(defaultUserService.existsById(anyString()))
                .thenReturn(true);
        when(defaultLessonService.existsById(anyString()))
                .thenReturn(true);
        when(homeworkRepository.existsById(any(HomeworkCompositeId.class)))
                .thenReturn(false);
        when(defaultUserCourseService.existsByLessonIdAndUserId(anyString(), anyString()))
                .thenReturn(false);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existsById(anyString());
        verify(defaultUserService)
                .existsById(stringCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .existsById(anyString());
        verify(defaultLessonService)
                .existsById(stringCaptor.capture());
        verify(homeworkRepository, atMostOnce())
                .existsById(any(HomeworkCompositeId.class));
        verify(homeworkRepository)
                .existsById(homeworkCompositeIdCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .existsByLessonIdAndUserId(anyString(),anyString());
        verify(defaultUserCourseService)
                .existsByLessonIdAndUserId(stringCaptor.capture(),stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(1));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), homeworkCompositeIdCaptor.getValue().getUser().getId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), homeworkCompositeIdCaptor.getValue().getLesson().getId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(2));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(3));
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsNotFoundExceptionIfFailedToDetectUserCourseStatus(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        when(defaultUserService.existsById(anyString()))
                .thenReturn(true);
        when(defaultLessonService.existsById(anyString()))
                .thenReturn(true);
        when(homeworkRepository.existsById(any(HomeworkCompositeId.class)))
                .thenReturn(false);
        when(toBaseDTOConverter.fromCreationToBase(any(HomeworkCreationDTO.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkDTO());
        when(defaultLessonService.findById(anyString()))
                .thenReturn(Optional.of(homeworkServiceTestPayload.getLessonDTO()));
        when(homeworkConverter.toEntity(any(HomeworkDTO.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkEntity());
        when(homeworkRepository.save(any(HomeworkEntity.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkEntity());
        when(homeworkConverter.toDto(any(HomeworkEntity.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkDTO());
        when(defaultUserCourseService.existsByLessonIdAndUserId(anyString(), anyString()))
                .thenReturn(true);
        when(defaultUserCourseService.findStatusByCourseIdAndUserId(anyString(),anyString()))
                .thenReturn(Optional.empty());

        //TEST
        Assertions.assertThrows(NotFoundException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existsById(anyString());
        verify(defaultUserService)
                .existsById(stringCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .existsById(anyString());
        verify(defaultLessonService)
                .existsById(stringCaptor.capture());
        verify(homeworkRepository, atMostOnce())
                .existsById(any(HomeworkCompositeId.class));
        verify(homeworkRepository)
                .existsById(homeworkCompositeIdCaptor.capture());
        verify(toBaseDTOConverter, atMostOnce())
                .fromCreationToBase(any(HomeworkCreationDTO.class));
        verify(defaultLessonService, atMostOnce())
                .findById(anyString());
        verify(defaultLessonService)
                .findById(stringCaptor.capture());
        verify(homeworkConverter, atMostOnce())
                .toEntity(any(HomeworkDTO.class));
        verify(homeworkRepository, atMostOnce())
                .save(any(HomeworkEntity.class));
        verify(homeworkConverter, atMostOnce())
                .toDto(any(HomeworkEntity.class));
        verify(s3Service, atMostOnce())
                .upload(anyString(), anyString(), any(MultipartFile.class));
        verify(defaultUserCourseService, atMostOnce())
                .existsByLessonIdAndUserId(anyString(),anyString());
        verify(defaultUserCourseService)
                .existsByLessonIdAndUserId(stringCaptor.capture(),stringCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .findStatusByCourseIdAndUserId(anyString(), anyString());
        verify(defaultUserCourseService)
                .findStatusByCourseIdAndUserId(stringCaptor.capture(), stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(1));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), homeworkCompositeIdCaptor.getValue().getUser().getId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), homeworkCompositeIdCaptor.getValue().getLesson().getId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(2));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(3));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(4));
        Assertions.assertEquals(homeworkServiceTestPayload.getLessonDTO().getCourse().getId(), stringCaptor.getAllValues().get(5));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(6));
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testDownload(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        String lessonId = homeworkServiceTestPayload.getLessonDTO().getId();
        String userId = homeworkServiceTestPayload.getUserDTO().getId();
        ByteArrayOutputStream byteArrayOutputStream =
                new ByteArrayOutputStream();
        byteArrayOutputStream.write(homeworkServiceTestPayload.getHomeworkCreation().getHomeworkFile().getBytes());

        when(homeworkRepository.findByLessonIdAndUserId(anyString(), anyString()))
                .thenReturn(Optional.of(homeworkServiceTestPayload.getHomeworkDTO()));
        when(s3Service.download(any(GetObjectRequest.class)))
                .thenReturn(Optional.of(byteArrayOutputStream));
        when(toBaseDTOConverter.fromProjectionToBase(any(HomeworkProjection.class)))
                .thenReturn(homeworkServiceTestPayload.getHomeworkDTO());
        when(fromBaseDTOConverter.fromBaseToExpanded(any(HomeworkDTO.class)))
                .thenReturn(homeworkServiceTestPayload.getExpandedHomeworkDTO());

        //TEST
        ExpandedHomeworkDTO expandedHomeworkDTO = defaultHomeworkService.download(lessonId, userId);

        //VERIFY
        verify(homeworkRepository, atMostOnce())
                .findByLessonIdAndUserId(anyString(), anyString());
        verify(homeworkRepository)
                .findByLessonIdAndUserId(stringCaptor.capture(), stringCaptor.capture());
        verify(s3Service, atMostOnce())
                .download(any(GetObjectRequest.class));
        verify(toBaseDTOConverter, atMostOnce())
                .fromProjectionToBase(any(HomeworkProjection.class));
        verify(fromBaseDTOConverter, atMostOnce())
                .fromBaseToExpanded(any(HomeworkDTO.class));

        //ASSERT
        Assertions.assertEquals(lessonId, stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(userId, stringCaptor.getAllValues().get(1));
        Assertions.assertNotNull(expandedHomeworkDTO);
        Assertions.assertNotNull(expandedHomeworkDTO.getUser());
        Assertions.assertEquals(userId, expandedHomeworkDTO.getUserId());
        Assertions.assertNotNull(expandedHomeworkDTO.getLessonId());
        Assertions.assertEquals(lessonId, expandedHomeworkDTO.getLessonId());
        Assertions.assertNotNull(expandedHomeworkDTO.getHomeworkFileStream());
        Assertions.assertNotNull(expandedHomeworkDTO.getFileName());
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testDownloadThrowsBadRequestExceptionIfLessonIdIsEmpty(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.download(null, homeworkServiceTestPayload.getUserDTO().getId()));
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.download(StringUtils.EMPTY, homeworkServiceTestPayload.getUserDTO().getId()));
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.download(StringUtils.SPACE, homeworkServiceTestPayload.getUserDTO().getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testDownloadThrowsBadRequestExceptionIfUserIdIsEmpty(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.download(homeworkServiceTestPayload.getLessonDTO().getId(), null));
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.download(homeworkServiceTestPayload.getLessonDTO().getId(), StringUtils.EMPTY));
        Assertions.assertThrows(BadRequestException.class, () -> defaultHomeworkService.download(homeworkServiceTestPayload.getLessonDTO().getId(), StringUtils.SPACE));
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testDownloadThrowsNotFoundExceptionIfThereIsNoHomeworkForGivenLessonIdAndUserId(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        String lessonId = homeworkServiceTestPayload.getLessonDTO().getId();
        String userId = homeworkServiceTestPayload.getUserDTO().getId();

        when(homeworkRepository.findByLessonIdAndUserId(anyString(), anyString()))
                .thenReturn(Optional.empty());

        //TEST
        Assertions.assertThrows(NotFoundException.class, () -> defaultHomeworkService.download(lessonId, userId));

        //VERIFY
        verify(homeworkRepository, atMostOnce())
                .findByLessonIdAndUserId(anyString(), anyString());
        verify(homeworkRepository)
                .findByLessonIdAndUserId(stringCaptor.capture(), stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(lessonId, stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(userId, stringCaptor.getAllValues().get(1));
    }
}

