package unit.com.peachy.service.lesson;

import argumentsprovider.com.peachy.sevice.lesson.LessonServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.lesson.LessonServiceTestPayload;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.converter.lesson.LessonConverter;
import com.peachy.converter.lesson.LessonFromBaseDTOConverter;
import com.peachy.converter.lesson.LessonToBaseDTOConverter;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.entity.LessonEntity;
import com.peachy.repository.LessonRepository;
import com.peachy.service.course.CourseService;
import com.peachy.service.lesson.DefaultLessonService;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import testdata.TestProperties;
import testdata.lesson.LessonServiceTestData;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class DefaultLessonServiceTest {
    @Mock
    private LessonRepository lessonRepository;

    @Mock
    private LessonConverter lessonConverter;

    @Mock
    private LessonToBaseDTOConverter toBaseDTOConverter;

    @Mock
    private LessonFromBaseDTOConverter fromBaseDTOConverter;

    @Mock
    private CourseService defaultCourseService;

    private DefaultLessonService defaultLessonService;

    @Captor
    private ArgumentCaptor<List<String>> stringsCaptor;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @BeforeEach
    public void beforeEach() {
        defaultLessonService = new DefaultLessonService(
                lessonRepository,
                lessonConverter,
                toBaseDTOConverter,
                fromBaseDTOConverter);
        defaultLessonService.setDefaultCourseService(defaultCourseService);
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testSaveAll(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        lessonServiceTestPayload.getLessonDTOS().forEach(lessonDTO -> lessonDTO.setCourse(null));
        when(defaultCourseService.existAllByIds(anyList()))
                .thenReturn(true);
        when(defaultCourseService.findAllByIds(anyList()))
                .thenReturn(lessonServiceTestPayload.getCourseDTOS());
        when(toBaseDTOConverter.fromCreationToBase(lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(0)))
                .thenReturn(lessonServiceTestPayload.getLessonDTOS().get(0));
        when(toBaseDTOConverter.fromCreationToBase(lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(1)))
                .thenReturn(lessonServiceTestPayload.getLessonDTOS().get(1));
        when(lessonRepository.saveAll(anyList()))
                .thenReturn(lessonServiceTestPayload.getLessonEntities());
        when(lessonConverter.toEntity(lessonServiceTestPayload.getLessonDTOS().get(0)))
                .thenReturn(lessonServiceTestPayload.getLessonEntities().get(0));
        when(lessonConverter.toEntity(lessonServiceTestPayload.getLessonDTOS().get(1)))
                .thenReturn(lessonServiceTestPayload.getLessonEntities().get(1));
        when(lessonConverter.toDto(lessonServiceTestPayload.getLessonEntities().get(0)))
                .thenReturn(lessonServiceTestPayload.getLessonDTOS().get(0));
        when(lessonConverter.toDto(lessonServiceTestPayload.getLessonEntities().get(1)))
                .thenReturn(lessonServiceTestPayload.getLessonDTOS().get(1));

        //TEST
        defaultLessonService.saveAll(lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses());

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existAllByIds(anyList());
        verify(defaultCourseService)
                .existAllByIds(stringsCaptor.capture());
        verify(defaultCourseService, atLeastOnce())
                .findAllByIds(anyList());
        verify(defaultCourseService)
                .findAllByIds(stringsCaptor.capture());
        verify(toBaseDTOConverter, times(lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().size()))
                .fromCreationToBase(any(LessonCreationDTO.class));
        verify(lessonRepository, atMostOnce())
                .saveAll(anyList());
        verify(lessonConverter, times(lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().size()))
                .toEntity(any(LessonDTO.class));
        verify(lessonConverter, times(lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().size()))
                .toDto(any(LessonEntity.class));

        //ASSERT
        Assertions.assertEquals(stringsCaptor.getAllValues().get(0).size(), 2);
        Assertions.assertTrue(
                lessonServiceTestPayload.getExpandedCourseDTOS().stream().map(ExpandedCourseDTO::getId).collect(Collectors.toList()).containsAll(stringsCaptor.getAllValues().get(0)));
        Assertions.assertEquals(stringsCaptor.getAllValues().get(1).size(), 2);
        Assertions.assertTrue(
                lessonServiceTestPayload.getExpandedCourseDTOS().stream().map(ExpandedCourseDTO::getId).collect(Collectors.toList()).containsAll(stringsCaptor.getAllValues().get(1)));
        Assertions.assertTrue(lessonServiceTestPayload.getLessonDTOS().stream().allMatch(lessonDTO -> Objects.nonNull(lessonDTO.getCourse())));
        Assertions.assertEquals(lessonServiceTestPayload.getLessonDTOS().get(0).getCourse().getId(), lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(0).getCourseId());
        Assertions.assertEquals(lessonServiceTestPayload.getLessonDTOS().get(1).getCourse().getId(), lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(1).getCourseId());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testSaveAllReturnsBadRequestExceptionIfLessonTitleIsEmpty(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        LessonCreationDTO lessonCreationDTO = lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(0);
        lessonCreationDTO.setTitle(StringUtils.EMPTY);

        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultLessonService.saveAll(Collections.singletonList(lessonCreationDTO)));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testSaveAllReturnsBadRequestExceptionIfLessonTitleIsTooLarge(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        LessonCreationDTO lessonCreationDTO = lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(0);
        lessonCreationDTO.setTitle(TestProperties.randomString(81));

        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultLessonService.saveAll(Collections.singletonList(lessonCreationDTO)));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testSaveAllReturnsBadRequestExceptionIfLessonShortDescriptionIsTooLarge(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        LessonCreationDTO lessonCreationDTO = lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(0);
        lessonCreationDTO.setTitle(TestProperties.randomString(501));

        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultLessonService.saveAll(Collections.singletonList(lessonCreationDTO)));
    }


    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testSaveAllReturnsBadRequestExceptionIfLessonIsBoundedToNonExistentCourse(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        LessonCreationDTO lessonCreationDTO = lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(0);

        when(defaultCourseService.existAllByIds(anyList()))
                .thenReturn(false);

        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultLessonService.saveAll(Collections.singletonList(lessonCreationDTO)));

        //VERIFY
        verify(defaultCourseService, atMostOnce())
                .existAllByIds(anyList());
        verify(defaultCourseService)
                .existAllByIds(stringsCaptor.capture());
        Assertions.assertEquals(1, stringsCaptor.getValue().size());
        Assertions.assertEquals(lessonCreationDTO.getCourseId(), stringsCaptor.getValue().get(0));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByIds(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        List<String> lessonIds = Collections.singletonList(lessonServiceTestPayload.getLessonDTOS().get(0).getId());
        when(lessonRepository.findAllById(anyList()))
                .thenReturn(Collections.singletonList(lessonServiceTestPayload.getLessonEntities().get(0)));
        when(lessonConverter.toDto(anyList()))
                .thenReturn(Collections.singletonList(lessonServiceTestPayload.getLessonDTOS().get(0)));

        //TEST
        List<LessonDTO> lessonDTOS = defaultLessonService.findAllByIds(lessonIds);

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .findAllById(anyList());
        verify(lessonRepository)
                .findAllById(stringsCaptor.capture());
        verify(lessonConverter, atMostOnce())
                .toDto(anyList());
        Assertions.assertTrue(stringsCaptor.getValue().containsAll(lessonIds));
        Assertions.assertEquals(1, lessonDTOS.size());
        Assertions.assertEquals(lessonIds.get(0), lessonDTOS.get(0).getId());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByIdsReturnsEmptyListIfThereAreNoLessonsFoGivenIds(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        List<String> lessonIds = Collections.singletonList(lessonServiceTestPayload.getLessonDTOS().get(0).getId());
        when(lessonRepository.findAllById(anyList()))
                .thenReturn(Collections.emptyList());
        when(lessonConverter.toDto(anyList()))
                .thenReturn(Collections.emptyList());

        //TEST
        Assertions.assertTrue(defaultLessonService.findAllByIds(lessonIds).isEmpty());

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .findAllById(anyList());
        verify(lessonRepository)
                .findAllById(stringsCaptor.capture());
        verify(lessonConverter, atMostOnce())
                .toDto(anyList());
        Assertions.assertTrue(stringsCaptor.getValue().containsAll(lessonIds));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByCourseId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String courseId = lessonServiceTestPayload.getExpandedCourseDTOS().get(0).getId();
        when(lessonRepository.findAllByCourseIdIn(anyList()))
                .thenReturn(Collections.singletonList(lessonServiceTestPayload.getLessonEntities().get(0)));
        when(lessonConverter.toDto(anyList()))
                .thenReturn(Collections.singletonList(lessonServiceTestPayload.getLessonDTOS().get(0)));
        when(fromBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
                .thenReturn(LessonServiceTestData.convertToLessonInCourseDTOFrom(lessonServiceTestPayload.getLessonDTOS().get(0)));

        //TEST
        LessonsPerCourseDTO lessonsPerCourseDTO = defaultLessonService.findAllByCourseId(courseId);

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .findAllByCourseIdIn(anyList());
        verify(lessonRepository)
                .findAllByCourseIdIn(stringsCaptor.capture());
        verify(lessonConverter, atMostOnce())
                .toDto(anyList());
        verify(fromBaseDTOConverter, atMostOnce())
                .toLessonInCourseFromBase(any(LessonDTO.class));
        Assertions.assertTrue(stringsCaptor.getValue().contains(courseId));
        Assertions.assertEquals(lessonServiceTestPayload.getLessonEntities().get(0).getId(), lessonsPerCourseDTO.getLessons().get(0).getId());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByCourseIdThrowsNotFoundExceptionIfThereAreNoLessonsForGivenCourseId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String courseId = lessonServiceTestPayload.getExpandedCourseDTOS().get(0).getId();
        when(lessonRepository.findAllByCourseId(anyString()))
                .thenReturn(Collections.emptyList());
        when(lessonConverter.toDto(any(LessonEntity.class)))
                .thenReturn(lessonServiceTestPayload.getLessonDTOS().get(0));

        //TEST
        Assertions.assertThrows(NotFoundException.class, () -> defaultLessonService.findAllByCourseId(courseId));

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .findAllByCourseIdIn(anyList());
        verify(lessonRepository)
                .findAllByCourseIdIn(stringsCaptor.capture());
        Assertions.assertTrue(stringsCaptor.getValue().contains(courseId));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByCourseIds(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String courseId = lessonServiceTestPayload.getCourseDTOS().get(0).getId();

        when(lessonRepository.findAllByCourseIdIn(anyList()))
                .thenReturn(Collections.singletonList(lessonServiceTestPayload.getLessonEntities().get(0)));
        when(lessonConverter.toDto(anyList()))
                .thenReturn(Collections.singletonList(lessonServiceTestPayload.getLessonDTOS().get(0)));
        when(fromBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
                .thenReturn(LessonServiceTestData.convertToLessonInCourseDTOFrom(lessonServiceTestPayload.getLessonDTOS().get(0)));

        //TEST
        List<LessonsPerCourseDTO> lessonsPerCourseDTOS = defaultLessonService.findAllByCourseIds(Collections.singletonList(courseId));

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .findAllByCourseIdIn(anyList());
        verify(lessonRepository)
                .findAllByCourseIdIn(stringsCaptor.capture());
        verify(lessonConverter, atMostOnce())
                .toDto(anyList());
        verify(fromBaseDTOConverter, atMostOnce())
                .toLessonInCourseFromBase(any(LessonDTO.class));

        //ASSERT
        Assertions.assertNotNull(lessonsPerCourseDTOS);
        Assertions.assertEquals(1, lessonsPerCourseDTOS.size());
        Assertions.assertEquals(courseId, lessonsPerCourseDTOS.get(0).getCourse().getId());
        Assertions.assertEquals(1, lessonsPerCourseDTOS.get(0).getLessons().size());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByCourseIdsReturnsEmptyListIfThereIsNoLessonsForGivenCourse(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String courseId = lessonServiceTestPayload.getCourseDTOS().get(0).getId();

        when(lessonRepository.findAllByCourseIdIn(anyList()))
                .thenReturn(Collections.emptyList());
        when(lessonConverter.toDto(anyList()))
                .thenReturn(Collections.emptyList());
        when(fromBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
                .thenReturn(LessonServiceTestData.convertToLessonInCourseDTOFrom(lessonServiceTestPayload.getLessonDTOS().get(0)));

        //TEST
        Assertions.assertTrue(defaultLessonService.findAllByCourseIds(Collections.singletonList(courseId)).isEmpty());

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .findAllByCourseIdIn(anyList());
        verify(lessonRepository)
                .findAllByCourseIdIn(stringsCaptor.capture());
        verify(lessonConverter, atMostOnce())
                .toDto(anyList());
        verifyNoInteractions(fromBaseDTOConverter);
    }

    @Test
    public void testFindAllByCourseIdsThrowsBadArgumentExceptionIfCourseIdsAreEmpty() {
        //TEST AND ASSERT
        Assertions.assertThrows(BadArgumentException.class, () -> defaultLessonService.findAllByCourseIds(null));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultLessonService.findAllByCourseIds(Collections.singletonList(StringUtils.EMPTY)));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultLessonService.findAllByCourseIds(Collections.singletonList(StringUtils.SPACE)));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindById(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String lessonId = lessonServiceTestPayload.getLessonEntities().get(0).getId();

        when(lessonRepository.findAllById(anyList()))
                .thenReturn(Collections.singletonList(lessonServiceTestPayload.getLessonEntities().get(0)));
        when(lessonConverter.toDto(anyList()))
                .thenReturn(Collections.singletonList(lessonServiceTestPayload.getLessonDTOS().get(0)));

        //TEST
        Optional<LessonDTO> lessonById = defaultLessonService.findById(lessonId);

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .findAllById(anyList());
        verify(lessonRepository)
                .findAllById(stringsCaptor.capture());
        verify(lessonConverter, atMostOnce())
                .toDto(anyList());

        //ASSERT
        Assertions.assertTrue(lessonById.isPresent());
        Assertions.assertEquals(lessonId, lessonById.get().getId());
        Assertions.assertTrue(stringsCaptor.getValue().contains(lessonId));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindByIdReturnsEmptyOptionalIfThereIsNoLessonForGivenId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String lessonId = lessonServiceTestPayload.getLessonEntities().get(0).getId();

        when(lessonRepository.findAllById(anyList()))
                .thenReturn(Collections.emptyList());
        when(lessonConverter.toDto(anyList()))
                .thenReturn(Collections.emptyList());

        //TEST
        Optional<LessonDTO> lessonById = defaultLessonService.findById(lessonId);

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .findAllById(anyList());
        verify(lessonRepository)
                .findAllById(stringsCaptor.capture());
        verify(lessonConverter, atMostOnce())
                .toDto(anyList());

        //ASSERT
        Assertions.assertFalse(lessonById.isPresent());
        Assertions.assertTrue(stringsCaptor.getValue().contains(lessonId));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testExistsByIdReturnsTrueIfLessonExistsForGivenId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String lessonId = lessonServiceTestPayload.getLessonDTOS().get(0).getId();

        when(lessonRepository.existsById(anyString()))
                .thenReturn(true);

        //TEST
        boolean exists = defaultLessonService.existsById(lessonId);

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .existsById(anyString());
        verify(lessonRepository)
                .existsById(stringCaptor.capture());


        //ASSERT
        Assertions.assertTrue(exists);
        Assertions.assertEquals(lessonId, stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testExistsByIdReturnsFalseIfLessonDoesNotExistForGivenId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String lessonId = lessonServiceTestPayload.getLessonDTOS().get(0).getId();

        when(lessonRepository.existsById(anyString()))
                .thenReturn(false);

        //TEST
        boolean exists = defaultLessonService.existsById(lessonId);

        //VERIFY
        verify(lessonRepository, atMostOnce())
                .existsById(anyString());
        verify(lessonRepository)
                .existsById(stringCaptor.capture());


        //ASSERT
        Assertions.assertFalse(exists);
        Assertions.assertEquals(lessonId, stringCaptor.getValue());
    }
}
