package unit.com.peachy.service.usercourse;

import argumentsprovider.com.peachy.sevice.usercourse.UserCourseServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.usercourse.UserCourseServiceTestPayload;
import com.peachy.commons.exception.ConflictException;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.converter.course.CourseFromBaseDTOConverter;
import com.peachy.converter.usercourse.UserCourseConverter;
import com.peachy.converter.usercourse.UserCourseToBaseDTOConverter;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.dto.usercourse.UserCourseProjection;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.repository.UserCourseRepository;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.course.CourseService;
import com.peachy.service.usercourse.DefaultUserCourseService;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import testdata.TestProperties;
import testdata.course.CourseServiceTestData;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class DefaultUserCourseServiceTest {

    @Mock
    private UserService defaultUserService;

    @Mock
    private CourseService defaultCourseService;

    @Mock
    private UserCourseRepository userCourseRepository;

    @Mock
    private UserCourseConverter userCourseConverter;

    @Mock
    private UserCourseToBaseDTOConverter toBaseDTOConverter;

    @Mock
    private CourseFromBaseDTOConverter courseFromBaseDTOConverter;

    private DefaultUserCourseService defaultUserCourseService;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Captor
    private ArgumentCaptor<List<String>> stringsCaptor;

    @Captor
    private ArgumentCaptor<List<UserAuthority>> userAuthoritiesCaptor;

    @BeforeEach
    public void beforeEach() {
        defaultUserCourseService = new DefaultUserCourseService(
                defaultUserService,
                defaultCourseService,
                userCourseRepository,
                userCourseConverter,
                toBaseDTOConverter,
                courseFromBaseDTOConverter);
    }


    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourse(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> instructorIds = Collections.singletonList(userCourseServiceTestPayload.getInstructor().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);
        when(defaultCourseService.findExpandedById(anyString()))
                .thenReturn(Optional.of(userCourseServiceTestPayload.getExpandedCourseDTO()));
        when(defaultUserService.findAllByIds(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getInstructor()));
        when(userCourseConverter.toEntity(any(UserCourseDTO.class)))
                .thenReturn(userCourseServiceTestPayload.getInstructorUserCourseEntity());
        when(userCourseRepository.saveAll(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getInstructorUserCourseEntity()));
        when(userCourseConverter.toDto(any(UserCourseEntity.class)))
                .thenReturn(userCourseServiceTestPayload.getInstructorUserCourseDTO());

        //TEST
        List<UserCourseDTO> userCourseDTOS = defaultUserCourseService.assignInstructorsToCourse(instructorIds, courseId);

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));
        verify(defaultCourseService, atMostOnce())
                .findExpandedById(anyString());
        verify(defaultCourseService)
                .findExpandedById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .findAllByIds(anyList());
        verify(defaultUserService)
                .findAllByIds(stringsCaptor.capture());
        verify(userCourseConverter, atMostOnce())
                .toEntity(any(UserCourseDTO.class));
        verify(userCourseConverter, atMostOnce())
                .toDto(any(UserCourseEntity.class));
        verify(userCourseRepository, atMostOnce())
                .saveAll(anyList());

        //ASSERT
        Assertions.assertEquals(2, stringCaptor.getAllValues().size());
        Assertions.assertEquals(2, stringsCaptor.getAllValues().size());
        Assertions.assertTrue(stringCaptor.getAllValues().stream().allMatch(arg -> StringUtils.equals(arg, courseId)));
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(instructorIds::containsAll));
        Assertions.assertEquals(1, userCourseDTOS.size());
        Assertions.assertTrue(StringUtils.equals(userCourseDTOS.get(0).getCourse().getId(), courseId));
        Assertions.assertTrue(StringUtils.equals(userCourseDTOS.get(0).getUser().getId(), instructorIds.get(0)));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourseThrowsBadRequestExceptionIfCourseIdIsEmpty(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> instructorIds = Collections.singletonList(userCourseServiceTestPayload.getInstructor().getId());


        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultUserCourseService.assignInstructorsToCourse(instructorIds, StringUtils.EMPTY));

        //VERIFY
        verifyNoInteractions(userCourseRepository, defaultCourseService, userCourseConverter, defaultUserService);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourseThrowsNotFoundExceptionIfNoCourseFoundForGivenCourseId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> instructorIds = Collections.singletonList(userCourseServiceTestPayload.getInstructor().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(false);

        //TEST
        Assertions.assertThrows(NotFoundException.class, () -> defaultUserCourseService.assignInstructorsToCourse(instructorIds, courseId));

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verifyNoInteractions(defaultUserService, userCourseConverter, userCourseRepository);

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourseThrowsBadRequestExceptionIfAnyOfInstructorIdsIsEmpty(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> instructorIds = new ArrayList<>(Arrays.asList(userCourseServiceTestPayload.getInstructor().getId(), StringUtils.EMPTY));
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);

        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultUserCourseService.assignInstructorsToCourse(instructorIds, courseId));

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verifyNoInteractions(defaultUserService, userCourseConverter, userCourseRepository);

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourseThrowsBadRequestExceptionIfAnyOfTheGivenInstructorIsNotActuallyOfTheInstructorAuthority(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> instructorIds = Collections.singletonList(userCourseServiceTestPayload.getInstructor().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(false);

        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultUserCourseService.assignInstructorsToCourse(instructorIds, courseId));

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));
        verifyNoInteractions(userCourseConverter, userCourseRepository);


        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
        Assertions.assertEquals(1, stringsCaptor.getValue().size());
        Assertions.assertEquals(instructorIds.get(0), stringsCaptor.getValue().get(0));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourseThrowsConflictInCaseOfRepetitiveAssignment(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> instructorIds = Collections.singletonList(userCourseServiceTestPayload.getInstructor().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);
        when(userCourseRepository.findAllCourseMembersIds(anyString()))
                .thenReturn(instructorIds);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultUserCourseService.assignInstructorsToCourse(instructorIds, courseId));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));

        verify(userCourseRepository, atMostOnce())
                .findAllCourseMembersIds(anyString());
        verify(userCourseRepository)
                .findAllCourseMembersIds(stringCaptor.capture());

        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());

        verifyNoInteractions(userCourseConverter);

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(strings -> strings.contains(instructorIds.get(0))));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentToCourse(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(userCourseServiceTestPayload.getStudent().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);
        when(defaultCourseService.findExpandedById(anyString()))
                .thenReturn(Optional.of(userCourseServiceTestPayload.getExpandedCourseDTO()));
        when(defaultUserService.findAllByIds(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getStudent()));
        when(userCourseConverter.toEntity(any(UserCourseDTO.class)))
                .thenReturn(userCourseServiceTestPayload.getStudentUserCourseEntity());
        when(userCourseRepository.saveAll(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getStudentUserCourseEntity()));
        when(userCourseConverter.toDto(any(UserCourseEntity.class)))
                .thenReturn(userCourseServiceTestPayload.getStudentUserCourseDTO());

        //TEST
        List<UserCourseDTO> userCourseDTOS = defaultUserCourseService.assignStudentsToCourse(studentIds, courseId);

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));
        verify(defaultCourseService, atMostOnce())
                .findExpandedById(anyString());
        verify(defaultCourseService)
                .findExpandedById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .findAllByIds(anyList());
        verify(defaultUserService)
                .findAllByIds(stringsCaptor.capture());
        verify(userCourseConverter, atMostOnce())
                .toEntity(any(UserCourseDTO.class));
        verify(userCourseConverter, atMostOnce())
                .toDto(any(UserCourseEntity.class));
        verify(userCourseRepository, atMostOnce())
                .saveAll(anyList());

        //ASSERT
        Assertions.assertEquals(2, stringCaptor.getAllValues().size());
        Assertions.assertEquals(2, stringsCaptor.getAllValues().size());
        Assertions.assertTrue(stringCaptor.getAllValues().stream().allMatch(arg -> StringUtils.equals(arg, courseId)));
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(studentIds::containsAll));
        Assertions.assertEquals(1, userCourseDTOS.size());
        Assertions.assertTrue(StringUtils.equals(userCourseDTOS.get(0).getCourse().getId(), courseId));
        Assertions.assertTrue(StringUtils.equals(userCourseDTOS.get(0).getUser().getId(), studentIds.get(0)));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentsToCourseThrowsBadRequestExceptionIfCourseIdIsEmpty(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(userCourseServiceTestPayload.getStudent().getId());


        //TEST AND ASSERT
        Assertions.assertThrows(BadRequestException.class, () -> defaultUserCourseService.assignStudentsToCourse(studentIds, StringUtils.EMPTY));

        //VERIFY
        verifyNoInteractions(userCourseRepository, defaultCourseService, userCourseConverter, defaultUserService);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentsToCourseThrowsNotFoundExceptionIfNoCourseFoundForGivenCourseId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(userCourseServiceTestPayload.getStudent().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(false);

        //TEST
        Assertions.assertThrows(NotFoundException.class, () -> defaultUserCourseService.assignInstructorsToCourse(studentIds, courseId));

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verifyNoInteractions(defaultUserService, userCourseConverter, userCourseRepository);

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentsToCourseThrowsBadRequestExceptionIfAnyOfStudentIdsIsBlank(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(StringUtils.SPACE);
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);

        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultUserCourseService.assignStudentsToCourse(studentIds, courseId));

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verifyNoInteractions(defaultUserService, userCourseConverter, userCourseRepository);

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourseThrowsBadRequestExceptionIfAnyOfTheGivenStudentIsNotActuallyOfTheStudentAuthority(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(userCourseServiceTestPayload.getStudent().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(false);

        //TEST
        Assertions.assertThrows(BadRequestException.class, () -> defaultUserCourseService.assignStudentsToCourse(studentIds, courseId));

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));
        verifyNoInteractions(userCourseConverter, userCourseRepository);


        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
        Assertions.assertEquals(1, stringsCaptor.getValue().size());
        Assertions.assertEquals(studentIds.get(0), stringsCaptor.getValue().get(0));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentsToCourseThrowsConflictInCaseOfRepetitiveAssignment(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(userCourseServiceTestPayload.getStudent().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);
        when(userCourseRepository.findAllCourseMembersIds(anyString()))
                .thenReturn(studentIds);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultUserCourseService.assignStudentsToCourse(studentIds, courseId));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));

        verify(userCourseRepository, atMostOnce())
                .findAllCourseMembersIds(anyString());
        verify(userCourseRepository)
                .findAllCourseMembersIds(stringCaptor.capture());

        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());

        verifyNoInteractions(userCourseConverter);

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(strings -> strings.contains(studentIds.get(0))));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentsToCourseThrowsBadRequestExceptionIfStudentReachedTheTopOfCoursesLimit(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(userCourseServiceTestPayload.getStudent().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();
        List<UserCourseProjection> studentCourses = Stream.generate(() -> new UserCourseProjection() {

            @Override
            public String getUserId() {
                return userCourseServiceTestPayload.getStudent().getId();
            }

            @Override
            public String getCourseId() {
                return TestProperties.randomString(12);
            }

            @Override
            public UserAuthority getUserAuthority() {
                return userCourseServiceTestPayload.getStudent().getAuthority();
            }
        }).limit(5).collect(Collectors.toList());

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);
        when(userCourseRepository.findAllCourseMembersIds(anyString()))
                .thenReturn(Collections.emptyList());
        when(userCourseRepository.findAllUserCourses(anyList()))
                .thenReturn(studentCourses);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultUserCourseService.assignStudentsToCourse(studentIds, courseId));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));

        verify(userCourseRepository, atMostOnce())
                .findAllCourseMembersIds(anyString());
        verify(userCourseRepository)
                .findAllCourseMembersIds(stringCaptor.capture());

        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());

        verify(userCourseRepository, atMostOnce())
                .findAllUserCourses(anyList());
        verify(userCourseRepository)
                .findAllUserCourses(stringsCaptor.capture());

        verifyNoInteractions(userCourseConverter);

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(strings -> strings.contains(studentIds.get(0))));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindCourseMembers(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();
        when(userCourseRepository.findAllCoursesMembers(anyList(), anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getInstructorUserCourseDTO()));
        when(defaultUserService.findAllByIds(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getInstructor()));
        when(defaultCourseService.findAllByIds(anyList()))
                .thenReturn(Collections.singletonList(CourseServiceTestData.convertToCourseDTOFrom(userCourseServiceTestPayload.getInstructorUserCourseDTO().getCourse())));

        //TEST
        List<UserDTO> userDTOS = defaultUserCourseService.findCourseMembers(courseId, UserAuthority.INSTRUCTOR);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .findAllCoursesMembers(anyList(), anyList());
        verify(userCourseRepository)
                .findAllCoursesMembers(stringsCaptor.capture(), stringsCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .findAllByIds(anyList());
        verify(defaultUserService)
                .findAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertEquals(1, userDTOS.size());
        Assertions.assertEquals(userCourseServiceTestPayload.getInstructor().getId(), userDTOS.get(0).getId());
        Assertions.assertTrue(stringsCaptor.getAllValues().get(0).contains(courseId));
        Assertions.assertTrue(stringsCaptor.getAllValues().get(1).contains(UserAuthority.INSTRUCTOR.name()));
        Assertions.assertTrue(stringsCaptor.getAllValues().get(2).contains(userCourseServiceTestPayload.getInstructor().getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindCourseMembersReturnsEmptyListIfThereAreNoCourseMembers(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();
        when(userCourseRepository.findAllCoursesMembers(anyList(), anyList()))
                .thenReturn(Collections.emptyList());
        when(defaultUserService.findAllByIds(anyList()))
                .thenReturn(Collections.emptyList());

        //TEST
        List<UserDTO> userDTOS = defaultUserCourseService.findCourseMembers(courseId, UserAuthority.INSTRUCTOR);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .findAllCoursesMembers(anyList(), anyList());
        verify(userCourseRepository)
                .findAllCoursesMembers(stringsCaptor.capture(), stringsCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .findAllByIds(anyList());
        verify(defaultUserService)
                .findAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertTrue(userDTOS.isEmpty());
        Assertions.assertTrue(stringsCaptor.getAllValues().get(0).contains(courseId));
        Assertions.assertTrue(stringsCaptor.getAllValues().get(1).contains(UserAuthority.INSTRUCTOR.name()));
        Assertions.assertTrue(stringsCaptor.getAllValues().get(2).isEmpty());
    }

    @Test
    public void testFindCourseMembersThrowsBadArgumentExceptionIfAuthorityIsNull() {
        //TEST AND ASSERT
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findCourseMembers(TestProperties.randomString(12), null));


        //VERIFY
        verifyNoInteractions(userCourseRepository, defaultUserService);
    }

    @Test
    public void testFindCourseMembersThrowsBadArgumentExceptionIfCourseIdIsEmpty() {
        //TEST AND ASSERT
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findCourseMembers(null, UserAuthority.INSTRUCTOR));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findCourseMembers(StringUtils.EMPTY, UserAuthority.INSTRUCTOR));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findCourseMembers(StringUtils.SPACE, UserAuthority.INSTRUCTOR));


        //VERIFY
        verifyNoInteractions(userCourseRepository, defaultUserService);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindCourseMemberIds(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();
        List<String> memberIds = Stream.of(userCourseServiceTestPayload.getInstructor().getId(), userCourseServiceTestPayload.getStudent().getId())
                .collect(Collectors.toList());

        when(userCourseRepository.findAllCourseMembersIds(anyString()))
                .thenReturn(memberIds);

        //TEST
        List<String> courseMemberIds = defaultUserCourseService.findCourseMemberIds(courseId);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .findAllCourseMembersIds(anyString());
        verify(userCourseRepository)
                .findAllCourseMembersIds(stringCaptor.capture());

        //ASSERT
        Assertions.assertNotNull(courseMemberIds);
        Assertions.assertTrue(courseMemberIds.containsAll(memberIds));
        Assertions.assertEquals(courseId, stringCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindCourseMemberIdsReturnsEmptyListIfThereIsNoMembersForGivenCourse(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        when(userCourseRepository.findAllCourseMembersIds(anyString()))
                .thenReturn(Collections.emptyList());

        //TEST
        Assertions.assertTrue(defaultUserCourseService.findCourseMemberIds(courseId).isEmpty());

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .findAllCourseMembersIds(anyString());
        verify(userCourseRepository)
                .findAllCourseMembersIds(stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
    }

    @Test
    public void testFindCourseMemberIdsThrowsBadArgumentExceptionIfGivenCourseIdIsEmpty() {
        //TEST AND ASSERT
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findCourseMemberIds(StringUtils.SPACE));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findCourseMemberIds(StringUtils.EMPTY));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findCourseMemberIds(null));

        //VERIFY
        verifyNoInteractions(userCourseRepository);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindUserCoursesCount(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> userIds = Stream.of(
                userCourseServiceTestPayload.getInstructor().getId(),
                userCourseServiceTestPayload.getStudent().getId())
                .collect(Collectors.toList());
        List<UserCourseProjection> userCourseProjections = userIds.stream()
                .map(userId -> new UserCourseProjection() {
                    @Override
                    public String getUserId() {
                        return userId;
                    }

                    @Override
                    public String getCourseId() {
                        return userCourseServiceTestPayload.getExpandedCourseDTO().getId();
                    }

                    @Override
                    public UserAuthority getUserAuthority() {
                        return userCourseServiceTestPayload.getStudent().getAuthority();
                    }
                }).collect(Collectors.toList());
        when(userCourseRepository.findAllUserCourses(anyList()))
                .thenReturn(userCourseProjections);

        //TEST
        Map<String, Long> userCoursesCount = defaultUserCourseService.findUserCoursesCount(userIds);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .findAllUserCourses(anyList());
        verify(userCourseRepository)
                .findAllUserCourses(stringsCaptor.capture());

        //ASSERT
        Assertions.assertNotNull(stringsCaptor.getValue());
        Assertions.assertTrue(stringsCaptor.getValue().containsAll(userIds));
        Assertions.assertNotNull(userCoursesCount);
        Assertions.assertTrue(userCoursesCount.containsKey(userIds.get(0)));
        Assertions.assertTrue(userCoursesCount.containsKey(userIds.get(1)));
        Assertions.assertEquals(1, userCoursesCount.get(userIds.get(0)));
        Assertions.assertEquals(1, userCoursesCount.get(userIds.get(1)));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindUserCoursesCountReturnsZeroCountIfUserHasNoAnyCourses(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> userIds = Stream.of(
                userCourseServiceTestPayload.getInstructor().getId(),
                userCourseServiceTestPayload.getStudent().getId())
                .collect(Collectors.toList());
        List<UserCourseProjection> userCourseProjections = Collections.singletonList(new UserCourseProjection() {
            @Override
            public String getUserId() {
                return userIds.get(0);
            }

            @Override
            public String getCourseId() {
                return userCourseServiceTestPayload.getExpandedCourseDTO().getId();
            }

            @Override
            public UserAuthority getUserAuthority() {
                return userCourseServiceTestPayload.getStudent().getAuthority();
            }
        });

        when(userCourseRepository.findAllUserCourses(anyList()))
                .thenReturn(userCourseProjections);

        //TEST
        Map<String, Long> userCoursesCount = defaultUserCourseService.findUserCoursesCount(userIds);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .findAllUserCourses(anyList());
        verify(userCourseRepository)
                .findAllUserCourses(stringsCaptor.capture());

        //ASSERT
        Assertions.assertNotNull(stringsCaptor.getValue());
        Assertions.assertTrue(stringsCaptor.getValue().containsAll(userIds));
        Assertions.assertNotNull(userCoursesCount);
        Assertions.assertTrue(userCoursesCount.containsKey(userIds.get(0)));
        Assertions.assertTrue(userCoursesCount.containsKey(userIds.get(1)));
        Assertions.assertEquals(1, userCoursesCount.get(userIds.get(0)));
        Assertions.assertEquals(0, userCoursesCount.get(userIds.get(1)));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testExistsByLessonIdAndUserIdReturnsTrueIfThereIsAUserCourseForGivenLessonIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String lessonId = TestProperties.randomString(12);
        String userId = userCourseServiceTestPayload.getStudent().getId();

        when(userCourseRepository.existsByLessonIdAndUserId(anyString(), anyString()))
                .thenReturn(true);

        //TEST
        boolean exists = defaultUserCourseService.existsByLessonIdAndUserId(lessonId, userId);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .existsByLessonIdAndUserId(anyString(), anyString());
        verify(userCourseRepository)
                .existsByLessonIdAndUserId(stringCaptor.capture(), stringCaptor.capture());

        //ASSERT
        Assertions.assertTrue(exists);
        Assertions.assertEquals(lessonId, stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(userId, stringCaptor.getAllValues().get(1));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testExistsByLessonIdAndUserIdReturnsFalseIfThereIsNoUserCourseForGivenLessonIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String lessonId = TestProperties.randomString(12);
        String userId = userCourseServiceTestPayload.getStudent().getId();

        when(userCourseRepository.existsByLessonIdAndUserId(anyString(), anyString()))
                .thenReturn(false);

        //TEST
        boolean exists = defaultUserCourseService.existsByLessonIdAndUserId(lessonId, userId);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .existsByLessonIdAndUserId(anyString(), anyString());
        verify(userCourseRepository)
                .existsByLessonIdAndUserId(stringCaptor.capture(), stringCaptor.capture());

        //ASSERT
        Assertions.assertFalse(exists);
        Assertions.assertEquals(lessonId, stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(userId, stringCaptor.getAllValues().get(1));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testExistsByLessonIdAndUserIdThrowsBadArgumentExceptionIfGivenLessonIdIsBlank(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String userId = userCourseServiceTestPayload.getStudent().getId();

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.existsByLessonIdAndUserId(userId, null));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.existsByLessonIdAndUserId(userId, StringUtils.EMPTY));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.existsByLessonIdAndUserId(userId, StringUtils.SPACE));

        //VERIFY
        verifyNoInteractions(userCourseRepository);
    }

    @Test
    public void testExistsByLessonIdAndUserIdThrowsBadArgumentExceptionIfGivenUserIdIsBlank() {
        //SETUP
        String lessonId = TestProperties.randomString(12);

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.existsByLessonIdAndUserId(null, lessonId));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.existsByLessonIdAndUserId(StringUtils.EMPTY, lessonId));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.existsByLessonIdAndUserId(StringUtils.SPACE, lessonId));

        //VERIFY
        verifyNoInteractions(userCourseRepository);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testUpdateStatusByCourseIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getCourseDTO().getId();
        String userId = userCourseServiceTestPayload.getStudent().getId();
        UserCourseStatus userCourseStatus = UserCourseStatus.STARTED;

        doNothing().when(userCourseRepository).updateStatusByCourseIdAndUserId(anyString(), anyString(), anyString());

        //TEST
        defaultUserCourseService.updateStatusByCourseIdAndUserId(courseId, userId, userCourseStatus);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .updateStatusByCourseIdAndUserId(anyString(), anyString(), anyString());
        verify(userCourseRepository)
                .updateStatusByCourseIdAndUserId(stringCaptor.capture(), stringCaptor.capture(), stringCaptor.capture());


        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(userId, stringCaptor.getAllValues().get(1));
        Assertions.assertEquals(userCourseStatus.name(), stringCaptor.getAllValues().get(2));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testUpdateStatusByCourseIdAndUserIdThrowsBadArgumentExceptionIfCourseIdIsBlank(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String userId = userCourseServiceTestPayload.getStudent().getId();
        UserCourseStatus userCourseStatus = UserCourseStatus.STARTED;

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.updateStatusByCourseIdAndUserId(null, userId, userCourseStatus));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.updateStatusByCourseIdAndUserId(StringUtils.EMPTY, userId, userCourseStatus));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.updateStatusByCourseIdAndUserId(StringUtils.SPACE, userId, userCourseStatus));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testUpdateStatusByCourseIdAndUserIdThrowsBadArgumentExceptionIfUserIdIsBlank(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getCourseDTO().getId();
        UserCourseStatus userCourseStatus = UserCourseStatus.STARTED;

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.updateStatusByCourseIdAndUserId(courseId, null, userCourseStatus));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.updateStatusByCourseIdAndUserId(courseId, StringUtils.EMPTY, userCourseStatus));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.updateStatusByCourseIdAndUserId(courseId, StringUtils.SPACE, userCourseStatus));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testUpdateStatusByCourseIdAndUserIdThrowsBadArgumentExceptionIfUserCourseStatusIsNull(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getCourseDTO().getId();
        String userId = userCourseServiceTestPayload.getStudent().getId();

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.updateStatusByCourseIdAndUserId(courseId, userId, null));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindStatusByCourseIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getCourseDTO().getId();
        String userId = userCourseServiceTestPayload.getStudent().getId();
        UserCourseStatus userCourseStatus = UserCourseStatus.STARTED;

        when(userCourseRepository.findStatusByCourseIdAndUserId(anyString(), anyString()))
                .thenReturn(Optional.of(userCourseStatus));

        //TEST
        Optional<UserCourseStatus> retrievedUserCourseStatus = defaultUserCourseService.findStatusByCourseIdAndUserId(courseId, userId);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .findStatusByCourseIdAndUserId(anyString(), anyString());
        verify(userCourseRepository)
                .findStatusByCourseIdAndUserId(stringCaptor.capture(), stringCaptor.capture());

        //ASSERT
        Assertions.assertTrue(retrievedUserCourseStatus.isPresent());
        Assertions.assertEquals(userCourseStatus, retrievedUserCourseStatus.get());
        Assertions.assertEquals(courseId, stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(userId, stringCaptor.getAllValues().get(1));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindStatusByCourseIdAndUserIdReturnsEmptyOptionalIfThereIsNoUserCourseStatusForGivenCourseIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getCourseDTO().getId();
        String userId = userCourseServiceTestPayload.getStudent().getId();

        when(userCourseRepository.findStatusByCourseIdAndUserId(anyString(), anyString()))
                .thenReturn(Optional.empty());

        //TEST
        Optional<UserCourseStatus> retrievedUserCourseStatus = defaultUserCourseService.findStatusByCourseIdAndUserId(courseId, userId);

        //VERIFY
        verify(userCourseRepository, atMostOnce())
                .findStatusByCourseIdAndUserId(anyString(), anyString());
        verify(userCourseRepository)
                .findStatusByCourseIdAndUserId(stringCaptor.capture(), stringCaptor.capture());

        //ASSERT
        Assertions.assertFalse(retrievedUserCourseStatus.isPresent());
        Assertions.assertEquals(courseId, stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(userId, stringCaptor.getAllValues().get(1));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindStatusByCourseIdAndUserIdThrowsBadArgumentExceptionIfCourseIdIsBlank(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String userId = userCourseServiceTestPayload.getStudent().getId();

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findStatusByCourseIdAndUserId(null, userId));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findStatusByCourseIdAndUserId(StringUtils.EMPTY, userId));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findStatusByCourseIdAndUserId(StringUtils.SPACE, userId));

        //VERIFY
        verifyNoInteractions(userCourseRepository);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindStatusByCourseIdAndUserIdThrowsBadArgumentExceptionIfUserIdIsBlank(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getCourseDTO().getId();

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findStatusByCourseIdAndUserId(courseId, null));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findStatusByCourseIdAndUserId(courseId, StringUtils.EMPTY));
        Assertions.assertThrows(BadArgumentException.class, () -> defaultUserCourseService.findStatusByCourseIdAndUserId(courseId, StringUtils.SPACE));

        //VERIFY
        verifyNoInteractions(userCourseRepository);
    }
}
