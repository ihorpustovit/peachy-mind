package integration.com.peachy.service.homework;

import argumentsprovider.com.peachy.sevice.homework.HomeworkServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.homework.HomeworkServiceTestPayload;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.peachy.commons.exception.ConflictException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.aws.s3.S3Service;
import com.peachy.service.homework.HomeworkService;
import com.peachy.service.lesson.LessonService;
import com.peachy.test.data.UserServiceTestUtils;
import integration.com.peachy.AbstractIntegrationTest;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.multipart.MultipartFile;
import testdata.UserServiceTestData;
import testdata.homework.HomeworkServiceTestUtils;
import testdata.lesson.LessonServiceTestData;
import testdata.lesson.LessonServiceTestUtils;
import testdata.usercourse.UserCourseServiceTestUtils;

import java.io.ByteArrayOutputStream;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class DefaultHomeworkServiceTest extends AbstractIntegrationTest {
    @MockBean
    private S3Service s3Service;

    @MockBean
    private LessonService defaultLessonService;

    @MockBean
    private UserService defaultUserService;

    @Autowired
    private HomeworkService defaultHomeworkService;

    @Autowired
    @Qualifier("peachyMindAuzJdbcTemplate")
    private JdbcTemplate peachyMindAuzJdbcTemplate;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        HomeworkServiceTestUtils.truncateHomeworks(jdbcTemplate);
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSave(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        when(defaultUserService.existsById(anyString()))
                .thenReturn(true);
        when(defaultLessonService.existsById(anyString()))
                .thenReturn(true);
        when(defaultLessonService.findById(anyString()))
                .thenReturn(Optional.of(homeworkServiceTestPayload.getLessonDTO()));
        doNothing().when(s3Service).upload(anyString(),anyString(),any(MultipartFile.class));

        LessonServiceTestUtils.save(LessonServiceTestData.convertToLessonEntityFrom(homeworkServiceTestPayload.getLessonDTO()), jdbcTemplate);
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(homeworkServiceTestPayload.getUserDTO()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(homeworkServiceTestPayload.getUserCourseEntity(), jdbcTemplate);

        //TEST
        HomeworkDTO savedHomework = defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation());

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existsById(anyString());
        verify(defaultUserService)
                .existsById(stringCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .existsById(anyString());
        verify(defaultLessonService)
                .existsById(stringCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .findById(anyString());
        verify(defaultLessonService)
                .findById(stringCaptor.capture());
        verify(s3Service, atMostOnce())
                .upload(anyString(),anyString(),any(MultipartFile.class));

        //ASSERT
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(1));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(2));
        Assertions.assertNotNull(savedHomework);
        Assertions.assertNotNull(savedHomework.getLessonId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), savedHomework.getLessonId());
        Assertions.assertNotNull(savedHomework.getUserId());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), savedHomework.getUserId());;
        Assertions.assertNotNull(savedHomework.getFileName());
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getHomeworkFile().getName(), savedHomework.getFileName());
        Assertions.assertTrue(HomeworkServiceTestUtils.existsByLessonIdAndUserId(
                homeworkServiceTestPayload.getHomeworkCreation().getLessonId(),
                homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(),
                jdbcTemplate));
        Assertions.assertNotNull(savedHomework.getS3Key());
    }

    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testSaveThrowsConflictExceptionIfHomeworkAlreadyExists(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        when(defaultUserService.existsById(anyString()))
                .thenReturn(true);
        when(defaultLessonService.existsById(anyString()))
                .thenReturn(true);

        LessonServiceTestUtils.save(LessonServiceTestData.convertToLessonEntityFrom(homeworkServiceTestPayload.getLessonDTO()), jdbcTemplate);
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(homeworkServiceTestPayload.getUserDTO()), peachyMindAuzJdbcTemplate);
        HomeworkServiceTestUtils.save(homeworkServiceTestPayload.getHomeworkEntity(), jdbcTemplate);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultHomeworkService.save(homeworkServiceTestPayload.getHomeworkCreation()));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existsById(anyString());
        verify(defaultUserService)
                .existsById(stringCaptor.capture());
        verify(defaultLessonService, atMostOnce())
                .existsById(anyString());
        verify(defaultLessonService)
                .existsById(stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getUser().getId(), stringCaptor.getAllValues().get(0));
        Assertions.assertEquals(homeworkServiceTestPayload.getHomeworkCreation().getLessonId(), stringCaptor.getAllValues().get(1));
     }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testDownload(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        String lessonId = homeworkServiceTestPayload.getLessonDTO().getId();
        String userId = homeworkServiceTestPayload.getUserDTO().getId();
        ByteArrayOutputStream byteArrayOutputStream =
                new ByteArrayOutputStream();
        byteArrayOutputStream.write(homeworkServiceTestPayload.getHomeworkCreation().getHomeworkFile().getBytes());

        LessonServiceTestUtils.save(LessonServiceTestData.convertToLessonEntityFrom(homeworkServiceTestPayload.getLessonDTO()), jdbcTemplate);
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(homeworkServiceTestPayload.getUserDTO()), peachyMindAuzJdbcTemplate);
        HomeworkServiceTestUtils.save(homeworkServiceTestPayload.getHomeworkEntity(), jdbcTemplate);


        when(s3Service.download(any(GetObjectRequest.class)))
                .thenReturn(Optional.of(byteArrayOutputStream));

        //TEST
        ExpandedHomeworkDTO expandedHomeworkDTO = defaultHomeworkService.download(lessonId, userId);

        //VERIFY
        verify(s3Service, atMostOnce())
                .download(any(GetObjectRequest.class));

        //ASSERT
        Assertions.assertNotNull(expandedHomeworkDTO);
        Assertions.assertNotNull(expandedHomeworkDTO.getUser());
        Assertions.assertEquals(userId, expandedHomeworkDTO.getUserId());
        Assertions.assertNotNull(expandedHomeworkDTO.getLessonId());
        Assertions.assertEquals(lessonId, expandedHomeworkDTO.getLessonId());
        Assertions.assertNotNull(expandedHomeworkDTO.getHomeworkFileStream());
        Assertions.assertNotNull(expandedHomeworkDTO.getFileName());
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(HomeworkServiceTestArgumentProvider.class)
    public void testDownloadThrowsNotFoundExceptionIfThereIsNoHomeworkForGivenLessonIdAndUserId(HomeworkServiceTestPayload homeworkServiceTestPayload) {
        //SETUP
        String lessonId = homeworkServiceTestPayload.getLessonDTO().getId();
        String userId = homeworkServiceTestPayload.getUserDTO().getId();

        //TEST
        Assertions.assertThrows(NotFoundException.class, () -> defaultHomeworkService.download(lessonId, userId));
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        HomeworkServiceTestUtils.truncateHomeworks(jdbcTemplate);
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
    }
}
