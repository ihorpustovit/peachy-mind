package integration.com.peachy.service.usercourse;

import argumentsprovider.com.peachy.sevice.usercourse.UserCourseServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.usercourse.UserCourseServiceTestPayload;
import com.peachy.commons.exception.ConflictException;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.entity.usercourse.UserCourseCompositeId;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.course.CourseService;
import com.peachy.service.usercourse.UserCourseService;
import com.peachy.test.data.UserServiceTestUtils;
import integration.com.peachy.AbstractIntegrationTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.UserServiceTestData;
import testdata.course.CourseServiceTestData;
import testdata.course.CourseServiceTestUtils;
import testdata.lesson.LessonServiceTestData;
import testdata.lesson.LessonServiceTestUtils;
import testdata.usercourse.UserCourseServiceTestData;
import testdata.usercourse.UserCourseServiceTestUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class DefaultUserCourseServiceTest extends AbstractIntegrationTest {
    @MockBean
    private UserService defaultUserService;

    @MockBean
    private CourseService defaultCourseService;

    @Autowired
    private UserCourseService defaultUserCourseService;

    @Autowired
    @Qualifier("peachyMindAuzJdbcTemplate")
    private JdbcTemplate peachyMindAuzJdbcTemplate;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Captor
    private ArgumentCaptor<List<String>> stringsCaptor;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourse(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        UserEntity userEntity = userCourseServiceTestPayload.getInstructorUserCourseEntity().getId().getUser();
        CourseEntity courseEntity = userCourseServiceTestPayload.getInstructorUserCourseEntity().getId().getCourse();
        UserServiceTestUtils.save(userEntity, peachyMindAuzJdbcTemplate);
        CourseServiceTestUtils.save(jdbcTemplate, courseEntity);

        List<String> instructorIds = Collections.singletonList(userEntity.getId());
        String courseId = courseEntity.getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);
        when(defaultCourseService.findExpandedById(anyString()))
                .thenReturn(Optional.of(userCourseServiceTestPayload.getExpandedCourseDTO()));
        when(defaultUserService.findAllByIds(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getInstructor()));

        //TEST
        List<UserCourseDTO> userCourseDTOS = defaultUserCourseService.assignInstructorsToCourse(instructorIds, courseId);

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));
        verify(defaultCourseService, atMostOnce())
                .findExpandedById(anyString());
        verify(defaultCourseService)
                .findExpandedById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .findAllByIds(anyList());
        verify(defaultUserService)
                .findAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertEquals(2, stringCaptor.getAllValues().size());
        Assertions.assertEquals(2, stringsCaptor.getAllValues().size());
        Assertions.assertTrue(stringCaptor.getAllValues().stream().allMatch(arg -> StringUtils.equals(arg, courseId)));
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(instructorIds::containsAll));
        Assertions.assertEquals(1, userCourseDTOS.size());
        Assertions.assertTrue(StringUtils.equals(userCourseDTOS.get(0).getCourse().getId(), courseId));
        Assertions.assertTrue(StringUtils.equals(userCourseDTOS.get(0).getUser().getId(), instructorIds.get(0)));
        Assertions.assertTrue(UserCourseServiceTestUtils.existAllByIds(userCourseDTOS.stream().map(userCourseDTO -> new UserCourseCompositeId(userCourseDTO.getUserId(), userCourseDTO.getCourseId())).collect(Collectors.toList()), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignInstructorsToCourseThrowsConflictInCaseOfRepetitiveAssignment(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> instructorIds = Collections.singletonList(userCourseServiceTestPayload.getInstructor().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        CourseServiceTestUtils.save(jdbcTemplate, userCourseServiceTestPayload.getInstructorUserCourseEntity().getId().getCourse());
        UserServiceTestUtils.save(userCourseServiceTestPayload.getInstructorUserCourseEntity().getId().getUser(), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(userCourseServiceTestPayload.getInstructorUserCourseEntity(), jdbcTemplate);
        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultUserCourseService.assignInstructorsToCourse(instructorIds, courseId));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));

        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(strings -> strings.contains(instructorIds.get(0))));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentsToCourse(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        UserEntity studentEntity = userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getUser();
        CourseEntity courseEntity = userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getCourse();
        UserServiceTestUtils.save(studentEntity, peachyMindAuzJdbcTemplate);
        CourseServiceTestUtils.save(jdbcTemplate, courseEntity);

        List<String> studentIds = Collections.singletonList(studentEntity.getId());
        String courseId = courseEntity.getId();

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);
        when(defaultCourseService.findExpandedById(anyString()))
                .thenReturn(Optional.of(userCourseServiceTestPayload.getExpandedCourseDTO()));
        when(defaultUserService.findAllByIds(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getStudent()));

        //TEST
        List<UserCourseDTO> userCourseDTOS = defaultUserCourseService.assignStudentsToCourse(studentIds, courseId);

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));
        verify(defaultCourseService, atMostOnce())
                .findExpandedById(anyString());
        verify(defaultCourseService)
                .findExpandedById(stringCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .findAllByIds(anyList());
        verify(defaultUserService)
                .findAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertEquals(2, stringCaptor.getAllValues().size());
        Assertions.assertEquals(2, stringsCaptor.getAllValues().size());
        Assertions.assertTrue(stringCaptor.getAllValues().stream().allMatch(arg -> StringUtils.equals(arg, courseId)));
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(studentIds::containsAll));
        Assertions.assertEquals(1, userCourseDTOS.size());
        Assertions.assertTrue(StringUtils.equals(userCourseDTOS.get(0).getCourse().getId(), courseId));
        Assertions.assertTrue(StringUtils.equals(userCourseDTOS.get(0).getUser().getId(), studentIds.get(0)));
        Assertions.assertTrue(UserCourseServiceTestUtils.existAllByIds(userCourseDTOS.stream().map(userCourseDTO -> new UserCourseCompositeId(userCourseDTO.getUserId(), userCourseDTO.getCourseId())).collect(Collectors.toList()), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentsToCourseThrowsConflictInCaseOfRepetitiveAssignment(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(userCourseServiceTestPayload.getStudent().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        CourseServiceTestUtils.save(jdbcTemplate, userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getCourse());
        UserServiceTestUtils.save(userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getUser(), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(userCourseServiceTestPayload.getStudentUserCourseEntity(), jdbcTemplate);

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultUserCourseService.assignStudentsToCourse(studentIds, courseId));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));

        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(strings -> strings.contains(studentIds.get(0))));
    }


    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testAssignStudentsToCourseThrowsConflictIfStudentReachedCoursesLimit(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> studentIds = Collections.singletonList(userCourseServiceTestPayload.getStudent().getId());
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();
        List<CourseEntity> randomCourses = CourseServiceTestData.generateCourseEntitiesOfCount(5);

        randomCourses.forEach(courseEntity -> CourseServiceTestUtils.save(jdbcTemplate, courseEntity));
        List<UserCourseEntity> studentCourses = randomCourses.stream().map(courseEntity -> {
            UserCourseEntity userCourseEntity = new UserCourseEntity();
            userCourseEntity.setId(new UserCourseCompositeId(courseEntity, userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getUser()));
            userCourseEntity.setStatus(UserCourseStatus.STARTED);
            return userCourseEntity;
        })
                .collect(Collectors.toList());

        CourseServiceTestUtils.save(jdbcTemplate, userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getCourse());
        UserServiceTestUtils.save(userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getUser(), peachyMindAuzJdbcTemplate);
        studentCourses.forEach(studentCourse -> UserCourseServiceTestUtils.save(studentCourse, jdbcTemplate));

        when(defaultCourseService.existsById(anyString()))
                .thenReturn(true);
        when(defaultUserService.existAllByIdsAndAuthority(anyList(), any(UserAuthority.class)))
                .thenReturn(true);

        //TEST
        Assertions.assertThrows(ConflictException.class, () -> defaultUserCourseService.assignStudentsToCourse(studentIds, courseId));

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .existAllByIdsAndAuthority(anyList(), any(UserAuthority.class));
        verify(defaultUserService)
                .existAllByIdsAndAuthority(stringsCaptor.capture(), any(UserAuthority.class));

        verify(defaultCourseService, atLeastOnce())
                .existsById(anyString());
        verify(defaultCourseService)
                .existsById(stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(courseId, stringCaptor.getValue());
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(strings -> strings.contains(studentIds.get(0))));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindCourseMembers(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        CourseEntity course = userCourseServiceTestPayload.getInstructorUserCourseEntity().getId().getCourse();
        UserEntity user = userCourseServiceTestPayload.getInstructorUserCourseEntity().getId().getUser();
        String courseId = course.getId();
        UserServiceTestUtils.save(user, peachyMindAuzJdbcTemplate);
        CourseServiceTestUtils.save(jdbcTemplate, course);
        UserCourseServiceTestUtils.save(userCourseServiceTestPayload.getInstructorUserCourseEntity(), jdbcTemplate);

        when(defaultUserService.findAllByIds(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getInstructor()));
        when(defaultCourseService.findAllByIds(anyList()))
                .thenReturn(Collections.singletonList(userCourseServiceTestPayload.getCourseDTO()));

        //TEST
        List<UserDTO> userDTOS = defaultUserCourseService.findCourseMembers(courseId, UserAuthority.INSTRUCTOR);

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .findAllByIds(anyList());
        verify(defaultUserService)
                .findAllByIds(stringsCaptor.capture());
        verify(defaultCourseService, atLeastOnce())
                .findAllByIds(anyList());
        verify(defaultCourseService)
                .findAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertEquals(1, userDTOS.size());
        Assertions.assertEquals(user.getId(), userDTOS.get(0).getId());
        Assertions.assertTrue(stringsCaptor.getAllValues().get(0).contains(userCourseServiceTestPayload.getInstructor().getId()));
        Assertions.assertTrue(stringsCaptor.getAllValues().get(1).contains(userCourseServiceTestPayload.getCourseDTO().getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindCourseMembersReturnsEmptyListIfThereAreNoCourseMembers(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();
        when(defaultUserService.findAllByIds(anyList()))
                .thenReturn(Collections.emptyList());

        //TEST
        List<UserDTO> userDTOS = defaultUserCourseService.findCourseMembers(courseId, UserAuthority.INSTRUCTOR);

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .findAllByIds(anyList());
        verify(defaultUserService)
                .findAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertTrue(userDTOS.isEmpty());
        Assertions.assertTrue(stringsCaptor.getValue().isEmpty());
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindCourseMemberIds(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();
        List<String> memberIds = Stream.of(
                userCourseServiceTestPayload.getInstructorUserCourseEntity().getId().getUser().getId(),
                userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getUser().getId())
                .collect(Collectors.toList());

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(userCourseServiceTestPayload.getExpandedCourseDTO()));
        UserServiceTestUtils.save(userCourseServiceTestPayload.getInstructorUserCourseEntity().getId().getUser(), peachyMindAuzJdbcTemplate);
        UserServiceTestUtils.save(userCourseServiceTestPayload.getStudentUserCourseEntity().getId().getUser(), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(userCourseServiceTestPayload.getInstructorUserCourseEntity(), jdbcTemplate);
        UserCourseServiceTestUtils.save(userCourseServiceTestPayload.getStudentUserCourseEntity(), jdbcTemplate);

        //TEST
        List<String> courseMemberIds = defaultUserCourseService.findCourseMemberIds(courseId);

        //ASSERT
        Assertions.assertNotNull(courseMemberIds);
        Assertions.assertTrue(courseMemberIds.containsAll(memberIds));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindCourseMemberIdsReturnsEmptyListIfThereIsNoMembersForGivenCourse(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        String courseId = userCourseServiceTestPayload.getExpandedCourseDTO().getId();

        //TEST
        Assertions.assertTrue(defaultUserCourseService.findCourseMemberIds(courseId).isEmpty());
    }


    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindUserCoursesCount(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> userIds = Stream.of(
                userCourseServiceTestPayload.getInstructor().getId(),
                userCourseServiceTestPayload.getStudent().getId())
                .collect(Collectors.toList());
        List<UserCourseEntity> userCourseEntities = Stream.of(
                userCourseServiceTestPayload.getStudentUserCourseEntity(),
                userCourseServiceTestPayload.getInstructorUserCourseEntity())
                .collect(Collectors.toList());
        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(userCourseServiceTestPayload.getExpandedCourseDTO()));
        userCourseEntities.stream()
                .map(userCourseEntity -> userCourseEntity.getId().getUser())
                .forEach(userEntity -> UserServiceTestUtils.save(userEntity, peachyMindAuzJdbcTemplate));
        userCourseEntities.forEach(userCourseEntity -> UserCourseServiceTestUtils.save(userCourseEntity, jdbcTemplate));

        //TEST
        Map<String, Long> userCoursesCount = defaultUserCourseService.findUserCoursesCount(userIds);


        //ASSERT
        Assertions.assertNotNull(userCoursesCount);
        Assertions.assertTrue(userCoursesCount.containsKey(userIds.get(0)));
        Assertions.assertTrue(userCoursesCount.containsKey(userIds.get(1)));
        Assertions.assertEquals(1, userCoursesCount.get(userIds.get(0)));
        Assertions.assertEquals(1, userCoursesCount.get(userIds.get(1)));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindUserCoursesCountReturnsZeroCountIfUserHasNoAnyCourses(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        List<String> userIds = Stream.of(
                userCourseServiceTestPayload.getInstructor().getId(),
                userCourseServiceTestPayload.getStudent().getId())
                .collect(Collectors.toList());
        List<UserCourseEntity> userCourseEntities = Stream.of(
                userCourseServiceTestPayload.getStudentUserCourseEntity(),
                userCourseServiceTestPayload.getInstructorUserCourseEntity())
                .collect(Collectors.toList());
        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(userCourseServiceTestPayload.getExpandedCourseDTO()));
        userCourseEntities.stream()
                .map(userCourseEntity -> userCourseEntity.getId().getUser())
                .forEach(userEntity -> UserServiceTestUtils.save(userEntity, peachyMindAuzJdbcTemplate));
        UserCourseServiceTestUtils.save(userCourseServiceTestPayload.getStudentUserCourseEntity(), jdbcTemplate);


        //TEST
        Map<String, Long> userCoursesCount = defaultUserCourseService.findUserCoursesCount(userIds);

        //VERIFY

        //ASSERT
        Assertions.assertNotNull(userCoursesCount);
        Assertions.assertTrue(userCoursesCount.containsKey(userIds.get(0)));
        Assertions.assertTrue(userCoursesCount.containsKey(userIds.get(1)));
        Assertions.assertEquals(0, userCoursesCount.get(userIds.get(0)));
        Assertions.assertEquals(1, userCoursesCount.get(userIds.get(1)));
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testExistsByLessonIdAndUserIdReturnsTrueIfThereIsAUserCourseForGivenLessonIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        LessonDTO lessonDTO = LessonServiceTestData.getLessonDTO();
        UserCourseDTO userCourseDTO = userCourseServiceTestPayload.getStudentUserCourseDTO();
        UserDTO userDTO = userCourseDTO.getUser();
        lessonDTO.setCourse(CourseServiceTestData.convertToCourseDTOFrom(userCourseDTO.getCourse()));

        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate);
        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(userCourseDTO.getCourse()));
        LessonServiceTestUtils.save(LessonServiceTestData.convertToLessonEntityFrom(lessonDTO), jdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        //TEST
        boolean exists = defaultUserCourseService.existsByLessonIdAndUserId(lessonDTO.getId(), userDTO.getId());

        //ASSERT
        Assertions.assertTrue(exists);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testExistsByLessonIdAndUserIdReturnsFalseIfThereIsNoUserCourseForGivenLessonIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        LessonDTO lessonDTO = LessonServiceTestData.getLessonDTO();
        UserCourseDTO userCourseDTO = userCourseServiceTestPayload.getStudentUserCourseDTO();
        UserDTO userDTO = userCourseDTO.getUser();
        lessonDTO.setCourse(CourseServiceTestData.convertToCourseDTOFrom(userCourseDTO.getCourse()));

        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate);
        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(userCourseDTO.getCourse()));
        LessonServiceTestUtils.save(LessonServiceTestData.convertToLessonEntityFrom(lessonDTO), jdbcTemplate);

        //TEST
        boolean exists = defaultUserCourseService.existsByLessonIdAndUserId(lessonDTO.getId(), userDTO.getId());

        //ASSERT
        Assertions.assertFalse(exists);
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testUpdateStatusByCourseIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        UserCourseDTO userCourseDTO = userCourseServiceTestPayload.getStudentUserCourseDTO();
        UserDTO userDTO = userCourseDTO.getUser();
        CourseDTO courseDTO = CourseServiceTestData.convertToCourseDTOFrom(userCourseDTO.getCourse());
        UserCourseStatus userCourseStatus = UserCourseStatus.IN_PROGRESS;
        userCourseDTO.setStatus(UserCourseStatus.STARTED);

        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate);
        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(courseDTO));
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        //TEST
        defaultUserCourseService.updateStatusByCourseIdAndUserId(courseDTO.getId(), userDTO.getId(), userCourseStatus);


        //ASSERT
        Optional<UserCourseDTO> userCourseById = UserCourseServiceTestUtils.findById(new UserCourseCompositeId(userCourseDTO.getUserId(), userCourseDTO.getCourseId()), jdbcTemplate);
        Assertions.assertTrue(userCourseById.isPresent());
        Assertions.assertEquals(userCourseStatus, userCourseById.get().getStatus());
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindStatusByCourseIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        UserCourseDTO userCourseDTO = userCourseServiceTestPayload.getStudentUserCourseDTO();
        UserDTO userDTO = userCourseDTO.getUser();
        CourseDTO courseDTO = CourseServiceTestData.convertToCourseDTOFrom(userCourseDTO.getCourse());
        UserCourseStatus userCourseStatus = UserCourseStatus.STARTED;
        userCourseDTO.setStatus(userCourseStatus);

        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate);
        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(courseDTO));
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        //TEST
        Optional<UserCourseStatus> retrievedUserCourseStatus = defaultUserCourseService.findStatusByCourseIdAndUserId(courseDTO.getId(), userDTO.getId());

        //ASSERT
        Assertions.assertTrue(retrievedUserCourseStatus.isPresent());
        Assertions.assertEquals(userCourseStatus, retrievedUserCourseStatus.get());
    }

    @ParameterizedTest
    @ArgumentsSource(UserCourseServiceTestArgumentProvider.class)
    public void testFindStatusByCourseIdAndUserIdReturnsEmptyOptionalIfThereIsNoUserCourseStatusForGivenCourseIdAndUserId(UserCourseServiceTestPayload userCourseServiceTestPayload) {
        //SETUP
        UserCourseDTO userCourseDTO = userCourseServiceTestPayload.getStudentUserCourseDTO();
        UserDTO userDTO = userCourseDTO.getUser();
        CourseDTO courseDTO = CourseServiceTestData.convertToCourseDTOFrom(userCourseDTO.getCourse());

        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userDTO), peachyMindAuzJdbcTemplate);
        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(courseDTO));

        //TEST
        Optional<UserCourseStatus> retrievedUserCourseStatus = defaultUserCourseService.findStatusByCourseIdAndUserId(courseDTO.getId(), userDTO.getId());

        //ASSERT
        Assertions.assertFalse(retrievedUserCourseStatus.isPresent());
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
    }
}
