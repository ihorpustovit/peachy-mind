package integration.com.peachy.service.course;

import argumentsprovider.com.peachy.sevice.course.CourseServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.course.CourseServiceTestPayload;
import com.peachy.converter.lesson.LessonFromBaseDTOConverter;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.dto.usercourse.UsersPerCourseDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.course.DefaultCourseService;
import com.peachy.service.lesson.LessonService;
import com.peachy.service.usercourse.UserCourseService;
import com.peachy.test.data.UserServiceTestUtils;
import integration.com.peachy.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.TestProperties;
import testdata.UserServiceTestData;
import testdata.course.CourseServiceTestData;
import testdata.course.CourseServiceTestUtils;
import testdata.usercourse.UserCourseServiceTestData;
import testdata.usercourse.UserCourseServiceTestUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class DefaultCourseServiceTest extends AbstractIntegrationTest {

    @MockBean
    private LessonService defaultLessonService;

    @MockBean
    private LessonFromBaseDTOConverter fromLessonBaseDTOConverter;

    @MockBean
    private UserService defaultUserService;

    @MockBean
    private UserCourseService defaultUserCourseService;

    @Autowired
    private DefaultCourseService defaultCourseService;

    @Autowired
    @Qualifier("peachyMindAuzJdbcTemplate")
    private JdbcTemplate peachyMindAuzJdbcTemplate;

    @Captor
    private ArgumentCaptor<List<String>> stringsCaptor;

    @Captor
    private ArgumentCaptor<CourseEntity> courseEntityCaptor;

    @Captor
    private ArgumentCaptor<List<LessonCreationDTO>> lessonCreationsCaptor;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testSave(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        when(defaultUserService.findAllByIds(Mockito.anyList()))
                .thenReturn(courseServiceTestPayload.getExpandedCourseDTO().getInstructors());
        when(defaultUserService.existAllByIds(Mockito.anyList()))
                .thenReturn(true);
        when(fromLessonBaseDTOConverter.toLessonCreationFromBase(Mockito.any(LessonDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonCreationsDTOs().get(0));
        when(defaultLessonService.saveAll(Mockito.anyList()))
                .thenReturn(courseServiceTestPayload.getLessonsDTOs());
        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(Mockito.any(LessonDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));
        when(defaultUserCourseService.assignInstructorsToCourse(Mockito.anyList(), anyString()))
                .thenReturn(Collections.singletonList(courseServiceTestPayload.getInstructorUserCourseDTO()));

        //TEST
        ExpandedCourseDTO expandedCourseDTO = defaultCourseService.save(courseServiceTestPayload.getCourseCreationDTO());

        //VERIFY
        verify(defaultUserService, atMostOnce())
                .findAllByIds(Mockito.anyList());
        verify(defaultUserService).findAllByIds(stringsCaptor.capture());
        verify(defaultUserService, atMostOnce())
                .existAllByIds(Mockito.anyList());
        verify(fromLessonBaseDTOConverter, Mockito.times(courseServiceTestPayload.getExpandedCourseDTO().getLessons().size()))
                .toLessonCreationFromBase(Mockito.any(LessonDTO.class));
        verify(fromLessonBaseDTOConverter, Mockito.times(courseServiceTestPayload.getExpandedCourseDTO().getLessons().size()))
                .toLessonInCourseFromBase(Mockito.any(LessonDTO.class));
        verify(defaultLessonService, atMostOnce())
                .saveAll(Mockito.anyList());
        verify(defaultLessonService).saveAll(lessonCreationsCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .assignInstructorsToCourse(Mockito.anyList(), anyString());
        verify(defaultUserCourseService)
                .assignInstructorsToCourse(stringsCaptor.capture(), stringCaptor.capture());

        //ASSERT
        Assertions.assertEquals(courseServiceTestPayload.getCourseCreationDTO().getInstructorIds(), stringsCaptor.getAllValues().get(0));
        Assertions.assertTrue(lessonCreationsCaptor.getValue().stream().allMatch(lessonCreationDTO ->
                Objects.equals(lessonCreationDTO, courseServiceTestPayload.getLessonCreationsDTOs().get(0))));
        Assertions.assertNotNull(expandedCourseDTO.getId());
        Assertions.assertTrue(CourseServiceTestUtils.existsById(expandedCourseDTO.getId(), jdbcTemplate));
        Assertions.assertEquals(courseServiceTestPayload.getCourseCreationDTO().getInstructorIds(), stringsCaptor.getAllValues().get(1));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testFindByIdReturnsEmptyOptionalIfThereIsNoCourseForGivenId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        String courseId = courseServiceTestPayload.getCourseEntity().getId();
        when(defaultLessonService.findAllByCourseIds(anyList()))
                .thenReturn(Collections.emptyList());
        when(defaultUserCourseService.findAllInstructorsByCourseIds(anyList()))
                .thenReturn(Collections.emptyList());
        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));

        //TEST
        Optional<ExpandedCourseDTO> courseDTO = defaultCourseService.findExpandedById(courseId);

        //VERIFY
        verify(defaultLessonService, atMostOnce())
                .findAllByCourseIds(anyList());
        verify(defaultLessonService)
                .findAllByCourseIds(stringsCaptor.capture());

        verify(defaultUserCourseService, atMostOnce())
                .findAllInstructorsByCourseIds(anyList());
        verify(defaultUserCourseService)
                .findAllInstructorsByCourseIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertFalse(courseDTO.isPresent());
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(capturedCourseIds -> capturedCourseIds.contains(courseId)));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testFindAllByIds(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        CourseServiceTestUtils.save(jdbcTemplate, courseServiceTestPayload.getCourseEntity());
        List<String> courseIds = Collections.singletonList(courseServiceTestPayload.getCourseEntity().getId());
        Map<String, List<LessonDTO>> courseIdLessonDTOs = Collections.singletonMap(courseServiceTestPayload.getCourseEntity().getId(), courseServiceTestPayload.getLessonsDTOs());

        when(defaultLessonService.findAllByCourseIds(anyList()))
                .thenReturn(Collections.singletonList(new LessonsPerCourseDTO(courseServiceTestPayload.getCourseDTO(), courseServiceTestPayload.getLessonInCourseDTOs())));
        when(defaultUserCourseService.findAllInstructorsByCourseIds(anyList()))
                .thenReturn(Collections.singletonList(new UsersPerCourseDTO(courseServiceTestPayload.getCourseDTO(), Collections.singletonList(courseServiceTestPayload.getInstructorUserCourseDTO().getUser()))));
        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));

        //TEST
        List<ExpandedCourseDTO> foundExpandedCourseDTOS = defaultCourseService.findExpandedAllByIds(courseIds);

        //VERIFY
        verify(defaultLessonService, atMostOnce())
                .findAllByCourseIds(anyList());
        verify(defaultLessonService)
                .findAllByCourseIds(stringsCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .findAllInstructorsByCourseIds(anyList());
        verify(defaultUserCourseService)
                .findAllInstructorsByCourseIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(stringsList -> stringsList.containsAll(courseIds)));
        Assertions.assertEquals(courseIds.get(0), courseServiceTestPayload.getExpandedCourseDTO().getId());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testFindAllByIdsReturnsEmptyListIfThereIsNoCoursesForGivenIds(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        List<String> courseIds = Collections.singletonList(courseServiceTestPayload.getCourseEntity().getId());
//        Map<String, List<LessonDTO>> courseIdLessonDTOs = Collections.singletonMap(courseServiceTestPayload.getCourseEntity().getId(), );

        when(defaultLessonService.findAllByCourseIds(anyList()))
                .thenReturn(Collections.singletonList(new LessonsPerCourseDTO(courseServiceTestPayload.getCourseDTO(), courseServiceTestPayload.getLessonInCourseDTOs())));
        when(defaultUserCourseService.findAllInstructorsByCourseIds(anyList()))
                .thenReturn(Collections.singletonList(new UsersPerCourseDTO(courseServiceTestPayload.getCourseDTO(), Collections.singletonList(courseServiceTestPayload.getInstructorUserCourseDTO().getUser()))));
        when(fromLessonBaseDTOConverter.toLessonInCourseFromBase(any(LessonDTO.class)))
                .thenReturn(courseServiceTestPayload.getLessonInCourseDTOs().get(0));

        //TEST
        List<ExpandedCourseDTO> foundExpandedCourseDTOS = defaultCourseService.findExpandedAllByIds(courseIds);

        //VERIFY
        verify(defaultLessonService, atMostOnce())
                .findAllByCourseIds(anyList());
        verify(defaultLessonService)
                .findAllByCourseIds(stringsCaptor.capture());
        verify(defaultUserCourseService, atMostOnce())
                .findAllInstructorsByCourseIds(anyList());
        verify(defaultUserCourseService)
                .findAllInstructorsByCourseIds(stringsCaptor.capture());

        verifyNoInteractions(fromLessonBaseDTOConverter);

        //ASSERT
        Assertions.assertTrue(stringsCaptor.getAllValues().stream().allMatch(stringsList -> stringsList.containsAll(courseIds)));
        Assertions.assertTrue(foundExpandedCourseDTOS.isEmpty());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testExistsByIdReturnsTrueIfThereIsACourseForGivenId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        String courseId = courseServiceTestPayload.getCourseEntity().getId();
        CourseServiceTestUtils.save(jdbcTemplate, courseServiceTestPayload.getCourseEntity());

        //TEST
        Assertions.assertTrue(defaultCourseService.existsById(courseId));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void testExistsByIdReturnsFalseIfThereIsNoCourseForGivenId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        String courseId = courseServiceTestPayload.getCourseEntity().getId();

        //TEST
        Assertions.assertFalse(defaultCourseService.existsById(courseId));
    }

    @Test
    public void testExistAllByIdsReturnsTrueIfThereAreCoursesForGivenIds() {
        //SETUP
        List<CourseEntity> courseEntities = CourseServiceTestData.generateCourseEntitiesOfCount(3);
        courseEntities.forEach(courseEntity -> CourseServiceTestUtils.save(jdbcTemplate, courseEntity));
        List<String> courseIds = courseEntities.stream().map(CourseEntity::getId).collect(Collectors.toList());

        //TEST
        Assertions.assertTrue(defaultCourseService.existAllByIds(courseIds));
    }

    @Test
    public void testExistAllByIdsReturnsFalseIfThereIsNoCourseForGivenId() {
        //SETUP
        List<String> courseIds = Stream.generate(() -> TestProperties.randomString(12)).limit(2).collect(Collectors.toList());

        //TEST
        Assertions.assertFalse(defaultCourseService.existAllByIds(courseIds));
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void findAllByUserId(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(userCourseDTO.getCourse()));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.save(UserCourseServiceTestData.convertToUserCourseEntity(userCourseDTO), jdbcTemplate);

        //TEST
        List<CourseDTO> courseDTOS = defaultCourseService.findAllByUserId(userCourseDTO.getUserId());

        //ASSERT
        Assertions.assertNotNull(courseDTOS);
        Assertions.assertEquals(1, courseDTOS.size());
        Assertions.assertEquals(userCourseDTO.getCourse().getId(), courseDTOS.get(0).getId());
    }

    @ParameterizedTest
    @ArgumentsSource(CourseServiceTestArgumentProvider.class)
    public void findAllByUserIdReturnsEmptyListIfThereIsNoCoursesForGiveUser(CourseServiceTestPayload courseServiceTestPayload) {
        //SETUP
        UserCourseDTO userCourseDTO = courseServiceTestPayload.getStudentUserCourseDTO();

        CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(userCourseDTO.getCourse()));
        UserServiceTestUtils.save(UserServiceTestData.convertToUserEntity(userCourseDTO.getUser()), peachyMindAuzJdbcTemplate);

        //TEST
        Assertions.assertTrue(defaultCourseService.findAllByUserId(userCourseDTO.getUserId()).isEmpty());
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        UserServiceTestUtils.truncateUsers(peachyMindAuzJdbcTemplate);
        UserCourseServiceTestUtils.truncateUserCourse(jdbcTemplate);
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
    }
}
