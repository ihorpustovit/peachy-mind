package integration.com.peachy.service.lesson;

import argumentsprovider.com.peachy.sevice.lesson.LessonServiceTestArgumentProvider;
import argumentsprovider.com.peachy.sevice.lesson.LessonServiceTestPayload;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.entity.LessonEntity;
import com.peachy.service.course.CourseService;
import com.peachy.service.lesson.LessonService;
import integration.com.peachy.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.course.CourseServiceTestData;
import testdata.course.CourseServiceTestUtils;
import testdata.lesson.LessonServiceTestUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

public class DefaultLessonServiceTest extends AbstractIntegrationTest {

    @MockBean
    private CourseService defaultCourseService;

    @Autowired
    private LessonService defaultLessonService;

    @Captor
    private ArgumentCaptor<List<String>> stringsCaptor;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {

    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testSaveAll(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        lessonServiceTestPayload.getLessonDTOS().forEach(lessonDTO -> lessonDTO.setCourse(null));
        when(defaultCourseService.existAllByIds(anyList()))
                .thenReturn(true);
        when(defaultCourseService.findAllByIds(anyList()))
                .thenReturn(lessonServiceTestPayload.getCourseDTOS());
        lessonServiceTestPayload.getExpandedCourseDTOS().forEach(courseDTO -> CourseServiceTestUtils.save(jdbcTemplate, CourseServiceTestData.convertToCourseEntityFrom(courseDTO)));

        //TEST
        List<LessonDTO> lessonDTOs = defaultLessonService.saveAll(lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses());

        //VERIFY
        verify(defaultCourseService, atLeastOnce())
                .existAllByIds(anyList());
        verify(defaultCourseService)
                .existAllByIds(stringsCaptor.capture());
        verify(defaultCourseService, atLeastOnce())
                .findAllByIds(anyList());
        verify(defaultCourseService)
                .findAllByIds(stringsCaptor.capture());

        //ASSERT
        Assertions.assertEquals(stringsCaptor.getAllValues().get(0).size(), 2);
        Assertions.assertTrue(
                lessonServiceTestPayload.getExpandedCourseDTOS().stream().map(ExpandedCourseDTO::getId).collect(Collectors.toList()).containsAll(stringsCaptor.getAllValues().get(0)));
        Assertions.assertEquals(stringsCaptor.getAllValues().get(1).size(), 2);
        Assertions.assertTrue(
                lessonServiceTestPayload.getExpandedCourseDTOS().stream().map(ExpandedCourseDTO::getId).collect(Collectors.toList()).containsAll(stringsCaptor.getAllValues().get(1)));
        Assertions.assertEquals(lessonDTOs.get(0).getCourse().getId(), lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(0).getCourseId());
        Assertions.assertEquals(lessonDTOs.get(1).getCourse().getId(), lessonServiceTestPayload.getLessonCreationsDTOForDifferentCourses().get(1).getCourseId());
        Assertions.assertTrue(LessonServiceTestUtils.existAllByIds(lessonDTOs.stream().map(LessonDTO::getId).collect(Collectors.toList()), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByIds(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        LessonEntity lessonEntity = lessonServiceTestPayload.getLessonEntities().get(0);
        List<String> lessonIds = Collections.singletonList(lessonEntity.getId());
        LessonServiceTestUtils.save(lessonServiceTestPayload.getLessonEntities().get(0), jdbcTemplate);

        //TEST
        List<LessonDTO> lessonDTOs = defaultLessonService.findAllByIds(lessonIds);

        //VERIFY
        Assertions.assertEquals(1, lessonDTOs.size());
        Assertions.assertEquals(lessonIds.get(0), lessonDTOs.get(0).getId());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByIdsReturnsEmptyListIfThereAreNoLessonsFoGivenIds(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        List<String> lessonIds = Collections.singletonList(lessonServiceTestPayload.getLessonDTOS().get(0).getId());

        //TEST AND ASSERT
        Assertions.assertTrue(defaultLessonService.findAllByIds(lessonIds).isEmpty());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByCourseId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        LessonEntity lessonEntity = lessonServiceTestPayload.getLessonEntities().get(0);
        String courseId = lessonEntity.getCourse().getId();
        LessonServiceTestUtils.save(lessonEntity, jdbcTemplate);

        //TEST
        LessonsPerCourseDTO lessonsPerCourseDTO = defaultLessonService.findAllByCourseId(courseId);

        //VERIFY
        Assertions.assertEquals(1, lessonsPerCourseDTO.getLessons().size());
        Assertions.assertEquals(lessonServiceTestPayload.getLessonEntities().get(0).getId(), lessonsPerCourseDTO.getLessons().get(0).getId());

    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByCourseIdThrowsNotFoundExceptionIfNoLessonsFoundForGivenCourseId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String courseId = lessonServiceTestPayload.getExpandedCourseDTOS().get(0).getId();

        //TEST AND ASSERT
        Assertions.assertThrows(NotFoundException.class, () -> defaultLessonService.findAllByCourseId(courseId));
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByCourseIds(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String courseId = lessonServiceTestPayload.getLessonEntities().get(0).getCourse().getId();

        LessonServiceTestUtils.save(lessonServiceTestPayload.getLessonEntities().get(0), jdbcTemplate);

        //TEST
        List<LessonsPerCourseDTO> lessonsPerCourseDTOS = defaultLessonService.findAllByCourseIds(Collections.singletonList(courseId));

        //ASSERT
        Assertions.assertNotNull(lessonsPerCourseDTOS);
        Assertions.assertEquals(1, lessonsPerCourseDTOS.size());
        Assertions.assertEquals(courseId, lessonsPerCourseDTOS.get(0).getCourse().getId());
        Assertions.assertEquals(1, lessonsPerCourseDTOS.get(0).getLessons().size());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindAllByCourseIdsReturnsEmptyListIfThereIsNoLessonsForGivenCourse(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String courseId = lessonServiceTestPayload.getCourseDTOS().get(0).getId();


        //TEST
        Assertions.assertTrue(defaultLessonService.findAllByCourseIds(Collections.singletonList(courseId)).isEmpty());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindById(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String lessonId = lessonServiceTestPayload.getLessonEntities().get(0).getId();

        LessonServiceTestUtils.save(lessonServiceTestPayload.getLessonEntities().get(0), jdbcTemplate);

        //TEST
        Optional<LessonDTO> lessonById = defaultLessonService.findById(lessonId);


        //ASSERT
        Assertions.assertTrue(lessonById.isPresent());
        Assertions.assertEquals(lessonId, lessonById.get().getId());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testFindByIdReturnsEmptyOptionalIfThereIsNoLessonForGivenId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String lessonId = lessonServiceTestPayload.getLessonEntities().get(0).getId();

        //TEST
        Optional<LessonDTO> lessonById = defaultLessonService.findById(lessonId);


        //ASSERT
        Assertions.assertFalse(lessonById.isPresent());
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testExistsByIdReturnsTrueIfLessonExistsForGivenId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String lessonId = lessonServiceTestPayload.getLessonDTOS().get(0).getId();

        LessonServiceTestUtils.save(lessonServiceTestPayload.getLessonEntities().get(0), jdbcTemplate);

        //TEST
        boolean exists = defaultLessonService.existsById(lessonId);


        //ASSERT
        Assertions.assertTrue(exists);
    }

    @ParameterizedTest
    @ArgumentsSource(LessonServiceTestArgumentProvider.class)
    public void testExistsByIdReturnsFalseIfLessonDoesNotExistForGivenId(LessonServiceTestPayload lessonServiceTestPayload) {
        //SETUP
        String lessonId = lessonServiceTestPayload.getLessonDTOS().get(0).getId();

        //TEST
        boolean exists = defaultLessonService.existsById(lessonId);


        //ASSERT
        Assertions.assertFalse(exists);
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        LessonServiceTestUtils.truncateLessons(jdbcTemplate);
        CourseServiceTestUtils.truncateCourses(jdbcTemplate);
    }
}
