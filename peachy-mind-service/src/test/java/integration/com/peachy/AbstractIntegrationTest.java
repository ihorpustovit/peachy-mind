package integration.com.peachy;

import com.peachy.PeachyMindService;
import com.peachy.test.AbstractJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = PeachyMindService.class)
@ActiveProfiles("standalone-test")
public abstract class AbstractIntegrationTest extends AbstractJpaTest {
}
