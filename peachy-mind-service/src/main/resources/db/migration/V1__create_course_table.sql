CREATE TABLE course(
    id VARCHAR(12) NOT NULL PRIMARY KEY,
    title VARCHAR(80) NOT NULL,
    short_description VARCHAR(500),
    created_at DATETIME,
    updated_at DATETIME
);