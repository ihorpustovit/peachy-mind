ALTER TABLE
    homework
DROP FOREIGN KEY
    FK_user_homework;

ALTER TABLE
    homework
DROP FOREIGN KEY
    FK_lesson_homework;

ALTER TABLE
    homework
ADD CONSTRAINT
    FK_user_homework
FOREIGN KEY
    homework(user_id)
REFERENCES
    peachy_mind_auz.user(id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE
    homework
ADD CONSTRAINT
    FK_lesson_homework
FOREIGN KEY
    homework(lesson_id)
REFERENCES
    lesson(id)
ON DELETE CASCADE
ON UPDATE CASCADE;

