ALTER TABLE user_course DROP COLUMN id;
ALTER TABLE user_course ADD CONSTRAINT PK_user_course PRIMARY KEY (user_id,course_id);
ALTER TABLE user_course ADD COLUMN instructor_feedback VARCHAR(1000) DEFAULT '';