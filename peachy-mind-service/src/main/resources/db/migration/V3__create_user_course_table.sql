CREATE TABLE user_course(
    id VARCHAR(12) NOT NULL PRIMARY KEY,
    user_id VARCHAR(12),
    course_id VARCHAR(12),
    created_at DATETIME,
    updated_at DATETIME
);

ALTER TABLE
    user_course
ADD CONSTRAINT
    FK_user_user_course
FOREIGN KEY
    user_course(user_id)
REFERENCES
    peachy_mind_auz.user(id)
ON DELETE CASCADE;

ALTER TABLE
    user_course
ADD CONSTRAINT
    FK_course_user_course
FOREIGN KEY
    user_course(course_id)
REFERENCES
    course(id)
ON DELETE CASCADE;
