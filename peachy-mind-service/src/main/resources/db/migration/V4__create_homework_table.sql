CREATE TABLE homework(
    user_id VARCHAR(12) NOT NULL,
    lesson_id VARCHAR(12) NOT NULL,
    s3Key VARCHAR(766) NOT NULL,
    s3Bucket VARCHAR(766) NOT NULL,
    file_name VARCHAR(255),
    created_at DATETIME,
    updated_at DATETIME,
    PRIMARY KEY (user_id,lesson_id)
);

ALTER TABLE
    homework
ADD CONSTRAINT
    FK_user_homework
FOREIGN KEY
    homework(user_id)
REFERENCES
    peachy_mind_auz.user(id);

ALTER TABLE
    homework
ADD CONSTRAINT
    FK_lesson_homework
FOREIGN KEY
    homework(lesson_id)
REFERENCES
    lesson(id);