ALTER TABLE
    user_course
DROP FOREIGN KEY
    FK_user_user_course;

ALTER TABLE
    user_course
DROP FOREIGN KEY
    FK_course_user_course;

ALTER TABLE
    user_course
ADD CONSTRAINT
    FK_user_user_course
FOREIGN KEY
    user_course(user_id)
REFERENCES
    peachy_mind_auz.user(id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE
    user_course
ADD CONSTRAINT
    FK_course_user_course
FOREIGN KEY
    user_course(course_id)
REFERENCES
    course(id)
ON DELETE CASCADE
ON UPDATE CASCADE;