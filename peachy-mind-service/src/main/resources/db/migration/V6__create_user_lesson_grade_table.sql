CREATE TABLE user_lesson_grade(
    lesson_id VARCHAR(12) NOT NULL,
    user_id VARCHAR(12) NOT NULL,
    instructor_id VARCHAR(12) DEFAULT 'N/A',
    grade INT NOT NULL,
    created_at DATETIME,
    updated_at DATETIME,
    PRIMARY KEY (lesson_id,user_id)
);

ALTER TABLE
    user_lesson_grade
ADD CONSTRAINT
    FK_user_user_lesson_grade
FOREIGN KEY
    user_lesson_grade(user_id)
REFERENCES
    peachy_mind_auz.user(id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE
    user_lesson_grade
ADD CONSTRAINT
    FK_lesson_user_lesson_grade
FOREIGN KEY
    user_lesson_grade(lesson_id)
REFERENCES
    lesson(id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE
    user_lesson_grade
ADD CONSTRAINT
    FK_instructor_user_lesson_grade
FOREIGN KEY
    user_lesson_grade(instructor_id)
REFERENCES
    peachy_mind_auz.user(id)
ON DELETE CASCADE
ON UPDATE CASCADE;