CREATE TABLE lesson(
    id VARCHAR(12) NOT NULL PRIMARY KEY,
    title VARCHAR(80) NOT NULL,
    short_description VARCHAR(500),
    content LONGTEXT NOT NULL,
    course_id VARCHAR(12),
    created_at DATETIME,
    updated_at DATETIME
);

ALTER TABLE
    lesson
ADD CONSTRAINT
    FK_course_lesson
FOREIGN KEY
    lesson(course_id)
REFERENCES
    course(id)
ON DELETE CASCADE;