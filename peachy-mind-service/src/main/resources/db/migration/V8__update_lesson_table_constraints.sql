ALTER TABLE
    lesson
DROP FOREIGN KEY
    FK_course_lesson;

ALTER TABLE
    lesson
ADD CONSTRAINT
    FK_course_lesson
FOREIGN KEY
    lesson(course_id)
REFERENCES
    course(id)
ON DELETE CASCADE
ON UPDATE CASCADE;