package com.peachy.entity.usercourse;

import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.jpa.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "user_course")
@NoArgsConstructor
public class UserCourseEntity extends BaseEntity {
    @Id
    @EmbeddedId
    private UserCourseCompositeId id;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private UserCourseStatus status;

    @Column(name = "instructor_feedback")
    private String instructorFeedback;
}
