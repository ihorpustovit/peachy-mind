package com.peachy.entity.usercourse;

import com.peachy.entity.CourseEntity;
import com.peachy.security.entity.UserEntity;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Embeddable
public class UserCourseCompositeId implements Serializable {
    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    private CourseEntity course;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserEntity user;

    public UserCourseCompositeId(String userId,
                                 String courseId) {
        setCourseId(courseId);
        setUserId(userId);
    }

    public void setCourseId(String courseId) {
        if (Objects.isNull(course)) {
            course = new CourseEntity();
        }

        course.setId(courseId);
    }

    public void setUserId(String userId) {
        if (Objects.isNull(user)) {
            user = new UserEntity();
        }

        user.setId(userId);
    }

    public String getCourseId() {
        return Objects.nonNull(course) ? course.getId() : StringUtils.EMPTY;
    }

    public String getUserId() {
        return Objects.nonNull(user) ? user.getId() : StringUtils.EMPTY;
    }

    public String getCollapsedId() {
        boolean canIdBeCollapsed = Objects.nonNull(course) &&
                Objects.nonNull(user) &&
                StringUtils.isNotBlank(course.getId()) &&
                StringUtils.isNotBlank(user.getId());

        if (canIdBeCollapsed) {
            return getUser().getId() + getCourse().getId();
        } else {
            throw new IllegalStateException("Unable to collapse user and course ids. Some of them is empty");
        }
    }
}
