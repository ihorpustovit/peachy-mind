package com.peachy.entity;

import com.peachy.jpa.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "course")
@NoArgsConstructor
public class CourseEntity extends BaseEntity{
    @Id
    @GenericGenerator(name = "default_id_generation_seq", strategy = "com.peachy.jpa.DefaultIdGenerator")
    @GeneratedValue(generator = "default_id_generation_seq")
    private String id;

    @Column(name = "title")
    private String title;

    @Column(name = "short_description")
    private String shortDescription;
}
