package com.peachy.entity.homework;

import com.peachy.entity.LessonEntity;
import com.peachy.security.entity.UserEntity;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Embeddable
public class HomeworkCompositeId implements Serializable {
    @ManyToOne
    @JoinColumn(name = "lesson_id", referencedColumnName = "id")
    private LessonEntity lesson;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserEntity user;

    public HomeworkCompositeId(String lessonId,
                               String userId) {
        setLessonId(lessonId);
        setUserId(userId);
    }

    public void setLessonId(String lessonId) {
        if (Objects.isNull(lesson)) {
            lesson = new LessonEntity();
        }

        lesson.setId(lessonId);
    }

    public void setUserId(String userId) {
        if (Objects.isNull(user)) {
            user = new UserEntity();
        }

        user.setId(userId);
    }
}
