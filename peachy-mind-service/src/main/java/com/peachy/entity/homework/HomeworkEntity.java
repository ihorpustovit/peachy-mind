package com.peachy.entity.homework;

import com.peachy.jpa.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "homework")
@NoArgsConstructor
public class HomeworkEntity extends BaseEntity {
    @Id
    @EmbeddedId
    private HomeworkCompositeId id;

    @Column(name = "s3Key")
    private String s3Key;

    @Column(name = "s3Bucket")
    private String s3Bucket;

    @Column(name = "file_name")
    private String fileName;
}
