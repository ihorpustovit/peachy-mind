package com.peachy.entity;

import com.peachy.jpa.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "lesson")
@NoArgsConstructor
public class LessonEntity extends BaseEntity {
    @Id
    @GenericGenerator(name = "default_id_generation_seq", strategy = "com.peachy.jpa.DefaultIdGenerator")
    @GeneratedValue(generator = "default_id_generation_seq")
    private String id;

    @Column(name = "title")
    private String title;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "content")
    private String content;

    @Column(name = "max_grade")
    private Integer maxGrade;

    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    private CourseEntity course;

    public CourseEntity getCourse() {
        return course;
    }
}
