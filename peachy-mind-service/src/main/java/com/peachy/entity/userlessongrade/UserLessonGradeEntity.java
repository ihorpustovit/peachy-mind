package com.peachy.entity.userlessongrade;

import com.peachy.jpa.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "user_lesson_grade")
@NoArgsConstructor
public class UserLessonGradeEntity extends BaseEntity {
    @Id
    @EmbeddedId
    private UserLessonGradeCompositeId id;

    @Column(name = "instructor_id")
    private String instructorId;

    @Column(name = "grade")
    private Integer grade;
}
