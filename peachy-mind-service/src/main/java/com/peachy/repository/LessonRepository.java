package com.peachy.repository;

import com.peachy.dto.lesson.MaxGradeProjection;
import com.peachy.entity.LessonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LessonRepository extends JpaRepository<LessonEntity, String> {
    List<LessonEntity> findAllByCourseId(String courseId);

    List<LessonEntity> findAllByCourseIdIn(List<String> courseIds);

    boolean existsAllByIdIn(List<String> lessonIds);

    @Query("SELECT l.id as id, l.maxGrade as maxGrade FROM LessonEntity l WHERE l.id IN (:lessonIds)")
    List<MaxGradeProjection> findMaxGradesByLessonIds(@Param("lessonIds") List<String> lessonIds);
}
