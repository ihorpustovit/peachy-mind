package com.peachy.repository;

import com.peachy.dto.usercourse.ExpandedUserCourseProjection;
import com.peachy.dto.usercourse.UserCourseFeedbackDTO;
import com.peachy.dto.usercourse.UserCourseProjection;
import com.peachy.dto.usercourse.UserCourseTotalGradeProjection;
import com.peachy.entity.usercourse.UserCourseCompositeId;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserCourseRepository extends JpaRepository<UserCourseEntity, UserCourseCompositeId> {
    @Query(value = "SELECT user_id as userId,course_id as courseId FROM user_course uc INNER JOIN peachy_mind_auz.user u ON uc.user_id = u.id WHERE uc.course_id IN (:courseIds) AND u.authority in (:userAuthorities) ",
            nativeQuery = true)
    List<UserCourseProjection> findAllCoursesMembers(@Param("courseIds") List<String> courseIds,
                                                     @Param("userAuthorities") List<String> userAuthorities);

    @Query("SELECT uc.id.user.id FROM UserCourseEntity uc WHERE uc.id.course.id=:courseId")
    List<String> findAllCourseMembersIds(@Param("courseId") String courseId);

    @Query(value = "SELECT uc.course_id as courseId, uc.user_id as userId FROM user_course uc WHERE uc.user_id IN (:userIds)", nativeQuery = true)
    List<UserCourseProjection> findAllUserCourses(@Param("userIds") List<String> userIds);

    @Query(value = "SELECT uc.course_id as courseId, l.id as lessonId, uc.user_id as userId, u.authority as userAuthority FROM user_course uc INNER JOIN lesson l ON uc.course_id=l.course_id INNER JOIN peachy_mind_auz.user u ON u.id=uc.user_id WHERE uc.user_id IN (:userIds) AND l.id IN (:lessonIds)",
            nativeQuery = true)
    List<ExpandedUserCourseProjection> findAllUserCoursesByLessonIdsAndUserIds(@Param("lessonIds") List<String> lessonIds,
                                                                               @Param("userIds") List<String> userIds);

    @Query(value = "SELECT IF(COUNT(*) > 0, 'true', 'false') FROM user_course uc INNER JOIN lesson l ON uc.course_id = l.course_id INNER JOIN peachy_mind_auz.user u ON u.id = uc.user_id WHERE u.id =:userId AND l.id =:lessonId",
            nativeQuery = true)
    Boolean existsByLessonIdAndUserId(@Param("lessonId") String lessonId,
                                      @Param("userId") String userId);

    @Modifying
    @Query(value = "UPDATE user_course uc SET uc.status =:status WHERE uc.user_id =:userId AND uc.course_id =:courseId",nativeQuery = true)
    void updateStatusByCourseIdAndUserId(@Param("courseId") String courseId,
                                         @Param("userId") String userId,
                                         @Param("status") String status);

    @Modifying
    @Query(value = "UPDATE user_course uc " +
            "SET uc.status =:status WHERE CONCAT(uc.user_id, uc.course_id) IN (:collapsedIds)" +
            "",nativeQuery = true)
    void updateStatusByUserCourseIds(@Param("collapsedIds") List<String> collapsedIds,
                                     @Param("status") String status);

    @Query("SELECT uc.status FROM UserCourseEntity uc WHERE uc.id.course.id =:courseId AND uc.id.user.id =:userId")
    Optional<UserCourseStatus> findStatusByCourseIdAndUserId(@Param("courseId") String courseId,
                                                             @Param("userId") String userId);

    @Query(value = "select uc.course_id AS courseId, uc.user_id AS userId, SUM(ulg.grade) as totalGrade from peachy_mind.user_course uc\n" +
            "inner join lesson l on uc.course_id = l.course_id\n" +
            "inner join user_lesson_grade ulg on ulg.lesson_id = l.id and uc.user_id = ulg.user_id\n" +
            "inner join (select course_id from peachy_mind.user_course where user_id in (:userIds)) iuc on iuc.course_id = uc.course_id and uc.course_id in (:courseIds)\n" +
            "group by uc.user_id, uc.course_id", nativeQuery = true)
    List<UserCourseTotalGradeProjection> findTotalGradesByUserCourseIds(@Param("courseIds") List<String> courseIds,
                                                                        @Param("userIds") List<String> userIds);

    @Query(value = "select uc.course_id as courseId, uc.user_id as userId, u.authority as userAuthority from user_course uc\n" +
            "     inner join lesson l on uc.course_id = l.course_id\n" +
            "     inner join (select concat(iuc.user_id, iuc.course_id) as collapsedIds from user_course iuc\n" +
            "                                               inner join lesson l on iuc.course_id = l.course_id left join user_lesson_grade ulg on l.id = ulg.lesson_id and ulg.user_id=iuc.user_id\n" +
            "                         where ulg.grade is null group by concat(iuc.user_id, iuc.course_id)) notFullyGraded on concat(uc.user_id, uc.course_id) <> notFullyGraded.collapsedIds and l.id in (:ids)\n" +
            "     inner join peachy_mind_auz.user u on u.id = uc.user_id", nativeQuery = true)
    List<UserCourseProjection> findFullyGradedUserCoursesByLessonIds(@Param("ids") List<String> lessonIds);

    @Modifying
    @Query("UPDATE UserCourseEntity uc SET uc.instructorFeedback=:#{#userCourseFeedback.feedback} WHERE uc.id.user.id=:#{#userCourseFeedback.userId} AND uc.id.course.id=:#{#userCourseFeedback.courseId}")
    void updateFeedbackForUserCourse(@Param("userCourseFeedback") UserCourseFeedbackDTO userCourseFeedbackDTO);
}
