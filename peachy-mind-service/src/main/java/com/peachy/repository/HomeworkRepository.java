package com.peachy.repository;

import com.peachy.dto.homework.HomeworkProjection;
import com.peachy.entity.homework.HomeworkCompositeId;
import com.peachy.entity.homework.HomeworkEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface HomeworkRepository extends JpaRepository<HomeworkEntity, HomeworkCompositeId> {
    @Query(value = "SELECT h.lesson_id as lessonId, h.user_id as userId, h.s3Key as s3Key, h.s3Bucket as s3Bucket, h.file_name as fileName FROM homework h WHERE h.lesson_id=:lessonId AND h.user_id=:userId",
            nativeQuery = true)
    Optional<HomeworkProjection> findByLessonIdAndUserId(@Param("lessonId") String lessonId,
                                                         @Param("userId") String userId);

    @Query(value = "SELECT h.user_id as userId, h.lesson_id as lessonId FROM homework h WHERE h.lesson_id IN (:lessonIds) AND h.user_id IN (:userIds)",
           nativeQuery = true)
    List<HomeworkProjection> findAllHomeworksByLessonIdsAndUserIds(List<String> lessonIds,
                                                                   List<String> userIds);
}
