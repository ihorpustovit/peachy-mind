package com.peachy.repository;

import com.peachy.dto.course.CourseMaxPossibleGradeProjection;
import com.peachy.dto.course.ExpandedCourseProjection;
import com.peachy.entity.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Repository
public interface CourseRepository extends JpaRepository<CourseEntity, String> {
    long countByIdIn(List<String> ids);

    @Query("SELECT c.id as id, c.title as title, c.shortDescription as shortDescription  FROM CourseEntity c INNER JOIN UserCourseEntity uc ON c.id=uc.id.course.id WHERE c.id IN (:ids)")
    CompletableFuture<List<ExpandedCourseProjection>> findAllByIdAsync(@Param("ids") List<String> ids);

    @Query("SELECT uc.id.course FROM UserCourseEntity uc WHERE uc.id.user.id=:userId")
    List<CourseEntity> findAllByUserId(@Param("userId") String userId);

    @Query("SELECT c.id as courseId, SUM(l.maxGrade) as maxPossibleGrade FROM CourseEntity c INNER JOIN LessonEntity l ON c.id = l.course.id WHERE c.id in (:courseIds) GROUP BY c.id")
    List<CourseMaxPossibleGradeProjection> findMaxPossibleGradesForCourses(@Param("courseIds") List<String> courseIds);
}
