package com.peachy.repository;

import com.peachy.entity.userlessongrade.UserLessonGradeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserLessonGradeRepository extends JpaRepository<UserLessonGradeEntity, String> {
    @Query("SELECT avg(ulg.grade) FROM UserLessonGradeEntity ulg WHERE ulg.id.lesson.course.id=:courseId")
    Optional<Integer> getCourseGrade(@Param("courseId") String courseId);
}
