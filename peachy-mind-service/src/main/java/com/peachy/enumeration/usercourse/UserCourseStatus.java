package com.peachy.enumeration.usercourse;

public enum UserCourseStatus {
    /**
     * User has just started a course. No homeworks have been uploaded yet
     */
    STARTED,
    /**
     * User has shown some activity while taking part in a course (uploaded at least single homework)
     */
    IN_PROGRESS,
    /**
     * User has successfully passed the course (has grade >80%)
     */
    FINISHED,
    /**
     * User has failed to pass the course (has grade <80%)
     */
    FAILED;
}
