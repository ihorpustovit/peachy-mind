package com.peachy.controller;

import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.userlessongrade.UserLessonGradeCreationDTO;
import com.peachy.dto.userlessongrade.UserLessonGradeDTO;
import com.peachy.security.SecurityUtils;
import com.peachy.security.dto.UserDTO;
import com.peachy.service.lesson.LessonService;
import com.peachy.service.userlessongrade.UserLessonGradeService;
import com.peachy.web.PeachyMindController;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/lesson")
@RequiredArgsConstructor
public class LessonController implements PeachyMindController {
    private final LessonService defaultLessonService;

    private final UserLessonGradeService defaultUserLessonGradeService;

    @PostMapping
    public ResponseEntity<LessonDTO> createLesson(@RequestBody LessonCreationDTO lesson) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(defaultLessonService.save(lesson));
    }

    @GetMapping
    public ResponseEntity<List<LessonsPerCourseDTO>> getLessonsForCourse(@RequestParam("courseId") String courseId) {
        return ResponseEntity.ok(defaultLessonService.findAllByCourseIds(Collections.singletonList(courseId)));
    }

    @PostMapping("/{lessonId}/grade")
    public ResponseEntity<List<UserLessonGradeDTO>> putGradeForUserLesson(@PathVariable("lessonId") String lessonId,
                                                                          @RequestParam("userId") String userId,
                                                                          @RequestParam("grade") Integer grade) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(defaultUserLessonGradeService.createGrades(Collections.singletonList(new UserLessonGradeCreationDTO(
                        lessonId,
                        userId,
                        SecurityUtils.getUserPrincipal()
                                .map(UserDTO::getId)
                                .orElse(null),
                        grade))));
    }

    @GetMapping("/test")
    public ResponseEntity<String> test() {
        return ResponseEntity.ok("test");
    }
}
