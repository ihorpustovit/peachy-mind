package com.peachy.controller;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.dto.course.CourseCreationDTO;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.dto.usercourse.UserCourseFeedbackDTO;
import com.peachy.dto.usercourse.UsersPerCourseDTO;
import com.peachy.service.course.CourseService;
import com.peachy.service.lesson.LessonService;
import com.peachy.service.usercourse.UserCourseService;
import com.peachy.web.PeachyMindController;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController implements PeachyMindController {
    private final CourseService defaultCourseService;

    private final UserCourseService defaultUserCourseService;

    private final LessonService defaultLessonService;

    @PostMapping
    public ResponseEntity<ExpandedCourseDTO> createCourse(@RequestBody CourseCreationDTO course) throws URISyntaxException {
        ExpandedCourseDTO createdCourse = defaultCourseService.save(course);

        return ResponseEntity.created(new URI("/course/".concat(createdCourse.getId())))
                .body(createdCourse);
    }

    @PostMapping("/{courseId}/assign/instructor")
    public ResponseEntity<Void> assignInstructorToCourse(@PathVariable("courseId") String courseId,
                                                         @RequestParam("instructorId") String instructorId) {
        defaultUserCourseService.assignInstructorToCourse(instructorId, courseId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{courseId}")
    public ResponseEntity<ExpandedCourseDTO> getCourseById(@PathVariable("courseId") String courseId) {
        return ResponseEntity.ok(defaultCourseService.findExpandedById(courseId)
                .orElseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException("Unable to retrieve course"))
                        .withReason(String.format("No course found for id: %s", courseId))
                        .build()));
    }

    @PostMapping("/{courseId}/assign/student")
    public ResponseEntity<Void> assignStudentToCourse(@PathVariable("courseId") String courseId,
                                                      @RequestParam("studentId") String studentId) {
        defaultUserCourseService.assignStudentToCourse(studentId, courseId);
        return ResponseEntity.noContent().build();
    }


    @GetMapping("/self")
    public ResponseEntity<List<CourseDTO>> getCoursesForUser() {
        return ResponseEntity.ok(defaultCourseService.findAllByAuthenticatedUserId());
    }

    @GetMapping("/{courseId}/students")
    public ResponseEntity<UsersPerCourseDTO> getStudentsForCourse(@PathVariable("courseId") String courseId) {
        return ResponseEntity.ok(defaultUserCourseService.findAllStudentsByCourseId(courseId));
    }

    @GetMapping("/{courseId}/lessons")
    public ResponseEntity<LessonsPerCourseDTO> getLessonsForCourse(@PathVariable("courseId") String courseId) {
        return ResponseEntity.ok(defaultLessonService.findAllByCourseId(courseId));
    }

    @PatchMapping("/feedback")
    public ResponseEntity<Void> createInstructorUserCourseFeedback(@RequestBody UserCourseFeedbackDTO userCourseFeedbackDTO) {
        defaultUserCourseService.createInstructorUserCourseFeedback(userCourseFeedbackDTO);
        return ResponseEntity.noContent().build();
    }
}


