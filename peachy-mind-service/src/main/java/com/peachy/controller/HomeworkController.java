package com.peachy.controller;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkCreationDTO;
import com.peachy.security.SecurityUtils;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.exception.UnauthorizedException;
import com.peachy.service.homework.HomeworkService;
import com.peachy.web.PeachyMindController;
import com.peachy.web.PeachyMindHttpHeaders;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;

@RestController
@RequiredArgsConstructor
@RequestMapping("/homework")
public class HomeworkController implements PeachyMindController {
    private final HomeworkService defaultHomeworkService;

    @GetMapping("/download")
    public ResponseEntity<Resource> downloadHomework(@RequestParam("lessonId") String lessonId,
                                                     @RequestParam("userId") String userId) {
        ExpandedHomeworkDTO expandedHomeworkDTO =
                defaultHomeworkService.download(lessonId, userId);

        return ResponseEntity.status(HttpStatus.OK)
                .header(PeachyMindHttpHeaders.CONTENT_DISPOSITION, "Content-Disposition", "attachment; filename=\"" + expandedHomeworkDTO.getFileName() + "\"")
                .body(new InputStreamResource(new ByteArrayInputStream(expandedHomeworkDTO.getHomeworkFileStream().toByteArray())));
    }

    @PostMapping("/upload")
    public ResponseEntity<Void> uploadHomework(@RequestParam("lessonId") String lessonId,
                                               @RequestParam("homework") MultipartFile homework) {
        UserDTO authenticatedUser = SecurityUtils.getUserPrincipal()
                .orElseThrow(() -> ExceptionBuilder.buildFrom(new UnauthorizedException("Unable to upload homework"))
                        .withReason("You are not authenticated")
                        .withSolution("Please authenticate by hitting POST /auth/sign/in")
                        .build());
        defaultHomeworkService.save(new HomeworkCreationDTO(authenticatedUser, lessonId, homework));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
