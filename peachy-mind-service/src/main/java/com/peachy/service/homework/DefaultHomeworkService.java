package com.peachy.service.homework;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.peachy.commons.exception.ConflictException;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.commons.utils.ListParty;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.commons.utils.assertion.PartyAssert;
import com.peachy.converter.homework.HomeworkConverter;
import com.peachy.converter.homework.HomeworkFromBaseDTOConverter;
import com.peachy.converter.homework.HomeworkToBaseDTOConverter;
import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkCreationDTO;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.homework.HomeworkProjection;
import com.peachy.entity.homework.HomeworkCompositeId;
import com.peachy.entity.homework.HomeworkEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.repository.HomeworkRepository;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.aws.s3.S3Service;
import com.peachy.service.lesson.LessonService;
import com.peachy.service.usercourse.UserCourseService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultHomeworkService implements HomeworkService {

    private final S3Service s3Service;

    private final LessonService defaultLessonService;

    private final UserService defaultUserService;

    private final UserCourseService defaultUserCourseService;

    private final HomeworkConverter homeworkConverter;

    private final HomeworkToBaseDTOConverter toBaseDTOConverter;

    private final HomeworkFromBaseDTOConverter fromBaseDTOConverter;

    private final HomeworkRepository homeworkRepository;

    @Value("${aws.s3.bucket.base}")
    private String s3BaseBucketName;

    @Override
    @Transactional
    public HomeworkDTO save(HomeworkCreationDTO homeworkCreationDTO) {
        validateHomeworkForCreation(homeworkCreationDTO);

        HomeworkDTO homeworkDTO = convertFromCreationToBase(homeworkCreationDTO);
        String homeworkKey = buildS3KeyFrom(homeworkDTO);

        HomeworkEntity homeworkEntity = homeworkConverter.toEntity(homeworkDTO);
        homeworkEntity.setS3Key(homeworkKey);
        homeworkEntity.setS3Bucket(s3BaseBucketName);
        homeworkEntity = homeworkRepository.save(homeworkEntity);
        HomeworkDTO savedHomework = homeworkConverter.toDto(homeworkEntity);

        updateUserCourseStatus(homeworkDTO.getLesson().getCourse().getId(), homeworkDTO.getUserId());
        s3Service.upload(s3BaseBucketName, homeworkKey, homeworkCreationDTO.getHomeworkFile());

        return savedHomework;
    }

    private HomeworkDTO convertFromCreationToBase(HomeworkCreationDTO homeworkCreation) {
        HomeworkDTO homeworkDTO = toBaseDTOConverter.fromCreationToBase(homeworkCreation);
        homeworkDTO.setLesson(defaultLessonService.findById(homeworkCreation.getLessonId()).get());
        homeworkDTO.setFileName(homeworkCreation.getHomeworkFile().getName());

        return homeworkDTO;
    }

    private void validateHomeworkForCreation(HomeworkCreationDTO homeworkCreationDTO) {
        Assert.that(homeworkCreationDTO)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to upload homework"))
                        .withReason("Homework object is empty")
                        .build())
                .isNotNull(HomeworkCreationDTO::getHomeworkFile)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to upload homework"))
                        .withReason("Homework file is empty")
                        .build())
                .isNotNull(HomeworkCreationDTO::getUser)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to upload homework"))
                        .withReason("User is empty")
                        .build())
                .isNotBlank(HomeworkCreationDTO::getLessonId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to upload homework"))
                        .withReason("Lesson id is empty")
                        .build())
                .isTrue(homeworkCreation -> defaultUserService.existsById(homeworkCreation.getUser().getId()))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to upload homework"))
                        .withReason(String.format("User doesn't exist for id: %s", homeworkCreationDTO.getUser().getId()))
                        .build())
                .isTrue(homeworkCreation -> defaultLessonService.existsById(homeworkCreation.getLessonId()))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to upload homework"))
                        .withReason(String.format("Lesson doesn't exist for id: %s", homeworkCreationDTO.getLessonId()))
                        .build())
                .isFalse(homeworkCreation -> existsByLessonIdAndUserId(homeworkCreation.getLessonId(), homeworkCreation.getUser().getId()))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to upload homework"))
                        .withReason("Homework has been uploaded already")
                        .build())
                .isTrue(homeworkCreation -> defaultUserCourseService.existsByLessonIdAndUserId(homeworkCreation.getLessonId(), homeworkCreation.getUser().getId()))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to upload homework"))
                        .withReason("User is not assigned to a course")
                        .withSolution("User should get assigned to the course first")
                        .build());
    }

    private void updateUserCourseStatus(String courseId,
                                        String userId) {
        UserCourseStatus status =
                defaultUserCourseService.findStatusByCourseIdAndUserId(courseId, userId)
                        .orElseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException("Failed to update user course status"))
                                .withReason(String.format("Unable to detect user course status for userId: %s and courseId: %s", userId, courseId))
                                .build());

        if (Objects.equals(status, UserCourseStatus.STARTED)) {
            defaultUserCourseService.updateStatusByCourseIdAndUserId(courseId, userId, UserCourseStatus.IN_PROGRESS);
        }
    }

    public boolean existsByLessonIdAndUserId(String lessonId,
                                             String userId) {
        Assert.that(lessonId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if homework exists"))
                        .withReason("Lesson id is empty")
                        .build());

        Assert.that(userId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if homework exists"))
                        .withReason("User id is empty")
                        .build());
        return homeworkRepository.existsById(new HomeworkCompositeId(lessonId, userId));
    }

    @Override
    public boolean existAllByLessonIdAndUserId(List<Pair<String, String>> lessonUserIdPairs) {
        lessonUserIdPairs.forEach(lessonUserIdPair -> {
            Assert.that(lessonUserIdPair.getKey())
                    .isNotBlank()
                    .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if homework exists"))
                            .withReason("Lesson id is empty")
                            .build());

            Assert.that(lessonUserIdPair.getValue())
                    .isNotBlank()
                    .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if homework exists"))
                            .withReason("User id is empty")
                            .build());
        });

        List<String> lessonIds = lessonUserIdPairs.stream()
                .map(Pair::getKey)
                .distinct()
                .collect(Collectors.toList());

        List<String> userIds = lessonUserIdPairs.stream()
                .map(Pair::getValue)
                .distinct()
                .collect(Collectors.toList());

        Map<String, List<String>> lessonGroupedUsers = ListParty.of(lessonUserIdPairs)
                .groupByNestedProperty(Pair::getKey, Function.identity())
                .turnIntoMap(entry -> Pair.of(entry.getKey(), entry.getValue().stream().map(Pair::getValue).collect(Collectors.toList())))
                .endParty();

        List<HomeworkProjection> homeworkProjections = homeworkRepository.findAllHomeworksByLessonIdsAndUserIds(lessonIds, userIds);

        Map<String, List<String>> dbLessonGroupedUsers = ListParty.of(homeworkProjections)
                .groupByNestedProperty(HomeworkProjection::getLessonId, lessonId -> lessonId)
                .turnIntoMap(entry -> Pair.of(entry.getKey(), entry.getValue().stream().map(HomeworkProjection::getUserId).collect(Collectors.toList())))
                .endParty();

        return lessonGroupedUsers.entrySet().stream()
                .allMatch(entry -> dbLessonGroupedUsers.containsKey(entry.getKey()) && dbLessonGroupedUsers.get(entry.getKey()).containsAll(entry.getValue()));
    }

    public String buildS3KeyFrom(HomeworkProjection homeworkProjection) {
        Assert.that(homeworkProjection)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to build S3 key"))
                        .withReason("Homework object is empty")
                        .withFault("Developer's fault")
                        .build())
                .isNotBlank(HomeworkProjection::getLessonId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to build S3 key"))
                        .withReason("Lesson id is empty")
                        .withFault("Developer's fault")
                        .build())
                .isNotNull(HomeworkProjection::getUserId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to build S3 key"))
                        .withReason("User id is empty")
                        .withFault("Developer's fault")
                        .build());
        return buildS3KeyFrom(
                homeworkProjection.getLessonId(),
                homeworkProjection.getUserId(),
                homeworkProjection.getFileName());
    }

    public String buildS3KeyFrom(String lessonId,
                                 String userId,
                                 String fileName) {
        Assert.that(Arrays.asList(lessonId, userId))
                .areNotBlank()
                .and(PartyAssert::hasNoNullElements)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to build S3 key"))
                        .withReason("Either lessonId or userId is blank")
                        .build());
        Assert.that(fileName)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to build S3 key"))
                        .withReason("File name should not be blank")
                        .build());
        return "/"
                .concat(lessonId)
                .concat("/")
                .concat(userId)
                .concat("/")
                .concat(fileName);
    }

    public Optional<HomeworkProjection> findByLessonIdAndUserId(String lessonId,
                                                                String userId) {
        return homeworkRepository.findByLessonIdAndUserId(lessonId, userId);
    }

    @Override
    public ExpandedHomeworkDTO download(String lessonId, String userId) {
        Assert.that(lessonId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to download homework"))
                        .withReason("Lesson id is empty")
                        .build());

        Assert.that(userId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to download homework"))
                        .withReason("User id is empty")
                        .build());

        HomeworkProjection homeworkProjection = findByLessonIdAndUserId(lessonId, userId)
                .orElseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException("Unable to download homework"))
                        .withReason(String.format("There is no homework for lesson id: %s and user id: %s", lessonId, userId))
                        .build());
        GetObjectRequest getObjectRequest = new GetObjectRequest(homeworkProjection.getS3Bucket(), homeworkProjection.getS3Key());
        ByteArrayOutputStream homeworkFileStream = s3Service.download(getObjectRequest)
                .orElseThrow(() -> ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to download homework"))
                        .withReason(String.format("Homework with lesson id: %s user id: %s and s3Key: %s  considered to be existent according to DB records, but is absent on S3",
                                homeworkProjection.getLessonId(),
                                homeworkProjection.getUserId(),
                                homeworkProjection.getS3Key()))
                        .withFault("Developer's fault")
                        .withSolution("Synchronize ")
                        .build());
        ExpandedHomeworkDTO expandedHomeworkDTO = fromBaseDTOConverter.fromBaseToExpanded(toBaseDTOConverter.fromProjectionToBase(homeworkProjection));
        expandedHomeworkDTO.setHomeworkFileStream(homeworkFileStream);
        return expandedHomeworkDTO;
    }
}
