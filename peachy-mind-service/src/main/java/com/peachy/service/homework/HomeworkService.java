package com.peachy.service.homework;

import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkCreationDTO;
import com.peachy.dto.homework.HomeworkDTO;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

public interface HomeworkService {
    HomeworkDTO save(HomeworkCreationDTO homeworkCreationDTO);

    ExpandedHomeworkDTO download(String lessonId,
                                 String userId);

    boolean existsByLessonIdAndUserId(String lessonId,
                                      String userId);

    boolean existAllByLessonIdAndUserId(List<Pair<String,String>> lessonUserIdPairs);
}
