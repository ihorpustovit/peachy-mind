package com.peachy.service.aws.s3;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.utils.Utils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Service
public class DefaultS3Service implements S3Service {
    private final AWSCredentials s3Credentials;

    private final AmazonS3 s3Client;

    public DefaultS3Service(@Value("${aws.s3.access-key}") String s3AccessKey,
                            @Value("${aws.s3.secret-access-key}") String s3SecretAccessKey,
                            @Value("${aws.s3.region}") String regionName) {
        this.s3Credentials = new BasicAWSCredentials(s3AccessKey, s3SecretAccessKey);
        this.s3Client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(s3Credentials))
                .withRegion(Regions.fromName(regionName))
                .build();
    }

    @Override
    public void upload(String bucketName,
                       String key,
                       MultipartFile multipartFile) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(multipartFile.getSize());
        s3Client.putObject(
                bucketName,
                key,
                Utils.replaceIOException(multipartFile::getInputStream, "An IOException while uploading file to S3"),
                objectMetadata);
    }

    @Override
    public Optional<ByteArrayOutputStream> download(GetObjectRequest getObjectRequest) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            S3Object s3Object = s3Client.getObject(getObjectRequest);
            Utils.replaceIOException(() -> IOUtils.copy(s3Object.getObjectContent(), byteArrayOutputStream),
                    "IOException while downloading file from S3 (turning input stream into output)");
            return Optional.of(byteArrayOutputStream);
        } catch (AmazonS3Exception e) {
            if (Objects.equals(e.getStatusCode(), HttpStatus.NOT_FOUND.value())) {
                return Optional.empty();
            } else {
                throw e;
            }
        } finally {
            Utils.replaceException(
                    byteArrayOutputStream::close,
                    ex -> ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException while closing output stream after downloading from S3 (turning input stream into output)"))
                            .withFault("Developer's fault")
                            .build(),
                    IOException.class);
        }
    }
}
