package com.peachy.service.aws.s3;

import com.amazonaws.services.s3.model.GetObjectRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.util.Optional;

public interface S3Service {
    void upload(String bucketName,
                String key,
                MultipartFile multipartFile);

    Optional<ByteArrayOutputStream> download(GetObjectRequest getObjectRequest);
}
