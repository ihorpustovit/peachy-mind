package com.peachy.service.course;

import com.peachy.dto.course.CourseCreationDTO;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface CourseService {
    ExpandedCourseDTO save(CourseCreationDTO course);

    boolean existsById(String id);

    boolean existAllByIds(List<String> ids);

    Optional<ExpandedCourseDTO> findExpandedById(String id);

    Optional<CourseDTO> findById(String courseId);

    List<ExpandedCourseDTO> findExpandedAllByIds(List<String> courseIds);

    List<CourseDTO> findAllByIds(List<String> courseIds);

    List<CourseDTO> findAllByUserId(String userId);

    List<CourseDTO> findAllByAuthenticatedUserId();

    Map<String, Integer> findMaxPossibleGradesForCourses(List<String> courseIds);
}
