package com.peachy.service.course;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.utils.Utils;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.commons.utils.assertion.PartyAssert;
import com.peachy.converter.course.CourseConverter;
import com.peachy.converter.course.CourseFromBaseDTOConverter;
import com.peachy.converter.course.CourseToBaseDTOConverter;
import com.peachy.converter.lesson.LessonFromBaseDTOConverter;
import com.peachy.converter.lesson.LessonToBaseDTOConverter;
import com.peachy.dto.course.*;
import com.peachy.dto.lesson.LessonInCourseDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.dto.usercourse.UsersPerCourseDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.repository.CourseRepository;
import com.peachy.security.SecurityUtils;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.exception.UnauthorizedException;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.lesson.LessonService;
import com.peachy.service.usercourse.UserCourseService;
import com.peachy.service.userlessongrade.UserLessonGradeService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultCourseService implements CourseService {
    private final int MINIMAL_LESSONS_AMOUNT_PER_COURSE = 5;

    private final int MINIMAL_INSTRUCTORS_COUNT_PER_COURSE = 1;

    private final int MAXIMUM_COURSE_TITLE_LENGTH = 80;

    private final int MAXIMUM_COURSE_SHORT_DESCRIPTION_LENGTH = 500;

    private final CourseRepository courseRepository;

    private final LessonService defaultLessonService;

    private final CourseConverter courseConverter;

    private final CourseToBaseDTOConverter toBaseDTOConverter;

    private final CourseFromBaseDTOConverter fromBaseDTOConverter;

    private final LessonFromBaseDTOConverter fromLessonBaseDTOConverter;

    private final LessonToBaseDTOConverter toLessonBaseDTOConverter;

    private final UserService defaultUserService;

    private UserLessonGradeService userLessonGradeService;

    private UserCourseService defaultUserCourseService;

    @Lazy
    @Autowired
    public void setUserLessonGradeService(UserLessonGradeService userLessonGradeService) {
        this.userLessonGradeService = userLessonGradeService;
    }

    @Lazy
    @Autowired
    public void setUserCourseService(UserCourseService defaultUserCourseService) {
        this.defaultUserCourseService = defaultUserCourseService;
    }

    @Override
    @Transactional
    public ExpandedCourseDTO save(CourseCreationDTO courseCreation) {
        validateCourseForCreation(courseCreation);

        CourseDTO courseBase = convertFromCreationToBase(courseCreation);

        CourseEntity courseEntity = courseRepository.save(courseConverter.toEntity(courseBase));
        ExpandedCourseDTO savedCourse = fromBaseDTOConverter.fromBaseToExpanded(courseConverter.toDto(courseEntity));

        List<LessonCreationDTO> lessonCreations = courseCreation.getLessons().stream()
                .map(toLessonBaseDTOConverter::fromInCourseCreationToBase)
                .map(fromLessonBaseDTOConverter::toLessonCreationFromBase)
                .collect(Collectors.toList());
        lessonCreations.forEach(lessonCreation -> lessonCreation.setCourseId(savedCourse.getId()));

        savedCourse.setLessons(defaultLessonService.saveAll(lessonCreations).stream()
                .map(fromLessonBaseDTOConverter::toLessonInCourseFromBase)
                .collect(Collectors.toList()));

        savedCourse.setInstructors(defaultUserService.findAllByIds(courseCreation.getInstructorIds()));

        List<UserCourseDTO> userCourses = defaultUserCourseService.assignInstructorsToCourse(courseCreation.getInstructorIds(), savedCourse.getId());

        savedCourse.setInstructors(userCourses.stream().map(UserCourseDTO::getUser).collect(Collectors.toList()));

        return savedCourse;
    }

    private CourseDTO convertFromCreationToBase(CourseCreationDTO courseCreation) {
        return toBaseDTOConverter.fromCreationToBase(courseCreation);
    }


    private void validateCourseForCreation(CourseCreationDTO courseCreation) {
        Assert.that(courseCreation)

                .isNotBlank(CourseCreationDTO::getTitle)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create course"))
                        .withReason("Course title should not be empty")
                        .build())

                .isTrue(courseDTO -> courseDTO.getTitle().length() <= MAXIMUM_COURSE_TITLE_LENGTH)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create course"))
                        .withReason(String.format("Course title should not exceed %s characters", MAXIMUM_COURSE_TITLE_LENGTH))
                        .build())

                .isTrue(courseDTO ->
                        Objects.isNull(courseDTO.getShortDescription()) ||
                                courseDTO.getShortDescription().length() <= MAXIMUM_COURSE_SHORT_DESCRIPTION_LENGTH)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create course"))
                        .withReason(String.format("Course description should not exceed %s characters", MAXIMUM_COURSE_SHORT_DESCRIPTION_LENGTH))
                        .build())

                .isTrue(courseDTO -> Objects.nonNull(courseDTO.getLessons()))
                .and(courseAssert -> courseAssert.isFalse(courseDTO -> courseDTO.getLessons().size() < MINIMAL_LESSONS_AMOUNT_PER_COURSE))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create course"))
                        .withReason(String.format("Course should have at least %s lessons within", MINIMAL_LESSONS_AMOUNT_PER_COURSE))
                        .build())

                .isFalse(courseDTO -> courseDTO.getInstructorIds().size() < MINIMAL_INSTRUCTORS_COUNT_PER_COURSE)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create course"))
                        .withReason(String.format("At least %s instructors should be assigned per course", MINIMAL_INSTRUCTORS_COUNT_PER_COURSE))
                        .build())

                .isTrue(courseDTO -> defaultUserService.existAllByIds(courseDTO.getInstructorIds()))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create course"))
                        .withReason("Non-existent user specified as instructor")
                        .build());
    }

    @Override
    public Optional<ExpandedCourseDTO> findExpandedById(String id) {
        return Optional.ofNullable(findExpandedAllByIds(Collections.singletonList(id)))
                .filter(list -> !list.isEmpty())
                .map(list -> list.get(0))
                .map(course -> {
                    course.setGrade(userLessonGradeService.getCourseGrade(id));
                    return course;
                });
    }

    @Override
    public List<ExpandedCourseDTO> findExpandedAllByIds(List<String> courseIds) {

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        try {
            CompletableFuture<List<ExpandedCourseProjection>> futureCourse = courseRepository.findAllByIdAsync(courseIds);
            CompletableFuture<List<LessonsPerCourseDTO>> futureLessons =
                    CompletableFuture.supplyAsync(() -> defaultLessonService.findAllByCourseIds(courseIds), executorService);
            CompletableFuture<List<UsersPerCourseDTO>> futureInstructors =
                    CompletableFuture.supplyAsync(() -> defaultUserCourseService.findAllInstructorsByCourseIds(courseIds), executorService);

            CompletableFuture.allOf(futureCourse, futureLessons, futureInstructors);

            List<ExpandedCourseDTO> expandedCourseDTOS = Utils.unmarshallFuture(futureCourse).stream()
                    .map(fromBaseDTOConverter::fromProjectionToExpanded)
                    .collect(Collectors.toList());

            Map<String, List<LessonInCourseDTO>> lessons = Utils.unmarshallFuture(futureLessons).stream()
                    .map(lessonsPerCourseDTO -> Pair.of(lessonsPerCourseDTO.getCourse().getId(), lessonsPerCourseDTO.getLessons()))
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

            Map<String, List<UserDTO>> instructors = Utils.unmarshallFuture(futureInstructors).stream()
                    .map(usersPerCourseDTO -> Pair.of(usersPerCourseDTO.getCourse().getId(), usersPerCourseDTO.getUsers()))
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));


            expandedCourseDTOS.forEach(courseDTO -> {
                if (Objects.nonNull(lessons.get(courseDTO.getId()))) {
                    courseDTO.setLessons(lessons.get(courseDTO.getId()).stream().map(fromLessonBaseDTOConverter::toLessonInCourseFromBase).collect(Collectors.toList()));
                } else {
                    courseDTO.setLessons(Collections.emptyList());
                }

                if (Objects.nonNull(instructors.get(courseDTO.getId()))) {
                    courseDTO.setInstructors(instructors.get(courseDTO.getId()));
                } else {
                    courseDTO.setInstructors(Collections.emptyList());
                }
            });
            return expandedCourseDTOS;
        } finally {
            executorService.shutdown();
        }
    }

    @Override
    public boolean existsById(String id) {
        return existAllByIds(Optional.ofNullable(id)
                .map(Collections::singletonList)
                .orElse(Collections.emptyList()));
    }


    @Override
    public boolean existAllByIds(List<String> ids) {
        Assert.that(ids)
                .isNotNull()
                .and(PartyAssert::areNotBlank)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if course exists"))
                        .withReason("Course id must not be empty")
                        .build());

        return Optional.of(ids)
                .filter(idList -> !idList.isEmpty())
                .map(idList -> idList.stream().distinct().collect(Collectors.toList()))
                .map(idList -> courseRepository.countByIdIn(idList) == idList.size())
                .orElse(false);
    }

    @Override
    public List<CourseDTO> findAllByAuthenticatedUserId() {
        String authenticatedUserId = SecurityUtils.getUserPrincipal()
                .orElseThrow(() -> ExceptionBuilder.buildFrom(new UnauthorizedException("Unable to retrieve user's courses"))
                        .withReason("You are not authenticated")
                        .withSolution("Please authenticate by hitting POST /auth/sign/in")
                        .build()).getId();
        return findAllByUserId(authenticatedUserId);
    }

    @Override
    public List<CourseDTO> findAllByUserId(String userId) {
        Assert.that(userId)
                .isNotBlank()
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to retrieve user's courses"))
                        .withReason("User id should not be empty")
                        .build());
        return courseConverter.toDto(courseRepository.findAllByUserId(userId));
    }

    @Override
    public List<CourseDTO> findAllByIds(List<String> courseIds) {
        return courseConverter.toDto(courseRepository.findAllById(courseIds));
    }

    @Override
    public Optional<CourseDTO> findById(String courseId) {
        return courseRepository.findById(courseId)
                .map(courseConverter::toDto);
    }


    @Override
    public Map<String, Integer> findMaxPossibleGradesForCourses(List<String> courseIds) {
        return courseRepository.findMaxPossibleGradesForCourses(courseIds).stream()
                .collect(Collectors.toMap(CourseMaxPossibleGradeProjection::getCourseId, CourseMaxPossibleGradeProjection::getMaxPossibleGrade));
    }
}
