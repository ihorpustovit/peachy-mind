package com.peachy.service.userlessongrade;

import com.peachy.commons.exception.ConflictException;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.converter.userlessongrade.UserLessonGradeConverter;
import com.peachy.converter.userlessongrade.UserLessonGradeToBaseDTOConverter;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.dto.usercourse.UserCourseTotalGradeProjection;
import com.peachy.dto.userlessongrade.UserLessonGradeCreationDTO;
import com.peachy.dto.userlessongrade.UserLessonGradeDTO;
import com.peachy.entity.usercourse.UserCourseCompositeId;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.repository.UserLessonGradeRepository;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.course.CourseService;
import com.peachy.service.homework.HomeworkService;
import com.peachy.service.lesson.LessonService;
import com.peachy.service.usercourse.UserCourseService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultUserLessonGradeService implements UserLessonGradeService {

    private final UserService defaultUserService;

    private final LessonService defaultLessonService;

    private final UserCourseService defaultUserCourseService;

    private final UserLessonGradeConverter userLessonGradeConverter;

    private final UserLessonGradeToBaseDTOConverter toBaseDTOConverter;

    private final UserLessonGradeRepository userLessonGradeRepository;

    private final HomeworkService defaultHomeworkService;

    private final CourseService defaultCourseService;

    private final ApplicationEventPublisher applicationEventPublisher;

    @Value("${self.course.border-grade}")
    private Integer courseBorderGrade;

    @PostConstruct
    private void checkState() {
        Assert.that(courseBorderGrade)
                .isNotNull()
                .otherwiseThrow(() -> new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "course border grade should not be empty"));
    }

    @Override
    @Transactional
    public List<UserLessonGradeDTO> createGrades(List<UserLessonGradeCreationDTO> userLessonGradeCreation) {
        validateUserLessonGradeForCreation(userLessonGradeCreation);
        List<UserLessonGradeDTO> userLessonGrades = userLessonGradeCreation.stream().map(toBaseDTOConverter::fromCreationToBase).collect(Collectors.toList());

        applicationEventPublisher.publishEvent(new CoursesCompletionEvent(userLessonGrades));

        return userLessonGradeConverter.toDto(userLessonGradeRepository.saveAll(userLessonGradeConverter.toEntity(userLessonGrades)));
    }

    private void validateUserLessonGradeForCreation(List<UserLessonGradeCreationDTO> userLessonGradeCreationDTO) {
        Assert.that(userLessonGradeCreationDTO)
                .hasNoNullElements()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to put a grade for lesson"))
                        .withReason("User lesson grade creation object is empty")
                        .build())
                .areNotBlank(UserLessonGradeCreationDTO::getUserId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to put a grade for lesson"))
                        .withReason("User id is blank")
                        .build())
                .areNotBlank(UserLessonGradeCreationDTO::getLessonId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to put a grade for lesson"))
                        .withReason("Lesson id is blank")
                        .build())
                .areNotNull(UserLessonGradeCreationDTO::getGrade)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to put a grade for lesson"))
                        .withReason("Grade is empty")
                        .build())
                .isTrueNested(defaultUserService::existAllByIds, UserLessonGradeCreationDTO::getUserId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException("Unable to put a grade for lesson"))
                        .withReason("Some users don't exist")
                        .build())
                .isTrueNested(defaultLessonService::existAllByIds, UserLessonGradeCreationDTO::getLessonId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException("Unable to put a grade for lesson"))
                        .withReason("Some lessons don't exist")
                        .build())
                .isTrueNested(
                        defaultUserCourseService::existAllByLessonIdAndUserId,
                        userLessonGradeCreation -> Pair.of(userLessonGradeCreation.getLessonId(), userLessonGradeCreation.getUserId()))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to put a grade for lesson"))
                        .withReason("You are trying to grade a lesson that user is not related to")
                        .build())
                .isTrueNested(
                        defaultHomeworkService::existAllByLessonIdAndUserId,
                        userLessonGradeCreation -> Pair.of(userLessonGradeCreation.getLessonId(), userLessonGradeCreation.getUserId()))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to put a grade for lesson"))
                        .withReason("Some users have not uploaded homework yet")
                        .build())
                .isTrue(this::gradesDoesntExceedMaximum)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to put a grade for lesson"))
                        .withReason("Some grades are higher than allowed")
                        .build());


        Assert.that(userLessonGradeCreationDTO)
                .areNotBlank(UserLessonGradeCreationDTO::getInstructorId)
                .then(() -> Assert.that(userLessonGradeCreationDTO)
                        .isTrueNested(
                                defaultUserCourseService::existsInstructorByLessonIdAndUserId,
                                userLessonGradeCreation -> Pair.of(userLessonGradeCreation.getLessonId(), userLessonGradeCreation.getInstructorId()))
                        .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to put a grade for lesson"))
                                .withReason("Some user passed in as instructor doesn't have an instructor authority actually or doesn't exist at all")
                                .build()))
                .otherwiseIgnore();
    }

    private boolean gradesDoesntExceedMaximum(List<UserLessonGradeCreationDTO> userLessonGradeCreations) {
        List<String> lessonIds = userLessonGradeCreations.stream()
                .map(UserLessonGradeCreationDTO::getLessonId)
                .distinct()
                .collect(Collectors.toList());
        Map<String, Integer> lessonIdMaxGradeMap = defaultLessonService.findMaxGradesForLessonIds(lessonIds);
        return userLessonGradeCreations.stream()
                .allMatch(userLessonGradeCreation -> lessonIdMaxGradeMap.containsKey(userLessonGradeCreation.getLessonId()) &&
                        lessonIdMaxGradeMap.get(userLessonGradeCreation.getLessonId()) >= userLessonGradeCreation.getGrade());
    }

    @RequiredArgsConstructor
    @Getter
    public static class CoursesCompletionEvent {
        private final List<UserLessonGradeDTO> userLessonGradeCreation;
    }

    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @TransactionalEventListener
    public void completeCoursesIfLastLessonGraded(CoursesCompletionEvent coursesCompletionEvent) {
        BiPredicate<UserCourseTotalGradeProjection, Map<String, Integer>> userPassedTheCourse = (userCourseTotalGradeProjection, maxPossibleGradesMap) ->
                ((Double.valueOf(userCourseTotalGradeProjection.getTotalGrade()) / Double.valueOf(maxPossibleGradesMap.get(userCourseTotalGradeProjection.getCourseId()))) * 100) >= courseBorderGrade;


        List<UserCourseStatus> completionStatuses =
                new ArrayList<>(Arrays.asList(UserCourseStatus.FINISHED, UserCourseStatus.FAILED));

        List<UserLessonGradeDTO> userLessonGradeCreation = coursesCompletionEvent.getUserLessonGradeCreation();

        List<String> lessonIds = userLessonGradeCreation.stream()
                .filter(userLessonGradeDTO -> Objects.nonNull(userLessonGradeDTO.getLesson()))
                .map(userLessonGradeDTO -> userLessonGradeDTO.getLesson().getId())
                .collect(Collectors.toList());

        List<UserCourseDTO> fullyGradedUncompletedUserCourses = defaultUserCourseService.findFullyGradedUserCoursesByLessonIds(lessonIds).stream()
                .filter(userCourseDTO -> !completionStatuses.contains(userCourseDTO.getStatus()))
                .collect(Collectors.toList());

        List<UserCourseCompositeId> fullyGradedUserCourseIds = fullyGradedUncompletedUserCourses.stream()
                .map(userCourseDTO -> new UserCourseCompositeId(userCourseDTO.getUserId(), userCourseDTO.getCourseId()))
                .collect(Collectors.toList());

        if (!fullyGradedUncompletedUserCourses.isEmpty()) {
            Map<String, Integer> coursesMaxPossibleGrades = defaultCourseService.findMaxPossibleGradesForCourses(fullyGradedUserCourseIds.stream()
                    .map(UserCourseCompositeId::getCourseId)
                    .collect(Collectors.toList()));
            List<UserCourseTotalGradeProjection> userCourseTotalGrades = defaultUserCourseService.findUserCourseTotalGrades(fullyGradedUserCourseIds);

            List<UserCourseCompositeId> failedCourseIds = userCourseTotalGrades.stream()
                    .filter(userCourseTotalGradeProjection -> userPassedTheCourse.negate().test(userCourseTotalGradeProjection, coursesMaxPossibleGrades))
                    .map(userCourseTotalGradeProjection -> new UserCourseCompositeId(userCourseTotalGradeProjection.getUserId(), userCourseTotalGradeProjection.getCourseId()))
                    .collect(Collectors.toList());

            List<UserCourseCompositeId> finishedCourseIds = userCourseTotalGrades.stream()
                    .filter(userCourseTotalGradeProjection -> userPassedTheCourse.test(userCourseTotalGradeProjection, coursesMaxPossibleGrades))
                    .map(userCourseTotalGradeProjection -> new UserCourseCompositeId(userCourseTotalGradeProjection.getUserId(), userCourseTotalGradeProjection.getCourseId()))
                    .collect(Collectors.toList());

            if (!failedCourseIds.isEmpty()) {
                defaultUserCourseService.updateStatusByUserCourseIds(failedCourseIds, UserCourseStatus.FAILED);
            }

            if (!finishedCourseIds.isEmpty()) {
                defaultUserCourseService.updateStatusByUserCourseIds(finishedCourseIds, UserCourseStatus.FINISHED);
            }
        }
    }

    @Override
    public Integer getCourseGrade(String courseId) {
        return userLessonGradeRepository.getCourseGrade(courseId)
                .orElse(0);
    }
}
