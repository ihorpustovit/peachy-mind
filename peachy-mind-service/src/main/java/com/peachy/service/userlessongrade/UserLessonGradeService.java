package com.peachy.service.userlessongrade;

import com.peachy.dto.userlessongrade.UserLessonGradeCreationDTO;
import com.peachy.dto.userlessongrade.UserLessonGradeDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserLessonGradeService {
    List<UserLessonGradeDTO> createGrades(List<UserLessonGradeCreationDTO> userLessonGradeCreation);

    Integer getCourseGrade(String courseId);
}
