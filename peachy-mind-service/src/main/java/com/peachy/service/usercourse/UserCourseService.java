package com.peachy.service.usercourse;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.ForbiddenException;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.dto.usercourse.UserCourseFeedbackDTO;
import com.peachy.dto.usercourse.UserCourseTotalGradeProjection;
import com.peachy.dto.usercourse.UsersPerCourseDTO;
import com.peachy.entity.usercourse.UserCourseCompositeId;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.security.SecurityUtils;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

public interface UserCourseService {
    List<UserCourseDTO> assignInstructorsToCourse(List<String> instructorIds,
                                                  String courseId);

    default UserCourseDTO assignInstructorToCourse(String instructorId,
                                                   String courseId) {
        return assignInstructorsToCourse(Collections.singletonList(instructorId), courseId).get(0);
    }

    List<UserCourseDTO> assignStudentsToCourse(List<String> userIds, String courseId);

    default UserCourseDTO assignStudentToCourse(String studentId,
                                                String courseId) {
        SecurityUtils.getUserPrincipal().ifPresent(authenticatedUser -> {
            if (Objects.equals(authenticatedUser.getAuthority(), UserAuthority.STUDENT)) {
                Assert.that(authenticatedUser)
                        .isTrue(user -> StringUtils.equals(authenticatedUser.getId(), studentId))
                        .otherwiseThrow(ExceptionBuilder.buildFrom(new ForbiddenException("Unable to assign student to course"))
                                .withReason("You are not authorized to assign other users to curse")
                                .build());
            }
        });

        return assignStudentsToCourse(Collections.singletonList(studentId), courseId).get(0);
    }

    Map<String, Long> findUserCoursesCount(List<String> userIds);

    List<String> findCourseMemberIds(String courseId);

    List<UserDTO> findCourseMembers(String courseId,
                                    UserAuthority userAuthority);


    List<UsersPerCourseDTO> findCoursesMembers(List<String> courseIds, List<UserAuthority> userAuthority);

    default List<UsersPerCourseDTO> findAllInstructorsByCourseIds(List<String> courseIds) {
        return findCoursesMembers(courseIds, Collections.singletonList(UserAuthority.INSTRUCTOR));
    }

    UsersPerCourseDTO findAllStudentsByCourseId(String courseId);

    boolean existsByLessonIdAndUserId(String lessonId,
                                      String userId);

    void updateStatusByCourseIdAndUserId(String courseId,
                                         String userId,
                                         UserCourseStatus status);

    Optional<UserCourseStatus> findStatusByCourseIdAndUserId(String courseId,
                                                             String userId);

    boolean existsByLessonIdAndUserIdAndAuthority(List<Pair<String, String>> lessonUserIdPairs,
                                                  UserAuthority authority);

    default boolean existsInstructorByLessonIdAndUserId(List<Pair<String, String>> lessonUserIdPairs) {
        return existsByLessonIdAndUserIdAndAuthority(lessonUserIdPairs, UserAuthority.INSTRUCTOR);
    }

    boolean existAllByLessonIdAndUserId(List<Pair<String, String>> lessonUserIdPair);

    List<UserCourseDTO> findFullyGradedUserCoursesByLessonIds(List<String> lessonIds);

    List<UserCourseTotalGradeProjection> findUserCourseTotalGrades(List<UserCourseCompositeId> userCourseId);

    void updateStatusByUserCourseIds(List<UserCourseCompositeId> userCourseIds,
                                     UserCourseStatus status);

    void createInstructorUserCourseFeedback(UserCourseFeedbackDTO userCourseFeedbackDTO);
}
