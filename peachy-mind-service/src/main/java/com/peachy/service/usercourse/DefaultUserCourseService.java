package com.peachy.service.usercourse;

import com.peachy.commons.exception.ConflictException;
import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.commons.utils.ListParty;
import com.peachy.commons.utils.Utils;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.commons.utils.assertion.PartyAssert;
import com.peachy.converter.course.CourseFromBaseDTOConverter;
import com.peachy.converter.usercourse.UserCourseConverter;
import com.peachy.converter.usercourse.UserCourseToBaseDTOConverter;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.usercourse.*;
import com.peachy.entity.CourseEntity;
import com.peachy.entity.usercourse.UserCourseCompositeId;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.repository.UserCourseRepository;
import com.peachy.security.SecurityUtils;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import com.peachy.security.service.auth.UserService;
import com.peachy.service.course.CourseService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultUserCourseService implements UserCourseService {

    private final UserService defaultUserService;

    private final CourseService defaultCourseService;

    private final UserCourseRepository userCourseRepository;

    private final UserCourseConverter userCourseConverter;

    private final UserCourseToBaseDTOConverter toBaseDTOConverter;

    private final CourseFromBaseDTOConverter courseFromBaseDTOConverter;

    private final int MAXIMUM_COURSES_NUMBER_PER_STUDENT = 5;

    private final int MAXIMUM_INSTRUCTOR_USER_COURSE_FEEDBACK_LENGTH = 1000;

    @Override
    @Transactional
    public List<UserCourseDTO> assignInstructorsToCourse(List<String> instructorIds,
                                                         String courseId) {
        validateCourseIdForAssignment(courseId);
        validateInstructorIdForCourseAssignment(instructorIds, courseId);
        return assignUserToCourse(instructorIds, courseId);
    }

    private List<UserCourseDTO> assignUserToCourse(List<String> userIds,
                                                   String courseId) {
        CourseDTO course = defaultCourseService.findById(courseId).get();
        List<UserDTO> instructors = defaultUserService.findAllByIds(userIds);
        List<UserCourseEntity> userCourses = instructors.stream()
                .map(instructor -> {
                    UserCourseDTO userCourse = new UserCourseDTO();
                    userCourse.setCourse(courseFromBaseDTOConverter.fromBaseToExpanded(course));
                    userCourse.setUser(instructor);
                    userCourse.setStatus(UserCourseStatus.STARTED);
                    return userCourseConverter.toEntity(userCourse);
                })
                .collect(Collectors.toList());


        return userCourseRepository.saveAll(userCourses).stream()
                .map(userCourseConverter::toDto)
                .collect(Collectors.toList());
    }

    private void validateInstructorIdForCourseAssignment(List<String> instructorIds, String courseId) {
        Assert.that(courseId)
                .isNotBlank()
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to assign instructor to course"))
                        .withReason("Course id should not be empty")
                        .build());

        instructorIds.forEach(instructorId ->
                Assert.that(instructorId)
                        .isNotBlank()
                        .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to assign instructor to course"))
                                .withReason("Some instructor ids are empty")
                                .build()));

        Assert.that(instructorIds)
                .isTrue(ids -> defaultUserService.existAllByIdsAndAuthority(ids, UserAuthority.INSTRUCTOR))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to assign instructor to course"))
                        .withReason(String.format("User that you are trying to assign to course as %s is actually not of %s authority or doesn't exist at all",
                                UserAuthority.INSTRUCTOR.getStringyAuthority(),
                                UserAuthority.INSTRUCTOR.getStringyAuthority()))
                        .build())
                .isTrue(ids -> findCourseMemberIds(courseId).stream().noneMatch(ids::contains))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new ConflictException("Unable to assign instructor to course"))
                        .withReason("Some instructors are already assigned for given course")
                        .build());
    }

    private void validateCourseIdForAssignment(String courseId) {
        Assert.that(courseId)
                .isNotBlank()
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to assign user to course"))
                        .withReason("Course id should not be empty")
                        .build())
                .isTrue(defaultCourseService::existsById)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new NotFoundException("Unable to assign user to course"))
                        .withReason(String.format("No course found for id: %s", courseId))
                        .build());
    }

    @Override
    @Transactional
    public List<UserCourseDTO> assignStudentsToCourse(List<String> userIds, String courseId) {
        validateCourseIdForAssignment(courseId);
        validateStudentIdsForCourseAssignment(userIds, courseId);
        return assignUserToCourse(userIds, courseId);
    }

    private void validateStudentIdsForCourseAssignment(List<String> studentIds, String courseId) {
        Assert.that(studentIds)
                .areNotBlank()
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to assign student to course"))
                        .withReason("Some student ids are empty")
                        .build())
                .isTrue(ids -> defaultUserService.existAllByIdsAndAuthority(ids, UserAuthority.STUDENT))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to assign student to course"))
                        .withReason(String.format("User that you are trying to assign to course as %s is actually not of %s authority or doesn't exist at all",
                                UserAuthority.STUDENT.getStringyAuthority(),
                                UserAuthority.STUDENT.getStringyAuthority()))
                        .build())
                .isTrue(ids -> findCourseMemberIds(courseId).stream().noneMatch(ids::contains))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new ConflictException("Unable to assign student to course"))
                        .withReason("Some students are already assigned for given course")
                        .build())
                .isTrue(ids -> findUserCoursesCount(ids).values().stream().allMatch(count -> count < 5))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new ConflictException("Unable to assign student to course"))
                        .withReason(String.format("Some students are already enrolled for %s courses, and can't take anymore", MAXIMUM_COURSES_NUMBER_PER_STUDENT))
                        .build());
    }


    @Override
    public List<String> findCourseMemberIds(String courseId) {
        Assert.that(courseId)
                .isNotBlank()
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to retrieve course members"))
                        .withReason("Course id must not be empty")
                        .build());
        return userCourseRepository.findAllCourseMembersIds(courseId);
    }

    @Override
    public List<UserDTO> findCourseMembers(String courseId, UserAuthority userAuthority) {
        return Optional.ofNullable(findCoursesMembers(Collections.singletonList(courseId), Collections.singletonList(userAuthority)))
                .filter(members -> !members.isEmpty())
                .map(members -> members.get(0))
                .map(UsersPerCourseDTO::getUsers)
                .orElseGet(Collections::emptyList);
    }

    @Override
    public List<UsersPerCourseDTO> findCoursesMembers(List<String> courseIds, List<UserAuthority> userAuthorities) {
        Assert.that(courseIds)
                .isNotNull()
                .and(PartyAssert::hasNoNullElements)
                .and(PartyAssert::areNotBlank)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Failed to fetch course members"))
                        .withReason("Course ids should not be null")
                        .build());

        Assert.that(userAuthorities)
                .isNotNull()
                .and(PartyAssert::hasNoNullElements)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Failed to check if there is users with such authority for given ids"))
                        .withReason("Authorities should not be null")
                        .build());

        List<String> authoritiesNames = userAuthorities.stream().map(UserAuthority::name).collect(Collectors.toList());

        Map<String, List<String>> courseIdMemberIds = userCourseRepository.findAllCoursesMembers(courseIds, authoritiesNames).stream()
                .collect(Collectors.groupingBy(UserCourseProjection::getCourseId))
                .entrySet().stream()
                .map(entry -> Pair.of(entry.getKey(), entry.getValue().stream().map(UserCourseProjection::getUserId).collect(Collectors.toList())))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

        List<String> userIds = courseIdMemberIds.values().stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());


        CompletableFuture<List<UserDTO>> futureUsers = CompletableFuture.supplyAsync(() -> defaultUserService.findAllByIds(userIds));
        CompletableFuture<List<CourseDTO>> futureCourses = CompletableFuture.supplyAsync(() -> defaultCourseService.findAllByIds(courseIds));

        Utils.unmarshallFuture(CompletableFuture.allOf(futureCourses, futureUsers));

        List<UserDTO> users = Utils.unmarshallFuture(futureUsers);
        List<CourseDTO> courses = Utils.unmarshallFuture(futureCourses);

        return courseIdMemberIds.keySet().stream()
                .map(strings -> new UsersPerCourseDTO(
                        ListParty.of(courses)
                                .select(strings, CourseDTO::getId)
                                .orElseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Failed to fetch course members"))
                                        .withReason("Course ids should not be null")
                                        .build()),
                        ListParty.of(users)
                                .selectSubParty(userIds::contains, UserDTO::getId)))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Long> findUserCoursesCount(List<String> userIds) {
        Map<String, Long> userCoursesCount = userCourseRepository.findAllUserCourses(userIds).stream()
                .collect(Collectors.groupingBy(UserCourseProjection::getUserId, Collectors.counting()));

        userIds.forEach(userId -> userCoursesCount.putIfAbsent(userId, 0L));

        return userCoursesCount;
    }

    @Override
    public UsersPerCourseDTO findAllStudentsByCourseId(String courseId) {
        return Optional.ofNullable(findCoursesMembers(Collections.singletonList(courseId), Collections.singletonList(UserAuthority.STUDENT)))
                .filter(members -> !members.isEmpty())
                .map(members -> members.get(0))
                .orElseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException(String.format("Unable to retrieve students for course id: %s", courseId)))
                        .withReason(String.format("No students found for course id: %s", courseId))
                        .build());
    }

    @Override
    public boolean existsByLessonIdAndUserId(String lessonId,
                                             String userId) {
        Assert.that(lessonId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if user is assigned to course"))
                        .withReason("Unable to establish lesson to course relation, as lesson id is blank")
                        .build());

        Assert.that(userId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if user is assigned to course"))
                        .withReason("User id is blank")
                        .build());
        return userCourseRepository.existsByLessonIdAndUserId(lessonId, userId);
    }


    public boolean existAllByLessonIdAndUserId(List<Pair<String,String>> lessonUserIdPairs) {
      lessonUserIdPairs.forEach(lessonUserIdPair -> {
          Assert.that(lessonUserIdPair.getKey())
                  .isNotBlank()
                  .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if user is assigned to course"))
                          .withReason("Unable to establish lesson to course relation, as lesson id is blank")
                          .build());

          Assert.that(lessonUserIdPair.getValue())
                  .isNotBlank()
                  .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if user is assigned to course"))
                          .withReason("User id is blank")
                          .build());
      });
      return existsByLessonIdAndUserIdAndAuthority(lessonUserIdPairs, UserAuthority.ANY);
    }

    @Override
    public boolean existsByLessonIdAndUserIdAndAuthority(List<Pair<String,String>> lessonUserIdPairs,
                                                         UserAuthority givenAuthority) {
        Assert.that(givenAuthority)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if user with given role is assigned to course"))
                        .withReason("User authority is empty")
                        .build());

        lessonUserIdPairs.forEach(lessonUserIdPair -> {
            Assert.that(lessonUserIdPair.getKey())
                    .isNotBlank()
                    .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if user is assigned to course"))
                            .withReason("Unable to establish lesson to course relation, as lesson id is blank")
                            .build());

            Assert.that(lessonUserIdPair.getValue())
                    .isNotBlank()
                    .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to check if user is assigned to course"))
                            .withReason("User id is blank")
                            .build());
        });

        List<String> lessonIds = lessonUserIdPairs.stream()
                .map(Pair::getKey)
                .distinct()
                .collect(Collectors.toList());

        List<String> userIds = lessonUserIdPairs.stream()
                .map(Pair::getValue)
                .distinct()
                .collect(Collectors.toList());

        Map<String, List<String>> lessonGroupedUsers = ListParty.of(lessonUserIdPairs)
                .groupByNestedProperty(Pair::getKey, Function.identity())
                .turnIntoMap(entry -> Pair.of(entry.getKey(), entry.getValue().stream().map(Pair::getValue).collect(Collectors.toList())))
                .endParty();


        List<ExpandedUserCourseProjection> userCourseProjections = userCourseRepository.findAllUserCoursesByLessonIdsAndUserIds(lessonIds, userIds);

        Map<String, List<String>> dbLessonGroupedUsers = ListParty.of(userCourseProjections)
                .groupByNestedProperty(ExpandedUserCourseProjection::getLessonId, lessonId -> lessonId)
                .turnIntoMap(entry -> Pair.of(entry.getKey(), entry.getValue().stream().map(ExpandedUserCourseProjection::getUserId).collect(Collectors.toList())))
                .endParty();

        List<UserAuthority> userAuthorities = userCourseProjections.stream()
                .map(ExpandedUserCourseProjection::getUserAuthority)
                .distinct()
                .collect(Collectors.toList());

        return lessonGroupedUsers.entrySet().stream()
                .allMatch(entry ->
                        dbLessonGroupedUsers.containsKey(entry.getKey()) &&
                                dbLessonGroupedUsers.get(entry.getKey()).containsAll(entry.getValue()) &&
                                (userAuthorities.stream().allMatch(authority -> Objects.equals(authority,givenAuthority)) || Objects.equals(givenAuthority, UserAuthority.ANY)));
    }

    @Override
    @Transactional
    public void updateStatusByCourseIdAndUserId(String courseId,
                                                String userId,
                                                UserCourseStatus status) {
        Assert.that(courseId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to update user course status"))
                        .withReason("Course id is blank")
                        .build());
        Assert.that(userId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to update user course status"))
                        .withReason("User id is blank")
                        .build());
        Assert.that(status)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to update user course status"))
                        .withReason("Status is empty")
                        .build());

        userCourseRepository.updateStatusByCourseIdAndUserId(courseId, userId, status.name());
    }

    @Override
    @Transactional
    public void updateStatusByUserCourseIds(List<UserCourseCompositeId> userCourseIds,
                                            UserCourseStatus status) {

        Assert.that(userCourseIds)
                .hasNoNullElements()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to update user course status"))
                        .withReason("Some user course ids are null")
                        .withFault("Developer's fault")
                        .build())
                .isTrue(compositeIds -> compositeIds.stream()
                        .allMatch(compositeId -> StringUtils.isNotBlank(compositeId.getCourseId())))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to update user course status"))
                        .withReason("Some course ids are empty")
                        .build())
                .isTrue(compositeIds -> compositeIds.stream()
                        .allMatch(compositeId -> StringUtils.isNotBlank(compositeId.getUserId())))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to update user course status"))
                        .withReason("Some user ids are empty")
                        .build());

        Assert.that(status)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to update user course status"))
                        .withReason("Status is empty")
                        .build());

        List<String> collapsedIds = userCourseIds.stream()
                .map(UserCourseCompositeId::getCollapsedId)
                .collect(Collectors.toList());


        userCourseRepository.updateStatusByUserCourseIds(collapsedIds, status.name());
    }

    @Override
    public Optional<UserCourseStatus> findStatusByCourseIdAndUserId(String courseId,
                                                                    String userId) {
        Assert.that(courseId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to retrieve user course status"))
                        .withReason("Course id is blank")
                        .build());
        Assert.that(userId)
                .isNotBlank()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to retrieve user course status"))
                        .withReason("User id is blank")
                        .build());
        return userCourseRepository.findStatusByCourseIdAndUserId(courseId, userId);
    }

    @Override
    public List<UserCourseDTO> findFullyGradedUserCoursesByLessonIds(List<String> lessonIds) {
        return userCourseRepository.findFullyGradedUserCoursesByLessonIds(lessonIds).stream()
                .map(toBaseDTOConverter::fromProjectionToBase)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserCourseTotalGradeProjection> findUserCourseTotalGrades(List<UserCourseCompositeId> userCourseId) {
        List<String> courseIds = userCourseId.stream().map(UserCourseCompositeId::getCourse).map(CourseEntity::getId).collect(Collectors.toList());
        List<String> userIds = userCourseId.stream().map(UserCourseCompositeId::getUser).map(UserEntity::getId).collect(Collectors.toList());
        return userCourseRepository.findTotalGradesByUserCourseIds(courseIds, userIds);
    }

    @Override
    @Transactional
    public void createInstructorUserCourseFeedback(UserCourseFeedbackDTO userCourseFeedbackDTO) {
        validateFeedbackForCreation(userCourseFeedbackDTO);
        userCourseRepository.updateFeedbackForUserCourse(userCourseFeedbackDTO);
    }

    private void validateFeedbackForCreation(UserCourseFeedbackDTO userCourseFeedbackDTO) {
        Assert.that(userCourseFeedbackDTO)
                .isNotNull()
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to create user course feedback"))
                        .withReason("User course feedback object is empty")
                        .build())
                .isNotBlank(UserCourseFeedbackDTO::getCourseId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to create user course feedback"))
                        .withReason("Course id is blank")
                        .build())
                .isNotBlank(UserCourseFeedbackDTO::getUserId)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to create user course feedback"))
                        .withReason("User id is blank")
                        .build())
                .isTrue(userCourseFeedback -> userCourseRepository.existsById(new UserCourseCompositeId(userCourseFeedback.getUserId(), userCourseFeedback.getCourseId())))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to create user course feedback"))
                        .withReason("User is not assigned to course")
                        .build())
                .isNotBlank(UserCourseFeedbackDTO::getFeedback)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to create user course feedback"))
                        .withReason("Feedback is blank")
                        .build())
                .isTrue(userCourseFeedback -> userCourseFeedback.getFeedback().length() < MAXIMUM_INSTRUCTOR_USER_COURSE_FEEDBACK_LENGTH)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to create user course feedback"))
                        .withReason("Feedback is larger than 1000 chars")
                        .build());

        Optional<UserCourseStatus> userCourseStatus = findStatusByCourseIdAndUserId(userCourseFeedbackDTO.getCourseId(), userCourseFeedbackDTO.getUserId());

        userCourseStatus.orElseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException("Unable to create user course feedback"))
                .withReason("Failed to determine user course status")
                .withFault("Developer's fault")
                .build());

        Assert.that(userCourseStatus.get())
                .isTrue(status -> Arrays.asList(UserCourseStatus.FAILED, UserCourseStatus.FINISHED).contains(status))
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new ConflictException("Unable to create user course feedback"))
                        .withReason("User course is not completed")
                        .build());
    }


}
