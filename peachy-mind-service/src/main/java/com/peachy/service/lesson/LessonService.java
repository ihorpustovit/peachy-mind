package com.peachy.service.lesson;

import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface LessonService {
    List<LessonDTO> saveAll(List<LessonCreationDTO> lessons);

    default LessonDTO save(LessonCreationDTO lesson) {
        return saveAll(Collections.singletonList(lesson)).get(0);
    }

    List<LessonDTO> findAllByIds(List<String> lessonIds);

    Optional<LessonDTO> findById(String lessonId);

    LessonsPerCourseDTO findAllByCourseId(String courseId);

    List<LessonsPerCourseDTO> findAllByCourseIds(List<String> courseIds);

    Map<String, Integer> findMaxGradesForLessonIds(List<String> lessonIds);

    boolean existsById(String lessonId);

    boolean existAllByIds(List<String> lessonIds);
}
