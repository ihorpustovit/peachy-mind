package com.peachy.service.lesson;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.commons.utils.ListParty;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.commons.utils.assertion.PartyAssert;
import com.peachy.converter.lesson.LessonConverter;
import com.peachy.converter.lesson.LessonFromBaseDTOConverter;
import com.peachy.converter.lesson.LessonToBaseDTOConverter;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.LessonsPerCourseDTO;
import com.peachy.dto.lesson.MaxGradeProjection;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.repository.LessonRepository;
import com.peachy.service.course.CourseService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultLessonService implements LessonService {

    private final LessonRepository lessonRepository;

    private final LessonConverter lessonConverter;

    private final LessonToBaseDTOConverter toBaseDTOConverter;

    private final LessonFromBaseDTOConverter fromBaseDTOConverter;

    private CourseService defaultCourseService;

    private final int MAXIMUM_LESSON_TITLE_LENGTH = 80;

    private final int MAXIMUM_LESSON_SHORT_DESCRIPTION_LENGTH = 500;

    @Autowired
    @Lazy
    public void setDefaultCourseService(CourseService defaultCourseService) {
        this.defaultCourseService = defaultCourseService;
    }

    @Override
    @Transactional
    public List<LessonDTO> saveAll(List<LessonCreationDTO> lessonCreations) {
        validateLessonForCreation(lessonCreations);

        List<LessonDTO> lessons = convertFromCreationToBase(lessonCreations);
        Map<String, CourseDTO> courseIdCourse = lessons.stream()
                .map(LessonDTO::getCourse)
                .collect(Collectors.groupingBy(CourseDTO::getId)).entrySet().stream()
                .filter(entry -> !entry.getValue().isEmpty())
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().get(0)));

        return lessonRepository.saveAll(lessons.stream().map(lessonConverter::toEntity).collect(Collectors.toList())).stream()
                .map(savedLesson -> {
                    LessonDTO lessonDTO = lessonConverter.toDto(savedLesson);
                    lessonDTO.setCourse(courseIdCourse.get(savedLesson.getCourse().getId()));
                    return lessonDTO;
                })
                .collect(Collectors.toList());
    }

    private List<LessonDTO> convertFromCreationToBase(List<LessonCreationDTO> lessonCreations) {
        Map<String, List<LessonCreationDTO>> courseIdLesson = lessonCreations.stream().collect(Collectors.groupingBy(LessonCreationDTO::getCourseId));

        List<CourseDTO> courses = defaultCourseService.findAllByIds(new ArrayList<>(courseIdLesson.keySet()));
        return courses.stream()
                .map(course -> {
                    List<LessonDTO> lessons = courseIdLesson.get(course.getId()).stream()
                            .map(toBaseDTOConverter::fromCreationToBase)
                            .collect(Collectors.toList());
                    lessons.forEach(lessonDTO -> lessonDTO.setCourse(course));
                    return lessons;
                }).flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private void validateLessonForCreation(List<LessonCreationDTO> lessonsCreations) {
        Consumer<LessonCreationDTO> simpleAssertions = lessonCreationDTO -> {
            Assert.that(lessonCreationDTO)
                    .isNotBlank(LessonCreationDTO::getTitle)
                    .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create lesson"))
                            .withReason("Lesson title should not be empty")
                            .build())

                    .isTrue(courseDTO -> courseDTO.getTitle().length() <= MAXIMUM_LESSON_TITLE_LENGTH)
                    .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create lesson"))
                            .withReason(String.format("Course title should not exceed %s characters", MAXIMUM_LESSON_TITLE_LENGTH))
                            .build())

                    .isTrue(courseDTO ->
                            Objects.isNull(courseDTO.getShortDescription()) ||
                                    courseDTO.getShortDescription().length() <= MAXIMUM_LESSON_SHORT_DESCRIPTION_LENGTH)
                    .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create lesson"))
                            .withReason(String.format("Course description should not exceed %s characters", MAXIMUM_LESSON_SHORT_DESCRIPTION_LENGTH))
                            .build());
        };

        Consumer<List<LessonCreationDTO>> complexAssertions = lessonCreationDTOs -> {
            Assert.that(lessonCreationDTOs).isTrue(lessonCreations -> {
                List<String> courseIds = lessonCreations.stream().map(LessonCreationDTO::getCourseId).collect(Collectors.toList());
                Assert.that(courseIds)
                        .areNotBlank()
                        .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadRequestException("Unable to create lesson"))
                                .withReason("Course id that lesson is assigned to should not be blank or empty")
                                .build());
                return defaultCourseService.existAllByIds(courseIds);
            })
                    .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to create lesson"))
                            .withReason("Course that lesson's being assigned to doesn't exist")
                            .withSolution("Assign lesson to course that actually exists")
                            .build());

        };

        lessonsCreations.forEach(simpleAssertions);
        complexAssertions.accept(lessonsCreations);
    }

    @Override
    public List<LessonDTO> findAllByIds(List<String> lessonIds) {
        return lessonConverter.toDto(lessonRepository.findAllById(lessonIds));
    }

    @Override
    public Optional<LessonDTO> findById(String lessonId) {
        return Optional.of(findAllByIds(Collections.singletonList(lessonId)))
                .filter(lessons -> !lessons.isEmpty())
                .map(lessons -> lessons.get(0));
    }

    @Override
    public boolean existsById(String lessonId) {
        return lessonRepository.existsById(lessonId);
    }

    @Override
    public boolean existAllByIds(List<String> lessonIds) {
        return lessonRepository.existsAllByIdIn(lessonIds);
    }

    @Override
    public LessonsPerCourseDTO findAllByCourseId(String courseId) {
        return Optional.of(findAllByCourseIds(Collections.singletonList(courseId)))
                .filter(lessonsPerCourseDTOS -> !lessonsPerCourseDTOS.isEmpty())
                .map(lessonsPerCourseDTOS -> lessonsPerCourseDTOS.get(0))
                .orElseThrow(() -> ExceptionBuilder.buildFrom(new NotFoundException(String.format("Unable to retrieve lessons for course id: %s", courseId)))
                        .withReason(String.format("No lessons found for course id: %s", courseId))
                        .build());
    }

    @Override
    public List<LessonsPerCourseDTO> findAllByCourseIds(List<String> courseIds) {
        Assert.that(courseIds)
                .isNotNull()
                .and(PartyAssert::hasNoNullElements)
                .and(PartyAssert::areNotBlank)
                .otherwiseThrow(() -> ExceptionBuilder.buildFrom(new BadArgumentException("Unable to retrieve lessons for courses"))
                        .withReason("Course ids should not be empty")
                        .build());

        return ListParty.of(lessonConverter.toDto(lessonRepository.findAllByCourseIdIn(courseIds)))
                .groupByNestedProperty(LessonDTO::getCourse, CourseDTO::getId)
                .turnInto(entry -> new LessonsPerCourseDTO(entry.getKey(), entry.getValue().stream().map(fromBaseDTOConverter::toLessonInCourseFromBase).collect(Collectors.toList())));
    }

    @Override
    public Map<String, Integer> findMaxGradesForLessonIds(List<String> lessonIds) {
        return lessonRepository.findMaxGradesByLessonIds(lessonIds).stream()
                .collect(Collectors.toMap(MaxGradeProjection::getId,MaxGradeProjection::getMaxGrade));
    }
}
