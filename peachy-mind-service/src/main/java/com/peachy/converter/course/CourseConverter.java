package com.peachy.converter.course;

import com.peachy.dto.course.CourseDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.jpa.converter.EntityDtoConverter;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseConverter extends EntityDtoConverter<CourseEntity, CourseDTO> {

    @Override
    CourseDTO to(CourseEntity e);

    @Override
    CourseEntity from(CourseDTO expandedCourseDTO);
}
