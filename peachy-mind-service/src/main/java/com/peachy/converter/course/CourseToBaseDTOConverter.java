package com.peachy.converter.course;

import com.peachy.dto.course.CourseCreationDTO;
import com.peachy.dto.course.CourseDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseToBaseDTOConverter {
    CourseDTO fromCreationToBase(CourseCreationDTO courseCreation);
}
