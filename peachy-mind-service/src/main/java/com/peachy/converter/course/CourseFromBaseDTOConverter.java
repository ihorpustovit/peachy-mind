package com.peachy.converter.course;

import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.dto.course.ExpandedCourseProjection;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseFromBaseDTOConverter {
    ExpandedCourseDTO fromBaseToExpanded(CourseDTO course);

    ExpandedCourseDTO fromProjectionToExpanded(ExpandedCourseProjection expandedCourseProjection);
}
