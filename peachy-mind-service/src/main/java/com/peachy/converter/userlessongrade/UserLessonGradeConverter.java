package com.peachy.converter.userlessongrade;

import com.peachy.converter.lesson.LessonConverter;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.userlessongrade.UserLessonGradeDTO;
import com.peachy.entity.LessonEntity;
import com.peachy.entity.userlessongrade.UserLessonGradeEntity;
import com.peachy.jpa.converter.EntityDtoConverter;
import com.peachy.security.converter.UserConverter;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.Objects;

@Mapper(componentModel = "spring")
public interface UserLessonGradeConverter extends EntityDtoConverter<UserLessonGradeEntity, UserLessonGradeDTO> {

    @Override
    @Mapping(source = "id.lesson", target = "lesson", qualifiedByName = "mapLessonToDTO")
    @Mapping(source = "id.user", target = "user", qualifiedByName = "mapUserToDTO")
    @Mapping(source = "instructorId", target = "instructor", qualifiedByName = "mapUserIdToDTO")
    UserLessonGradeDTO to(UserLessonGradeEntity e);

    @Named("mapUserToDTO")
    default UserDTO mapUserToDTO(UserEntity userEntity) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toDto(userEntity);
    }

    @Named("mapUserIdToDTO")
    default UserDTO mapUserIdToDTO(String userId) {
        if (StringUtils.isNotBlank(userId)) {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(userId);
            return userDTO;
        } else {
            return null;
        }
    }

    @Named("mapLessonToDTO")
    default LessonDTO mapLessonToDTO(LessonEntity lessonEntity) {
        LessonConverter lessonConverter = Mappers.getMapper(LessonConverter.class);
        return lessonConverter.toDto(lessonEntity);
    }

    @Override
    @Mapping(source = "lesson", target = "id.lesson", qualifiedByName = "mapLessonToEntity")
    @Mapping(source = "user", target = "id.user", qualifiedByName = "mapUserToEntity")
    @Mapping(source = "instructor.id", target = "instructorId", qualifiedByName = "mapUserDTOToUserId")
    UserLessonGradeEntity from(UserLessonGradeDTO userLessonGradeDTO);

    @Named("mapUserToEntity")
    default UserEntity mapUserToEntity(UserDTO userDTO) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toEntity(userDTO);
    }

    @Named("mapUserIdToEntity")
    default String mapUserDTOToUserId(UserDTO userDTO) {
        return Objects.isNull(userDTO) ? null : userDTO.getId();
    }

    @Named("mapLessonToEntity")
    default LessonEntity mapLessonToEntity(LessonDTO lessonDTO) {
        LessonConverter lessonConverter = Mappers.getMapper(LessonConverter.class);
        return lessonConverter.toEntity(lessonDTO);
    }
}
