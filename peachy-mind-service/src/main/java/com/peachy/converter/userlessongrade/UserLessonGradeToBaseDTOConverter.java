package com.peachy.converter.userlessongrade;

import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.userlessongrade.UserLessonGradeCreationDTO;
import com.peachy.dto.userlessongrade.UserLessonGradeDTO;
import com.peachy.security.dto.UserDTO;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface UserLessonGradeToBaseDTOConverter {
    @Mapping(source = "lessonId", target = "lesson", qualifiedByName = "mapLessonIdToDTO")
    @Mapping(source = "userId", target = "user" ,qualifiedByName = "mapUserIdToDTO")
    @Mapping(source = "instructorId", target = "instructor" ,qualifiedByName = "mapUserIdToDTO")
    UserLessonGradeDTO fromCreationToBase(UserLessonGradeCreationDTO userLessonGradeCreation);

    @Named("mapUserIdToDTO")
    default UserDTO mapUserIdToDTO(String userId) {
        if (StringUtils.isNotBlank(userId)) {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(userId);
            return userDTO;
        } else {
            return null;
        }
    }

    @Named("mapLessonIdToDTO")
    default LessonDTO mapLessonIdToDTO(String lessonId) {
        LessonDTO lessonDTO = new LessonDTO();
        lessonDTO.setId(lessonId);
        return lessonDTO;
    }
}
