package com.peachy.converter.lesson;

import com.peachy.converter.course.CourseConverter;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.entity.LessonEntity;
import com.peachy.jpa.converter.EntityDtoConverter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface LessonConverter extends EntityDtoConverter<LessonEntity, LessonDTO> {
    @Override
    @Mapping(target = "course", source = "course", qualifiedByName = "mapCourseToDTO")
    LessonDTO to(LessonEntity e);

    @Named("mapCourseToDTO")
    default CourseDTO mapCourseToDTO(CourseEntity courseEntity) {
        CourseConverter courseConverter = Mappers.getMapper(CourseConverter.class);
        return courseConverter.toDto(courseEntity);
    }

    @Override
    @Mapping(target = "course", source = "course", qualifiedByName = "mapCourseToEntity")
    LessonEntity from(LessonDTO lessonDTO);

    @Named("mapCourseToEntity")
    default CourseEntity mapCourseToEntity(CourseDTO expandedCourseDTO) {
        CourseConverter courseConverter = Mappers.getMapper(CourseConverter.class);
        return courseConverter.toEntity(expandedCourseDTO);
    }
}
