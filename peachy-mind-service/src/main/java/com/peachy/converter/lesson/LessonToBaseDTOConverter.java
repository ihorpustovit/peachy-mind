package com.peachy.converter.lesson;

import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.dto.lesson.creation.LessonInCourseCreationDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LessonToBaseDTOConverter {
    LessonDTO fromCreationToBase(LessonCreationDTO lessonCreation);

    LessonDTO fromInCourseCreationToBase(LessonInCourseCreationDTO lessonCreation);
}
