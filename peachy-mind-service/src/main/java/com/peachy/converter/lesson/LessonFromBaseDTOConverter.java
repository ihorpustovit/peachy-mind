package com.peachy.converter.lesson;

import com.peachy.dto.lesson.LessonInCourseDTO;
import com.peachy.dto.lesson.creation.LessonCreationDTO;
import com.peachy.dto.lesson.LessonDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LessonFromBaseDTOConverter {
    LessonCreationDTO toLessonCreationFromBase(LessonDTO lesson);

    LessonInCourseDTO toLessonInCourseFromBase(LessonDTO lesson);
}
