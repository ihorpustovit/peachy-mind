package com.peachy.converter.homework;

import com.peachy.dto.homework.ExpandedHomeworkDTO;
import com.peachy.dto.homework.HomeworkDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HomeworkFromBaseDTOConverter {
    ExpandedHomeworkDTO fromBaseToExpanded(HomeworkDTO homeworkDTO);
}
