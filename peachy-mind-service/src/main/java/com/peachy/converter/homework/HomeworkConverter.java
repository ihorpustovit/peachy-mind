package com.peachy.converter.homework;

import com.peachy.converter.lesson.LessonConverter;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.entity.LessonEntity;
import com.peachy.entity.homework.HomeworkEntity;
import com.peachy.jpa.converter.EntityDtoConverter;
import com.peachy.security.converter.UserConverter;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface HomeworkConverter extends EntityDtoConverter<HomeworkEntity, HomeworkDTO> {
    @Override
    @Mapping(target = "user", source = "id.user", qualifiedByName = "mapUserToDTO")
    @Mapping(target = "lesson", source = "id.lesson", qualifiedByName = "mapLessonToDTO")
    HomeworkDTO to(HomeworkEntity e);

    @Named("mapUserToDTO")
    default UserDTO mapUserToDTO(UserEntity userEntity) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toDto(userEntity);
    }

    @Named("mapLessonToDTO")
    default LessonDTO mapLessonToDTO(LessonEntity lessonEntity) {
        LessonConverter lessonConverter = Mappers.getMapper(LessonConverter.class);
        return lessonConverter.toDto(lessonEntity);
    }

    @Override
    @Mapping(target = "id.user", source = "user", qualifiedByName = "mapUserToEntity")
    @Mapping(target = "id.lesson", source = "lesson", qualifiedByName = "mapLessonToEntity")
    HomeworkEntity from(HomeworkDTO homeworkDTO);

    @Named("mapUserToEntity")
    default UserEntity mapUserToEntity(UserDTO userDTO) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toEntity(userDTO);
    }

    @Named("mapLessonToEntity")
    default LessonEntity mapLessonToEntity(LessonDTO lessonDTO) {
        LessonConverter lessonConverter = Mappers.getMapper(LessonConverter.class);
        return lessonConverter.toEntity(lessonDTO);
    }
}
