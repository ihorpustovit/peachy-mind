package com.peachy.converter.homework;

import com.peachy.dto.homework.HomeworkCreationDTO;
import com.peachy.dto.homework.HomeworkDTO;
import com.peachy.dto.homework.HomeworkProjection;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.security.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface HomeworkToBaseDTOConverter {
    HomeworkDTO fromCreationToBase(HomeworkCreationDTO homeworkCreation);

    @Mapping(target = "user", source = "userId", qualifiedByName = "mapUserToDTO")
    @Mapping(target = "lesson", source = "lessonId", qualifiedByName = "mapLessonToDTO")
    HomeworkDTO fromProjectionToBase(HomeworkProjection homeworkProjection);

    @Named("mapLessonToDTO")
    default LessonDTO mapLessonToDTO(String lessonId) {
        LessonDTO lessonDTO = new LessonDTO();
        lessonDTO.setId(lessonId);
        return lessonDTO;
    }

    @Named("mapUserToDTO")
    default UserDTO mapUserToDTO(String userId) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userId);
        return userDTO;
    }
}
