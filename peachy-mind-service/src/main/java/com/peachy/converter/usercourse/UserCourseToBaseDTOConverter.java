package com.peachy.converter.usercourse;

import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.dto.usercourse.UserCourseProjection;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserCourseToBaseDTOConverter {
    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "courseId", target = "course.id")
    @Mapping(source = "userAuthority", target = "user.authority")
    UserCourseDTO fromProjectionToBase(UserCourseProjection userCourseProjection);
}
