package com.peachy.converter.usercourse;

import com.peachy.converter.course.CourseConverter;
import com.peachy.dto.course.CourseDTO;
import com.peachy.dto.usercourse.UserCourseDTO;
import com.peachy.entity.CourseEntity;
import com.peachy.entity.usercourse.UserCourseEntity;
import com.peachy.jpa.converter.EntityDtoConverter;
import com.peachy.security.converter.UserConverter;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserCourseConverter extends EntityDtoConverter<UserCourseEntity, UserCourseDTO> {
    @Override
    @Mapping(target = "user", source = "id.user", qualifiedByName = "mapUserToDTO")
    @Mapping(target = "course", source = "id.course", qualifiedByName = "mapCourseToDTO")
    UserCourseDTO to(UserCourseEntity e);

    @Named("mapUserToDTO")
    default UserDTO mapUserToDTO(UserEntity userEntity) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toDto(userEntity);
    }

    @Named("mapCourseToDTO")
    default CourseDTO mapCourseToDTO(CourseEntity courseEntity) {
        CourseConverter courseConverter = Mappers.getMapper(CourseConverter.class);
        return courseConverter.toDto(courseEntity);
    }

    @Override
    @Mapping(target = "id.user", source = "user", qualifiedByName = "mapUserToEntity")
    @Mapping(target = "id.course", source = "course", qualifiedByName = "mapCourseToEntity")
    UserCourseEntity from(UserCourseDTO userCourseDTO);

    @Named("mapUserToEntity")
    default UserEntity mapUserToEntity(UserDTO userDTO) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toEntity(userDTO);
    }

    @Named("mapCourseToEntity")
    default CourseEntity mapCourseToEntity(CourseDTO expandedCourseDTO) {
        CourseConverter courseConverter = Mappers.getMapper(CourseConverter.class);
        return courseConverter.toEntity(expandedCourseDTO);
    }
}
