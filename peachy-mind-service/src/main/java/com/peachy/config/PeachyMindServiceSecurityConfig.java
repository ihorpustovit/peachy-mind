package com.peachy.config;

import com.peachy.security.UserAuthority;
import com.peachy.security.filter.InternalAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class PeachyMindServiceSecurityConfig extends WebSecurityConfigurerAdapter {

    private final InternalAuthenticationFilter internalAuthenticationFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/").permitAll()

                .antMatchers("/course/**/assign/instructor").hasAuthority(UserAuthority.ADMIN.getStringyAuthority())
                .antMatchers(HttpMethod.POST, "/course").hasAnyAuthority(UserAuthority.INSTRUCTOR.getStringyAuthority())
                .antMatchers(HttpMethod.POST, "/lesson").hasAnyAuthority(UserAuthority.INSTRUCTOR.getStringyAuthority())
                .antMatchers(HttpMethod.GET,"/course/**/students").hasAuthority(UserAuthority.INSTRUCTOR.getStringyAuthority())
                .antMatchers(HttpMethod.PATCH, "/course/feedback").hasAuthority(UserAuthority.INSTRUCTOR.getStringyAuthority())
                .antMatchers(HttpMethod.GET,"/lesson/test").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .csrf().disable()
                .addFilterAfter(internalAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

}
