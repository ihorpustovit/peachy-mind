package com.peachy.dto.userlessongrade;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.peachy.dto.lesson.LessonDTO;
import com.peachy.jpa.dto.BaseDTO;
import com.peachy.security.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserLessonGradeDTO extends BaseDTO {

    private LessonDTO lesson;

    private UserDTO user;

    private UserDTO instructor;

    private Integer grade;
}
