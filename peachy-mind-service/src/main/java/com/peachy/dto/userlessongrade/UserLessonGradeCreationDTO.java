package com.peachy.dto.userlessongrade;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserLessonGradeCreationDTO {
    private String lessonId;

    private String userId;

    private String instructorId;

    private Integer grade;
}
