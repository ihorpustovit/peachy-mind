package com.peachy.dto.homework;

import com.peachy.dto.lesson.LessonDTO;
import com.peachy.jpa.dto.BaseDTO;
import com.peachy.security.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomeworkDTO extends BaseDTO implements HomeworkProjection {
    private LessonDTO lesson;

    private UserDTO user;

    private String s3Key;

    private String s3Bucket;

    private String fileName;

    @Override
    public String getLessonId() {
        return Objects.nonNull(lesson) ? lesson.getId() : null;
    }

    @Override
    public String getUserId() {
        return Objects.nonNull(user) ? user.getId() : null;
    }
}
