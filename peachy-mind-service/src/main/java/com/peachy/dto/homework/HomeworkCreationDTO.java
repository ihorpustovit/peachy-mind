package com.peachy.dto.homework;

import com.peachy.security.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HomeworkCreationDTO {
    private UserDTO user;

    private String lessonId;

    private MultipartFile homeworkFile;
}
