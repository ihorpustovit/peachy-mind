package com.peachy.dto.homework;

public interface HomeworkProjection {
    String getLessonId();

    String getUserId();

    String getS3Key();

    String getS3Bucket();

    String getFileName();
}
