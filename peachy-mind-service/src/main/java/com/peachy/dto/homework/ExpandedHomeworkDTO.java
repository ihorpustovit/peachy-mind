package com.peachy.dto.homework;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.ByteArrayOutputStream;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExpandedHomeworkDTO extends HomeworkDTO {
    @JsonIgnore
    private ByteArrayOutputStream homeworkFileStream;
}
