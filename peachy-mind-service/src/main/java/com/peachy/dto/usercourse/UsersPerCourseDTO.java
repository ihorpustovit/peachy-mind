package com.peachy.dto.usercourse;

import com.peachy.dto.course.CourseDTO;
import com.peachy.security.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsersPerCourseDTO {
    private CourseDTO course;

    private List<UserDTO> users;
}
