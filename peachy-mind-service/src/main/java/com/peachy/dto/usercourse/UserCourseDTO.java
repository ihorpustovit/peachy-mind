package com.peachy.dto.usercourse;

import com.peachy.dto.course.ExpandedCourseDTO;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.jpa.dto.BaseDTO;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCourseDTO extends BaseDTO implements UserCourseProjection {
    private UserDTO user;

    private ExpandedCourseDTO course;

    private UserCourseStatus status;

    @Override
    public String getUserId() {
        return Objects.nonNull(user) ? user.getId() : null;
    }

    @Override
    public String getCourseId() {
        return Objects.nonNull(course) ? course.getId() : null;
    }

    @Override
    public UserAuthority getUserAuthority() {
        return Objects.nonNull(user) ? user.getAuthority() : UserAuthority.NONE;
    }
}
