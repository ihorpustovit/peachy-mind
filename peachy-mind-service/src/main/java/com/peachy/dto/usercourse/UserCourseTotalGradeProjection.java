package com.peachy.dto.usercourse;

public interface UserCourseTotalGradeProjection {
    String getUserId();

    String getCourseId();

    Integer getTotalGrade();
}
