package com.peachy.dto.usercourse;

public interface ExpandedUserCourseProjection extends UserCourseProjection {
    String getLessonId();
}
