package com.peachy.dto.usercourse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class UserCourseFeedbackDTO {
    private String userId;

    private String courseId;

    private String feedback;
}
