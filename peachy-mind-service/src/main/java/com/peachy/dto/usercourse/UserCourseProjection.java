package com.peachy.dto.usercourse;

import com.peachy.security.UserAuthority;

public interface UserCourseProjection {
   String getUserId();

   UserAuthority getUserAuthority();

   String getCourseId();
}
