package com.peachy.dto.lesson;

import com.peachy.dto.course.CourseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LessonsPerCourseDTO {
    private CourseDTO course;

    private List<LessonInCourseDTO> lessons;
}
