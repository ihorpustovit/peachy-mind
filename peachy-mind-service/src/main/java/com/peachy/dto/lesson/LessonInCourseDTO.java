package com.peachy.dto.lesson;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.peachy.dto.course.CourseDTO;

public class LessonInCourseDTO extends LessonDTO {
    public LessonInCourseDTO() {
    }

    public LessonInCourseDTO(String id,
                             String title,
                             String shortDescription,
                             String content,
                             Integer maxGrade,
                             CourseDTO course) {
        super(id, title, shortDescription, content, maxGrade, course);
    }

    @Override
    @JsonIgnore
    public CourseDTO getCourse() {
        return super.getCourse();
    }
}
