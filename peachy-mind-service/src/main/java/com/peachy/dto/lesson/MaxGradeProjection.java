package com.peachy.dto.lesson;

public interface MaxGradeProjection {
    String getId();

    Integer getMaxGrade();
}
