package com.peachy.dto.lesson.creation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LessonInCourseCreationDTO {
    private String title;

    private String shortDescription;

    private String content;

    private Integer maxGrade;
}
