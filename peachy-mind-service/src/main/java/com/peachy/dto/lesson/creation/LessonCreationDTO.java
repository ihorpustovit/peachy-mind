package com.peachy.dto.lesson.creation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LessonCreationDTO {
    private String title;

    private String shortDescription;

    private String content;

    private String courseId;

    private Integer maxGrade;
}
