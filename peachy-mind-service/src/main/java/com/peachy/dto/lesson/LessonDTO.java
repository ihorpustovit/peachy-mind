package com.peachy.dto.lesson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.peachy.dto.course.CourseDTO;
import com.peachy.jpa.dto.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LessonDTO extends BaseDTO {
    private String id;

    private String title;

    private String shortDescription;

    private String content;

    private Integer maxGrade;

    private CourseDTO course;
}
