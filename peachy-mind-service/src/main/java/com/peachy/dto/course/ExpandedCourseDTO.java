package com.peachy.dto.course;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.peachy.dto.lesson.LessonInCourseDTO;
import com.peachy.enumeration.usercourse.UserCourseStatus;
import com.peachy.jpa.dto.BaseDTO;
import com.peachy.security.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExpandedCourseDTO extends BaseDTO {
    private String id;

    private String title;

    private String shortDescription;

    private List<UserDTO> instructors;

    private List<LessonInCourseDTO> lessons;

    private UserCourseStatus status;

    private Integer grade;
}
