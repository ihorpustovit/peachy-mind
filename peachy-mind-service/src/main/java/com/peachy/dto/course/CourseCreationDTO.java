package com.peachy.dto.course;

import com.peachy.dto.lesson.creation.LessonInCourseCreationDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseCreationDTO {
    private String title;

    private String shortDescription;

    private List<String> instructorIds;

    private List<LessonInCourseCreationDTO> lessons;

    public List<String> getInstructorIds() {
        return Objects.nonNull(instructorIds) ? instructorIds : Collections.emptyList();
    }

    public List<LessonInCourseCreationDTO> getLessons() {
        return Objects.nonNull(lessons) ? lessons : Collections.emptyList();
    }
}
