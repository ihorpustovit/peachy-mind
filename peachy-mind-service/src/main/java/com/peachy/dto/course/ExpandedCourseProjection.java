package com.peachy.dto.course;

import com.peachy.enumeration.usercourse.UserCourseStatus;

public interface ExpandedCourseProjection {
    String getId();

    String getTitle();

    String getShortDescription();

    UserCourseStatus getStatus();
}
