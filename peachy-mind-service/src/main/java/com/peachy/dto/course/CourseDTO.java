package com.peachy.dto.course;

import com.peachy.jpa.dto.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseDTO extends BaseDTO {
    private String id;

    private String title;

    private String shortDescription;
}
