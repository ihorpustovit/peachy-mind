package com.peachy.dto.course;

public interface CourseMaxPossibleGradeProjection {
    String getCourseId();

    Integer getMaxPossibleGrade();
}
