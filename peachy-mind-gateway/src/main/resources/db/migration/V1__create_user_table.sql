CREATE TABLE user(
    id VARCHAR(12) NOT NULL PRIMARY KEY,
    username VARCHAR(255) UNIQUE,
    hashed_password VARCHAR(255) NOT NULL,
    authority VARCHAR(100) NOT NULL,
    created_at DATETIME,
    updated_at DATETIME);

INSERT INTO user VALUE ('Krr93xdqvl4R','admin','$2a$10$KMjwuU.wKvvMDf7gl7Akr.Mmd1o48OI4ycA4V6bsOVUUxsUt4xCX6','ADMIN',now(),now());