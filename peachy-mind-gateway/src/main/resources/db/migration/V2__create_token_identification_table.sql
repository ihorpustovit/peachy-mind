CREATE TABLE token_identification(
    owner_id VARCHAR(12) NOT NULL PRIMARY KEY,
    token_id VARCHAR(12) NOT NULL,
    created_at DATETIME,
    updated_at DATETIME
);

ALTER TABLE
    token_identification
ADD CONSTRAINT
    FK_user_token_identification
FOREIGN KEY
    token_identification(owner_id)
REFERENCES
    user(id)
ON DELETE CASCADE;