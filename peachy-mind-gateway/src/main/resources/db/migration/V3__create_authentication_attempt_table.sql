CREATE TABLE authentication_attempt(
    user_id VARCHAR(12) NOT NULL PRIMARY KEY,
    attempts_count INTEGER NOT NULL,
    created_at DATETIME,
    updated_at DATETIME
);

ALTER TABLE
    authentication_attempt
ADD CONSTRAINT
    FK_user_authentication_attempt
FOREIGN KEY
    authentication_attempt(user_id)
REFERENCES
    user(id)
ON DELETE CASCADE;