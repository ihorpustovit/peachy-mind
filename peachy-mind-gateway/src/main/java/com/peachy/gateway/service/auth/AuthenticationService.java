package com.peachy.gateway.service.auth;


import com.peachy.gateway.dto.UserCredentialsDTO;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.JwtToken;

public interface AuthenticationService<T extends JwtToken> {

     GatewayAuthToken signIn(UserCredentialsDTO userCredentials);

     UserDTO signUp(UserDTO user);

     default boolean isActive(T token) {
          return !token.isExpired() && !isRevoked(token);
     }

     boolean isRevoked(T token);

     void revokeForUser(UserDTO user);

     T refresh(String pureRefreshToken);

     T parse(String stringyToken);
}
