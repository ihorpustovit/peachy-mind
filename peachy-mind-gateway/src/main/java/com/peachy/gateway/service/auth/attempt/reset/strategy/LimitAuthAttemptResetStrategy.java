package com.peachy.gateway.service.auth.attempt.reset.strategy;

import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class LimitAuthAttemptResetStrategy extends TTLAndCountBasedAuthAttemptsResetStrategy {

    public LimitAuthAttemptResetStrategy(AuthenticationAttemptService defaultAuthenticationAttemptsService,
                                         @Value("${brute-force.protection.auth-attempt.limit.ttl}") long authAttemptLimitTTLInSeconds,
                                         @Value("${brute-force.protection.auth-attempt.limit.count}") int authAttemptsCount) {
        super(authAttemptLimitTTLInSeconds,
                authAttemptsCount,
                defaultAuthenticationAttemptsService);
    }

    @Override
    protected void setAuthAttemptLimitTTLInSeconds(@Value("${brute-force.protection.auth-attempt.limit.ttl}") long authAttemptLimitTTLInSeconds) {
        super.setAuthAttemptLimitTTLInSeconds(authAttemptLimitTTLInSeconds);
    }

    @Override
    protected void setAuthAttemptsCount(@Value("${brute-force.protection.auth-attempt.limit.count}") int authAttemptsCount) {
        super.setAuthAttemptsCount(authAttemptsCount);
    }

    @Override
    protected boolean attemptsCountMatches(AuthenticationAttemptDTO authenticationAttempt) {
        return authenticationAttempt.getAttemptsCount() >= authAttemptsCount;
    }
}
