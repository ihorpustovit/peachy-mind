package com.peachy.gateway.service.auth.provider;

import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.security.service.auth.provider.PeachyMindAuthenticationProvider;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class GatewayAuthenticationProvider extends PeachyMindAuthenticationProvider<GatewayAuthToken> {
    public GatewayAuthenticationProvider(Environment environment) {
        super(GatewayAuthToken.class, environment);
    }
}
