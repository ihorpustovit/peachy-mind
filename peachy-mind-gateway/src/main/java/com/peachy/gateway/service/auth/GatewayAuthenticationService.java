package com.peachy.gateway.service.auth;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.commons.exception.notfound.PropertyNotFoundException;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.dto.UserCredentialsDTO;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.gateway.event.auth.GatewayAuthenticationFailureEvent;
import com.peachy.gateway.event.auth.GatewayAuthenticationSuccessEvent;
import com.peachy.gateway.service.auth.token.GatewayAuthTokenService;
import com.peachy.gateway.service.auth.token.RefreshTokenService;
import com.peachy.gateway.service.auth.token.identification.TokenIdentificationService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.exception.BadCredentialsException;
import com.peachy.security.exception.UnauthorizedException;
import com.peachy.security.service.auth.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
public class GatewayAuthenticationService implements AuthenticationService<GatewayAuthToken> {
    private final GatewayAuthTokenService gatewayAuthTokenService;

    private final RefreshTokenService refreshTokenService;

    private final TokenIdentificationService tokenIdentificationService;

    private final UserService defaultUserService;

    private final ApplicationEventPublisher applicationEventPublisher;

    private DaoAuthenticationProvider daoAuthenticationProvider;

    @Autowired
    @Lazy
    public void setDaoAuthenticationProvider(DaoAuthenticationProvider daoAuthenticationProvider) {
        this.daoAuthenticationProvider = daoAuthenticationProvider;
    }

    @Override
    public GatewayAuthToken signIn(UserCredentialsDTO userCredentials) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(userCredentials.getUsername(), userCredentials.getPassword());

        Authentication authentication;

        try {
            authentication = daoAuthenticationProvider.authenticate(usernamePasswordAuthenticationToken);
        } catch (org.springframework.security.authentication.BadCredentialsException | NotFoundException e) {
            applicationEventPublisher.publishEvent(new GatewayAuthenticationFailureEvent(userCredentials));
            throw ExceptionBuilder.buildFrom(new BadCredentialsException("Unable to perform authentication"))
                    .withReason("Username or password doesn't match")
                    .withSolution("Retry with other credentials")
                    .withFault("User's fault")
                    .build();
        } catch (InternalAuthenticationServiceException e) {
            applicationEventPublisher.publishEvent(new GatewayAuthenticationFailureEvent(userCredentials));
            if (NotFoundException.class.isAssignableFrom(e.getCause().getClass())) {
                throw ExceptionBuilder.buildFrom(new BadCredentialsException("Unable to perform authentication"))
                        .withReason("There is no user for such credentials")
                        .withSolution("Retry with other credentials")
                        .withFault("User's fault")
                        .build();
            } else {
                throw ExceptionBuilder.buildFrom(new PeachyMindException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to perform authentication"))
                        .withReason("Unresolved exception")
                        .withFault("Developer's fault")
                        .build();
            }
        }

        GatewayAuthToken gatewayAuthToken = generateAuthToken((UserDTO) authentication.getPrincipal());
        applicationEventPublisher.publishEvent(new GatewayAuthenticationSuccessEvent(userCredentials));
        return gatewayAuthToken;
    }

    @Override
    public UserDTO signUp(UserDTO user) {
        user = defaultUserService.save(user);
        return user;
    }

    @Override
    public void revokeForUser(UserDTO user) {
        Assert.that(user)
                .isNotNull()
                .and(userAssert -> userAssert.isNotEmpty(UserDTO::getId))
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to revoke token"))
                        .withReason("Malformed gateway auth token. Unable to detect user principal of gateway auth token")
                        .build());
        tokenIdentificationService.deleteByTokenOwnerId(user.getId());
    }

    @Override
    public GatewayAuthToken refresh(String pureRefreshToken) {
        Assert.that(pureRefreshToken)
                .isNotBlank()
                .otherwiseThrow(ExceptionBuilder
                        .buildFrom(new BadArgumentException("Unable to refresh token"))
                        .withReason("You've passed empty refresh token")
                        .build());
        ;

        RefreshToken refreshToken = refreshTokenService.parseToken(pureRefreshToken);

        Assert.that(refreshToken)
                .isTrue(rt -> rt.isOfType(TokenType.REFRESH))
                .otherwiseThrow(ExceptionBuilder
                        .buildFrom(new BadRequestException("Unable to refresh token"))
                        .withReason(String.format("Wrong type of passed refresh token. Is %s, but must be %s",
                                refreshToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_TYPE, String.class).orElse("absent"), TokenType.REFRESH))
                        .build());

        String refreshTokenId = refreshToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class)
                .orElseThrow(() ->
                        ExceptionBuilder
                                .buildFrom(new BadRequestException("Unable to refresh token"))
                                .withReason(String.format("%s claim is missing, though must be", PeachyMindJwtClaim.TOKEN_ID))
                                .build());

        TokenIdentificationDTO tokenIdentification =
                tokenIdentificationService.findByTokenId(refreshTokenId)
                        .orElseThrow(() -> ExceptionBuilder
                                .buildFrom(new UnauthorizedException("Unable to refresh token"))
                                .withReason("Given refresh token either has never been issued by us or is revoked by user")
                                .build());

        Assert.that(tokenIdentification.getOwner().getId())
                .isEqualToString(refreshToken.getPrincipal())
                .otherwiseThrow(ExceptionBuilder
                        .buildFrom(new UnauthorizedException("Unable to refresh token"))
                        .withReason("You aren't the owner of this refresh token")
                        .build());

        Assert.that(refreshToken.getExpiresAt())
                .isTrue(expirationTime -> Instant.now().isBefore(expirationTime))
                .otherwiseThrow(ExceptionBuilder
                        .buildFrom(new BadRequestException("Unable to refresh token"))
                        .withReason("Given refresh token has expired")
                        .withSolution("Please issue new one by hitting POST /auth/sign/in")
                        .build());


        return generateAuthToken(tokenIdentification.getOwner());
    }

    private GatewayAuthToken generateAuthToken(UserDTO user) {
        GatewayAuthToken gatewayAuthToken = gatewayAuthTokenService.generateToken(user);

        tokenIdentificationService.save(
                new TokenIdentificationDTO(
                        user,
                        gatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class)
                                .orElseThrow(() ->
                                        ExceptionBuilder.buildFrom(new PropertyNotFoundException(String.format("%s claim of %s is missing", PeachyMindJwtClaim.TOKEN_ID, GatewayAuthToken.class.getSimpleName())))
                                                .withSolution(String.format("Check implementation of %s and make tokens that it generates include %s claim", gatewayAuthTokenService.getClass().getSimpleName(), PeachyMindJwtClaim.TOKEN_ID))
                                                .withFault("Developer's fault")
                                                .build())));

        return gatewayAuthToken;
    }

    public boolean isRevoked(GatewayAuthToken token) {
        return !tokenIdentificationService.existsByTokenId(
                token.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).orElse(StringUtils.EMPTY));
    }

    @Override
    public GatewayAuthToken parse(String stringyToken) {
        return gatewayAuthTokenService.parseToken(stringyToken);
    }
}
