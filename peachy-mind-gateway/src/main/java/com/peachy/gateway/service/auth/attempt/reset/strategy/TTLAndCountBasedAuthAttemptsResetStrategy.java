package com.peachy.gateway.service.auth.attempt.reset.strategy;

import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import lombok.AccessLevel;
import lombok.Setter;

import java.time.LocalDateTime;

public abstract class TTLAndCountBasedAuthAttemptsResetStrategy extends AbstractAuthAttemptsResetStrategy {

    @Setter(AccessLevel.PROTECTED)
    protected long authAttemptLimitTTLInSeconds;

    @Setter(AccessLevel.PROTECTED)
    protected int authAttemptsCount;

    public TTLAndCountBasedAuthAttemptsResetStrategy(long authAttemptLimitTTLInSeconds,
                                                     int authAttemptsCount,
                                                     AuthenticationAttemptService defaultAuthenticationAttemptsService) {
        super(defaultAuthenticationAttemptsService);
        this.authAttemptLimitTTLInSeconds = authAttemptLimitTTLInSeconds;
        this.authAttemptsCount = authAttemptsCount;
    }

    @Override
    public boolean canReset(AuthenticationAttemptDTO authenticationAttempt) {
        return attemptsCountMatches(authenticationAttempt) && ttlExceeded(authenticationAttempt);
    }

    protected boolean ttlExceeded(AuthenticationAttemptDTO authenticationAttempt) {
        LocalDateTime expiresAt = authenticationAttempt.getUpdatedAt().plusSeconds(authAttemptLimitTTLInSeconds);
        return expiresAt.isBefore(LocalDateTime.now());
    }

    protected abstract boolean attemptsCountMatches(AuthenticationAttemptDTO authenticationAttempt);
}
