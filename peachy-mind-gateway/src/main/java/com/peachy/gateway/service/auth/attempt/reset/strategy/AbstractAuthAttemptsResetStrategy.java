package com.peachy.gateway.service.auth.attempt.reset.strategy;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public abstract class AbstractAuthAttemptsResetStrategy implements AuthAttemptsResetStrategy {
    private final AuthenticationAttemptService defaultAuthenticationAttemptsService;

    @Override
    public void resetAttempts(AuthenticationAttemptDTO authenticationAttempt) {
        Assert.that(authenticationAttempt.getUser())
                .isNotNull()
                .otherwiseThrow(ExceptionBuilder
                        .buildFrom(new BadArgumentException(String.format("Unable to apply %s", this.getClass().getSimpleName())))
                        .withReason("Authentication attempt \"user\" property is absent")
                        .withFault("Developer's fault")
                        .withSolution("Trace back to the place from where this strategy was called and check, if authentication attempt passed here as an argument was built correctly")
                        .build());

        defaultAuthenticationAttemptsService.forceResetAuthenticationAttemptsForUsername(authenticationAttempt.getUser().getUsername());
    }
}
