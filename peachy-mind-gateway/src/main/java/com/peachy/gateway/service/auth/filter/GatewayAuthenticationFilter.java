package com.peachy.gateway.service.auth.filter;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.service.auth.GatewayAuthenticationService;
import com.peachy.gateway.service.auth.provider.GatewayAuthenticationProvider;
import com.peachy.security.TokenPrefix;
import com.peachy.security.exception.UnauthorizedException;
import com.peachy.security.filter.AbstractAuthenticationFilter;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GatewayAuthenticationFilter extends AbstractAuthenticationFilter {
    private final GatewayAuthenticationProvider gatewayAuthenticationProvider;

    private final GatewayAuthenticationService gatewayAuthenticationService;

    public GatewayAuthenticationFilter(GatewayAuthenticationProvider gatewayAuthenticationProvider,
                                       GatewayAuthenticationService gatewayAuthenticationService) {
        super(TokenPrefix.BEARER.getStringyPrefix());
        this.gatewayAuthenticationProvider = gatewayAuthenticationProvider;
        this.gatewayAuthenticationService = gatewayAuthenticationService;
    }

    @Override
    protected void doAuthentication(HttpServletRequest request, HttpServletResponse response, String authToken) throws CredentialsExpiredException {
        GatewayAuthToken gatewayAuthToken = gatewayAuthenticationService.parse(authToken);

        Assert.that(gatewayAuthToken)
                .isFalse(gatewayAuthenticationService::isRevoked)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new UnauthorizedException("Unable to perform authentication"))
                        .withReason("Given token either has never been issued by us or is revoked by user")
                        .build());


        Authentication authentication = gatewayAuthenticationProvider.authenticate(gatewayAuthToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
