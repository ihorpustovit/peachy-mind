package com.peachy.gateway.service.auth.attempt.reset.strategy;

import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class PreLimitAuthAttemptResetStrategy extends TTLAndCountBasedAuthAttemptsResetStrategy {
    public PreLimitAuthAttemptResetStrategy(@Value("${brute-force.protection.auth-attempt.limit.count}") int authAttemptsCount,
                                            @Value("${brute-force.protection.auth-attempt.pre-limit.ttl}") long authAttemptLimitTTLInSeconds,
                                            AuthenticationAttemptService defaultAuthenticationAttemptsService) {
        super(authAttemptLimitTTLInSeconds,
                authAttemptsCount,
                defaultAuthenticationAttemptsService);
    }

    @Override
    protected boolean attemptsCountMatches(AuthenticationAttemptDTO authenticationAttempt) {
        return authenticationAttempt.getAttemptsCount() < authAttemptsCount;
    }

    @Override
    protected void setAuthAttemptsCount(@Value("${brute-force.protection.auth-attempt.limit.count}") int authAttemptsCount) {
        super.setAuthAttemptsCount(authAttemptsCount);
    }
}
