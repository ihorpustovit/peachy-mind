package com.peachy.gateway.service.auth.token;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.utils.Utils;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.SecurityUtils;
import com.peachy.security.TokenPrefix;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.InternalAuthToken;
import com.peachy.security.dto.token.JwtToken;
import com.peachy.security.service.auth.token.InternalAuthTokenService;
import com.peachy.security.service.auth.token.JwtTokenService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.*;

@RefreshScope
@RequiredArgsConstructor
@Service
public class GatewayAuthTokenService extends JwtTokenService<GatewayAuthToken> {
    @Value("${token.jwt.claim.lifetime}")
    protected Long lifetime;

    @Value("${token.jwt.claim.iss}")
    protected String issuer;

    private final RefreshTokenService refreshTokenService;

    private final InternalAuthTokenService internalAuthTokenService;

    @Override
    protected GatewayAuthToken convertGeneratedToken(JwtToken token, UserDTO user) {
        GatewayAuthToken gatewayAuthToken = new GatewayAuthToken(
                token.getPureToken(),
                token.getAuthority(),
                token.getIssuedAt(),
                token.getExpiresAt(),
                token.getAdditionalClaimForName(PeachyMindJwtClaim.SUBJECT, String.class).orElse(null),
                token.getAdditionalClaims(),
                generateRefreshTokenBasedOn(token, user).getPrefixedToken());
        gatewayAuthToken.setIssuer(token.getIssuer());
        return gatewayAuthToken;
    }

    private RefreshToken generateRefreshTokenBasedOn(JwtToken jwtToken,
                                                     UserDTO user) {
        String tokenId =
                jwtToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class)
                        .orElseThrow(() ->
                                ExceptionBuilder
                                        .buildFrom(new BadArgumentException("Unable to generate gateway token"))
                                        .withReason(String.format("Generated gateway token doesn't contain %s claim within itself, though ought to", PeachyMindJwtClaim.TOKEN_ID))
                                        .withFault("Developer's fault")
                                        .build());

        return refreshTokenService.generateTokenWithClaims(user,
                Collections.singletonMap(PeachyMindJwtClaim.TOKEN_ID, tokenId));
    }

    @Override
    protected GatewayAuthToken convertParsedToken(JwtToken token) {
        GatewayAuthToken gatewayAuthToken =
                new GatewayAuthToken(
                        token.getPureToken(),
                        token.getAuthority(),
                        token.getIssuedAt(),
                        token.getExpiresAt(),
                        token.getAdditionalClaimForName(PeachyMindJwtClaim.SUBJECT, String.class).orElse(null),
                        token.getAdditionalClaims(),
                        "hidden");
        gatewayAuthToken.setIssuer(token.getIssuer());

        return gatewayAuthToken;
    }

    @Override
    protected Set<String> getTokenSpecificClaimsNames() {
        return new HashSet<>(Arrays.asList(
                PeachyMindJwtClaim.SUBJECT,
                PeachyMindJwtClaim.TOKEN_ID,
                PeachyMindJwtClaim.TOKEN_TYPE,
                PeachyMindJwtClaim.INTERNAL_TOKEN));
    }

    @Override
    protected Map<String, Object> getTokenSpecificClaims(UserDTO user) {
        String tokenId = SecurityUtils.generateTokenId();

        InternalAuthToken internalAuthToken =
                internalAuthTokenService.generateTokenWithClaims(user,
                        Collections.singletonMap(PeachyMindJwtClaim.TOKEN_ID, tokenId));

        return Utils.asMap(Arrays.asList(
                Pair.of(PeachyMindJwtClaim.SUBJECT, user.getId()),
                Pair.of(PeachyMindJwtClaim.TOKEN_ID, tokenId),
                Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.GATEWAY.name()),
                Pair.of(PeachyMindJwtClaim.INTERNAL_TOKEN, internalAuthToken.getPrefixedToken())));
    }

    @Override
    public String getIssuer() {
        return issuer;
    }

    @Override
    public String getStringyPrefix() {
        return TokenPrefix.BEARER.getStringyPrefix();
    }

    @Override
    public Long getLifetime() {
        return lifetime;
    }
}
