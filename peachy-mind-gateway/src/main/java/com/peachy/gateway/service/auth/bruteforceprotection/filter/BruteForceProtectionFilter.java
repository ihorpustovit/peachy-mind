package com.peachy.gateway.service.auth.bruteforceprotection.filter;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.gateway.exception.BruteForceProtectionException;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import com.peachy.gateway.service.auth.bruteforceprotection.BruteForceProtectionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
public class BruteForceProtectionFilter extends OncePerRequestFilter {
    private final BruteForceProtectionService defaultBruteForceProtectionService;

    private final AuthenticationAttemptService defaultAuthenticationAttemptService;

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return isNotAuthentication(request);
    }

    private boolean isNotAuthentication(HttpServletRequest request) {
        return !(request.getServletPath().endsWith("/auth/sign/in/straight") && HttpMethod.POST.matches(request.getMethod()));
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String username = extractUsername(httpServletRequest)
                .orElseThrow(() -> ExceptionBuilder
                        .buildFrom(new BadRequestException("Unable to authenticate user"))
                        .withReason("\"username\" request parameter absent")
                        .withSolution("Add \"username\" parameter to authenticating request")
                        .build());


        defaultAuthenticationAttemptService.strategicalResetAuthenticationAttemptsForUsername(username);

        if (defaultBruteForceProtectionService.authenticationAttemptsExceededForUsername(username)) {
            throw ExceptionBuilder.buildFrom(new BruteForceProtectionException(HttpStatus.UNAUTHORIZED, "Unable to perform authentication"))
                    .withReason("You have exceeded authentication attempts limit. Brute force protection triggered")
                    .withSolution("Please retry re-authenticate later")
                    .build();
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private Optional<String> extractUsername(HttpServletRequest request) {
        return Optional.ofNullable(request.getParameter("username"));
    }
}
