package com.peachy.gateway.service.auth.filter;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.gateway.service.auth.token.GatewayAuthTokenService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.SecurityUtils;
import com.peachy.security.TokenPrefix;
import com.peachy.security.exception.UnauthorizedException;
import com.peachy.web.MutableHeadersHttpServletRequest;
import com.peachy.web.PeachyMindHttpHeaders;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class GatewayToInternalAuthTokenConversionFilter extends OncePerRequestFilter {

    private final GatewayAuthTokenService gatewayAuthTokenService;

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return requestIsNotAddressedToServicedApplication(request);
    }

    private boolean requestIsNotAddressedToServicedApplication(HttpServletRequest request) {
        return getServicedApplicationNames()
                .stream()
                .map("/"::concat)
                .noneMatch(servicedApplicationNamePath -> request.getServletPath().startsWith(servicedApplicationNamePath));
    }

    private List<String> getServicedApplicationNames() {
        return Arrays.stream(Optional.ofNullable(getEnvironment().getProperty("gateway.serviced-application-names"))
                .orElse(StringUtils.EMPTY)
                .split(","))
                .filter(StringUtils::isNotBlank)
                .map(servicedApplicationName -> servicedApplicationName.replaceAll(StringUtils.SPACE, StringUtils.EMPTY))
                .collect(Collectors.toList());
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        MutableHeadersHttpServletRequest mutableHeadersHttpServletRequest =
                new MutableHeadersHttpServletRequest(httpServletRequest);

        SecurityUtils.authTokenFrom(mutableHeadersHttpServletRequest, TokenPrefix.BEARER.getStringyPrefix())
                .ifPresent(authToken -> {
                    mutableHeadersHttpServletRequest.addHeader(PeachyMindHttpHeaders.AUTHORIZATION, convertToInternalToken(authToken));
                });

        filterChain.doFilter(mutableHeadersHttpServletRequest, httpServletResponse);
    }


    private String convertToInternalToken(String gatewayAuthToken) {
        return gatewayAuthTokenService.parseToken(gatewayAuthToken)
                .getAdditionalClaimForName(PeachyMindJwtClaim.INTERNAL_TOKEN, String.class)
                .orElseThrow(
                        () ->
                                ExceptionBuilder.buildFrom(new UnauthorizedException("Unable to proceed execution further than Gateway service"))
                                        .withReason(String.format("%s absent", PeachyMindJwtClaim.INTERNAL_TOKEN))
                                        .build()
                );
    }
}
