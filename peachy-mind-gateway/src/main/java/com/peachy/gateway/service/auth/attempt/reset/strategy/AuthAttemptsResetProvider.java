package com.peachy.gateway.service.auth.attempt.reset.strategy;

import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthAttemptsResetProvider implements AuthAttemptsResetStrategy {
    private final List<AbstractAuthAttemptsResetStrategy> authAttemptsResetStrategies;

    public AuthAttemptsResetProvider(List<AbstractAuthAttemptsResetStrategy> authAttemptsResetStrategies) {
        this.authAttemptsResetStrategies = authAttemptsResetStrategies;
    }

    @Override
    public void resetAttempts(AuthenticationAttemptDTO authenticationAttempt) {
        authAttemptsResetStrategies.stream()
                .filter(authAttemptsResetStrategy -> authAttemptsResetStrategy.canReset(authenticationAttempt))
                .findAny()
                .ifPresent(authAttemptsResetStrategy -> authAttemptsResetStrategy.resetAttempts(authenticationAttempt));
    }

    @Override
    public boolean canReset(AuthenticationAttemptDTO authenticationAttempt) {
        return true;
    }
}
