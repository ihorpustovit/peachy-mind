package com.peachy.gateway.service.auth.attempt;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.gateway.converter.AuthenticationAttemptConverter;
import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.repository.AuthenticationAttemptRepository;
import com.peachy.gateway.service.auth.attempt.reset.strategy.AuthAttemptsResetStrategy;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.service.auth.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DefaultAuthenticationAttemptService implements AuthenticationAttemptService {
    private final AuthenticationAttemptRepository authenticationAttemptRepository;

    private final AuthenticationAttemptConverter authenticationAttemptConverter;

    private final UserService userService;

    private final AuthAttemptsResetStrategy authAttemptsResetProvider;

    @Override
    public int attemptsCountForUsername(String username) {
        return authenticationAttemptRepository.attemptsCountForUsername(username).orElse(0);
    }

    @Override
    public Optional<AuthenticationAttemptDTO> authAttemptForUsername(String username) {
        return authenticationAttemptConverter.toDto(authenticationAttemptRepository.authAttemptForUsername(username));
    }


    @Override
    @Transactional
    public void strategicalResetAuthenticationAttemptsForUsername(String username) {
        authAttemptForUsername(username)
                .ifPresent(authAttemptsResetProvider::resetAttempts);
    }

    @Override
    @Transactional
    public void forceResetAuthenticationAttemptsForUsername(String username) {
        authenticationAttemptRepository.deleteForUsername(username);
    }

    @Override
    @Transactional
    public void increaseAuthenticationAttempts(String forUsername) {
        UserDTO userForUsername = userService.findByUsername(forUsername)
                .orElseThrow(() ->
                        ExceptionBuilder.buildFrom(new NotFoundException(String.format("Unable to increase authentication attempts for user: %s", forUsername)))
                                .withReason(String.format("No user found for username: %s", forUsername))
                                .build());

        int attemptsForUsername = attemptsCountForUsername(forUsername);

        AuthenticationAttemptDTO authenticationAttempt =
                new AuthenticationAttemptDTO(userForUsername.getId(), attemptsForUsername + 1);

        authenticationAttemptRepository.save(authenticationAttemptConverter.toEntity(authenticationAttempt));
    }
}
