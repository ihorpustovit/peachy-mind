package com.peachy.gateway.service.auth.bruteforceprotection;

import com.peachy.gateway.event.auth.GatewayAuthenticationFailureEvent;
import com.peachy.gateway.event.auth.GatewayAuthenticationSuccessEvent;

public interface BruteForceProtectionService {
    void onAuthenticationSuccess(GatewayAuthenticationSuccessEvent authenticationSuccessEvent);

    void onAuthenticationFailure(GatewayAuthenticationFailureEvent authenticationFailureEvent);

    boolean authenticationAttemptsExceededForUsername(String username);
}
