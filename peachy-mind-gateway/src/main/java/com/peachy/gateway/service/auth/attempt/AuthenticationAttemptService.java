package com.peachy.gateway.service.auth.attempt;

import com.peachy.gateway.dto.AuthenticationAttemptDTO;

import java.util.Optional;

public interface AuthenticationAttemptService {
    int attemptsCountForUsername(String username);

    Optional<AuthenticationAttemptDTO> authAttemptForUsername(String username);

    void strategicalResetAuthenticationAttemptsForUsername(String username);

    void forceResetAuthenticationAttemptsForUsername(String username);

    void increaseAuthenticationAttempts(String forUsername);
}
