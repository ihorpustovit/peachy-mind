package com.peachy.gateway.service.auth.attempt.reset.strategy;

import com.peachy.gateway.dto.AuthenticationAttemptDTO;

public interface AuthAttemptsResetStrategy {

    void resetAttempts(AuthenticationAttemptDTO authenticationAttempt);

    boolean canReset(AuthenticationAttemptDTO authenticationAttempt);
}
