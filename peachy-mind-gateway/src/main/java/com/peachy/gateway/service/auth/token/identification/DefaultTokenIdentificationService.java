package com.peachy.gateway.service.auth.token.identification;

import com.peachy.gateway.converter.TokenIdentificationConverter;
import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.entity.TokenIdentificationEntity;
import com.peachy.gateway.repository.TokenIdentificationRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DefaultTokenIdentificationService implements TokenIdentificationService {

    private final TokenIdentificationRepository tokenIdentificationRepository;

    private final TokenIdentificationConverter tokenIdentificationConverter;

    @Override
    @Transactional
    public TokenIdentificationDTO save(@NonNull TokenIdentificationDTO authTokenIdentification) {
        TokenIdentificationEntity tokenIdentificationEntity = tokenIdentificationConverter.toEntity(authTokenIdentification);
        tokenIdentificationEntity.setOwnerId(tokenIdentificationEntity.getOwner().getId());
        tokenIdentificationEntity.setOwner(null);
        return tokenIdentificationConverter.toDto(tokenIdentificationRepository.save(tokenIdentificationEntity));
    }

    @Override
    @Transactional
    public void deleteByTokenOwnerId(String tokenOwnerId) {
        tokenIdentificationRepository.deleteByOwnerId(tokenOwnerId);
    }

    @Override
    public Optional<TokenIdentificationDTO> findByTokenId(String tokenId) {
        return tokenIdentificationConverter.toDto(tokenIdentificationRepository.findByTokenId(tokenId));
    }
}
