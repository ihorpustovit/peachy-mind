package com.peachy.gateway.service.auth.token;

import com.peachy.commons.utils.Utils;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.SecurityUtils;
import com.peachy.security.TokenPrefix;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.JwtToken;
import com.peachy.security.service.auth.token.JwtTokenService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
@RefreshScope
public class RefreshTokenService extends JwtTokenService<RefreshToken> {
    @Value("${token.jwt.refresh.claim.lifetime}")
    protected Long lifetime;

    @Value("${token.jwt.claim.iss}")
    protected String issuer;

    @Value("${token.jwt.refresh.signature.algorithm}")
    protected String signatureAlgorithmName;

    @Value("${token.jwt.refresh.signature.secret}")
    protected String signature;

    @Override
    public String getSignatureAlgorithmName() {
        return signatureAlgorithmName;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    @Override
    protected RefreshToken convertGeneratedToken(JwtToken token, UserDTO user) {
        return refreshTokenOf(token);
    }

    @Override
    protected RefreshToken convertParsedToken(JwtToken token) {
        return refreshTokenOf(token);
    }

    private RefreshToken refreshTokenOf(JwtToken token) {
        RefreshToken refreshToken = new RefreshToken(
                token.getPureToken(),
                token.getAuthority(),
                token.getIssuedAt(),
                token.getExpiresAt(),
                token.getAdditionalClaimForName(PeachyMindJwtClaim.SUBJECT, String.class).orElse(null),
                token.getAdditionalClaims());

        refreshToken.setIssuer(token.getIssuer());

        return refreshToken;
    }

    @Override
    protected Map<String, Object> getTokenSpecificClaims(UserDTO user) {
        return Utils.asMap(Arrays.asList(
                Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.REFRESH.name()),
                Pair.of(PeachyMindJwtClaim.TOKEN_ID, SecurityUtils.generateTokenId()),
                Pair.of(PeachyMindJwtClaim.SUBJECT, user.getId())));
    }

    @Override
    protected Set<String> getTokenSpecificClaimsNames() {
        return new HashSet<>(Arrays.asList(PeachyMindJwtClaim.TOKEN_TYPE, PeachyMindJwtClaim.TOKEN_ID, PeachyMindJwtClaim.SUBJECT));
    }

    @Override
    public String getIssuer() {
        return issuer;
    }

    @Override
    public String getStringyPrefix() {
        return TokenPrefix.REFRESH.getStringyPrefix();
    }

    @Override
    public Long getLifetime() {
        return lifetime;
    }
}
