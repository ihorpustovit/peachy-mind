package com.peachy.gateway.service.auth.bruteforceprotection;

import com.peachy.gateway.event.auth.GatewayAuthenticationFailureEvent;
import com.peachy.gateway.event.auth.GatewayAuthenticationSuccessEvent;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@RefreshScope
@RequiredArgsConstructor
public class DefaultBruteForceProtectionService implements BruteForceProtectionService {
    private final AuthenticationAttemptService defaultAuthenticationAttemptService;

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Value("${brute-force.protection.auth-attempt.limit.count}")
    private int attemptsLimit;

    @PreDestroy
    public void shutdownExecutorService() {
        executorService.shutdown();
    }

    @Override
    public void onAuthenticationSuccess(GatewayAuthenticationSuccessEvent authenticationSuccessEvent) {
        defaultAuthenticationAttemptService.forceResetAuthenticationAttemptsForUsername(authenticationSuccessEvent.getUserCredentials().getUsername());
    }

    @Override
    public void onAuthenticationFailure(GatewayAuthenticationFailureEvent authenticationFailureEvent) {
        executorService.submit(() ->
                defaultAuthenticationAttemptService.increaseAuthenticationAttempts(authenticationFailureEvent.getUserCredentials().getUsername()));
    }

    @Override
    public boolean authenticationAttemptsExceededForUsername(String username) {
        return defaultAuthenticationAttemptService.attemptsCountForUsername(username) >= attemptsLimit;
    }
}
