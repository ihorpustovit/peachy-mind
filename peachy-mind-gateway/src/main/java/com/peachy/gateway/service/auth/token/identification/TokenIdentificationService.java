package com.peachy.gateway.service.auth.token.identification;

import com.peachy.gateway.dto.TokenIdentificationDTO;

import java.util.Optional;

public interface TokenIdentificationService {
    TokenIdentificationDTO save(TokenIdentificationDTO authTokenIdentification);

    void deleteByTokenOwnerId(String tokenOwnerId);

    default boolean existsByTokenId(String tokenId) {
        return findByTokenId(tokenId).isPresent();
    }

    Optional<TokenIdentificationDTO> findByTokenId(String id);
}
