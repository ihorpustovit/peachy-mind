package com.peachy.gateway.config;

import com.peachy.gateway.service.auth.GatewayAuthenticationService;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import com.peachy.gateway.service.auth.bruteforceprotection.BruteForceProtectionService;
import com.peachy.gateway.service.auth.bruteforceprotection.filter.BruteForceProtectionFilter;
import com.peachy.gateway.service.auth.filter.GatewayAuthenticationFilter;
import com.peachy.gateway.service.auth.filter.GatewayToInternalAuthTokenConversionFilter;
import com.peachy.gateway.service.auth.provider.GatewayAuthenticationProvider;
import com.peachy.gateway.service.auth.token.GatewayAuthTokenService;
import com.peachy.security.UserAuthority;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class GatewayServiceSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService defaultUserDetailsService;

    private final GatewayAuthenticationProvider gatewayAuthenticationProvider;

    private final GatewayAuthenticationService gatewayAuthenticationService;

    private final GatewayAuthTokenService gatewayAuthTokenService;

    private final BruteForceProtectionService defaultBruteForceProtectionService;

    private final AuthenticationAttemptService defaultAuthenticationAttemptService;

    private final PasswordEncoder defaultPasswordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()

                .antMatchers(HttpMethod.GET, "/").permitAll()

                .antMatchers(HttpMethod.GET, "/user/**").permitAll()
                .antMatchers(HttpMethod.PATCH, "/user/**/authority").hasAuthority(UserAuthority.ADMIN.getStringyAuthority())

                .antMatchers("/peachy-mind/**").permitAll()
                .antMatchers("/auth/sign/**", "/auth/refresh", "/auth/revoke").permitAll()
                .anyRequest().authenticated()
                .and()
                .csrf()
                .disable()
                .addFilterAfter(bruteForceProtectionFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(gatewayAuthenticationFilter(), BruteForceProtectionFilter.class)
                .addFilterAfter(gatewayAuthTokenConversionFilter(), GatewayAuthenticationFilter.class);

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public BruteForceProtectionFilter bruteForceProtectionFilter() {
        return new BruteForceProtectionFilter(
                defaultBruteForceProtectionService,
                defaultAuthenticationAttemptService);
    }

    @Bean
    public GatewayAuthenticationFilter gatewayAuthenticationFilter() {
        return new GatewayAuthenticationFilter(
                gatewayAuthenticationProvider,
                gatewayAuthenticationService);
    }

    @Bean
    public GatewayToInternalAuthTokenConversionFilter gatewayAuthTokenConversionFilter() {
        return new GatewayToInternalAuthTokenConversionFilter(
                gatewayAuthTokenService);
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(defaultUserDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(defaultPasswordEncoder);
        return daoAuthenticationProvider;
    }
}
