package com.peachy.gateway.repository;

import com.peachy.gateway.entity.AuthenticationAttemptEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AuthenticationAttemptRepository extends JpaRepository<AuthenticationAttemptEntity, String> {
    @Query("SELECT aae.attemptsCount FROM AuthenticationAttemptEntity aae WHERE aae.user.username=:username")
    Optional<Integer> attemptsCountForUsername(@Param("username") String username);

    @Query("SELECT aae FROM AuthenticationAttemptEntity aae WHERE aae.user.username=:username")
    Optional<AuthenticationAttemptEntity> authAttemptForUsername(@Param("username") String username);

    @Modifying
    @Query("DELETE FROM AuthenticationAttemptEntity aae WHERE aae.userId = (select u.id from UserEntity u where u.username=:username)")
    void deleteForUsername(@Param("username") String username);


    @Modifying
    @Query("DELETE FROM AuthenticationAttemptEntity aae WHERE aae.userId=:userId")
    void deleteForUserId(@Param("userId") String userId);


}
