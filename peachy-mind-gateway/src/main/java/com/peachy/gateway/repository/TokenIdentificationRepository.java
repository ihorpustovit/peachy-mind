package com.peachy.gateway.repository;

import com.peachy.gateway.entity.TokenIdentificationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TokenIdentificationRepository extends JpaRepository<TokenIdentificationEntity, String> {
    Optional<TokenIdentificationEntity> findByTokenId(String tokenId);

    void deleteByOwnerId(String ownerId);
}
