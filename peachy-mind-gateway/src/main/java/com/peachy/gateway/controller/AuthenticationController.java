package com.peachy.gateway.controller;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.commons.proxy.ObjectMapperProxy;
import com.peachy.commons.utils.Utils;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.gateway.dto.UserCredentialsDTO;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.service.auth.AuthenticationService;
import com.peachy.security.SecurityUtils;
import com.peachy.security.TokenPrefix;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.Token;
import com.peachy.security.exception.UnauthorizedException;
import com.peachy.web.PeachyMindController;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthenticationController implements PeachyMindController {

    private final AuthenticationService<GatewayAuthToken> peachyMindAuthenticationService;

    @PostMapping("/sign/in")
    public ResponseEntity<Token> signIn(@RequestBody UserCredentialsDTO userCredentials,
                                        @RequestParam("username") String username) {
        Assert.that(username)
                .isEqualTo(userCredentials.getUsername())
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadRequestException("Unable to perform authentication"))
                        .withReason("Username value given in credentials and specified as \"username\" request parameter must be identical")
                        .withFault("User's fault")
                        .build());

        return ResponseEntity.ok(peachyMindAuthenticationService.signIn(userCredentials));
    }

    @PostMapping("/sign/up")
    public ResponseEntity<UserDTO> signUp(@RequestBody UserDTO user) throws URISyntaxException {
        UserDTO createdUser = peachyMindAuthenticationService.signUp(user);

        return ResponseEntity.created(new URI("/user/".concat(createdUser.getId())))
                .body(createdUser);
    }

    @PutMapping("/refresh")
    public ResponseEntity<Token> refresh(HttpServletRequest request) {
        String authorizationHeader = SecurityUtils.authTokenFrom(request, TokenPrefix.REFRESH.getStringyPrefix()).orElse(StringUtils.EMPTY);
        return ResponseEntity.ok(peachyMindAuthenticationService.refresh(authorizationHeader));
    }

    @DeleteMapping("/revoke")
    public ResponseEntity<Void> revoke() {
        peachyMindAuthenticationService.revokeForUser(SecurityUtils.getUserPrincipal()
                .orElseThrow(() ->
                        ExceptionBuilder
                                .buildFrom(new UnauthorizedException("Unable to revoke auth token"))
                                .withReason("You aren't authorized for such action")
                                .build()));
        return ResponseEntity.noContent().build();
    }
}
