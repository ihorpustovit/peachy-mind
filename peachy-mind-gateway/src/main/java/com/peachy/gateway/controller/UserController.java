package com.peachy.gateway.controller;

import com.peachy.commons.exception.ExceptionBuilder;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.commons.utils.assertion.Assert;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.service.auth.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService defaultUserService;

    @GetMapping("/{userId}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable("userId") String userId) {
        return ResponseEntity.ok(defaultUserService.findById(userId)
                .orElseThrow(() ->
                        ExceptionBuilder.buildFrom(new NotFoundException("Unable to retrieve user"))
                                .withReason(String.format("No user found for id: %s", userId))
                                .build()));
    }

    @PatchMapping("/{userId}/authority")
    public ResponseEntity<UserDTO> updateUserAuthority(@PathVariable("userId") String userId,
                                                       @RequestParam("authority") String authority) {
        Assert.that(authority)
                .isTrue(UserAuthority::existsByName)
                .otherwiseThrow(ExceptionBuilder.buildFrom(new BadArgumentException("Unable to update user"))
                        .withReason(String.format("Non-existent authority passed to set. Please make your choice of the following authorities: %s", String.join(", ", UserAuthority.names())))
                        .build());
        return ResponseEntity.ok(defaultUserService.updateUserAuthority(userId, UserAuthority.valueOf(authority)));
    }
}
