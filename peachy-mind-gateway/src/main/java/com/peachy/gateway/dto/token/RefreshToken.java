package com.peachy.gateway.dto.token;

import com.peachy.security.TokenPrefix;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.token.JwtToken;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@Getter
public class RefreshToken extends JwtToken {

    public RefreshToken(String pureToken,
                        UserAuthority authority,
                        Instant issuedAt,
                        Instant expiresAt,
                        String principal,
                        Map<String, Object> additionalClaims) {
        super(pureToken,
                authority,
                issuedAt,
                expiresAt,
                TokenPrefix.REFRESH,
                principal,
                additionalClaims);
    }

    public RefreshToken(String pureToken,
                        UserAuthority authority,
                        Instant issuedAt,
                        Instant expiresAt,
                        String principal,
                        List<Pair<String, Object>> additionalClaims) {
        super(pureToken,
                authority,
                issuedAt,
                expiresAt,
                TokenPrefix.REFRESH,
                principal,
                additionalClaims);
    }
}
