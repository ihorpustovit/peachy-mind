package com.peachy.gateway.dto;

import com.peachy.jpa.dto.BaseDTO;
import com.peachy.security.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationAttemptDTO extends BaseDTO {
    private String userId;

    private UserDTO user;

    private Integer attemptsCount;

    public AuthenticationAttemptDTO(String userId, Integer attemptsCount) {
        this.userId = userId;
        this.attemptsCount = attemptsCount;
    }
}
