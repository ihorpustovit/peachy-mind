package com.peachy.gateway.dto;

import com.peachy.jpa.dto.BaseDTO;
import com.peachy.security.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TokenIdentificationDTO extends BaseDTO {

    private UserDTO owner;

    private String tokenId;
}
