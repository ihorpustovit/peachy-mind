package com.peachy.gateway.dto.token;

import com.peachy.security.TokenPrefix;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.token.JwtToken;
import com.peachy.security.dto.token.RefreshableToken;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.time.Instant;
import java.util.List;
import java.util.Map;

/**
 * Peachy mind auth token policy:
 * - auth token always has corresponding refresh token
 * - every time user authenticates using username/password combination new auth token issued
 * - every time new auth token generated the previous one (expired or not) gets revoked,
 * hence there is no more than one active auth token per user
 * - refresh token can be used only once to obtain new auth token, then it (refresh token) gets revoked
 */
@Setter
public class GatewayAuthToken
        extends JwtToken
        implements RefreshableToken {

    private String prefixedRefreshToken;

    public GatewayAuthToken(String pureToken,
                            UserAuthority authority,
                            Instant issuedAt,
                            Instant expiresAt,
                            String principal,
                            Map<String, Object> additionalClaims,
                            String prefixedRefreshToken) {
        super(pureToken,
                authority,
                issuedAt,
                expiresAt,
                TokenPrefix.BEARER,
                principal,
                additionalClaims);
        this.prefixedRefreshToken = prefixedRefreshToken;
    }

    public GatewayAuthToken(String pureToken,
                            UserAuthority authority,
                            Instant issuedAt,
                            Instant expiresAt,
                            String principal,
                            List<Pair<String, Object>> additionalClaims,
                            String prefixedRefreshToken) {
        super(pureToken,
                authority,
                issuedAt,
                expiresAt,
                TokenPrefix.BEARER,
                principal,
                additionalClaims);
        this.prefixedRefreshToken = prefixedRefreshToken;
    }


    public String getPrefixedRefreshToken() {
        return prefixedRefreshToken;
    }
}
