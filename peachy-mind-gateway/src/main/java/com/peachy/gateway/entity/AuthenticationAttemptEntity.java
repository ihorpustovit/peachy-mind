package com.peachy.gateway.entity;

import com.peachy.jpa.entity.BaseEntity;
import com.peachy.security.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "authentication_attempt")
public class AuthenticationAttemptEntity extends BaseEntity {
    @Id
    @Column(name = "user_id")
    private String userId;

    @OneToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "attempts_count")
    private Integer attemptsCount;
}
