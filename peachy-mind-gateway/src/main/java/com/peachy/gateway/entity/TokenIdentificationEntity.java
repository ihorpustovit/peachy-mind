package com.peachy.gateway.entity;

import com.peachy.jpa.entity.BaseEntity;
import com.peachy.security.entity.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "token_identification")
@NoArgsConstructor
public class TokenIdentificationEntity extends BaseEntity {
    @Id
    @Column(name = "owner_id")
    private String ownerId;

    @OneToOne
    @JoinColumn(name = "owner_id")
    private UserEntity owner;

    @Column(name = "token_id")
    private String tokenId;
}
