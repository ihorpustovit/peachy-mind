package com.peachy.gateway;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication(scanBasePackages = {"com.peachy.**"})
@EnableZuulProxy
@EnableEurekaClient
@EnableEncryptableProperties
public class PeachyMindGateway extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PeachyMindGateway.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PeachyMindGateway.class);
    }
}
