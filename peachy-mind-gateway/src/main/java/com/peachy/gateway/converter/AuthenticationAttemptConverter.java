package com.peachy.gateway.converter;

import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.entity.AuthenticationAttemptEntity;
import com.peachy.jpa.converter.EntityDtoConverter;
import com.peachy.security.converter.UserConverter;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AuthenticationAttemptConverter extends EntityDtoConverter<AuthenticationAttemptEntity, AuthenticationAttemptDTO> {
    @Override
    @Mapping(target = "user", source = "user", qualifiedByName = "mapUserToDTO")
    AuthenticationAttemptDTO to(AuthenticationAttemptEntity entity);

    @Named("mapUserToDTO")
    default UserDTO mapUserToDTO(UserEntity userEntity) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toDto(userEntity);
    }

    @Override
    @Mapping(target = "user", source = "user", qualifiedByName = "mapUserToEntity")
    AuthenticationAttemptEntity from(AuthenticationAttemptDTO authenticationAttemptDTO);

    @Named("mapUserToEntity")
    default UserEntity mapUserToEntity(UserDTO userDTO) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toEntity(userDTO);
    }
}
