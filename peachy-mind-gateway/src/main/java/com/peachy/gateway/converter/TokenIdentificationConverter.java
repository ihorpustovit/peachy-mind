package com.peachy.gateway.converter;

import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.entity.TokenIdentificationEntity;
import com.peachy.jpa.converter.EntityDtoConverter;
import com.peachy.security.converter.UserConverter;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface TokenIdentificationConverter extends EntityDtoConverter<TokenIdentificationEntity, TokenIdentificationDTO> {
    TokenIdentificationDTO to(TokenIdentificationEntity e);

    @Mapping(target = "owner", source = "owner", qualifiedByName = "mapUserToEntity")
    TokenIdentificationEntity from(TokenIdentificationDTO d);

    @Named("mapUserToEntity")
    default UserEntity mapUserToEntity(UserDTO userDTO) {
        UserConverter userConverter = Mappers.getMapper(UserConverter.class);
        return userConverter.toEntity(userDTO);
    }
}
