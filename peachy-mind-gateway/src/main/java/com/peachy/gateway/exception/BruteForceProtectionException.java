package com.peachy.gateway.exception;

import com.peachy.commons.exception.PeachyMindException;
import org.springframework.http.HttpStatus;

public class BruteForceProtectionException extends PeachyMindException {
    public BruteForceProtectionException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }

    public BruteForceProtectionException(HttpStatus httpStatus) {
        super(httpStatus, "Failed to perform brute force protection measures");
    }

    public BruteForceProtectionException() {
        this(HttpStatus.UNAUTHORIZED, "Failed to perform brute force protection measures");
    }
}
