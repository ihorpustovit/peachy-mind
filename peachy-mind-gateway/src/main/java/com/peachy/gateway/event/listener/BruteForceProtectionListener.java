package com.peachy.gateway.event.listener;

import com.peachy.gateway.event.auth.GatewayAuthenticationFailureEvent;
import com.peachy.gateway.event.auth.GatewayAuthenticationSuccessEvent;
import com.peachy.gateway.service.auth.bruteforceprotection.BruteForceProtectionService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BruteForceProtectionListener {
    private final BruteForceProtectionService bruteForceProtectionService;

    @EventListener(GatewayAuthenticationSuccessEvent.class)
    public void onAuthenticationSuccess(GatewayAuthenticationSuccessEvent authenticationSuccessEvent) {
        bruteForceProtectionService.onAuthenticationSuccess(authenticationSuccessEvent);
    }

    @EventListener(GatewayAuthenticationFailureEvent.class)
    public void onAuthenticationFailure(GatewayAuthenticationFailureEvent authenticationFailureEvent) {
        bruteForceProtectionService.onAuthenticationFailure(authenticationFailureEvent);
    }
}
