package com.peachy.gateway.event.auth;

import com.peachy.gateway.dto.UserCredentialsDTO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GatewayAuthenticationFailureEvent {
    private final UserCredentialsDTO userCredentials;
}

