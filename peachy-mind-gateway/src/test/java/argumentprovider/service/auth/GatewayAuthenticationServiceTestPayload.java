package argumentprovider.service.auth;

import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.security.dto.UserDTO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GatewayAuthenticationServiceTestPayload {
    private final RefreshToken refreshToken;

    private final GatewayAuthToken gatewayAuthToken;

    private final UserDTO userDTO;
}
