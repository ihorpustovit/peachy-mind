package argumentprovider.service.auth.token;

import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.dto.token.InternalAuthToken;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class TokenServiceTestPayload {
    private final GatewayAuthToken gatewayAuthToken;

    private final RefreshToken refreshToken;

    private final InternalAuthToken internalAuthToken;

    private final UserDTO userDTO;

    private final String tokenId;
}
