package argumentprovider.service.auth.token;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import testdata.TestProperties;

import java.util.stream.Stream;

import static testdata.GatewayServiceTestData.*;

public class TokenServiceTestArgumentProvider
        implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {

        return Stream.of(
                Arguments.of(new TokenServiceTestPayload(
                        getGatewayAuthToken(),
                        getRefreshToken(),
                        getInternalAuthToken(),
                        getUserDTO(),
                        TestProperties.getTokenId()))
        );
    }
}
