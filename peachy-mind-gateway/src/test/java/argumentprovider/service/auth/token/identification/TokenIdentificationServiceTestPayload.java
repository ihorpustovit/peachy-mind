package argumentprovider.service.auth.token.identification;

import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.entity.TokenIdentificationEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class TokenIdentificationServiceTestPayload {

    private final TokenIdentificationDTO tokenIdentificationDTO;

    private final TokenIdentificationEntity tokenIdentificationEntity;
}
