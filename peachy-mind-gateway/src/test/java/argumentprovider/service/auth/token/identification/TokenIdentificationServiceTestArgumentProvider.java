package argumentprovider.service.auth.token.identification;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

import static testdata.GatewayServiceTestData.getTokenIdentificationDTO;
import static testdata.GatewayServiceTestData.getTokenIdentificationEntity;

public class TokenIdentificationServiceTestArgumentProvider
        implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(Arguments.of(new TokenIdentificationServiceTestPayload(
                getTokenIdentificationDTO(),
                getTokenIdentificationEntity())));
    }
}
