package argumentprovider.service.auth;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

import static testdata.GatewayServiceTestData.*;

public class GatewayAuthenticationServiceTestArgumentProvider
        implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(
                Arguments.of(new GatewayAuthenticationServiceTestPayload(
                        getRefreshToken(),
                        getGatewayAuthToken(),
                        getUserDTO()))
        );
    }
}
