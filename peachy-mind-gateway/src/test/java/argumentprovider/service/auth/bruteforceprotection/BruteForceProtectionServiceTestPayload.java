package argumentprovider.service.auth.bruteforceprotection;

import com.peachy.gateway.entity.AuthenticationAttemptEntity;
import com.peachy.gateway.event.auth.GatewayAuthenticationFailureEvent;
import com.peachy.gateway.event.auth.GatewayAuthenticationSuccessEvent;
import com.peachy.security.entity.UserEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BruteForceProtectionServiceTestPayload {
    private final GatewayAuthenticationSuccessEvent gatewayAuthenticationSuccessEvent;

    private final GatewayAuthenticationFailureEvent gatewayAuthenticationFailureEvent;

    private final UserEntity userEntity;

    private final AuthenticationAttemptEntity authenticationAttemptEntity;
}
