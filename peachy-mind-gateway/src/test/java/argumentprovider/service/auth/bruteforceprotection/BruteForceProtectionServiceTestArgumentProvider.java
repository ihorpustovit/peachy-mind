package argumentprovider.service.auth.bruteforceprotection;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

import static testdata.GatewayServiceTestData.*;

public class BruteForceProtectionServiceTestArgumentProvider
        implements ArgumentsProvider{

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(Arguments.of(new BruteForceProtectionServiceTestPayload(
                getGatewayAuthenticationSuccessEvent(),
                getGatewayAuthenticationFailureEvent(),
                getUserEntity(),
                getAuthenticationAttemptEntity())));
    }
}
