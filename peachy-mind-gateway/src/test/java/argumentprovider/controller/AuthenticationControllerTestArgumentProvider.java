package argumentprovider.controller;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

import static testdata.GatewayServiceTestData.*;

public class AuthenticationControllerTestArgumentProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(Arguments.of(
                new AuthenticationControllerTestPayload(
                        getUserDTO(),
                        getUserEntity(),
                        getGatewayAuthToken(),
                        getRefreshToken())));
    }
}
