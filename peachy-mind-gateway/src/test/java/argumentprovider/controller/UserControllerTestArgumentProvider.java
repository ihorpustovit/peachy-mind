package argumentprovider.controller;

import com.peachy.test.data.TestData;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import testdata.GatewayServiceTestData;

import java.util.stream.Stream;

public class UserControllerTestArgumentProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        return Stream.of(Arguments.of(new UserControllerTestPayload(
                TestData.getUserEntity(),
                TestData.getUserDTO(),
                GatewayServiceTestData.getGatewayAuthToken())));
    }
}
