package argumentprovider.controller;

import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class UserControllerTestPayload {
    private final UserEntity userEntity;

    private final UserDTO userDTO;

    private final GatewayAuthToken gatewayAuthToken;
}
