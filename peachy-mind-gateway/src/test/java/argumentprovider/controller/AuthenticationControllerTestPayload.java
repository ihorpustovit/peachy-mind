package argumentprovider.controller;

import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class AuthenticationControllerTestPayload {
    private final UserDTO user;

    private final UserEntity userEntity;

    private final GatewayAuthToken gatewayAuthToken;

    private final RefreshToken refreshToken;
}
