package integration.com.peachy.gateway.service.auth.token;

import argumentprovider.service.auth.GatewayAuthenticationServiceTestArgumentProvider;
import argumentprovider.service.auth.GatewayAuthenticationServiceTestPayload;
import argumentprovider.service.auth.token.TokenServiceTestArgumentProvider;
import argumentprovider.service.auth.token.TokenServiceTestPayload;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.gateway.service.auth.token.RefreshTokenService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import integration.com.peachy.gateway.AbstractIntegrationTest;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.JwtTokenTestUtils;
import testdata.TestProperties;
import testdata.TokenServiceTestUtils;

import java.util.Collections;

import static testdata.TokenServiceTestUtils.refreshTokenAssertions;

public class RefreshTokenServiceTest extends AbstractIntegrationTest {
    @Autowired
    private RefreshTokenService refreshTokenService;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) { }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {}

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateToken(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        UserDTO user = tokenServiceTestPayload.getUserDTO();

        //TEST
        RefreshToken actualRefreshToken = refreshTokenService.generateToken(user);

        //ASSERT
        refreshTokenAssertions(actualRefreshToken);
        TokenServiceTestUtils.isAdditionalClaimMatches(actualRefreshToken, Pair.of(PeachyMindJwtClaim.SUBJECT, user.getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testParseToken(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        String pureRefreshToken = JwtTokenTestUtils.generatePureTestTokenBasedOn(gatewayAuthenticationServiceTestPayload.getRefreshToken(),
                TestProperties.getRefreshTokenSignature(),
                TestProperties.getSignatureAlgorithmName());

        //TEST
        RefreshToken actualRefreshToken = refreshTokenService.parseToken(pureRefreshToken);

        //ASSERT
        refreshTokenAssertions(actualRefreshToken);
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testGenerateTokenWithEmptyAdditionalClaims(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();

        //TEST
        RefreshToken actualRefreshToken = refreshTokenService.generateTokenWithClaims(user, Collections.emptyMap());

        //ASSERT
        refreshTokenAssertions(actualRefreshToken);
        TokenServiceTestUtils.isAdditionalClaimMatches(actualRefreshToken, Pair.of(PeachyMindJwtClaim.SUBJECT, user.getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testGenerateTokenWithAdditionalClaims(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        Pair<String, Object> testClaim = Pair.of("testClaimName", "testClaimValue");

        //TEST
        RefreshToken actualRefreshToken = refreshTokenService.generateTokenWithClaims(user, Collections.singletonMap(testClaim.getLeft(), testClaim.getRight()));

        //ASSERT
        refreshTokenAssertions(actualRefreshToken);
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(actualRefreshToken, testClaim.getLeft());
        TokenServiceTestUtils.isAdditionalClaimMatches(actualRefreshToken, testClaim);
        TokenServiceTestUtils.isAdditionalClaimMatches(actualRefreshToken, Pair.of(PeachyMindJwtClaim.SUBJECT, user.getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testGenerateTokenWithClaimsAdditionalClaimsOverrideTokenSpecific(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        Pair<String, Object> fakeTokenType = Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.NONE.name());

        //TEST
        RefreshToken actualRefreshToken = refreshTokenService.generateTokenWithClaims(user, Collections.singletonMap(fakeTokenType.getLeft(), fakeTokenType.getRight()));

        //ASSERT
        TokenServiceTestUtils.isAdditionalClaimMatches(actualRefreshToken, fakeTokenType);
    }
}
