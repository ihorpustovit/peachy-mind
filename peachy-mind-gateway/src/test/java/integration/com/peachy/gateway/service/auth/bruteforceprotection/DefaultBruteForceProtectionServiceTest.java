package integration.com.peachy.gateway.service.auth.bruteforceprotection;

import argumentprovider.service.auth.bruteforceprotection.BruteForceProtectionServiceTestArgumentProvider;
import argumentprovider.service.auth.bruteforceprotection.BruteForceProtectionServiceTestPayload;
import com.peachy.gateway.entity.AuthenticationAttemptEntity;
import com.peachy.gateway.service.auth.bruteforceprotection.BruteForceProtectionService;
import com.peachy.security.entity.UserEntity;
import integration.com.peachy.gateway.AbstractIntegrationTest;
import integration.com.peachy.gateway.AbstractJpaTest;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.AuthAttemptServiceTestUtils;
import testdata.TestProperties;

import static testdata.AuthAttemptServiceTestUtils.*;
import static testdata.UserServiceTestUtils.saveUser;
import static testdata.UserServiceTestUtils.truncateUsers;

public class DefaultBruteForceProtectionServiceTest extends AbstractIntegrationTest {
    @Autowired
    private BruteForceProtectionService defaultBruteForceProtectionService;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        AuthAttemptServiceTestUtils.truncateAuthenticationAttempts(jdbcTemplate);
        truncateUsers(jdbcTemplate);
    }

    @ParameterizedTest
    @ArgumentsSource(BruteForceProtectionServiceTestArgumentProvider.class)
    public void testOnAuthenticationSuccessRefreshesAuthenticationAttempts(BruteForceProtectionServiceTestPayload bruteForceProtectionServiceTestPayload) {
        //SETUP
        saveUser(bruteForceProtectionServiceTestPayload.getUserEntity(), jdbcTemplate);
        AuthenticationAttemptEntity presumptiveAuthAttempt = bruteForceProtectionServiceTestPayload.getAuthenticationAttemptEntity();
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        defaultBruteForceProtectionService.onAuthenticationSuccess(bruteForceProtectionServiceTestPayload.getGatewayAuthenticationSuccessEvent());

        //ASSERT
        Assertions.assertFalse(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));
    }


    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(BruteForceProtectionServiceTestArgumentProvider.class)
    public void testOnAuthenticationFailureAuthenticationAttemptIncreasesAuthenticationAttemptsCountIfThereWasAttemptsBefore(BruteForceProtectionServiceTestPayload bruteForceProtectionServiceTestPayload) {
        //SETUP
        UserEntity presumptiveUser = bruteForceProtectionServiceTestPayload.getUserEntity();
        saveUser(presumptiveUser, jdbcTemplate);
        AuthenticationAttemptEntity presumptiveAuthAttemptEntity =
                bruteForceProtectionServiceTestPayload.getAuthenticationAttemptEntity();
        saveAuthenticationAttempt(presumptiveAuthAttemptEntity, jdbcTemplate);

        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttemptEntity.getUserId(), jdbcTemplate));

        //TEST
        defaultBruteForceProtectionService.onAuthenticationFailure(bruteForceProtectionServiceTestPayload.getGatewayAuthenticationFailureEvent());
        //latency added as increasing of a count of authentication attempts is performed in a separate thread
        Thread.sleep(1000);

        //ASSERT
        Assertions.assertTrue(isAuthenticationAttemptForUserId(presumptiveUser.getId(), jdbcTemplate));
        Assertions.assertEquals(2, authenticationAttemptsCount(presumptiveUser.getId(), jdbcTemplate));
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(BruteForceProtectionServiceTestArgumentProvider.class)
    public void testOnAuthenticationFailureAuthenticationAttemptCreatesIfThereWereNoAttemptsBefore(BruteForceProtectionServiceTestPayload bruteForceProtectionServiceTestPayload) {
        //SETUP
        UserEntity presumptiveUser = bruteForceProtectionServiceTestPayload.getUserEntity();
        saveUser(presumptiveUser, jdbcTemplate);
        Assumptions.assumeFalse(isAuthenticationAttemptForUserId(presumptiveUser.getId(), jdbcTemplate));

        //TEST
        defaultBruteForceProtectionService.onAuthenticationFailure(bruteForceProtectionServiceTestPayload.getGatewayAuthenticationFailureEvent());
        //latency added as increasing of a count of authentication attempts is performed in a separate thread
        Thread.sleep(1000);

        //ASSERT
        Assertions.assertTrue(isAuthenticationAttemptForUserId(presumptiveUser.getId(), jdbcTemplate));
        Assertions.assertEquals(1, authenticationAttemptsCount(presumptiveUser.getId(), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(BruteForceProtectionServiceTestArgumentProvider.class)
    public void testAuthenticationAttemptsExceededReturnsFalseIfAttemptsArentExceeded(BruteForceProtectionServiceTestPayload bruteForceProtectionServiceTestPayload) {
        //SETUP
        UserEntity presumptiveUser = bruteForceProtectionServiceTestPayload.getUserEntity();
        saveUser(presumptiveUser, jdbcTemplate);
        AuthenticationAttemptEntity presumptiveAuthAttemptEntity =
                bruteForceProtectionServiceTestPayload.getAuthenticationAttemptEntity();
        saveAuthenticationAttempt(presumptiveAuthAttemptEntity, jdbcTemplate);

        //TEST
        boolean authenticationAttemptsExceeded =
                defaultBruteForceProtectionService.authenticationAttemptsExceededForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertFalse(authenticationAttemptsExceeded);
    }

    @ParameterizedTest
    @ArgumentsSource(BruteForceProtectionServiceTestArgumentProvider.class)
    public void testAuthenticationAttemptsExceededReturnsTrueIfAttemptsAreExceeded(BruteForceProtectionServiceTestPayload bruteForceProtectionServiceTestPayload) {
        //SETUP
        UserEntity presumptiveUser = bruteForceProtectionServiceTestPayload.getUserEntity();
        saveUser(presumptiveUser, jdbcTemplate);
        AuthenticationAttemptEntity presumptiveAuthAttemptEntity =
                bruteForceProtectionServiceTestPayload.getAuthenticationAttemptEntity();
        presumptiveAuthAttemptEntity.setAttemptsCount(TestProperties.getAuthAttemptLimit() + 1);
        saveAuthenticationAttempt(presumptiveAuthAttemptEntity, jdbcTemplate);

        //TEST
        boolean authenticationAttemptsExceeded =
                defaultBruteForceProtectionService.authenticationAttemptsExceededForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertTrue(authenticationAttemptsExceeded);
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        truncateAuthenticationAttempts(jdbcTemplate);
        truncateUsers(jdbcTemplate);
    }
}
