package integration.com.peachy.gateway.service.auth.token;

import argumentprovider.service.auth.GatewayAuthenticationServiceTestArgumentProvider;
import argumentprovider.service.auth.GatewayAuthenticationServiceTestPayload;
import argumentprovider.service.auth.token.TokenServiceTestArgumentProvider;
import argumentprovider.service.auth.token.TokenServiceTestPayload;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.service.auth.token.GatewayAuthTokenService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenPrefix;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import integration.com.peachy.gateway.AbstractIntegrationTest;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import testdata.JwtTokenTestUtils;
import testdata.TestProperties;
import testdata.TokenServiceTestUtils;

import java.util.Collections;

import static testdata.TokenServiceTestUtils.*;

public class GatewayAuthTokenServiceTest extends AbstractIntegrationTest {

    @Autowired
    private GatewayAuthTokenService gatewayAuthTokenService;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {

    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {

    }

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateToken(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        UserDTO user = tokenServiceTestPayload.getUserDTO();

        //TEST
        GatewayAuthToken actualGatewayAuthToken = gatewayAuthTokenService.generateToken(user);

        //ASSERT
        gatewayAuthTokenAssertions(actualGatewayAuthToken);
        TokenServiceTestUtils.isAdditionalClaimMatches(actualGatewayAuthToken, Pair.of(PeachyMindJwtClaim.SUBJECT, user.getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testParseToken(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        String presumptivePureGatewayToken = JwtTokenTestUtils.generatePureTestTokenBasedOn(
                gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(),
                TestProperties.getGatewayAuthTokenSignature(),
                TestProperties.getSignatureAlgorithmName());

        //TEST
        GatewayAuthToken actualGatewayAuthToken = gatewayAuthTokenService.parseToken(presumptivePureGatewayToken);

        //ASSERT
        jwtGeneratedTokenAssertions(actualGatewayAuthToken);
        Assertions.assertNotNull(actualGatewayAuthToken.getPrefixedRefreshToken(),
                "Gateway refresh token should not be null");
        Assertions.assertTrue(StringUtils.isNotBlank(actualGatewayAuthToken.getPrefixedRefreshToken()),
                "Gateway refresh token should not be empty");
        Assertions.assertEquals("hidden", actualGatewayAuthToken.getPrefixedRefreshToken(),
                "Gateway refresh token should be hidden after gateway token has been parsed");
        Assertions.assertEquals((long) TestProperties.getGatewayTokenLifetime(), actualGatewayAuthToken.getExpiresAtUnix() - actualGatewayAuthToken.getIssuedAtUnix(),
                String.format("Gateway token lifetime should be %s, but is %s", TestProperties.getGatewayTokenLifetime(), actualGatewayAuthToken.getExpiresAtUnix() - actualGatewayAuthToken.getIssuedAtUnix()));
        Assertions.assertEquals(actualGatewayAuthToken.getPrefix(), TokenPrefix.BEARER,
                String.format("Gateway token prefix have to be %s, but is %s", TokenPrefix.BEARER.getStringyPrefix(), actualGatewayAuthToken.getStringyPrefix()));
        Assertions.assertTrue(isTokenPrefixed(actualGatewayAuthToken.getPrefixedToken(), TokenPrefix.BEARER));
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(actualGatewayAuthToken, PeachyMindJwtClaim.INTERNAL_TOKEN);
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(actualGatewayAuthToken, PeachyMindJwtClaim.TOKEN_ID);
        TokenServiceTestUtils.isAdditionalClaimMatches(actualGatewayAuthToken, Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.GATEWAY.name()));
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(actualGatewayAuthToken, PeachyMindJwtClaim.SUBJECT);
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testGenerateTokenWithEmptyAdditionalClaims(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();

        //TEST
        GatewayAuthToken actualGatewayAuthToken = gatewayAuthTokenService.generateTokenWithClaims(user, Collections.emptyMap());

        //ASSERT
        gatewayAuthTokenAssertions(actualGatewayAuthToken);
        TokenServiceTestUtils.isAdditionalClaimMatches(actualGatewayAuthToken, Pair.of(PeachyMindJwtClaim.SUBJECT, user.getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testGenerateTokenWithAdditionalClaims(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        Pair<String, Object> testClaim = Pair.of("testClaimName", "testClaimValue");

        //TEST
        GatewayAuthToken actualGatewayAuthToken = gatewayAuthTokenService.generateTokenWithClaims(user, Collections.singletonMap(testClaim.getLeft(), testClaim.getRight()));

        //ASSERT
        gatewayAuthTokenAssertions(actualGatewayAuthToken);
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(actualGatewayAuthToken, testClaim.getLeft());
        TokenServiceTestUtils.isAdditionalClaimMatches(actualGatewayAuthToken, testClaim);
        TokenServiceTestUtils.isAdditionalClaimMatches(actualGatewayAuthToken, Pair.of(PeachyMindJwtClaim.SUBJECT, user.getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testGenerateTokenWithClaimsAdditionalClaimsOverrideTokenSpecific(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        Pair<String, Object> fakeTokenType = Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.NONE.name());

        //TEST
        GatewayAuthToken actualGatewayAuthToken = gatewayAuthTokenService.generateTokenWithClaims(user, Collections.singletonMap(fakeTokenType.getLeft(), fakeTokenType.getRight()));

        //ASSERT
        TokenServiceTestUtils.isAdditionalClaimMatches(actualGatewayAuthToken, fakeTokenType);
    }
}
