package integration.com.peachy.gateway.service.auth;

import argumentprovider.service.auth.GatewayAuthenticationServiceTestArgumentProvider;
import argumentprovider.service.auth.GatewayAuthenticationServiceTestPayload;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.dto.UserCredentialsDTO;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.event.auth.GatewayAuthenticationFailureEvent;
import com.peachy.gateway.event.auth.GatewayAuthenticationSuccessEvent;
import com.peachy.gateway.event.listener.BruteForceProtectionListener;
import com.peachy.gateway.service.auth.AuthenticationService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenPrefix;
import com.peachy.security.UserAuthority;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.exception.BadCredentialsException;
import integration.com.peachy.gateway.AbstractIntegrationTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import testdata.TestProperties;
import testdata.TokenServiceTestUtils;

import java.time.Instant;

import static testdata.UserServiceTestUtils.truncateUsers;

public class GatewayAuthenticationServiceTest extends AbstractIntegrationTest {
    @Autowired
    private AuthenticationService<GatewayAuthToken> gatewayAuthenticationService;

    @Autowired
    private PasswordEncoder defaultPasswordEncoder;

    @MockBean
    private BruteForceProtectionListener bruteForceProtectionListener;

    @Captor
    private ArgumentCaptor<GatewayAuthenticationSuccessEvent> authenticationSuccessEventArgumentCaptor;

    @Captor
    private ArgumentCaptor<GatewayAuthenticationFailureEvent> authenticationFailureEventArgumentCaptor;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("truncate table token_identification");
        truncateUsers(jdbcTemplate);

        String passHash = defaultPasswordEncoder.encode(TestProperties.getPassword());
        jdbcTemplate.update("insert into user value (?,?,?,'ADMIN',now(),now())",
                TestProperties.getUserId(),
                TestProperties.getUsername(),
                passHash);
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testIsRevoked(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //TEST AND ASSERT
        Assertions.assertTrue(gatewayAuthenticationService.isRevoked(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken()));
        assertUserHasNoTokens(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testIsNotRevoked(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(), gatewayAuthenticationServiceTestPayload.getUserDTO());

        //TEST AND ASSERT
        Assertions.assertFalse(gatewayAuthenticationService.isRevoked(activatedGatewayAuthToken));
        assertUserHasOnlyOneGatewayToken(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testIsActive(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(), gatewayAuthenticationServiceTestPayload.getUserDTO());

        //TEST AND ASSERT
        Assertions.assertTrue(gatewayAuthenticationService.isActive(activatedGatewayAuthToken));
        assertUserHasOnlyOneGatewayToken(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testIsNotActiveIfTokenIsUnidentifiable(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //TEST AND ASSERT
        Assertions.assertFalse(gatewayAuthenticationService.isActive(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken()));
        assertUserHasNoTokens(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testIsNotActiveIfTokenHasExpired(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        GatewayAuthToken inactiveGatewayAuthToken = new GatewayAuthToken(
                gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getPureToken(),
                gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getAuthority(),
                Instant.now().minusSeconds(2),
                Instant.now().minusSeconds(1),
                gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getPrincipal(),
                gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getAdditionalClaims(),
                gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getPrefixedRefreshToken());

        String tokenId = inactiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class)
                .orElse(StringUtils.EMPTY);
        Assertions.assertTrue(StringUtils.isNotEmpty(tokenId));
        saveTokenIdentification(new TokenIdentificationDTO(gatewayAuthenticationServiceTestPayload.getUserDTO(), tokenId));

        //TEST AND ASSERT
        Assertions.assertFalse(gatewayAuthenticationService.isActive(inactiveGatewayAuthToken));
        assertUserHasOnlyOneGatewayToken(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
    }


    @Test
    public void testSignIn() {
        //SETUP
        UserCredentialsDTO userCredentials = new UserCredentialsDTO(TestProperties.getUsername(), TestProperties.getPassword());

        //TEST
        GatewayAuthToken actualGatewayAuthToken = gatewayAuthenticationService.signIn(userCredentials);

        //ASSERT
        TokenServiceTestUtils.gatewayAuthTokenAssertions(actualGatewayAuthToken);
        assertUserHasOnlyOneGatewayToken(actualGatewayAuthToken.getPrincipal());
        assertTokenIdSaved(actualGatewayAuthToken);
        assertTokenBelongsToClaimedPrincipal(actualGatewayAuthToken);

        Mockito.verify(bruteForceProtectionListener, Mockito.times(1))
                .onAuthenticationSuccess(Mockito.any(GatewayAuthenticationSuccessEvent.class));
        Mockito.verify(bruteForceProtectionListener)
                .onAuthenticationSuccess(authenticationSuccessEventArgumentCaptor.capture());
        Assertions.assertEquals(userCredentials,
                authenticationSuccessEventArgumentCaptor.getValue().getUserCredentials());
    }

    private void assertUserHasOnlyOneGatewayToken(String userId) {
        long countOfGatewayTokensUserHas = jdbcTemplate.queryForObject("select count(*) from token_identification where owner_id=?", Long.class, userId);
        Assertions.assertEquals(
                1L, countOfGatewayTokensUserHas, String.format("User should have only one gateway token but have %s", countOfGatewayTokensUserHas));
    }

    private void assertTokenIdSaved(GatewayAuthToken gatewayAuthToken) {
        String tokenId = gatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).orElse("empty");
        Assertions.assertTrue(jdbcTemplate.queryForObject("select count(*) > 0 from token_identification where token_id=?", Boolean.class, tokenId),
                "Token id should be stored in DB");
    }

    private void assertTokenBelongsToClaimedPrincipal(GatewayAuthToken gatewayAuthToken) {
        String tokenId =
                gatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).orElse("empty");
        String actualTokenOwnerId =
                jdbcTemplate.queryForObject("select owner_id from token_identification where token_id=?", String.class, tokenId);
        Assertions.assertEquals(gatewayAuthToken.getPrincipal(), actualTokenOwnerId, String.format("There is an token ownership ambiguity. Claimed owner id: %s; actual owner: id %s", gatewayAuthToken.getPrincipal(), actualTokenOwnerId));
    }

    @Test
    public void testSignInWithCredentialsOfNonExistentUser() {
        //SETUP
        UserCredentialsDTO fakeCredentials = new UserCredentialsDTO("Jersey", "McLaren");

        //TEST
        BadCredentialsException badCredentialsException =
                Assertions.assertThrows(BadCredentialsException.class, () -> gatewayAuthenticationService.signIn(fakeCredentials));

        //ASSERT
        Assertions.assertEquals(HttpStatus.UNAUTHORIZED, badCredentialsException.getHttpStatus());

        Mockito.verify(bruteForceProtectionListener, Mockito.times(1))
                .onAuthenticationFailure(Mockito.any(GatewayAuthenticationFailureEvent.class));
        Mockito.verify(bruteForceProtectionListener)
                .onAuthenticationFailure(authenticationFailureEventArgumentCaptor.capture());
        Assertions.assertEquals(fakeCredentials,
                authenticationFailureEventArgumentCaptor.getValue().getUserCredentials());
    }

    @Test
    public void testSignInWithBadCredentialsOfExistentUser() {
        //SETUP
        UserCredentialsDTO fakeCredentials = new UserCredentialsDTO(TestProperties.getUsername(), "McLaren");

        //TEST
        BadCredentialsException badCredentialsException =
                Assertions.assertThrows(BadCredentialsException.class, () -> gatewayAuthenticationService.signIn(fakeCredentials));

        //ASSERT
        Assertions.assertEquals(HttpStatus.UNAUTHORIZED, badCredentialsException.getHttpStatus());

        Mockito.verify(bruteForceProtectionListener, Mockito.times(1))
                .onAuthenticationFailure(Mockito.any(GatewayAuthenticationFailureEvent.class));
        Mockito.verify(bruteForceProtectionListener)
                .onAuthenticationFailure(authenticationFailureEventArgumentCaptor.capture());
        Assertions.assertEquals(fakeCredentials,
                authenticationFailureEventArgumentCaptor.getValue().getUserCredentials());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testSignUp(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        user.setUsername("Jersey");
        user.setId(StringUtils.EMPTY);

        //TEST
        UserDTO signedUpUser = gatewayAuthenticationService.signUp(user);

        //ASSERT
        Assertions.assertTrue(StringUtils.isNotBlank(signedUpUser.getId()) && StringUtils.isNotEmpty(signedUpUser.getId()),
                "Id of the signed up user should not be blank or empty ");
        assertUserIsWithinDB(signedUpUser.getId());
        assertUserPasswordEquals(signedUpUser.getId(), signedUpUser.getHashedPassword());
        Assertions.assertTrue(defaultPasswordEncoder.matches(user.getPlainPassword(), signedUpUser.getHashedPassword()), "User password hash doesn't match");
    }

    private void assertUserPasswordEquals(String userId,
                                          String passwordHash) {
        String storedPasswordHash = jdbcTemplate.queryForObject("select hashed_password from user where id = ?", String.class, userId);
        Assertions.assertEquals(passwordHash, storedPasswordHash, String.format("Stored user for id %s password should be %s, but is %s", userId, passwordHash, storedPasswordHash));
    }

    private void assertUserIsWithinDB(String userId) {
        Assertions.assertTrue(jdbcTemplate.queryForObject("select count(*) = 1 from user where id = ?", Boolean.class, userId), "User should be within DB");
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRevokeForUser(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(), gatewayAuthenticationServiceTestPayload.getUserDTO());

        //TEST
        gatewayAuthenticationService.revokeForUser(gatewayAuthenticationServiceTestPayload.getUserDTO());

        //ASSERT
        assertUserHasNoTokens(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
        Assertions.assertFalse(gatewayAuthenticationService.isActive(activatedGatewayAuthToken), "Gateway auth token should not be active after it has been revoked");
    }

    private void assertUserHasNoTokens(String userId) {
        long countOfGatewayTokensUserHas = jdbcTemplate.queryForObject("select count(*) from token_identification where owner_id=?", Long.class, userId);
        Assertions.assertEquals(
                0L, countOfGatewayTokensUserHas, String.format("User should have only one gateway token but have %s", countOfGatewayTokensUserHas));
    }

    private void saveTokenIdentification(TokenIdentificationDTO tokenIdentification) {
        jdbcTemplate.update("insert into token_identification(owner_id,token_id) value (?,?)", tokenIdentification.getOwner().getId(), tokenIdentification.getTokenId());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRevokeForUserThrowsBadArgumentExceptionForNullUser(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(), gatewayAuthenticationServiceTestPayload.getUserDTO());

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> gatewayAuthenticationService.revokeForUser(null),
                "Revoke operation should inform user with exception about null-passed argument");

        //ASSERT
        assertUserHasOnlyOneGatewayToken(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
        Assertions.assertTrue(gatewayAuthenticationService.isActive(activatedGatewayAuthToken));
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRevokeForUserThrowsBadArgumentExceptionForEmptyUserId(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(), gatewayAuthenticationServiceTestPayload.getUserDTO());
        UserDTO user = new UserDTO();
        user.setId(StringUtils.EMPTY);

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> gatewayAuthenticationService.revokeForUser(user),
                "Revoke operation should inform user with exception about absent user id property");

        //ASSERT
        assertUserHasOnlyOneGatewayToken(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
        Assertions.assertTrue(gatewayAuthenticationService.isActive(activatedGatewayAuthToken));
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefresh(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(
                        gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(),
                        gatewayAuthenticationServiceTestPayload.getUserDTO());

        //TEST
        String pureRefreshToken = activatedGatewayAuthToken.getPrefixedRefreshToken().substring(TokenPrefix.REFRESH.getStringyPrefix().concat(StringUtils.SPACE).length());
        GatewayAuthToken refreshedGatewayAuthToken =
                gatewayAuthenticationService.refresh(pureRefreshToken);

        //ASSERT
        assertUserHasOnlyOneGatewayToken(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
        Assertions.assertTrue(gatewayAuthenticationService.isActive(refreshedGatewayAuthToken), "Refreshed gateway auth token should be active");
        Assertions.assertFalse(gatewayAuthenticationService.isActive(activatedGatewayAuthToken), "Revoked gateway auth token should not be active");
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsExceptionIfRefreshTokenIsEmpty(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(
                        gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(),
                        gatewayAuthenticationServiceTestPayload.getUserDTO());

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> gatewayAuthenticationService.refresh(StringUtils.EMPTY));

        //ASSERT
        assertUserHasOnlyOneGatewayToken(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
        Assertions.assertTrue(gatewayAuthenticationService.isActive(activatedGatewayAuthToken));
        ;
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsExceptionIfRefreshTokenIsBlank(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(
                        gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(),
                        gatewayAuthenticationServiceTestPayload.getUserDTO());

        //TEST
        Assertions.assertThrows(BadArgumentException.class, () -> gatewayAuthenticationService.refresh(StringUtils.SPACE));

        //ASSERT
        assertUserHasOnlyOneGatewayToken(gatewayAuthenticationServiceTestPayload.getUserDTO().getId());
        Assertions.assertTrue(gatewayAuthenticationService.isActive(activatedGatewayAuthToken));
        ;
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsExceptionIfRefreshTokenIsNotIdentifiable(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP

        //TEST
        String pureRefreshToken = gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getPrefixedRefreshToken().substring(TokenPrefix.REFRESH.getStringyPrefix().concat(StringUtils.SPACE).length());
        PeachyMindException unauthorizedException =
                Assertions.assertThrows(PeachyMindException.class, () -> gatewayAuthenticationService.refresh(pureRefreshToken));

        //ASSERT
        Assertions.assertEquals(HttpStatus.UNAUTHORIZED, unauthorizedException.getHttpStatus());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsExceptionIfRefreshTokenDoesNotBelongToUser(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO someUser = new UserDTO();
        someUser.setId("randomId");
        someUser.setUsername("someUser");
        someUser.setHashedPassword("hashed password");
        someUser.setAuthority(UserAuthority.STUDENT);
        saveUser(someUser);
        GatewayAuthToken activatedGatewayAuthToken =
                activateGatewayAuthToken(
                        gatewayAuthenticationServiceTestPayload.getGatewayAuthToken(),
                        someUser);

        Assumptions.assumeTrue(gatewayAuthenticationService.isActive(activatedGatewayAuthToken));

        //TEST
        String pureRefreshToken = gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getPrefixedRefreshToken().substring(TokenPrefix.REFRESH.getStringyPrefix().concat(StringUtils.SPACE).length());
        PeachyMindException forbiddenException =
                Assertions.assertThrows(PeachyMindException.class, () -> gatewayAuthenticationService.refresh(pureRefreshToken));

        //ASSERT
        Assertions.assertEquals(HttpStatus.UNAUTHORIZED, forbiddenException.getHttpStatus());
        Assumptions.assumeTrue(gatewayAuthenticationService.isActive(activatedGatewayAuthToken));
    }

    private GatewayAuthToken activateGatewayAuthToken(GatewayAuthToken gatewayAuthToken, UserDTO tokenOwner) {
        GatewayAuthToken activeGatewayAuthToken = new GatewayAuthToken(
                gatewayAuthToken.getPureToken(),
                gatewayAuthToken.getAuthority(),
                Instant.now(),
                Instant.now().plusSeconds(TestProperties.getGatewayTokenLifetime()),
                gatewayAuthToken.getPrincipal(),
                gatewayAuthToken.getAdditionalClaims(),
                gatewayAuthToken.getPrefixedRefreshToken());

        String tokenId = activeGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class)
                .orElse(StringUtils.EMPTY);
        Assertions.assertTrue(StringUtils.isNotEmpty(tokenId));
        saveTokenIdentification(new TokenIdentificationDTO(tokenOwner, tokenId));
        Assumptions.assumeTrue(gatewayAuthenticationService.isActive(activeGatewayAuthToken),
                "Gateway auth token should be active before revoke operation");
        return activeGatewayAuthToken;
    }

    private void saveUser(UserDTO user) {
        jdbcTemplate.update("insert into user (id,username,hashed_password,authority) value (?,?,?,?)",
                user.getId(), user.getUsername(), user.getHashedPassword(), user.getAuthority().name());
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("truncate table token_identification");
        truncateUsers(jdbcTemplate);
    }
}
