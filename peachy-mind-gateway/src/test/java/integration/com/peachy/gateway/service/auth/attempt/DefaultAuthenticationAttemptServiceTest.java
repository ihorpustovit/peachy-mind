package integration.com.peachy.gateway.service.auth.attempt;

import argumentprovider.service.auth.attempt.AuthenticationAttemptServiceTestArgumentProvider;
import argumentprovider.service.auth.attempt.AuthenticationAttemptServiceTestPayload;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.entity.AuthenticationAttemptEntity;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import com.peachy.security.entity.UserEntity;
import integration.com.peachy.gateway.AbstractIntegrationTest;
import integration.com.peachy.gateway.AbstractJpaTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;
import testdata.AuthAttemptServiceTestUtils;
import testdata.TestProperties;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

import static testdata.AuthAttemptServiceTestUtils.isAuthenticationAttemptForUserId;
import static testdata.AuthAttemptServiceTestUtils.saveAuthenticationAttempt;
import static testdata.UserServiceTestUtils.saveUser;
import static testdata.UserServiceTestUtils.truncateUsers;

public class DefaultAuthenticationAttemptServiceTest extends AbstractIntegrationTest {

    @Autowired
    private AuthenticationAttemptService defaultAuthenticationAttemptService;

    @Autowired
    private Environment environment;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        AuthAttemptServiceTestUtils.truncateAuthenticationAttempts(jdbcTemplate);
        truncateUsers(jdbcTemplate);
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testAttemptsCountForUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        int presumptiveAuthenticationAttemptsCount = presumptiveAuthAttempt.getAttemptsCount();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();

        saveUser(authenticationAttemptServiceTestPayload.getUserEntity(), jdbcTemplate);
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        int actualAuthenticationAttemptsCount = defaultAuthenticationAttemptService.attemptsCountForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertEquals(presumptiveAuthenticationAttemptsCount, actualAuthenticationAttemptsCount);
    }


    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testAttemptsCountForUsernameReturnsZeroIfThereIsNoAttemptsForGIvenUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //TEST
        int actualAuthenticationAttemptsCount =
                defaultAuthenticationAttemptService.attemptsCountForUsername(authenticationAttemptServiceTestPayload.getUserEntity().getUsername());

        //ASSERT
        Assertions.assertEquals(0, actualAuthenticationAttemptsCount);
    }


    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testAuthAttemptForUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();

        saveUser(authenticationAttemptServiceTestPayload.getUserEntity(), jdbcTemplate);
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        Optional<AuthenticationAttemptDTO> actualAuthAttempt = defaultAuthenticationAttemptService.authAttemptForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertTrue(actualAuthAttempt.isPresent());
        Assertions.assertEquals(presumptiveUser.getId(), actualAuthAttempt.get().getUserId());
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testAuthAttemptForUsernameReturnsEmptyOptionalIfThereIsNoAttemptsForGivenUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        UserEntity presumptiveUser = authenticationAttemptServiceTestPayload.getUserEntity();
        Assumptions.assumeFalse(isAuthenticationAttemptForUserId(presumptiveUser.getId(), jdbcTemplate));

        //TEST
        Optional<AuthenticationAttemptDTO> actualAuthAttempt = defaultAuthenticationAttemptService.authAttemptForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertFalse(actualAuthAttempt.isPresent());
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testForceResetAuthenticationAttemptForUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();

        saveUser(authenticationAttemptServiceTestPayload.getUserEntity(), jdbcTemplate);
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        defaultAuthenticationAttemptService.forceResetAuthenticationAttemptsForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertFalse(isAuthenticationAttemptForUserId(presumptiveUser.getId(), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testStrategicalResetAuthenticationAttemptsDoesntRemoveAttemptIfCountHitLimitButTTLIsNotExceeded(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();
        presumptiveAuthAttempt.setAttemptsCount(TestProperties.getAuthAttemptLimit() + 1);
        ReflectionTestUtils.setField(presumptiveAuthAttempt, "createdAt", LocalDateTime.now());
        ReflectionTestUtils.setField(presumptiveAuthAttempt, "updatedAt", LocalDateTime.now());

        saveUser(presumptiveUser, jdbcTemplate);
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        defaultAuthenticationAttemptService.strategicalResetAuthenticationAttemptsForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testStrategicalResetAuthenticationAttemptsRemovesAttemptIfCountHitLimitAndTTLIsExceeded(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        int authAttemptLimitTTLInSeconds = Integer.parseInt(environment.getProperty("brute-force.protection.auth-attempt.limit.ttl"));
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();
        presumptiveAuthAttempt.setAttemptsCount(TestProperties.getAuthAttemptLimit() + 1);
        ReflectionTestUtils.setField(presumptiveAuthAttempt, "createdAt", LocalDateTime.now());
        ReflectionTestUtils.setField(presumptiveAuthAttempt, "updatedAt", LocalDateTime.now().minusSeconds(authAttemptLimitTTLInSeconds + 1));

        saveUser(presumptiveUser, jdbcTemplate);
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        defaultAuthenticationAttemptService.strategicalResetAuthenticationAttemptsForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertFalse(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testStrategicalResetAuthenticationAttemptsDoesntRemoveAttemptIfCountDoesntHitLimitAndTTLIsNotExceeded(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();
        presumptiveAuthAttempt.setAttemptsCount(TestProperties.getAuthAttemptLimit() - 1);
        ReflectionTestUtils.setField(presumptiveAuthAttempt, "createdAt", LocalDateTime.now());
        ReflectionTestUtils.setField(presumptiveAuthAttempt, "updatedAt", LocalDateTime.now());

        saveUser(presumptiveUser, jdbcTemplate);
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        defaultAuthenticationAttemptService.strategicalResetAuthenticationAttemptsForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));
    }


    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testStrategicalResetAuthenticationAttemptsDoesntRemoveAttemptIfCountDoesntHitLimitAndTTLIsExceeded(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        int authAttemptPreLimitTTLInSeconds = Integer.parseInt(environment.getProperty("brute-force.protection.auth-attempt.pre-limit.ttl"));
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();
        presumptiveAuthAttempt.setAttemptsCount(TestProperties.getAuthAttemptLimit() - 1);
        ReflectionTestUtils.setField(presumptiveAuthAttempt, "createdAt", LocalDateTime.now());
        ReflectionTestUtils.setField(presumptiveAuthAttempt, "updatedAt", LocalDateTime.now().minusSeconds(authAttemptPreLimitTTLInSeconds + 1));

        saveUser(presumptiveUser, jdbcTemplate);
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        defaultAuthenticationAttemptService.strategicalResetAuthenticationAttemptsForUsername(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertFalse(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testIncreaseAuthenticationAttemptsIncreasesAuthenticationAttemptsIfThereWereAttemptsForUserBefore(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();

        saveUser(authenticationAttemptServiceTestPayload.getUserEntity(), jdbcTemplate);
        saveAuthenticationAttempt(presumptiveAuthAttempt, jdbcTemplate);
        Assumptions.assumeTrue(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));
        Assumptions.assumeTrue(Objects.equals(AuthAttemptServiceTestUtils.authenticationAttemptsCount(presumptiveUser.getId(), jdbcTemplate), presumptiveAuthAttempt.getAttemptsCount()));

        //TEST
        defaultAuthenticationAttemptService.increaseAuthenticationAttempts(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertEquals(presumptiveAuthAttempt.getAttemptsCount() + 1, AuthAttemptServiceTestUtils.authenticationAttemptsCount(presumptiveUser.getId(), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testIncreaseAuthenticationAttemptsCreatesAuthenticationAttemptsIfThereWereNoAttemptsForUserBefore(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        AuthenticationAttemptEntity presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity();
        UserEntity presumptiveUser = presumptiveAuthAttempt.getUser();

        saveUser(authenticationAttemptServiceTestPayload.getUserEntity(), jdbcTemplate);
        Assumptions.assumeFalse(isAuthenticationAttemptForUserId(presumptiveAuthAttempt.getUserId(), jdbcTemplate));

        //TEST
        defaultAuthenticationAttemptService.increaseAuthenticationAttempts(presumptiveUser.getUsername());

        //ASSERT
        Assertions.assertEquals(1, AuthAttemptServiceTestUtils.authenticationAttemptsCount(presumptiveUser.getId(), jdbcTemplate));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testIncreaseAuthenticationAttemptsThrowsNotFoundExceptionIfThereIsNoSuchUser(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //TEST
        NotFoundException notFoundException =
                Assertions.assertThrows(NotFoundException.class, () -> defaultAuthenticationAttemptService.increaseAuthenticationAttempts(authenticationAttemptServiceTestPayload.getUserEntity().getUsername()));

        //ASSERT
        Assertions.assertEquals(HttpStatus.NOT_FOUND, notFoundException.getHttpStatus());
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        AuthAttemptServiceTestUtils.truncateAuthenticationAttempts(jdbcTemplate);
        truncateUsers(jdbcTemplate);
    }
}
