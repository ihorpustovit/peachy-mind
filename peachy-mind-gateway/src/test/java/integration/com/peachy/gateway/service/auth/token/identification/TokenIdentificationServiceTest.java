package integration.com.peachy.gateway.service.auth.token.identification;

import argumentprovider.service.auth.token.identification.TokenIdentificationServiceTestArgumentProvider;
import argumentprovider.service.auth.token.identification.TokenIdentificationServiceTestPayload;
import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.service.auth.token.identification.TokenIdentificationService;
import integration.com.peachy.gateway.AbstractIntegrationTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;

import static testdata.TokenIdentificationServiceTestUtils.saveTokenIdentification;
import static testdata.TokenIdentificationServiceTestUtils.truncateTokenIdentification;
import static testdata.UserServiceTestUtils.saveUser;
import static testdata.UserServiceTestUtils.truncateUsers;

public class TokenIdentificationServiceTest extends AbstractIntegrationTest {
    @Autowired
    private TokenIdentificationService tokenIdentificationService;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        truncateUsers(jdbcTemplate);
        truncateTokenIdentification(jdbcTemplate);
    }

    @ParameterizedTest
    @ArgumentsSource(TokenIdentificationServiceTestArgumentProvider.class)
    public void testSave(TokenIdentificationServiceTestPayload tokenIdentificationServiceTestPayload) {
        //SETUP
        saveUser(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity().getOwner(), jdbcTemplate);

        //TEST
        TokenIdentificationDTO tokenIdentificationDTO = tokenIdentificationService.save(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO());

        //ASSERT
        assertTokenIdentificationSaved(tokenIdentificationDTO.getTokenId());
    }



    private void assertTokenIdentificationSaved(String tokenId) {
        Assertions.assertEquals(1L,jdbcTemplate.queryForObject("select count(*) from token_identification where token_id = ?",Long.class,tokenId));
    }

    private void assertTokenIdentificationNotSaved(String tokenId) {
        Assertions.assertEquals(0L,jdbcTemplate.queryForObject("select count(*) from token_identification where token_id = ?",Long.class,tokenId));
    }

    @ParameterizedTest
    @ArgumentsSource(TokenIdentificationServiceTestArgumentProvider.class)
    public void testOnlyOneTokenPerUser(TokenIdentificationServiceTestPayload tokenIdentificationServiceTestPayload) {
        TokenIdentificationDTO tokenIdentificationDTO = tokenIdentificationServiceTestPayload.getTokenIdentificationDTO();
        saveUser(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity().getOwner(), jdbcTemplate);

        //TEST
        tokenIdentificationService.save(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO());
        assertTokenIdentificationSaved(tokenIdentificationDTO.getTokenId());
        tokenIdentificationService.save(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO());
        assertTokenIdentificationSaved(tokenIdentificationDTO.getTokenId());
    }

    @ParameterizedTest
    @ArgumentsSource(TokenIdentificationServiceTestArgumentProvider.class)
    public void testDeleteByTokenOwnerId(TokenIdentificationServiceTestPayload tokenIdentificationServiceTestPayload) {
        //SETUP
        saveUser(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity().getOwner(), jdbcTemplate);
        saveTokenIdentification(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity(), jdbcTemplate);
        assertTokenIdentificationSaved(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getTokenId());

        //TEST
        tokenIdentificationService.deleteByTokenOwnerId(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getOwner().getId());

        //ASSERT
        assertTokenIdentificationNotSaved(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getTokenId());
    }


    @ParameterizedTest
    @ArgumentsSource(TokenIdentificationServiceTestArgumentProvider.class)
    public void testFindByTokenId(TokenIdentificationServiceTestPayload tokenIdentificationServiceTestPayload) {
        //SETUP
        saveUser(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity().getOwner(), jdbcTemplate);
        saveTokenIdentification(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity(), jdbcTemplate);
        assertTokenIdentificationSaved(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getTokenId());

        //TEST
        Optional<TokenIdentificationDTO> presumptiveTokenIdentificationDTO =
                tokenIdentificationService.findByTokenId(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getTokenId());

        //ASSERT
        Assertions.assertTrue(presumptiveTokenIdentificationDTO.isPresent(), String.format("There is not token identification found for token id %s", tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getTokenId()));
        Assertions.assertEquals(presumptiveTokenIdentificationDTO.get().getTokenId(), tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getTokenId(),
                String.format("Found token identification has unsatisfying token id. Found token id: %s ; Sample token id: %s",
                        presumptiveTokenIdentificationDTO.get().getTokenId(),
                        tokenIdentificationServiceTestPayload.getTokenIdentificationEntity().getTokenId()));
        Assertions.assertEquals(
                presumptiveTokenIdentificationDTO.get().getOwner().getId(),
                tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getOwner().getId(),
                String.format("Found token identification has unsatisfying owner. Found token owner: %s ; Sample token owner: %s",
                        presumptiveTokenIdentificationDTO.get().getOwner().getId(),
                        tokenIdentificationServiceTestPayload.getTokenIdentificationDTO().getOwner().getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(TokenIdentificationServiceTestArgumentProvider.class)
    public void testFindByTokenIdReturnsEmptyOptionalIfThereIsNotTokenIdentificationForGivenTokenId(TokenIdentificationServiceTestPayload tokenIdentificationServiceTestPayload) {
        //SETUP
        String randomTokenId = RandomStringUtils.random(12,true,true);
        assertTokenIdentificationNotSaved(randomTokenId);

        //TEST
        Optional<TokenIdentificationDTO> presumptiveTokenIdentificationDTO =
                tokenIdentificationService.findByTokenId(randomTokenId);

        //ASSERT
        Assertions.assertFalse(presumptiveTokenIdentificationDTO.isPresent());
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        truncateUsers(jdbcTemplate);
        truncateTokenIdentification(jdbcTemplate);
    }
}
