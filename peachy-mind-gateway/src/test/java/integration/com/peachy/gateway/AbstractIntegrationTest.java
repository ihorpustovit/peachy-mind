package integration.com.peachy.gateway;

import com.peachy.gateway.PeachyMindGateway;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = PeachyMindGateway.class)
@ActiveProfiles("standalone-test")
public abstract class AbstractIntegrationTest extends AbstractJpaTest {
}
