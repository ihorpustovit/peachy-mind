package integration.com.peachy.gateway;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractJpaTest {
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void beforeEach() {
        setup(jdbcTemplate);
    }

    @AfterEach
    public void afterEach() {
        tearDown(jdbcTemplate);
    }

    protected abstract void setup(JdbcTemplate jdbcTemplate);

    protected abstract void tearDown(JdbcTemplate jdbcTemplate);
}
