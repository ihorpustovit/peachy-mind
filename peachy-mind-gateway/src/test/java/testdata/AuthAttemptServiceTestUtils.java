package testdata;

import com.peachy.gateway.entity.AuthenticationAttemptEntity;
import org.springframework.jdbc.core.JdbcTemplate;

public final class AuthAttemptServiceTestUtils {
    public static void saveAuthenticationAttempt(AuthenticationAttemptEntity authenticationAttemptEntity,
                                                 JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("INSERT INTO authentication_attempt(user_id,attempts_count,created_at,updated_at) VALUE (?,?,?,?)",
                authenticationAttemptEntity.getUserId(),
                authenticationAttemptEntity.getAttemptsCount(),
                authenticationAttemptEntity.getCreatedAt(),
                authenticationAttemptEntity.getUpdatedAt());
    }

    public static void truncateAuthenticationAttempts(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("TRUNCATE authentication_attempt");
    }

    public static boolean isAuthenticationAttemptForUserId(String userId,
                                                     JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM authentication_attempt WHERE user_id = ?",Integer.class,userId) > 0;
    }

    public static Integer authenticationAttemptsCount(String userId,
                                                      JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("SELECT attempts_count FROM authentication_attempt WHERE user_id = ?",Integer.class, userId);
    }
}
