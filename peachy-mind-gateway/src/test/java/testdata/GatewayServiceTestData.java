package testdata;

import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.dto.UserCredentialsDTO;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.gateway.entity.AuthenticationAttemptEntity;
import com.peachy.gateway.entity.TokenIdentificationEntity;
import com.peachy.gateway.event.auth.GatewayAuthenticationFailureEvent;
import com.peachy.gateway.event.auth.GatewayAuthenticationSuccessEvent;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import com.peachy.test.data.TestData;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Arrays;

public class GatewayServiceTestData extends TestData {

    public static UserCredentialsDTO getUserCredentials() {
        return new UserCredentialsDTO(TestProperties.getUsername(), TestProperties.getPassword());
    }

    public static RefreshToken getRefreshToken() {
        UserDTO userDTO = getUserDTO();
        RefreshToken refreshToken = new RefreshToken(
                TestProperties.getPureRefreshToken(),
                userDTO.getAuthority(),
                Instant.ofEpochSecond(TestProperties.getRefreshTokenIssuedAtUnix()),
                Instant.ofEpochSecond(TestProperties.getRefreshTokenExpiresAtUnix()),
                userDTO.getId(),
                Arrays.asList(
                        Pair.of(PeachyMindJwtClaim.TOKEN_ID, TestProperties.getTokenId()),
                        Pair.of(PeachyMindJwtClaim.SUBJECT, userDTO.getId()),
                        Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.REFRESH.name()),
                        Pair.of(PeachyMindJwtClaim.ISSUER, TestProperties.getIssuer()),
                        Pair.of(PeachyMindJwtClaim.ISSUED_AT_UNIX, TestProperties.getRefreshTokenIssuedAtUnix()),
                        Pair.of(PeachyMindJwtClaim.EXPIRATION_UNIX, TestProperties.getRefreshTokenExpiresAtUnix()))
        );

        refreshToken.setIssuer(TestProperties.getIssuer());

        return refreshToken;
    }

    public static GatewayAuthToken getGatewayAuthToken() {
        UserDTO userDTO = getUserDTO();

        GatewayAuthToken gatewayAuthToken =
                new GatewayAuthToken(
                        TestProperties.getPureGatewayToken(),
                        userDTO.getAuthority(),
                        Instant.ofEpochSecond(TestProperties.getGatewayTokenIssuedAtUnix()),
                        Instant.ofEpochSecond(TestProperties.getGatewayTokenExpiresAtUnix()),
                        userDTO.getId(),
                        Arrays.asList(
                                Pair.of(PeachyMindJwtClaim.TOKEN_ID, TestProperties.getTokenId()),
                                Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.GATEWAY.name()),
                                Pair.of(PeachyMindJwtClaim.SUBJECT, userDTO.getId()),
                                Pair.of(PeachyMindJwtClaim.INTERNAL_TOKEN, getInternalAuthToken().getPureToken()),
                                Pair.of(PeachyMindJwtClaim.ISSUER, TestProperties.getIssuer()),
                                Pair.of(PeachyMindJwtClaim.ISSUED_AT_UNIX, TestProperties.getGatewayTokenIssuedAtUnix()),
                                Pair.of(PeachyMindJwtClaim.EXPIRATION_UNIX, TestProperties.getGatewayTokenExpiresAtUnix())),
                        getRefreshToken().getPrefixedToken());

        gatewayAuthToken.setIssuer(TestProperties.getIssuer());

        return gatewayAuthToken;
    }

    public static TokenIdentificationEntity getTokenIdentificationEntity() {
        TokenIdentificationEntity tokenIdentificationEntity = new TokenIdentificationEntity();
        tokenIdentificationEntity.setTokenId(TestProperties.getTokenId());
        tokenIdentificationEntity.setOwner(getUserEntity());
        tokenIdentificationEntity.setOwnerId(getUserEntity().getId());
        return tokenIdentificationEntity;
    }

    public static TokenIdentificationDTO getTokenIdentificationDTO() {
        TokenIdentificationDTO tokenIdentificationDTO = new TokenIdentificationDTO();
        tokenIdentificationDTO.setTokenId(TestProperties.getTokenId());
        tokenIdentificationDTO.setOwner(getUserDTO());
        tokenIdentificationDTO.setCreatedAt(LocalDateTime.now());
        tokenIdentificationDTO.setCreatedAt(LocalDateTime.now());
        return tokenIdentificationDTO;
    }

    public static GatewayAuthenticationSuccessEvent getGatewayAuthenticationSuccessEvent() {
        return new GatewayAuthenticationSuccessEvent(getUserCredentials());
    }

    public static GatewayAuthenticationFailureEvent getGatewayAuthenticationFailureEvent() {
        return new GatewayAuthenticationFailureEvent(getUserCredentials());
    }

    public static AuthenticationAttemptEntity getAuthenticationAttemptEntity() {
        AuthenticationAttemptEntity authenticationAttemptEntity =
                new AuthenticationAttemptEntity();
        authenticationAttemptEntity.setAttemptsCount(1);
        authenticationAttemptEntity.setUser(getUserEntity());
        authenticationAttemptEntity.setUserId(getUserEntity().getId());
        return authenticationAttemptEntity;
    }

    public static AuthenticationAttemptDTO getAuthenticationAttemptDTO() {
        return new AuthenticationAttemptDTO(TestProperties.getUserId(), getUserDTO(), 1);
    }

    public static GatewayAuthToken getReadyToUseGatewayAuthTokenFor(UserEntity user, JdbcTemplate jdbcTemplate) {
        GatewayAuthToken gatewayAuthToken = getGatewayAuthToken();
        ReflectionTestUtils.setField(gatewayAuthToken, "principal", user.getId());
        ReflectionTestUtils.setField(gatewayAuthToken, "authority", user.getAuthority());
        gatewayAuthToken.getAdditionalClaims().put(PeachyMindJwtClaim.AUTHORITY, user.getAuthority().name());
        gatewayAuthToken.getAdditionalClaims().put(PeachyMindJwtClaim.SUBJECT, user.getId());

        String presumptivePureGatewayAuthToken = JwtTokenTestUtils.generatePureTestTokenBasedOn(
                gatewayAuthToken,
                TestProperties.getGatewayAuthTokenSignature(),
                TestProperties.getSignatureAlgorithmName());

        ReflectionTestUtils.setField(gatewayAuthToken,"pureToken",presumptivePureGatewayAuthToken);

        UserServiceTestUtils.saveUser(user,jdbcTemplate);

        TokenIdentificationEntity tokenIdentification = getTokenIdentificationEntity();
        tokenIdentification.setOwner(user);
        TokenIdentificationServiceTestUtils.saveTokenIdentification(tokenIdentification, jdbcTemplate);

        return gatewayAuthToken;
    }
}
