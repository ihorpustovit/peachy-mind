package testdata;

import io.jsonwebtoken.SignatureAlgorithm;

import java.time.Duration;
import java.time.Instant;

public final class TestProperties {

    public static int getAuthAttemptLimit() {
        return 10;
    }

    public static String getUsername() {
        return "username";
    }

    public static String getPassword() {
        return "password";
    }

    public static String getUserId() {
        return "KTFTjfCPWe2Y";
    }

    public static Long getGatewayTokenExpiresAtUnix() {
        return Instant.now().plus(Duration.ofSeconds(getGatewayTokenLifetime())).getEpochSecond();
    }

    public static Long getGatewayTokenIssuedAtUnix() {
        return Instant.now().getEpochSecond();
    }

    public static Long getRefreshTokenExpiresAtUnix() {
        return Instant.now().plus(Duration.ofSeconds(getRefreshTokenLifetime())).getEpochSecond();
    }

    public static Long getRefreshTokenIssuedAtUnix() {
        return Instant.now().getEpochSecond();
    }

    public static Long getInternalTokenExpiresAtUnix() {
        return Instant.now().plus(Duration.ofSeconds(getInternalTokenLifetime())).getEpochSecond();
    }

    public static Long getInternalTokenIssuedAtUnix() {
        return Instant.now().getEpochSecond();
    }

    public static String getTokenId() {
        return "SnhwKUN2ZzSU";
    }

    public static Long getGatewayTokenLifetime() {
        return 604200L;
    }

    public static Long getRefreshTokenLifetime() {
        return 2678400L;
    }

    public static Long getInternalTokenLifetime() {
        return 604800L;
    }

    public static String getIssuer() {
        return "www.peachy-mind.com";
    }

    public static String getSignatureAlgorithmName() {
        return SignatureAlgorithm.HS512.getValue();
    }

    public static String getGatewayAuthTokenSignature() {
        return "ybh3+N9auwFTcqOGl0AEE1GbPf27ivTTmgzGK3P8oS+vcY1PXV6feI2gz1dX3a3yayVvdE5ONiIwDsLZf7rOwg==";
    }

    public static String getRefreshTokenSignature() {
        return "QcbQuxZHPyzgBAqJbOrJM94RQzXDF+I2IIk4+OIrmMbh7q133CaMIt7OqqaCIBAxF6+S3Iyy1n7CrimB8qZPlA==";
    }

    public static String getInternalAuthTokenSignature() {
        return "ybh3+N9auwFTcqOGl0AEE1GbPf27ivTTmgzGK3P8oS+vcY1PXV6feI2gz1dX3a3yayVvdE5ONiIwDsLZf7rOwg==";
    }

    public static String getPureRefreshToken() {
        return "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJLVEZUamZDUFdlMlkiLCJhdXoiOiJTVFVERU5UIiwiaXNzIjoid3d3LnBlYWNoeS1taW5kLmNvbSIsInRwZSI6IlJFRlJFU0giLCJldXgiOjE2MjA3NTg0MTY1MDgsIml1eCI6MTYxODA4MDAxNjUwOCwidGlkIjoiU25od0tVTjJaelNVIn0.9gqucC8YrVdAKMVUpglDnnhH9JuCRUNfX6TqKNMSgOCkJLlv1bk70ndrvCCcWGEdqoGMWrMl7643du5C-PJiPA";
    }

    public static String getPureInternalToken() {
        return "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJLVEZUamZDUFdlMlkiLCJhdXoiOiJTVFVERU5UIiwiaXNzIjoid3d3LnBlYWNoeS1taW5kLmNvbSIsInRwZSI6IlJFRlJFU0giLCJldXgiOjE2MjEwMjUyMDc0MDAsIml1eCI6MTYxODM0NjgwNzQwMCwidGlkIjoiRjJubzdOSkRHN21FIn0.0_dZjya1Q34jeDQH4kxEfIK6DJfZv_LItmyGKsQFtRNfrQd-APKyvlVfzLRJKAse9ZH1BT5BaUFb8Exmc30vXA";
    }

    public static String getPureGatewayToken() {
        return "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJLVEZUamZDUFdlMlkiLCJpdG4iOiJJbnRlcm5hbCBleUpoYkdjaU9pSklVelV4TWlKOS5leUp6ZFdJaU9pSkxWRVpVYW1aRFVGZGxNbGtpTENKaGRYb2lPaUpUVkZWRVJVNVVJaXdpYVhOeklqb2lkM2QzTG5CbFlXTm9lUzF0YVc1a0xtTnZiU0lzSW5Sd1pTSTZJa2xPVkVWU1RrRk1JaXdpWlhWNElqb3hOakU0T1RVeE5qQXlOamd4TENKcGRYZ2lPakUyTVRnek5EWTRNREkyT0RFc0luUnBaQ0k2SWtZeWJtODNUa3BFUnpkdFJTSjkucFRTSmZkeUxka2s1RmpYcVpFeHBra0lqcEtBUDE2WFp6aWFEWFhMUjN3dmJuSTVwT2QyanY0WnFTQUY1MVBTQVgxclJDbFRIQksyRXFDNlFNNlNYa0EiLCJhdXoiOiJTVFVERU5UIiwiaXNzIjoid3d3LnBlYWNoeS1taW5kLmNvbSIsInRwZSI6IkdBVEVXQVkiLCJldXgiOjE2MTg5NTEwMDczOTgsIml1eCI6MTYxODM0NjgwNzM5OCwidGlkIjoiRjJubzdOSkRHN21FIn0.6vIl1myYf0_I7wrHx02_9Hyj-6FmuU2DFgE6Q7AHhU8-mj7i-99S5y9sib7z4pNVak8_Ijolry8eYAhvlItgdw";
    }
}
