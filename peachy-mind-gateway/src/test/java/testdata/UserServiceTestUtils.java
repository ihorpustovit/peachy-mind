package testdata;

import com.peachy.security.UserAuthority;
import com.peachy.security.entity.UserEntity;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;

public final class UserServiceTestUtils {
    public static void saveUser(UserEntity user, JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("insert into user(id,username,hashed_password,authority,created_at,updated_at) value (?,?,?,?,now(),now())",
                user.getId(),
                user.getUsername(),
                user.getHashedPassword(),
                user.getAuthority().name());
    }

    public static boolean existsByUsername(String username, JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("select count(*) from user where username=?", Integer.class, username) > 0;
    }

    public static void truncateUsers(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("delete from user where username <> 'admin'");
    }

    public static UserEntity generateUser(PasswordEncoder passwordEncoder) {
        UserEntity generatedUser = new UserEntity();
        String generatedUserPassword = RandomStringUtils.random(12, true, true);

        generatedUser.setUsername(RandomStringUtils.random(12, true, true));
        generatedUser.setId(RandomStringUtils.random(12, true, true));
        generatedUser.setHashedPassword(passwordEncoder.encode(generatedUserPassword));
        generatedUser.setAuthority(UserAuthority.NONE);

        return generatedUser;
    }
}
