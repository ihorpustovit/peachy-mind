package testdata;

import com.peachy.security.dto.token.JwtToken;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public final class JwtTokenTestUtils {
    public static String generatePureTestTokenBasedOn(JwtToken tokenBlueprint,
                                                      String signature,
                                                      String signatureAlgorithmName) {
        return Jwts.builder()
                .addClaims(tokenBlueprint.getAdditionalClaims())
                .signWith(SignatureAlgorithm.forName(signatureAlgorithmName), signature)
                .compact();
    }
}
