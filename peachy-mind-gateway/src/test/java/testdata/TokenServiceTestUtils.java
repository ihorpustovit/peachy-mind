package testdata;

import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenPrefix;
import com.peachy.security.TokenType;
import com.peachy.security.dto.token.JwtToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;

import java.util.Objects;
import java.util.Optional;

public final class TokenServiceTestUtils {
    public static void jwtGeneratedTokenAssertions(JwtToken jwtToken) {
        Assertions.assertNotNull(jwtToken.getIssuedAt(), "Token issued at property shouldn't be empty");
        Assertions.assertNotNull(jwtToken.getExpiresAt(), "Token expires at property shouldn't be empty");

        Assertions.assertNotNull(jwtToken.getPrefix(), "Token prefix shouldn't be empty");
        Assertions.assertNotNull(jwtToken.getAuthority(), "Token user authority shouldn't be empty");
        Assertions.assertNotNull(jwtToken.getPureToken(), "Pure token shouldn't be empty");
        Assertions.assertTrue(StringUtils.isNotEmpty(jwtToken.getPureToken()), "Pure token shouldn't be empty");

        Assertions.assertNotNull(jwtToken.getIssuer(), "Token issuer property shouldn't be empty");
        Assertions.assertEquals(TestProperties.getIssuer(), jwtToken.getIssuer(),
                String.format("Token issuer should be %s, but is %s", TestProperties.getIssuer(), jwtToken.getIssuer()));
    }

    public static void isAdditionalClaimNotEmpty(JwtToken jwtToken,
                                                 String additionalClaimName) {
        Assertions.assertTrue(jwtToken.getAdditionalClaimForName(additionalClaimName).isPresent(),
                String.format("%s token claim is empty though shouldn't be", additionalClaimName));
    }

    public static void isAdditionalClaimMatches(JwtToken jwtToken,
                                                Pair<String, Object> claimSample) {
        Optional<Object> neededClaim = jwtToken.getAdditionalClaimForName(claimSample.getLeft());

        Assertions.assertTrue(
                neededClaim
                        .filter(claimValue -> claimSample.getRight().getClass().isAssignableFrom(claimValue.getClass()))
                        .isPresent(),
                String.format("%s token claim value type doesn't match. Have to be %s, but is %s",
                        claimSample.getLeft(),
                        claimSample.getRight().getClass().getCanonicalName(),
                        neededClaim
                                .map(Object::getClass)
                                .map(Class::getCanonicalName)
                                .orElse("empty")));
        Assertions.assertTrue(
                neededClaim
                        .filter(claimValue -> Objects.equals(claimValue, claimSample.getRight()))
                        .isPresent(),
                String.format("%s token claim doesn't match. Have to be %s, but is %s",
                        claimSample.getLeft(),
                        claimSample.getRight(),
                        neededClaim.orElse("empty")));
    }

    public static boolean isTokenPrefixed(String prefixedToken, TokenPrefix tokenPrefix) {
        return prefixedToken.startsWith(tokenPrefix.getStringyPrefix().concat(StringUtils.SPACE));
    }

    public static void refreshTokenAssertions(RefreshToken refreshToken) {
        jwtGeneratedTokenAssertions(refreshToken);
        Assertions.assertEquals((long) TestProperties.getRefreshTokenLifetime(), refreshToken.getExpiresAtUnix() - refreshToken.getIssuedAtUnix(),
                String.format("Refresh token lifetime should be %s, but is %s", TestProperties.getRefreshTokenLifetime(), refreshToken.getExpiresAtUnix() - refreshToken.getIssuedAtUnix()));
        Assertions.assertTrue(isTokenPrefixed(refreshToken.getPrefixedToken(), TokenPrefix.REFRESH));
        isAdditionalClaimNotEmpty(refreshToken, PeachyMindJwtClaim.TOKEN_ID);
        isAdditionalClaimMatches(refreshToken, Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.REFRESH.name()));
        isAdditionalClaimNotEmpty(refreshToken, PeachyMindJwtClaim.SUBJECT);
    }

    public static void gatewayAuthTokenAssertions(GatewayAuthToken gatewayAuthToken) {
        jwtGeneratedTokenAssertions(gatewayAuthToken);
        Assertions.assertNotNull(gatewayAuthToken.getPrefixedRefreshToken(),
                "Gateway refresh token should not be null");
        Assertions.assertTrue(StringUtils.isNotBlank(gatewayAuthToken.getPrefixedRefreshToken()),
                "Gateway refresh token should not be empty");
        Assertions.assertTrue(isTokenPrefixed(gatewayAuthToken.getPrefixedRefreshToken(), TokenPrefix.REFRESH),
                String.format("Gateway refresh token should be prefixed with %s", TokenPrefix.REFRESH));
        Assertions.assertEquals((long) TestProperties.getGatewayTokenLifetime(), gatewayAuthToken.getExpiresAtUnix() - gatewayAuthToken.getIssuedAtUnix(),
                String.format("Gateway token lifetime should be %s, but is %s", TestProperties.getGatewayTokenLifetime(), gatewayAuthToken.getExpiresAtUnix() - gatewayAuthToken.getIssuedAtUnix()));
        Assertions.assertEquals(gatewayAuthToken.getPrefix(), TokenPrefix.BEARER,
                String.format("Gateway token prefix have to be %s, but is %s", TokenPrefix.BEARER.getStringyPrefix(), gatewayAuthToken.getStringyPrefix()));
        Assertions.assertTrue(isTokenPrefixed(gatewayAuthToken.getPrefixedToken(), TokenPrefix.BEARER));
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(gatewayAuthToken, PeachyMindJwtClaim.INTERNAL_TOKEN);
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(gatewayAuthToken, PeachyMindJwtClaim.TOKEN_ID);
        TokenServiceTestUtils.isAdditionalClaimMatches(gatewayAuthToken, Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.GATEWAY.name()));
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(gatewayAuthToken, PeachyMindJwtClaim.SUBJECT);
        Assertions.assertEquals(gatewayAuthToken.getPrincipal(), gatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.SUBJECT, String.class).orElse(StringUtils.EMPTY));
    }
}
