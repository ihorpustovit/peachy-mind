package testdata;

import com.peachy.gateway.entity.TokenIdentificationEntity;
import org.springframework.jdbc.core.JdbcTemplate;

public final class TokenIdentificationServiceTestUtils {
    public static void saveTokenIdentification(TokenIdentificationEntity tokenIdentification, JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("insert into token_identification(owner_id,token_id) value (?,?)", tokenIdentification.getOwner().getId(), tokenIdentification.getTokenId());
    }

    public static boolean existByTokenId(String tokenId, JdbcTemplate jdbcTemplate) {
        return jdbcTemplate.queryForObject("select count(*) from token_identification where token_id=?", Integer.class, tokenId) > 0;
    }

    public static void truncateTokenIdentification(JdbcTemplate jdbcTemplate) {
        jdbcTemplate.update("truncate token_identification");
    }
}
