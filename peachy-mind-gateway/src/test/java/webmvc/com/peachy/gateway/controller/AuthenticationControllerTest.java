package webmvc.com.peachy.gateway.controller;

import argumentprovider.controller.AuthenticationControllerTestArgumentProvider;
import argumentprovider.controller.AuthenticationControllerTestPayload;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.peachy.gateway.dto.UserCredentialsDTO;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.gateway.entity.TokenIdentificationEntity;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenPrefix;
import com.peachy.security.TokenType;
import com.peachy.security.converter.UserConverter;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import com.peachy.web.PeachyMindHttpHeaders;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.util.UriComponentsBuilder;
import testdata.JwtTokenTestUtils;
import testdata.TestProperties;
import testdata.TokenIdentificationServiceTestUtils;
import testdata.UserServiceTestUtils;
import webmvc.com.peachy.gateway.WebMvcTest;

import java.time.Duration;
import java.time.Instant;

import static org.hamcrest.Matchers.*;
import static testdata.UserServiceTestUtils.saveUser;
import static testdata.UserServiceTestUtils.truncateUsers;

public class AuthenticationControllerTest extends WebMvcTest {
    protected final String AUTHENTICATION_CONTROLLER_PATH = "/auth";

    @Autowired
    private PasswordEncoder defaultPasswordEncoder;

    @Autowired
    private UserConverter userConverter;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        truncateUsers(jdbcTemplate);
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testSignUp(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserDTO user = authenticationControllerTestPayload.getUser();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode newUser = objectMapper.createObjectNode();
        newUser.put("username", user.getUsername());
        newUser.put("plainPassword", user.getPlainPassword());
        newUser.put("authority", user.getAuthority().name());

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .body(newUser.toPrettyString())
                .given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post(AUTHENTICATION_CONTROLLER_PATH.concat("/sign/up"))
                .then()
                .statusCode(HttpStatus.CREATED.value());
        response
                .body("id", notNullValue())
                .body("username", is(user.getUsername()))
                .body("hashedPassword", notNullValue())
                .body("authority", is(user.getAuthority().name()))
                .body("plainPassword", nullValue());

        String hashedPassword = response.extract().path("hashedPassword");
        String password = response.extract().path("password");
        Assertions.assertTrue(defaultPasswordEncoder.matches(user.getPlainPassword(), hashedPassword));
        Assertions.assertTrue(defaultPasswordEncoder.matches(user.getPlainPassword(), password));
    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testSignUpReturnsBadRequestStatusIfAuthorityPropertyIsMissing(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserDTO user = authenticationControllerTestPayload.getUser();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode newUser = objectMapper.createObjectNode();
        newUser.put("username", user.getUsername());
        newUser.put("plainPassword", user.getPlainPassword());

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .body(newUser.toPrettyString())
                .given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post(AUTHENTICATION_CONTROLLER_PATH.concat("/sign/up"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        response
                .body("message", is("Unable to create user"))
                .body("reason", is("Authority must not be empty"));

    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testSignUpReturnsBadRequestStatusIfPlainPasswordPropertyIsMissing(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserDTO user = authenticationControllerTestPayload.getUser();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode newUser = objectMapper.createObjectNode();
        newUser.put("username", user.getUsername());
        newUser.put("authority", user.getAuthority().name());

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .body(newUser.toPrettyString())
                .given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post(AUTHENTICATION_CONTROLLER_PATH.concat("/sign/up"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        response
                .body("message", is("Unable to create user"))
                .body("reason", is("Password must not be empty"));

    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testSignUpReturnsBadRequestStatusIfUsernamePropertyIsMissing(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserDTO user = authenticationControllerTestPayload.getUser();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode newUser = objectMapper.createObjectNode();
        newUser.put("plainPassword", user.getPlainPassword());
        newUser.put("authority", user.getAuthority().name());

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .body(newUser.toPrettyString())
                .given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post(AUTHENTICATION_CONTROLLER_PATH.concat("/sign/up"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        response
                .body("message", is("Unable to create user"))
                .body("reason", is("Username must not be empty"));

    }

    @SneakyThrows
    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testSignUpReturnsBadRequestStatusIfUsernameHasAlreadyBeenTaken(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserDTO user = authenticationControllerTestPayload.getUser();
        String presumptiveUsername = authenticationControllerTestPayload.getUserEntity().getUsername();
        UserServiceTestUtils.saveUser(authenticationControllerTestPayload.getUserEntity(), jdbcTemplate);

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode newUser = objectMapper.createObjectNode();
        newUser.put("username", presumptiveUsername);
        newUser.put("plainPassword", user.getPlainPassword());
        newUser.put("authority", user.getAuthority().name());

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .body(newUser.toPrettyString())
                .given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post(AUTHENTICATION_CONTROLLER_PATH.concat("/sign/up"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
        response
                .body("message", is("Unable to create user"))
                .body("reason", is(String.format("Username %s has already been taken", presumptiveUsername)));

    }

    private void gatewayAuthTokenResponseBodyAssertions(ValidatableResponse validatableResponse,
                                                        UserDTO presumptiveUser) {
        validatableResponse
                .body("pureToken", notNullValue())
                .body("issuer", notNullValue())
                .body("prefixedRefreshToken", notNullValue())
                .body("stringyPrefix", notNullValue())
                .body("prefixedToken", notNullValue())
                .body("stringyAuthority", notNullValue())
                .body("stringyAuthority", is(presumptiveUser.getAuthority().getStringyAuthority()))
                .body("issuedAtUnix", notNullValue())
                .body("expiresAtUnix", notNullValue())
                .body("name", notNullValue())
                .body("name", is(presumptiveUser.getId()));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testSignIn(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserEntity presumptiveUser = authenticationControllerTestPayload.getUserEntity();

        String presumptiveUserPlainPassword = presumptiveUser.getHashedPassword();
        presumptiveUser.setHashedPassword(defaultPasswordEncoder.encode(presumptiveUser.getHashedPassword()));
        saveUser(presumptiveUser, jdbcTemplate);

        UserCredentialsDTO userCredentials = new UserCredentialsDTO(presumptiveUser.getUsername(), presumptiveUserPlainPassword);

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .body(userCredentials)
                .queryParam("username", presumptiveUser.getUsername())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .given()
                .post(AUTHENTICATION_CONTROLLER_PATH.concat("/sign/in"))
                .then()
                .statusCode(HttpStatus.OK.value());

        gatewayAuthTokenResponseBodyAssertions(response, authenticationControllerTestPayload.getUser());
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testForwardedSignInReturnsUnauthorizedStatusCodeIfUserCredentialsDontMatch(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserEntity presumptiveUser = authenticationControllerTestPayload.getUserEntity();

        presumptiveUser.setHashedPassword(defaultPasswordEncoder.encode(presumptiveUser.getHashedPassword()));
        saveUser(presumptiveUser, jdbcTemplate);

        UserCredentialsDTO userCredentials = new UserCredentialsDTO(presumptiveUser.getUsername(), "wrong password");


        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .body(userCredentials)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .queryParam("username",presumptiveUser.getUsername())
                .given()
                .post(AUTHENTICATION_CONTROLLER_PATH.concat("/sign/in"))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .body("message", is("Unable to perform authentication"))
                .body("reason", is("Username or password doesn't match"));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testRefresh(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        GatewayAuthToken presumptiveGatewayAuthToken = authenticationControllerTestPayload.getGatewayAuthToken();
        Assumptions.assumeTrue(presumptiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID).isPresent());

        UserDTO presumptiveUser = authenticationControllerTestPayload.getUser();


        TokenIdentificationEntity tokenIdentification = new TokenIdentificationEntity();
        tokenIdentification.setTokenId(presumptiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get());
        tokenIdentification.setOwner(userConverter.toEntity(presumptiveUser));
        saveUser(authenticationControllerTestPayload.getUserEntity(), jdbcTemplate);
        TokenIdentificationServiceTestUtils.saveTokenIdentification(tokenIdentification, jdbcTemplate);

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, presumptiveGatewayAuthToken.getPrefixedRefreshToken())
                .put(AUTHENTICATION_CONTROLLER_PATH.concat("/refresh"))
                .then()
                .statusCode(HttpStatus.OK.value());

        gatewayAuthTokenResponseBodyAssertions(response, presumptiveUser);
    }


    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testRefreshReturnsBadRequestStatusCodeIfThereIsNoRefreshTokenGiven(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .put(AUTHENTICATION_CONTROLLER_PATH.concat("/refresh"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("message", is("Unable to refresh token"))
                .body("reason", is("You've passed empty refresh token"));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testRefreshReturnsBadRequestStatusCodeIfGivenTokenIsNotOfRefreshType(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        RefreshToken presumptiveRefreshToken = authenticationControllerTestPayload.getRefreshToken();
        TokenType presumptiveTokenType = TokenType.NONE;
        presumptiveRefreshToken.getAdditionalClaims().put(PeachyMindJwtClaim.TOKEN_TYPE, presumptiveTokenType);
        String presumptivePureRefreshToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveRefreshToken,
                        TestProperties.getRefreshTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.REFRESH.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureRefreshToken))
                .put(AUTHENTICATION_CONTROLLER_PATH.concat("/refresh"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("message", is("Unable to refresh token"))
                .body("reason", is(String.format("Wrong type of passed refresh token. Is %s, but must be %s", presumptiveTokenType, TokenType.REFRESH)));
    }


    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testRefreshReturnsBadRequestStatusCodeIfTokenIdClaimIsMissing(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        RefreshToken presumptiveRefreshToken = authenticationControllerTestPayload.getRefreshToken();
        presumptiveRefreshToken.getAdditionalClaims().put(PeachyMindJwtClaim.TOKEN_ID, null);
        String presumptivePureRefreshToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveRefreshToken,
                        TestProperties.getRefreshTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.REFRESH.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureRefreshToken))
                .put(AUTHENTICATION_CONTROLLER_PATH.concat("/refresh"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("message", is("Unable to refresh token"))
                .body("reason", is(String.format("%s claim is missing, though must be", PeachyMindJwtClaim.TOKEN_ID)));
    }


    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testRefreshReturnsUnauthorizedStatusCodeIfGivenRefreshTokenIsUnidentifiable(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        RefreshToken presumptiveRefreshToken = authenticationControllerTestPayload.getRefreshToken();
        String presumptivePureRefreshToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveRefreshToken,
                        TestProperties.getRefreshTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());

        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.REFRESH.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureRefreshToken))
                .put(AUTHENTICATION_CONTROLLER_PATH.concat("/refresh"))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .body("message", is("Unable to refresh token"))
                .body("reason", is("Given refresh token either has never been issued by us or is revoked by user"));
    }


    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testRefreshReturnsUnauthorizedStatusCodeIfRefreshTokenPrincipalAndOwnerAreInconsistent(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        RefreshToken presumptiveRefreshToken = authenticationControllerTestPayload.getRefreshToken();
        UserDTO presumptiveUser = authenticationControllerTestPayload.getUser();

        Assumptions.assumeTrue(presumptiveRefreshToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).isPresent());


        TokenIdentificationEntity tokenIdentification = new TokenIdentificationEntity();
        tokenIdentification.setTokenId(presumptiveRefreshToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get());
        tokenIdentification.setOwner(userConverter.toEntity(presumptiveUser));
        saveUser(authenticationControllerTestPayload.getUserEntity(), jdbcTemplate);
        TokenIdentificationServiceTestUtils.saveTokenIdentification(tokenIdentification, jdbcTemplate);

        String fakeUserId = RandomStringUtils.random(12, true, true);
        ReflectionTestUtils.setField(presumptiveRefreshToken, "principal", fakeUserId);
        presumptiveRefreshToken.getAdditionalClaims().put(PeachyMindJwtClaim.SUBJECT, fakeUserId);
        String presumptivePureRefreshToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveRefreshToken,
                        TestProperties.getRefreshTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());


        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.REFRESH.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureRefreshToken))
                .put(AUTHENTICATION_CONTROLLER_PATH.concat("/refresh"))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .body("message", is("Unable to refresh token"))
                .body("reason", is("You aren't the owner of this refresh token"));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testRefreshReturnsBadRequestIfRefreshTokenHasExpired(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        RefreshToken presumptiveRefreshToken = authenticationControllerTestPayload.getRefreshToken();
        ReflectionTestUtils.setField(presumptiveRefreshToken, "issuedAt", Instant.now().minus(Duration.ofDays(365)));
        ReflectionTestUtils.setField(presumptiveRefreshToken, "expiresAt", Instant.now().minus(Duration.ofDays(365)).plus(Duration.ofSeconds(TestProperties.getRefreshTokenLifetime())));
        presumptiveRefreshToken.getAdditionalClaims().put(PeachyMindJwtClaim.ISSUED_AT_UNIX, presumptiveRefreshToken.getIssuedAtUnix());
        presumptiveRefreshToken.getAdditionalClaims().put(PeachyMindJwtClaim.EXPIRATION_UNIX, presumptiveRefreshToken.getExpiresAtUnix());

        UserDTO presumptiveUser = authenticationControllerTestPayload.getUser();
        UserEntity presumptiveUserEntity = authenticationControllerTestPayload.getUserEntity();

        Assumptions.assumeTrue(presumptiveRefreshToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).isPresent());

        TokenIdentificationEntity tokenIdentification = new TokenIdentificationEntity();
        tokenIdentification.setTokenId(presumptiveRefreshToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get());
        tokenIdentification.setOwner(userConverter.toEntity(presumptiveUser));
        saveUser(presumptiveUserEntity, jdbcTemplate);
        TokenIdentificationServiceTestUtils.saveTokenIdentification(tokenIdentification, jdbcTemplate);

        String presumptivePureRefreshToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveRefreshToken,
                        TestProperties.getRefreshTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());


        //TEST AND ASSERT
        ValidatableResponse response = RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.REFRESH.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureRefreshToken))
                .put(AUTHENTICATION_CONTROLLER_PATH.concat("/refresh"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("message", is("Unable to refresh token"))
                .body("reason", is("Given refresh token has expired"));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testRevoke(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        GatewayAuthToken presumptiveGatewayAuthToken = authenticationControllerTestPayload.getGatewayAuthToken();
        UserDTO presumptiveUser = authenticationControllerTestPayload.getUser();
        ReflectionTestUtils.setField(presumptiveGatewayAuthToken, "principal", presumptiveUser.getId());
        presumptiveGatewayAuthToken.getAdditionalClaims().put(PeachyMindJwtClaim.SUBJECT, presumptiveUser.getId());
        String presumptivePureGatewayAuthToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveGatewayAuthToken,
                        TestProperties.getGatewayAuthTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());


        Assumptions.assumeTrue(presumptiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).isPresent());
        String presumptiveTokenId = presumptiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get();

        TokenIdentificationEntity tokenIdentification = new TokenIdentificationEntity();
        tokenIdentification.setTokenId(presumptiveTokenId);
        tokenIdentification.setOwner(userConverter.toEntity(presumptiveUser));
        saveUser(authenticationControllerTestPayload.getUserEntity(), jdbcTemplate);
        TokenIdentificationServiceTestUtils.saveTokenIdentification(tokenIdentification, jdbcTemplate);
        Assumptions.assumeTrue(TokenIdentificationServiceTestUtils.existByTokenId(presumptiveTokenId, jdbcTemplate));

        //TEST
        RestAssured
                .with()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.BEARER.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureGatewayAuthToken))
                .delete(AUTHENTICATION_CONTROLLER_PATH.concat("/revoke"))
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());

        //ASSERT
        Assertions.assertFalse(TokenIdentificationServiceTestUtils.existByTokenId(presumptiveTokenId, jdbcTemplate));
    }

    @Test
    public void testRevokeReturnsUnauthorizedStatusCodeIfUserIsNotAuthenticated() {
        //TEST AND ASSERT
        RestAssured
                .with()
                .delete(AUTHENTICATION_CONTROLLER_PATH.concat("/revoke"))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .body("message", is("Unable to revoke auth token"))
                .body("reason", is("You aren't authorized for such action"));
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        truncateUsers(jdbcTemplate);
    }
}
