package webmvc.com.peachy.gateway;

import com.peachy.gateway.PeachyMindGateway;
import integration.com.peachy.gateway.AbstractJpaTest;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = PeachyMindGateway.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("standalone-test")
public abstract class WebMvcTest extends AbstractJpaTest {

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setPort() {
        RestAssured.port = port;
    }
}
