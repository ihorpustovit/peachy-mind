package webmvc.com.peachy.gateway.controller;

import argumentprovider.controller.UserControllerTestArgumentProvider;
import argumentprovider.controller.UserControllerTestPayload;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.security.TokenPrefix;
import com.peachy.security.UserAuthority;
import com.peachy.security.entity.UserEntity;
import com.peachy.web.PeachyMindHttpHeaders;
import io.restassured.RestAssured;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import testdata.GatewayServiceTestData;
import testdata.UserServiceTestUtils;
import webmvc.com.peachy.gateway.WebMvcTest;

import java.util.Objects;

import static org.hamcrest.Matchers.is;
import static testdata.TokenIdentificationServiceTestUtils.truncateTokenIdentification;
import static testdata.UserServiceTestUtils.truncateUsers;

public class UserControllerTest extends WebMvcTest {
    private final String USER_CONTROLLER_PATH = "/user";

    @Autowired
    private PasswordEncoder defaultPasswordEncoder;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        truncateUsers(jdbcTemplate);
        truncateTokenIdentification(jdbcTemplate);
    }

    @ParameterizedTest
    @ArgumentsSource(UserControllerTestArgumentProvider.class)
    public void testGetUserById(UserControllerTestPayload userControllerTestPayload) {
        //SETUP
        UserEntity presumptiveUser = userControllerTestPayload.getUserEntity();
        UserServiceTestUtils.saveUser(presumptiveUser, jdbcTemplate);

        //TEST AND ASSERT
        RestAssured.given()
                .get(USER_CONTROLLER_PATH.concat("/").concat(presumptiveUser.getId()))
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("id", is(presumptiveUser.getId()))
                .body("username", is(presumptiveUser.getUsername()))
                .body("authority", is(presumptiveUser.getAuthority().name()));
    }

    @Test
    public void testGetUserByIdReturnsNotFoundStatusCodeIfNoUserFoundForGivenUserId() {
        //SETUP
        String presumptiveUserId = "invalidId";

        //TEST AND ASSERT
        RestAssured.given()
                .get(USER_CONTROLLER_PATH.concat("/").concat(presumptiveUserId))
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .body("message", is("Unable to retrieve user"))
                .body("reason", is("No user found for id: " + presumptiveUserId));
    }

    @ParameterizedTest
    @ArgumentsSource(UserControllerTestArgumentProvider.class)
    public void testUpdateUserAuthority(UserControllerTestPayload userControllerTestPayload) {
        //SETUP
        UserEntity adminUser = UserServiceTestUtils.generateUser(defaultPasswordEncoder);
        adminUser.setAuthority(UserAuthority.ADMIN);
        GatewayAuthToken presumptiveGatewayAuthToken = GatewayServiceTestData.getReadyToUseGatewayAuthTokenFor(adminUser, jdbcTemplate);

        UserEntity presumptiveUser = userControllerTestPayload.getUserEntity();

        UserServiceTestUtils.saveUser(userControllerTestPayload.getUserEntity(), jdbcTemplate);
        UserAuthority oldAuthority = presumptiveUser.getAuthority();
        UserAuthority newAuthority = UserAuthority.ADMIN;

        Assumptions.assumeTrue(UserServiceTestUtils.existsByUsername(presumptiveUser.getUsername(), jdbcTemplate));
        Assumptions.assumeFalse(Objects.equals(oldAuthority, newAuthority));

        //TEST AND ASSERT
        RestAssured
                .with()
                .queryParam("authority", newAuthority)
                .headers(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.BEARER.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptiveGatewayAuthToken.getPureToken()))
                .given()
                .patch(USER_CONTROLLER_PATH.concat("/").concat(presumptiveUser.getId()).concat("/authority"))
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("id", is(presumptiveUser.getId()))
                .body("username", is(presumptiveUser.getUsername()))
                .body("authority", is(newAuthority.name()));
    }

    @ParameterizedTest
    @ArgumentsSource(UserControllerTestArgumentProvider.class)
    public void testUpdateUserAuthorityReturnsBadRequestStatusCodeIfBadUserAuthorityPassedToSetForUser(UserControllerTestPayload userControllerTestPayload) {
        //SETUP
        UserEntity adminUser = UserServiceTestUtils.generateUser(defaultPasswordEncoder);
        adminUser.setAuthority(UserAuthority.ADMIN);
        GatewayAuthToken presumptiveGatewayAuthToken = GatewayServiceTestData.getReadyToUseGatewayAuthTokenFor(adminUser, jdbcTemplate);

        UserEntity presumptiveUser = userControllerTestPayload.getUserEntity();
        String presumptiveBadAuthority = "badAuthority";

        //TEST AND ASSERT
        RestAssured
                .with()
                .queryParam("authority", presumptiveBadAuthority)
                .headers(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.BEARER.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptiveGatewayAuthToken.getPureToken()))
                .given()
                .patch(USER_CONTROLLER_PATH.concat("/").concat(presumptiveUser.getId()).concat("/authority"))
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("message", is("Unable to update user"))
                .body("reason", is(String.format("Non-existent authority passed to set. Please make your choice of the following authorities: %s", String.join(", ", UserAuthority.names()))));
    }

    @Test
    public void testUpdateUserAuthorityReturnsNotFoundStatusCodeIfUpdateIsRequestedForNonExistentUser() {
        //SETUP
        UserEntity adminUser = UserServiceTestUtils.generateUser(defaultPasswordEncoder);
        adminUser.setAuthority(UserAuthority.ADMIN);
        GatewayAuthToken presumptiveGatewayAuthToken = GatewayServiceTestData.getReadyToUseGatewayAuthTokenFor(adminUser, jdbcTemplate);


        String randomUserId = RandomStringUtils.random(12, true, true);
        UserAuthority newAuthority = UserAuthority.ADMIN;

        //TEST AND ASSERT
        RestAssured
                .with()
                .queryParam("authority", newAuthority)
                .headers(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.BEARER.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptiveGatewayAuthToken.getPureToken()))
                .given()
                .patch(USER_CONTROLLER_PATH.concat("/").concat(randomUserId).concat("/authority"))
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .body("message", is("Unable to update user authority"))
                .body("reason", is(String.format("No user found for id: %s", randomUserId)));
    }

    @ParameterizedTest
    @ArgumentsSource(UserControllerTestArgumentProvider.class)
    public void testUpdateUserAuthorityReturnsForbiddenStatusCodeIfUserRequestingAnUpdateIsNotOfAnAdminAuthority(UserControllerTestPayload userControllerTestPayload) {
        //SETUP
        UserEntity adminUser = UserServiceTestUtils.generateUser(defaultPasswordEncoder);
        adminUser.setAuthority(UserAuthority.STUDENT);
        GatewayAuthToken presumptiveGatewayAuthToken = GatewayServiceTestData.getReadyToUseGatewayAuthTokenFor(adminUser, jdbcTemplate);

        String newAuthority = UserAuthority.INSTRUCTOR.name();
        String randomUserId = RandomStringUtils.random(12, true, true);

        //TEST AND ASSERT
        RestAssured
                .with()
                .queryParam("authority", newAuthority)
                .headers(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.BEARER.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptiveGatewayAuthToken.getPureToken()))
                .given()
                .patch(USER_CONTROLLER_PATH.concat("/").concat(randomUserId).concat("/authority"))
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Override
    protected void tearDown(JdbcTemplate jdbcTemplate) {
        truncateUsers(jdbcTemplate);
        truncateTokenIdentification(jdbcTemplate);
    }
}
