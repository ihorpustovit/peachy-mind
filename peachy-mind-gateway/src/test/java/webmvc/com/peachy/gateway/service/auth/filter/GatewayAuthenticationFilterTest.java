package webmvc.com.peachy.gateway.service.auth.filter;

import argumentprovider.controller.AuthenticationControllerTestArgumentProvider;
import argumentprovider.controller.AuthenticationControllerTestPayload;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.entity.TokenIdentificationEntity;
import com.peachy.gateway.service.auth.GatewayAuthenticationService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenPrefix;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.entity.UserEntity;
import com.peachy.web.PeachyMindHttpHeaders;
import io.restassured.RestAssured;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;
import testdata.JwtTokenTestUtils;
import testdata.TestProperties;
import testdata.TokenIdentificationServiceTestUtils;
import webmvc.com.peachy.gateway.controller.AuthenticationControllerTest;

import java.time.Instant;

import static org.hamcrest.Matchers.is;
import static testdata.UserServiceTestUtils.saveUser;

public class GatewayAuthenticationFilterTest extends AuthenticationControllerTest {
    @Autowired
    private GatewayAuthenticationService gatewayAuthenticationService;

    private GatewayAuthenticationService gatewayAuthenticationServiceSpy;

    @Override
    protected void setup(JdbcTemplate jdbcTemplate) {
        super.setup(jdbcTemplate);
        gatewayAuthenticationServiceSpy = Mockito.spy(gatewayAuthenticationService);
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testGatewayAuthenticationFilter(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserEntity presumptiveUser = authenticationControllerTestPayload.getUserEntity();

        GatewayAuthToken presumptiveGatewayAuthToken = authenticationControllerTestPayload.getGatewayAuthToken();
        ReflectionTestUtils.setField(presumptiveGatewayAuthToken, "principal", presumptiveUser.getId());
        presumptiveGatewayAuthToken.getAdditionalClaims().put(PeachyMindJwtClaim.SUBJECT, presumptiveUser.getId());
        String presumptivePureGatewayAuthToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveGatewayAuthToken,
                        TestProperties.getGatewayAuthTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());


        Assumptions.assumeTrue(presumptiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).isPresent());
        String presumptiveTokenId = presumptiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get();

        TokenIdentificationEntity tokenIdentification = new TokenIdentificationEntity();
        tokenIdentification.setTokenId(presumptiveTokenId);
        tokenIdentification.setOwner(presumptiveUser);
        saveUser(authenticationControllerTestPayload.getUserEntity(), jdbcTemplate);
        TokenIdentificationServiceTestUtils.saveTokenIdentification(tokenIdentification, jdbcTemplate);
        Assumptions.assumeTrue(TokenIdentificationServiceTestUtils.existByTokenId(presumptiveTokenId, jdbcTemplate));

        Mockito.doNothing().when(gatewayAuthenticationServiceSpy).revokeForUser(Mockito.any(UserDTO.class));


        //TEST AND ASSERT
        RestAssured.given()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.BEARER.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureGatewayAuthToken))
                .delete(AUTHENTICATION_CONTROLLER_PATH.concat("/revoke"))
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testGatewayAuthenticationFilterReturnsUnauthorizedStatusCodeIfTokenIsUnidentifiable(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserDTO presumptiveUser = authenticationControllerTestPayload.getUser();

        GatewayAuthToken presumptiveGatewayAuthToken = authenticationControllerTestPayload.getGatewayAuthToken();
        ReflectionTestUtils.setField(presumptiveGatewayAuthToken, "principal", presumptiveUser.getId());
        presumptiveGatewayAuthToken.getAdditionalClaims().put(PeachyMindJwtClaim.SUBJECT, presumptiveUser.getId());
        String presumptivePureGatewayAuthToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveGatewayAuthToken,
                        TestProperties.getGatewayAuthTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());

        Mockito.doNothing().when(gatewayAuthenticationServiceSpy).revokeForUser(Mockito.any(UserDTO.class));


        //TEST AND ASSERT
        RestAssured.given()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.BEARER.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureGatewayAuthToken))
                .delete(AUTHENTICATION_CONTROLLER_PATH.concat("/revoke"))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .body("message", is("Unable to perform authentication"))
                .body("reason", is("Given token either has never been issued by us or is revoked by user"));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testGatewayAuthenticationFilterReturnsUnauthorizedStatusCodeIfTokenHasExpired(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP
        UserEntity presumptiveUser = authenticationControllerTestPayload.getUserEntity();

        GatewayAuthToken presumptiveGatewayAuthToken = authenticationControllerTestPayload.getGatewayAuthToken();
        ReflectionTestUtils.setField(presumptiveGatewayAuthToken, "principal", presumptiveUser.getId());
        ReflectionTestUtils.setField(presumptiveGatewayAuthToken, "issuedAt", Instant.now().minusSeconds(2));
        ReflectionTestUtils.setField(presumptiveGatewayAuthToken, "expiresAt", Instant.now().minusSeconds(1));
        presumptiveGatewayAuthToken.getAdditionalClaims().put(PeachyMindJwtClaim.EXPIRATION_UNIX, presumptiveGatewayAuthToken.getExpiresAtUnix());
        presumptiveGatewayAuthToken.getAdditionalClaims().put(PeachyMindJwtClaim.ISSUED_AT_UNIX, presumptiveGatewayAuthToken.getIssuedAtUnix());
        presumptiveGatewayAuthToken.getAdditionalClaims().put(PeachyMindJwtClaim.SUBJECT, presumptiveUser.getId());


        String presumptivePureGatewayAuthToken =
                JwtTokenTestUtils.generatePureTestTokenBasedOn(
                        presumptiveGatewayAuthToken,
                        TestProperties.getGatewayAuthTokenSignature(),
                        TestProperties.getSignatureAlgorithmName());


        Assumptions.assumeTrue(presumptiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).isPresent());
        String presumptiveTokenId = presumptiveGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get();

        TokenIdentificationEntity tokenIdentification = new TokenIdentificationEntity();
        tokenIdentification.setTokenId(presumptiveTokenId);
        tokenIdentification.setOwner(presumptiveUser);
        saveUser(authenticationControllerTestPayload.getUserEntity(), jdbcTemplate);
        TokenIdentificationServiceTestUtils.saveTokenIdentification(tokenIdentification, jdbcTemplate);
        Assumptions.assumeTrue(TokenIdentificationServiceTestUtils.existByTokenId(presumptiveTokenId, jdbcTemplate));

        Mockito.doNothing().when(gatewayAuthenticationServiceSpy).revokeForUser(Mockito.any(UserDTO.class));


        //TEST AND ASSERT
        RestAssured.given()
                .header(PeachyMindHttpHeaders.AUTHORIZATION, TokenPrefix.BEARER.getStringyPrefix().concat(StringUtils.SPACE).concat(presumptivePureGatewayAuthToken))
                .delete(AUTHENTICATION_CONTROLLER_PATH.concat("/revoke"))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .body("message", is("Unable to perform authentication"))
                .body("reason", is("Given token has expired"));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationControllerTestArgumentProvider.class)
    public void testGatewayAuthenticationFilterDoesntAuthenticateUserIfThereIsNoTokenGiven(AuthenticationControllerTestPayload authenticationControllerTestPayload) {
        //SETUP


        //TEST AND ASSERT
        RestAssured
                .delete(AUTHENTICATION_CONTROLLER_PATH.concat("/revoke"))
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .body("message", is("Unable to revoke auth token"))
                .body("reason", is("You aren't authorized for such action"));
    }
}
