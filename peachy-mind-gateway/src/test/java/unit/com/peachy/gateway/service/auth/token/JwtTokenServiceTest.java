package unit.com.peachy.gateway.service.auth.token;

import com.peachy.security.dto.token.JwtToken;
import com.peachy.security.service.auth.token.JwtTokenService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.internal.util.MockUtil;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import testdata.TestProperties;
import unit.com.peachy.gateway.exception.IsNotAMockException;
import unit.com.peachy.gateway.exception.IsNotASpyException;

import java.util.Objects;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public abstract class JwtTokenServiceTest<T extends JwtTokenService<? extends JwtToken>> {

    private T currentMockedServiceUnderTest;

    private T currentSpiedServiceUnderTest;

    /**
     * Returns new mock everytime
     */
    private T getMockedServiceObjectUnderTest() {
        T jwtTokenService = getMockedTokenServiceUnderTestFlow();

        if (!MockUtil.isMock(jwtTokenService)) {
            throw new IsNotAMockException(String.format("Service under test object of %s is not a mock though must be", this.getClass().getSimpleName()));
        }

        return jwtTokenService;
    }

    /**
     * Returns new spy everytime
     */
    private T getSpiedServiceObjectUnderTest() {
        T jwtTokenService = getSpiedTokenServiceUnderTestFlow();

        if (!MockUtil.isSpy(jwtTokenService)) {
            throw new IsNotASpyException(String.format("Service under test object of %s is not a spy  though must be", this.getClass().getSimpleName()));
        }

        return jwtTokenService;
    }

    @BeforeEach
    public void beforeEach() {
        Mockito.when(getMockedCurrentServiceUnderTest().getIssuer()).then(answer -> TestProperties.getIssuer());
        Mockito.when(getMockedCurrentServiceUnderTest().getLifetime()).then(answer -> TestProperties.getGatewayTokenLifetime());
        Mockito.when(getMockedCurrentServiceUnderTest().getSignatureAlgorithmName()).then(answer -> TestProperties.getSignatureAlgorithmName());
        Mockito.when(getMockedCurrentServiceUnderTest().getSignature()).then(answer -> TestProperties.getGatewayAuthTokenSignature());

        Mockito.when(getSpiedCurrentServiceUnderTest().getIssuer()).then(answer -> TestProperties.getIssuer());
        Mockito.when(getSpiedCurrentServiceUnderTest().getLifetime()).then(answer -> TestProperties.getGatewayTokenLifetime());
        Mockito.when(getSpiedCurrentServiceUnderTest().getSignatureAlgorithmName()).then(answer -> TestProperties.getSignatureAlgorithmName());
        Mockito.when(getSpiedCurrentServiceUnderTest().getSignature()).then(answer -> TestProperties.getGatewayAuthTokenSignature());
    }

    @AfterEach
    public void afterEach() {
        refreshMeasures();
    }

    private void refreshMeasures() {
        currentMockedServiceUnderTest = null;
    }


    /**
     * Returns new mock per test running
     */
    protected T getMockedCurrentServiceUnderTest() {
        if (Objects.isNull(currentMockedServiceUnderTest)) {
            currentMockedServiceUnderTest = getMockedServiceObjectUnderTest();
        }

        return currentMockedServiceUnderTest;
    }

    /**
     * Returns new spy per test running
     */
    protected T getSpiedCurrentServiceUnderTest() {
        if (Objects.isNull(currentSpiedServiceUnderTest)) {
            currentSpiedServiceUnderTest = getSpiedServiceObjectUnderTest();
        }

        return currentSpiedServiceUnderTest;
    }

    /**
     * Should return new spy everytime
     */
    protected abstract T getSpiedTokenServiceUnderTestFlow();

    /**
     * Should return new mock everytime
     */
    protected abstract T getMockedTokenServiceUnderTestFlow();
}
