package unit.com.peachy.gateway.service.auth;

import argumentprovider.service.auth.GatewayAuthenticationServiceTestArgumentProvider;
import argumentprovider.service.auth.GatewayAuthenticationServiceTestPayload;
import com.peachy.commons.exception.PeachyMindException;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.commons.exception.badrequest.BadRequestException;
import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.dto.UserCredentialsDTO;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.gateway.event.auth.GatewayAuthenticationSuccessEvent;
import com.peachy.gateway.service.auth.GatewayAuthenticationService;
import com.peachy.gateway.service.auth.token.GatewayAuthTokenService;
import com.peachy.gateway.service.auth.token.RefreshTokenService;
import com.peachy.gateway.service.auth.token.identification.TokenIdentificationService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.exception.UnauthorizedException;
import com.peachy.security.service.auth.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.misusing.UnnecessaryStubbingException;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import testdata.TestProperties;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

/**
 * {@link Strictness.LENIENT} was selected to ignore {@link UnnecessaryStubbingException}
 * */
@MockitoSettings(strictness = Strictness.LENIENT)
@ExtendWith(MockitoExtension.class)
public class GatewayAuthenticationServiceTest {
    @Mock
    private GatewayAuthTokenService gatewayAuthTokenService;

    @Mock
    private RefreshTokenService refreshTokenService;

    @Mock
    private TokenIdentificationService tokenIdentificationService;

    @Mock
    private UserService userService;

    @Mock
    private DaoAuthenticationProvider daoAuthenticationProvider;

    private GatewayAuthenticationService gatewayAuthenticationServiceSpy;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;


    @BeforeEach
    public void beforeEach() {
        gatewayAuthenticationServiceSpy = Mockito.spy(new GatewayAuthenticationService(
                gatewayAuthTokenService,
                refreshTokenService,
                tokenIdentificationService,
                userService,
                applicationEventPublisher));
        gatewayAuthenticationServiceSpy.setDaoAuthenticationProvider(daoAuthenticationProvider);
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testSignIn(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserCredentialsDTO userCredentials = new UserCredentialsDTO(TestProperties.getUsername(), TestProperties.getPassword());
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(gatewayAuthenticationServiceTestPayload.getUserDTO(), userCredentials.getPassword());
        Mockito.when(daoAuthenticationProvider.authenticate(Mockito.any(UsernamePasswordAuthenticationToken.class)))
                .then(answer -> usernamePasswordAuthenticationToken);
        Mockito.when(gatewayAuthTokenService.generateToken(Mockito.any(UserDTO.class)))
                .then(answer -> gatewayAuthenticationServiceTestPayload.getGatewayAuthToken());

        //TEST
        GatewayAuthToken gatewayAuthToken = gatewayAuthenticationServiceSpy.signIn(userCredentials);

        //VERIFY
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce()).save(Mockito.any(TokenIdentificationDTO.class));
        Mockito.verify(daoAuthenticationProvider, Mockito.atMostOnce()).authenticate(usernamePasswordAuthenticationToken);
        Mockito.verify(gatewayAuthTokenService, Mockito.atMostOnce()).generateToken(gatewayAuthenticationServiceTestPayload.getUserDTO());
        Mockito.verify(applicationEventPublisher, Mockito.atMostOnce()).publishEvent(Mockito.any(GatewayAuthenticationSuccessEvent.class));

        //ASSERTIONS
        gatewayAuthTokenAssertions(gatewayAuthToken);
    }

    private void gatewayAuthTokenAssertions(GatewayAuthToken issuerGatewayAuthToken) {
        Assertions.assertNotNull(issuerGatewayAuthToken.getPrefixedRefreshToken(),
                "Refresh token should not be null");
        Assertions.assertTrue(StringUtils.isNotBlank(issuerGatewayAuthToken.getPrefixedRefreshToken()),
                "Refresh token should not be empty");
        Assertions.assertNotNull(issuerGatewayAuthToken.getAuthority(),
                "Gateway token authority should not be null");
        Assertions.assertNotNull(issuerGatewayAuthToken.getIssuer(),
                "Gateway token issuer should not be null");
        Assertions.assertTrue(StringUtils.isNotBlank(issuerGatewayAuthToken.getIssuer()),
                "Gateway token issuer should not be empty");
        Assertions.assertNotNull(issuerGatewayAuthToken.getIssuedAt(),
                "Gateway token issued at property should not be null");
        Assertions.assertNotNull(issuerGatewayAuthToken.getExpiresAt(),
                "Gateway token expires at property should not be null");
        Assertions.assertNotNull(issuerGatewayAuthToken.getIssuedAtUnix(),
                "Gateway token issued at unix property should not be null");
        Assertions.assertNotNull(issuerGatewayAuthToken.getExpiresAtUnix(),
                "Gateway token expires at unix property should not be null");
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testSignUp(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        Mockito.when(userService.save(Mockito.any(UserDTO.class))).then(answer -> user);

        //TEST
        UserDTO signedUpUser = gatewayAuthenticationServiceSpy.signUp(user);

        //VERIFICATIONS
        Mockito.verify(userService, Mockito.atMostOnce()).save(user);

        //ASSERTIONS
        Assertions.assertNotNull(signedUpUser.getId(),
                "Signed up user id should not be null");
        Assertions.assertNotNull(signedUpUser.getAuthority(),
                "Signed up user authority should not be null");
        Assertions.assertNotNull(signedUpUser.getPlainPassword(),
                "Signed up user hashed password should not be null");
        Assertions.assertNotNull(signedUpUser.getPlainPassword(),
                "Signed up user plain password should not be null");
        Assertions.assertNotNull(signedUpUser.getUsername(),
                "Signed up user username should not be null");
    }


    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRevokeForUser(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        Mockito.doNothing().when(tokenIdentificationService).deleteByTokenOwnerId(Mockito.any(String.class));

        //TEST
        Assertions.assertDoesNotThrow(() -> gatewayAuthenticationServiceSpy.revokeForUser(user));

        //VERIFY
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce()).deleteByTokenOwnerId(user.getId());
    }

    @Test
    public void testRevokeForUserThrowsBadArgumentExceptionIfUserIsNull() {
        //TEST
        Assertions.assertThrows(
                BadArgumentException.class,
                () -> gatewayAuthenticationServiceSpy.revokeForUser(null),
                "Revoke operation should inform user with exception about null-passed argument");
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRevokeForUserThrowsBadArgumentExceptionIfUserIdIsNull(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        user.setId(null);

        //TEST
        Assertions.assertThrows(
                BadArgumentException.class,
                () -> gatewayAuthenticationServiceSpy.revokeForUser(user),
                "Revoke operation should inform user with exception about absent user id property");
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRevokeForUserThrowsBadArgumentExceptionIfUserIdIsEmpty(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO user = gatewayAuthenticationServiceTestPayload.getUserDTO();
        user.setId(StringUtils.EMPTY);

        //TEST
        Assertions.assertThrows(
                BadArgumentException.class,
                () -> gatewayAuthenticationServiceSpy.revokeForUser(user),
                "Revoke operation should inform user with exception about empty user id property");
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefresh(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        RefreshToken refreshToken = new RefreshToken(
                gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken(),
                gatewayAuthenticationServiceTestPayload.getRefreshToken().getAuthority(),
                Instant.now(),
                Instant.now().plus(Duration.ofHours(2)),
                gatewayAuthenticationServiceTestPayload.getRefreshToken().getPrincipal(),
                gatewayAuthenticationServiceTestPayload.getRefreshToken().getAdditionalClaims());
        Mockito.when(refreshTokenService.parseToken(Mockito.anyString()))
                .then(answer -> refreshToken);
        Mockito.when(tokenIdentificationService.findByTokenId(Mockito.anyString()))
                .then(answer -> Optional.of(new TokenIdentificationDTO(gatewayAuthenticationServiceTestPayload.getUserDTO(), TestProperties.getTokenId())));
        Mockito.when(gatewayAuthTokenService.generateToken(Mockito.any(UserDTO.class)))
                .then(answer -> gatewayAuthenticationServiceTestPayload.getGatewayAuthToken());

        //TEST
        GatewayAuthToken gatewayAuthToken = gatewayAuthenticationServiceSpy.refresh(refreshToken.getPureToken());

        //VERIFICATIONS
        Mockito.verify(refreshTokenService, Mockito.atMostOnce()).parseToken(refreshToken.getPureToken());
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce()).findByTokenId(Mockito.anyString());
        Mockito.verify(gatewayAuthTokenService, Mockito.atMostOnce()).generateToken(gatewayAuthenticationServiceTestPayload.getUserDTO());
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce()).save(Mockito.any(TokenIdentificationDTO.class));

        //ASSERTIONS
        gatewayAuthTokenAssertions(gatewayAuthToken);
    }

    @Test
    public void testRefreshThrowsBadArgumentExceptionWhenRefreshTokenIsNull() {
        //TEST
        Assertions.assertThrows(
                BadArgumentException.class,
                () -> gatewayAuthenticationServiceSpy.refresh(null),
                "Refresh token operation should inform user about null-passed refresh token");
    }

    @Test
    public void testRefreshThrowsBadArgumentExceptionWhenRefreshTokenIsEmpty() {
        //TEST
        Assertions.assertThrows(
                BadArgumentException.class,
                () -> gatewayAuthenticationServiceSpy.refresh(StringUtils.EMPTY),
                "Refresh token operation should inform user about an attempt to refresh gateway token with an empty refresh token");
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsPeachyMindExceptionWhenRefreshTokenIsOfWrongType(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        RefreshToken refreshToken = gatewayAuthenticationServiceTestPayload.getRefreshToken();
        refreshToken.getAdditionalClaims().put(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.NONE);
        Mockito.when(refreshTokenService.parseToken(Mockito.anyString()))
                .then(answer -> refreshToken);

        //TEST
        Assertions.assertThrows(
                PeachyMindException.class,
                () -> gatewayAuthenticationServiceSpy.refresh(gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken()),
                String.format("Refresh token operation should inform user about an attempt to refresh gateway token with refresh token that has token type other than %s", TokenType.REFRESH));

        //VERIFY
        Mockito.verify(refreshTokenService, Mockito.atMostOnce()).parseToken(refreshToken.getPureToken());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsPeachyMindExceptionWhenRefreshTokenIdClaimIsMissing(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        RefreshToken refreshToken = gatewayAuthenticationServiceTestPayload.getRefreshToken();
        refreshToken.getAdditionalClaims().remove(PeachyMindJwtClaim.TOKEN_ID);
        Mockito.when(refreshTokenService.parseToken(Mockito.anyString()))
                .then(answer -> refreshToken);

        //TEST
        Assertions.assertThrows(
                BadRequestException.class,
                () -> gatewayAuthenticationServiceSpy.refresh(gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken()),
                "Refresh token operation should inform user about an attempt to refresh gateway token with refresh token that has not token id claim");

        //VERIFY
        Mockito.verify(refreshTokenService, Mockito.atMostOnce()).parseToken(refreshToken.getPureToken());
    }


    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsPeachyMindExceptionIfRefreshTokenIsUnknownToTheSystem(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        Mockito.when(refreshTokenService.parseToken(Mockito.anyString()))
                .then(answer -> gatewayAuthenticationServiceTestPayload.getRefreshToken());
        Mockito.when(tokenIdentificationService.findByTokenId(Mockito.anyString()))
                .then(answer -> Optional.empty());

        //TEST
        Assertions.assertThrows(
                UnauthorizedException.class,
                () -> gatewayAuthenticationServiceSpy.refresh(gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken()),
                "Refresh token operation should inform user about an attempt to refresh gateway token with refresh token that system failed to identify");

        //VERIFY
        Mockito.verify(refreshTokenService, Mockito.atMostOnce()).parseToken(gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken());
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce())
                .findByTokenId(gatewayAuthenticationServiceTestPayload.getRefreshToken().getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsPeachyMindExceptionIfRefreshTokenPrincipalAndTokenOwnerIsNotTheSame(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        UserDTO farFetchedUser = new UserDTO();
        farFetchedUser.setId(RandomStringUtils.random(8, true, true));
        farFetchedUser.setUsername("randomName");

        RefreshToken refreshToken = gatewayAuthenticationServiceTestPayload.getRefreshToken();

        Mockito.when(refreshTokenService.parseToken(Mockito.anyString()))
                .then(answer -> gatewayAuthenticationServiceTestPayload.getRefreshToken());
        Mockito.when(tokenIdentificationService.findByTokenId(Mockito.anyString()))
                .then(answer -> Optional.of(new TokenIdentificationDTO(farFetchedUser, refreshToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get())));

        //TEST
        Assertions.assertThrows(
                PeachyMindException.class,
                () -> gatewayAuthenticationServiceSpy.refresh(gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken()),
                "Refresh token operation should inform user about an attempt to refresh gateway token with refresh token that has different principal and owner." +
                        "Meaning the refresh token could be forged");

        //VERIFY
        Mockito.verify(refreshTokenService, Mockito.atMostOnce()).parseToken(gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken());
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce())
                .findByTokenId(gatewayAuthenticationServiceTestPayload.getRefreshToken().getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testRefreshThrowsPeachyMindExceptionIfRefreshTokenHasExpired(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        RefreshToken refreshToken = new RefreshToken(
                gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken(),
                gatewayAuthenticationServiceTestPayload.getRefreshToken().getAuthority(),
                Instant.now().minusSeconds(2),
                Instant.now().minusSeconds(1),
                gatewayAuthenticationServiceTestPayload.getRefreshToken().getPrincipal(),
                gatewayAuthenticationServiceTestPayload.getRefreshToken().getAdditionalClaims());


        Mockito.when(refreshTokenService.parseToken(Mockito.anyString()))
                .then(answer -> refreshToken);
        Mockito.when(tokenIdentificationService.findByTokenId(Mockito.anyString()))
                .then(answer -> Optional.of(new TokenIdentificationDTO(gatewayAuthenticationServiceTestPayload.getUserDTO(), refreshToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get())));

        //TEST
        Assertions.assertThrows(
                PeachyMindException.class,
                () -> gatewayAuthenticationServiceSpy.refresh(gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken()),
                "Refresh token operation should inform user about an attempt to refresh gateway token with an expired refresh token");

        //VERIFY
        Mockito.verify(refreshTokenService, Mockito.atMostOnce()).parseToken(gatewayAuthenticationServiceTestPayload.getRefreshToken().getPureToken());
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce())
                .findByTokenId(gatewayAuthenticationServiceTestPayload.getRefreshToken().getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testIsRevoked(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        Mockito.when(tokenIdentificationService.existsByTokenId(Mockito.anyString()))
                .then(answer -> true);

        //TEST
        Assertions.assertFalse(gatewayAuthenticationServiceSpy.isRevoked(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken()));

        //VERIFICATIONS
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce())
                .existsByTokenId(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testIsNotRevoked(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        Mockito.when(tokenIdentificationService.existsByTokenId(Mockito.anyString()))
                .then(answer -> false);

        //TEST
        Assertions.assertTrue(gatewayAuthenticationServiceSpy.isRevoked(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken()));

        //VERIFICATIONS
        Mockito.verify(tokenIdentificationService, Mockito.atMostOnce())
                .existsByTokenId(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).get());
    }

    @ParameterizedTest
    @ArgumentsSource(GatewayAuthenticationServiceTestArgumentProvider.class)
    public void testParse(GatewayAuthenticationServiceTestPayload gatewayAuthenticationServiceTestPayload) {
        //SETUP
        Mockito.when(gatewayAuthTokenService.parseToken(Mockito.anyString()))
                .then(answer -> gatewayAuthenticationServiceTestPayload.getGatewayAuthToken());

        //TEST
        GatewayAuthToken gatewayAuthToken = gatewayAuthenticationServiceSpy.parse(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getPureToken());

        //VERIFY
        Mockito.verify(gatewayAuthTokenService, Mockito.atMostOnce())
                .parseToken(gatewayAuthenticationServiceTestPayload.getGatewayAuthToken().getPureToken());
        gatewayAuthTokenAssertions(gatewayAuthToken);
    }

}