package unit.com.peachy.gateway.service.auth.token.identification;

import argumentprovider.service.auth.token.identification.TokenIdentificationServiceTestArgumentProvider;
import argumentprovider.service.auth.token.identification.TokenIdentificationServiceTestPayload;
import com.peachy.gateway.converter.TokenIdentificationConverter;
import com.peachy.gateway.dto.TokenIdentificationDTO;
import com.peachy.gateway.entity.TokenIdentificationEntity;
import com.peachy.gateway.repository.TokenIdentificationRepository;
import com.peachy.gateway.service.auth.token.identification.DefaultTokenIdentificationService;
import com.peachy.gateway.service.auth.token.identification.TokenIdentificationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import testdata.TestProperties;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class DefaultTokenIdentificationServiceTest {
    @Mock
    private TokenIdentificationRepository tokenIdentificationRepository;

    @Mock
    private TokenIdentificationConverter tokenIdentificationConverter;

    private TokenIdentificationService tokenIdentificationServiceSpy;

    @BeforeEach
    public void beforeEach() {
        this.tokenIdentificationServiceSpy =
                Mockito.spy(new DefaultTokenIdentificationService(tokenIdentificationRepository, tokenIdentificationConverter));
    }

    @ParameterizedTest
    @ArgumentsSource(TokenIdentificationServiceTestArgumentProvider.class)
    public void testSave(TokenIdentificationServiceTestPayload tokenIdentificationServiceTestPayload) {
        //SETUP
        Mockito.when(tokenIdentificationConverter.toEntity(Mockito.any(TokenIdentificationDTO.class)))
                .thenReturn(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity());
        Mockito.when(tokenIdentificationRepository.save(Mockito.any(TokenIdentificationEntity.class)))
                .thenReturn(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity());
        Mockito.when(tokenIdentificationConverter.toDto(Mockito.any(TokenIdentificationEntity.class)))
                .thenReturn(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO());

        //TEST
        TokenIdentificationDTO tokenIdentificationDTO =
                tokenIdentificationServiceSpy.save(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO());

        //VERIFY
        Mockito.verify(tokenIdentificationConverter, Mockito.atMostOnce())
                .toEntity(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO());
        Mockito.verify(tokenIdentificationConverter, Mockito.atMostOnce())
                .toDto(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity());
        Mockito.verify(tokenIdentificationRepository, Mockito.atMostOnce())
                .save(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity());
    }

    @Test
    public void testDeleteByTokenOwnerId() {
        //SETUP
        Mockito.doNothing().when(tokenIdentificationRepository).deleteByOwnerId(Mockito.anyString());
        String tokenOwnerId = TestProperties.getUserId();

        //TEST
        tokenIdentificationServiceSpy.deleteByTokenOwnerId(tokenOwnerId);

        //VERIFY
        Mockito.verify(tokenIdentificationRepository, Mockito.atMostOnce())
                .deleteByOwnerId(tokenOwnerId);
    }

    @ParameterizedTest
    @ArgumentsSource(TokenIdentificationServiceTestArgumentProvider.class)
    public void testFindByTokenId(TokenIdentificationServiceTestPayload tokenIdentificationServiceTestPayload) {
        //SETUP
        Mockito.when(tokenIdentificationRepository.findByTokenId(Mockito.anyString()))
                .thenReturn(Optional.of(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity()));
        Mockito.when(tokenIdentificationConverter.toDto(Mockito.any(Optional.class)))
                .thenReturn(Optional.of(tokenIdentificationServiceTestPayload.getTokenIdentificationDTO()));
        String tokenId = TestProperties.getTokenId();

        //TEST
        Optional<TokenIdentificationDTO> actualTokenIdentificationDTO = tokenIdentificationServiceSpy.findByTokenId(tokenId);

        //VERIFY
        Mockito.verify(tokenIdentificationRepository, Mockito.atMostOnce())
                .findByTokenId(tokenId);
        Mockito.verify(tokenIdentificationConverter, Mockito.atMostOnce())
                .toDto(Optional.of(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity()));

        //ASSERTIONS
        Assertions.assertTrue(actualTokenIdentificationDTO.isPresent());
        Assertions.assertEquals(actualTokenIdentificationDTO.get(), tokenIdentificationServiceTestPayload.getTokenIdentificationDTO());
    }

    @ParameterizedTest
    @ArgumentsSource(TokenIdentificationServiceTestArgumentProvider.class)
    public void testFindByTokenIdReturnsEmptyOptionalIfThereIsNotTokenIdentificationForGivenTokenId(TokenIdentificationServiceTestPayload tokenIdentificationServiceTestPayload) {
        //SETUP
        Mockito.when(tokenIdentificationRepository.findByTokenId(Mockito.anyString()))
                .thenReturn(Optional.empty());
        Mockito.when(tokenIdentificationConverter.toDto(Optional.empty()))
                .thenReturn(Optional.empty());
        String tokenId = TestProperties.getTokenId();

        //TEST
        Optional<TokenIdentificationDTO> actualTokenIdentificationDTO = tokenIdentificationServiceSpy.findByTokenId(tokenId);

        //VERIFY
        Mockito.verify(tokenIdentificationRepository, Mockito.atMostOnce())
                .findByTokenId(tokenId);
        Mockito.verify(tokenIdentificationConverter, Mockito.atMostOnce())
                .toDto(Optional.of(tokenIdentificationServiceTestPayload.getTokenIdentificationEntity()));

        //ASSERT
        Assertions.assertFalse(actualTokenIdentificationDTO.isPresent());
    }
}