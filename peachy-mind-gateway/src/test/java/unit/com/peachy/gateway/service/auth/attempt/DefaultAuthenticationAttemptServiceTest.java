package unit.com.peachy.gateway.service.auth.attempt;

import argumentprovider.service.auth.attempt.AuthenticationAttemptServiceTestArgumentProvider;
import argumentprovider.service.auth.attempt.AuthenticationAttemptServiceTestPayload;
import com.peachy.commons.exception.notfound.NotFoundException;
import com.peachy.gateway.converter.AuthenticationAttemptConverter;
import com.peachy.gateway.dto.AuthenticationAttemptDTO;
import com.peachy.gateway.entity.AuthenticationAttemptEntity;
import com.peachy.gateway.repository.AuthenticationAttemptRepository;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import com.peachy.gateway.service.auth.attempt.DefaultAuthenticationAttemptService;
import com.peachy.gateway.service.auth.attempt.reset.strategy.AuthAttemptsResetProvider;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.service.auth.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import testdata.TestProperties;

import java.util.Optional;

@MockitoSettings(strictness = Strictness.LENIENT)
@ExtendWith(MockitoExtension.class)
public class DefaultAuthenticationAttemptServiceTest {
    @Mock
    private AuthenticationAttemptRepository authenticationAttemptRepository;

    private AuthenticationAttemptService defaultAuthenticationAttemptService;

    @Mock
    private UserService userService;

    @Mock
    private AuthenticationAttemptConverter authenticationAttemptConverter;

    @Captor
    private ArgumentCaptor<String> usernameArgumentCaptor;

    @Captor
    private ArgumentCaptor<AuthenticationAttemptDTO> authAttemptArgumentCaptor;

    @Mock
    private AuthAttemptsResetProvider authAttemptsResetProvider;

    @BeforeEach
    public void beforeEach() {
        defaultAuthenticationAttemptService = Mockito.spy(new DefaultAuthenticationAttemptService(
                authenticationAttemptRepository,
                authenticationAttemptConverter,
                userService,
                authAttemptsResetProvider));
    }

    @Test
    public void testAttemptsCountForUsername() {
        //SETUP
        int presumptiveAttemptsCount = 3;
        String presumptiveUsername = TestProperties.getUsername();
        Mockito.when(authenticationAttemptRepository.attemptsCountForUsername(Mockito.anyString()))
                .thenReturn(Optional.of(presumptiveAttemptsCount));

        //TEST
        int actualAttemptsCount = defaultAuthenticationAttemptService.attemptsCountForUsername(presumptiveUsername);

        //VERIFY
        Mockito.verify(authenticationAttemptRepository, Mockito.atMostOnce())
                .attemptsCountForUsername(Mockito.anyString());
        Mockito.verify(authenticationAttemptRepository)
                .attemptsCountForUsername(usernameArgumentCaptor.capture());

        //ASSERT
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getValue(),
                String.format("Wrong username was used to retrieve authentication attempt count. Should be: %s But was: %s", presumptiveUsername, usernameArgumentCaptor.getValue()));
        Assertions.assertEquals(presumptiveAttemptsCount, actualAttemptsCount,
                String.format("Unexpected authentication attempt count returned for username %s. Should be: %s But was: %s",
                        presumptiveUsername,
                        presumptiveAttemptsCount,
                        actualAttemptsCount));
    }

    @Test
    public void testAttemptsCountForUsernameReturnsZeroIfUserHasNoAttemptsCount() {
        //SETUP
        String presumptiveUsername = TestProperties.getUsername();
        Mockito.when(authenticationAttemptRepository.attemptsCountForUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());

        //TEST
        int actualAttemptsCount = defaultAuthenticationAttemptService.attemptsCountForUsername(presumptiveUsername);

        //VERIFY
        Mockito.verify(authenticationAttemptRepository, Mockito.atMostOnce())
                .attemptsCountForUsername(Mockito.anyString());
        Mockito.verify(authenticationAttemptRepository)
                .attemptsCountForUsername(usernameArgumentCaptor.capture());

        //ASSERT
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getValue(),
                String.format("Wrong username was used to retrieve authentication attempt count. Should be: %s But was: %s", presumptiveUsername, usernameArgumentCaptor.getValue()));
        Assertions.assertEquals(0, actualAttemptsCount,
                String.format("Unexpected authentication attempt count. Should be: %s But was: %s", 0, actualAttemptsCount));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testAuthAttemptForUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        String presumptiveUsername = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity().getUser().getUsername();
        Mockito.when(authenticationAttemptRepository.authAttemptForUsername(Mockito.anyString()))
                .thenReturn(Optional.of(authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity()));
        Mockito.when(authenticationAttemptConverter.toDto(Mockito.any(Optional.class)))
                .thenReturn(Optional.of(authenticationAttemptServiceTestPayload.getAuthenticationAttemptDTO()));

        //TEST
        Optional<AuthenticationAttemptDTO> actualAuthenticationAttempt = defaultAuthenticationAttemptService.authAttemptForUsername(presumptiveUsername);

        //VERIFY
        Mockito.verify(authenticationAttemptRepository, Mockito.atMostOnce())
                .authAttemptForUsername(Mockito.anyString());
        Mockito.verify(authenticationAttemptRepository)
                .authAttemptForUsername(usernameArgumentCaptor.capture());
        Mockito.verify(authenticationAttemptConverter, Mockito.atMostOnce())
                .toDto(Mockito.any(Optional.class));

        //ASSERT
        Assertions.assertTrue(actualAuthenticationAttempt.isPresent(),
                "Optional should not be empty if there is an authentication attempt for user");
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getValue(),
                String.format("Wrong username was used to retrieve authentication attempt. Should be: %s But was: %s", presumptiveUsername, usernameArgumentCaptor.getValue()));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testAuthAttemptForUsernameReturnsEmptyOptionalIfThereIsNoAttemptsForGivenUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        String presumptiveUsername = authenticationAttemptServiceTestPayload.getAuthenticationAttemptEntity().getUser().getUsername();
        Mockito.when(authenticationAttemptRepository.authAttemptForUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());
        Mockito.when(authenticationAttemptConverter.toDto(Mockito.any(Optional.class)))
                .thenCallRealMethod();

        //TEST
        Optional<AuthenticationAttemptDTO> actualAuthenticationAttempt = defaultAuthenticationAttemptService.authAttemptForUsername(presumptiveUsername);

        //VERIFY
        Mockito.verify(authenticationAttemptRepository, Mockito.atMostOnce())
                .authAttemptForUsername(Mockito.anyString());
        Mockito.verify(authenticationAttemptRepository)
                .authAttemptForUsername(usernameArgumentCaptor.capture());
        Mockito.verify(authenticationAttemptConverter, Mockito.atMostOnce())
                .toDto(Mockito.any(Optional.class));

        //ASSERT
        Assertions.assertFalse(actualAuthenticationAttempt.isPresent(),
                "Optional should be empty if there is no authentication attempt for user");
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getValue(),
                String.format("Wrong username was used to retrieve authentication attempt. Should be: %s But was: %s", presumptiveUsername, usernameArgumentCaptor.getValue()));
    }

    @Test
    public void testForceResetAuthenticationAttemptsForUsername() {
        //SETUP
        String presumptiveUsername = TestProperties.getUsername();

        //TEST
        defaultAuthenticationAttemptService.forceResetAuthenticationAttemptsForUsername(presumptiveUsername);

        //VERIFY
        Mockito.verify(authenticationAttemptRepository, Mockito.atMostOnce())
                .deleteForUsername(Mockito.anyString());
        Mockito.verify(authenticationAttemptRepository)
                .deleteForUsername(usernameArgumentCaptor.capture());

        //ASSERT
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getValue(),
                String.format("Wrong username was used to reset authentication attempt. Should be: %s But was: %s", presumptiveUsername, usernameArgumentCaptor.getValue()));
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testStrategicalResetAuthenticationAttemptsForUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        UserDTO presumptiveUser = authenticationAttemptServiceTestPayload.getUserDTO();
        AuthenticationAttemptDTO presumptiveAuthAttempt = authenticationAttemptServiceTestPayload.getAuthenticationAttemptDTO();

        Mockito.when(defaultAuthenticationAttemptService.authAttemptForUsername(presumptiveUser.getUsername()))
                .thenReturn(Optional.of(presumptiveAuthAttempt));

        //TEST
        defaultAuthenticationAttemptService.strategicalResetAuthenticationAttemptsForUsername(presumptiveUser.getUsername());

        //VERIFY
        Mockito.verify(defaultAuthenticationAttemptService, Mockito.times(2))
                .authAttemptForUsername(Mockito.anyString());
        Mockito.verify(defaultAuthenticationAttemptService, Mockito.times(2))
                .authAttemptForUsername(usernameArgumentCaptor.capture());
        Mockito.verify(authAttemptsResetProvider, Mockito.atMostOnce())
                .resetAttempts(Mockito.any(AuthenticationAttemptDTO.class));
        Mockito.verify(authAttemptsResetProvider)
                .resetAttempts(authAttemptArgumentCaptor.capture());

        //ASSERT
        Assertions.assertEquals(presumptiveUser.getUsername(), usernameArgumentCaptor.getValue());
        Assertions.assertEquals(presumptiveAuthAttempt.getUserId(), authAttemptArgumentCaptor.getValue().getUserId());
        Assertions.assertEquals(presumptiveAuthAttempt.getAttemptsCount(), authAttemptArgumentCaptor.getValue().getAttemptsCount());
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testStrategicalResetAuthenticationAttemptsForUsernameDoNoResetIfThereWereNoAttempts(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        UserDTO presumptiveUser = authenticationAttemptServiceTestPayload.getUserDTO();

        Mockito.when(defaultAuthenticationAttemptService.authAttemptForUsername(presumptiveUser.getUsername()))
                .thenReturn(Optional.empty());

        //TEST
        defaultAuthenticationAttemptService.strategicalResetAuthenticationAttemptsForUsername(presumptiveUser.getUsername());

        //VERIFY
        Mockito.verify(defaultAuthenticationAttemptService, Mockito.times(2))
                .authAttemptForUsername(Mockito.anyString());
        Mockito.verify(defaultAuthenticationAttemptService, Mockito.times(2))
                .authAttemptForUsername(usernameArgumentCaptor.capture());
        Mockito.verifyNoInteractions(authAttemptsResetProvider);

        //ASSERT
        Assertions.assertEquals(presumptiveUser.getUsername(), usernameArgumentCaptor.getValue());
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testIncreaseAuthenticationAttempts(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        String presumptiveUsername = TestProperties.getUsername();
        AuthenticationAttemptDTO presumptiveAuthAttempt =
                authenticationAttemptServiceTestPayload.getAuthenticationAttemptDTO();

        Mockito.when(userService.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(authenticationAttemptServiceTestPayload.getUserDTO()));
        Mockito.when(authenticationAttemptRepository.attemptsCountForUsername(Mockito.anyString()))
                .thenReturn(Optional.of(presumptiveAuthAttempt.getAttemptsCount()));

        //TEST
        defaultAuthenticationAttemptService.increaseAuthenticationAttempts(presumptiveUsername);

        //VERIFY
        Mockito.verify(userService, Mockito.atMostOnce())
                .findByUsername(Mockito.anyString());
        Mockito.verify(userService)
                .findByUsername(usernameArgumentCaptor.capture());

        Mockito.verify(defaultAuthenticationAttemptService, Mockito.atMostOnce())
                .attemptsCountForUsername(Mockito.anyString());
        Mockito.verify(defaultAuthenticationAttemptService)
                .attemptsCountForUsername(usernameArgumentCaptor.capture());

        Mockito.verify(authenticationAttemptConverter, Mockito.atMostOnce())
                .toEntity(Mockito.any(AuthenticationAttemptDTO.class));
        Mockito.verify(authenticationAttemptConverter)
                .toEntity(authAttemptArgumentCaptor.capture());

        Mockito.verify(authenticationAttemptRepository, Mockito.atMostOnce())
                .save(Mockito.any(AuthenticationAttemptEntity.class));

        //ASSERT
        Assertions.assertEquals(presumptiveAuthAttempt.getAttemptsCount() + 1, usernameArgumentCaptor.getAllValues().size());
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getAllValues().get(0),
                String.format("Wrong username was used to retrieve user. Should be: %s But was: %s", presumptiveUsername, usernameArgumentCaptor.getAllValues().get(0)));
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getAllValues().get(1),
                String.format("Wrong username was used to retrieve authentication attempts count. Should be: %s But was: %s", presumptiveUsername, usernameArgumentCaptor.getAllValues().get(1)));
        Assertions.assertEquals(presumptiveAuthAttempt.getAttemptsCount() + 1, authAttemptArgumentCaptor.getValue().getAttemptsCount());
        Assertions.assertEquals(presumptiveAuthAttempt.getUserId(), authAttemptArgumentCaptor.getValue().getUserId());
    }

    @ParameterizedTest
    @ArgumentsSource(AuthenticationAttemptServiceTestArgumentProvider.class)
    public void testIncreaseAuthenticationAttemptsThrowsNotFoundExceptionIfThereIsNoUserForGivenUsername(AuthenticationAttemptServiceTestPayload authenticationAttemptServiceTestPayload) {
        //SETUP
        String presumptiveUsername = TestProperties.getUsername();
        Mockito.when(userService.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());

        //TEST AND ASSERT
        Assertions.assertThrows(NotFoundException.class, () -> defaultAuthenticationAttemptService.increaseAuthenticationAttempts(presumptiveUsername),
                String.format("Increase authentication attempts count operation should inform called about its inability to increase authentication attempts for non existent user " +
                        "with %s", NotFoundException.class.getSimpleName()));
    }
}
