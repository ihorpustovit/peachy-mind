package unit.com.peachy.gateway.exception;

import com.peachy.commons.exception.badrequest.BadArgumentException;

public class IsNotASpyException extends BadArgumentException {
    public IsNotASpyException(String message) {
        super(message);
    }
}
