package unit.com.peachy.gateway.service.auth.bruteforceprotection;

import argumentprovider.service.auth.bruteforceprotection.BruteForceProtectionServiceTestArgumentProvider;
import argumentprovider.service.auth.bruteforceprotection.BruteForceProtectionServiceTestPayload;
import com.peachy.gateway.service.auth.attempt.AuthenticationAttemptService;
import com.peachy.gateway.service.auth.bruteforceprotection.BruteForceProtectionService;
import com.peachy.gateway.service.auth.bruteforceprotection.DefaultBruteForceProtectionService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.test.util.ReflectionTestUtils;
import testdata.TestProperties;

import java.util.concurrent.ExecutorService;

@MockitoSettings(strictness = Strictness.LENIENT)
@ExtendWith(MockitoExtension.class)
public class DefaultBruteForceProtectionServiceTest {
    @Mock
    private AuthenticationAttemptService defaultAuthenticationAttemptService;

    private BruteForceProtectionService defaultBruteForceProtectionService;

    @Mock
    private ExecutorService executorService;

    @Captor
    private ArgumentCaptor<String> usernameArgumentCaptor;

    @BeforeEach
    public void beforeEach() {
        defaultBruteForceProtectionService = Mockito.spy(new DefaultBruteForceProtectionService(defaultAuthenticationAttemptService));
        ReflectionTestUtils.setField(defaultBruteForceProtectionService, "attemptsLimit", TestProperties.getAuthAttemptLimit());
        ReflectionTestUtils.setField(defaultBruteForceProtectionService, "executorService", executorService);
    }

    @ParameterizedTest
    @ArgumentsSource(BruteForceProtectionServiceTestArgumentProvider.class)
    public void testOnAuthenticationSuccess(BruteForceProtectionServiceTestPayload bruteForceProtectionServiceTestPayload) {
        //TEST
        defaultBruteForceProtectionService.onAuthenticationSuccess(bruteForceProtectionServiceTestPayload.getGatewayAuthenticationSuccessEvent());

        //VERIFY
        Mockito.verify(defaultAuthenticationAttemptService, Mockito.atMostOnce()).forceResetAuthenticationAttemptsForUsername(Mockito.anyString());
        Mockito.verify(defaultAuthenticationAttemptService).forceResetAuthenticationAttemptsForUsername(usernameArgumentCaptor.capture());

        //ASSERT
        Assertions.assertEquals(
                bruteForceProtectionServiceTestPayload.getGatewayAuthenticationSuccessEvent().getUserCredentials().getUsername(),
                usernameArgumentCaptor.getValue(),
                String.format("Wrong username was used to reset authentication attempt. Should be: %s But was: %s",
                        bruteForceProtectionServiceTestPayload.getGatewayAuthenticationSuccessEvent().getUserCredentials().getUsername(),
                        usernameArgumentCaptor.getValue()));
    }

    @ParameterizedTest
    @ArgumentsSource(BruteForceProtectionServiceTestArgumentProvider.class)
    public void testOnAuthenticationFailure(BruteForceProtectionServiceTestPayload bruteForceProtectionServiceTestPayload) {
        //TEST
        defaultBruteForceProtectionService.onAuthenticationFailure(bruteForceProtectionServiceTestPayload.getGatewayAuthenticationFailureEvent());

        //VERIFY
        Mockito.verify(executorService, Mockito.atMostOnce()).submit(Mockito.any(Runnable.class));
    }

    @SneakyThrows
    @Test
    public void testAuthenticationAttemptsExceededForUsernameReturnsFalseIfAttemptsNotExceeded() {
        //SETUP
        Mockito.when(defaultAuthenticationAttemptService.attemptsCountForUsername(Mockito.anyString()))
                .thenReturn(TestProperties.getAuthAttemptLimit() - 1);
        String presumptiveUsername = TestProperties.getUsername();

        //TEST
        boolean isAuthenticationAttemptsExceeded = defaultBruteForceProtectionService.authenticationAttemptsExceededForUsername(presumptiveUsername);

        //VERIFY
        Mockito.verify(defaultAuthenticationAttemptService, Mockito.atMostOnce())
                .attemptsCountForUsername(Mockito.anyString());
        Mockito.verify(defaultAuthenticationAttemptService)
                .attemptsCountForUsername(usernameArgumentCaptor.capture());

        //ASSERT
        Assertions.assertFalse(isAuthenticationAttemptsExceeded,
                "Authentication attempts exceeded operation should return false if authentication attempts are not exceeded");
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getValue(),
                String.format("Wrong username was used to retrieve authentication attempt count. Should be: %s But was: %s",
                        presumptiveUsername,
                        usernameArgumentCaptor.getValue()));
    }

    @Test
    public void testAuthenticationAttemptsExceededForUsernameReturnsTrueIfAttemptsExceeded() {
        //SETUP
        Mockito.when(defaultAuthenticationAttemptService.attemptsCountForUsername(Mockito.anyString()))
                .thenReturn(TestProperties.getAuthAttemptLimit() + 1);
        String presumptiveUsername = TestProperties.getUsername();

        //TEST
        boolean isAuthenticationAttemptsExceeded = defaultBruteForceProtectionService.authenticationAttemptsExceededForUsername(presumptiveUsername);

        //VERIFY
        Mockito.verify(defaultAuthenticationAttemptService, Mockito.atMostOnce())
                .attemptsCountForUsername(Mockito.anyString());
        Mockito.verify(defaultAuthenticationAttemptService)
                .attemptsCountForUsername(usernameArgumentCaptor.capture());

        //ASSERT
        Assertions.assertTrue(isAuthenticationAttemptsExceeded,
                "Authentication attempts exceeded operation should return true if authentication attempts are exceeded");
        Assertions.assertEquals(presumptiveUsername, usernameArgumentCaptor.getValue(),
                String.format("Wrong username was used to retrieve authentication attempt count. Should be: %s But was: %s",
                        presumptiveUsername,
                        usernameArgumentCaptor.getValue()));
    }
}
