package unit.com.peachy.gateway.service.auth.token;

import argumentprovider.service.auth.token.TokenServiceTestArgumentProvider;
import argumentprovider.service.auth.token.TokenServiceTestPayload;
import com.peachy.commons.exception.badrequest.BadArgumentException;
import com.peachy.gateway.dto.token.GatewayAuthToken;
import com.peachy.gateway.service.auth.token.GatewayAuthTokenService;
import com.peachy.gateway.service.auth.token.RefreshTokenService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenPrefix;
import com.peachy.security.TokenType;
import com.peachy.security.dto.UserDTO;
import com.peachy.security.service.auth.token.InternalAuthTokenService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.misusing.UnnecessaryStubbingException;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import testdata.TestProperties;
import testdata.TokenServiceTestUtils;

import java.util.Collections;
import java.util.Map;

import static testdata.TokenServiceTestUtils.*;

@ExtendWith(MockitoExtension.class)
/**
 * {@link Strictness.LENIENT} was selected to ignore {@link UnnecessaryStubbingException}
 * */
@MockitoSettings(strictness = Strictness.LENIENT)
public class GatewayAuthTokenServiceTest extends JwtTokenServiceTest<GatewayAuthTokenServiceTest.GatewayAuthTokenServiceMock> {

    @Mock
    private RefreshTokenService refreshTokenService;

    @Mock
    private InternalAuthTokenService internalAuthTokenService;

    static class GatewayAuthTokenServiceMock extends GatewayAuthTokenService {
        public GatewayAuthTokenServiceMock(RefreshTokenService refreshTokenService, InternalAuthTokenService internalAuthTokenService) {
            super(refreshTokenService, internalAuthTokenService);
        }

        @Override
        public Map<String, Object> getTokenSpecificClaims(UserDTO user) {
            return super.getTokenSpecificClaims(user);
        }
    }

    @Override
    protected GatewayAuthTokenServiceMock getSpiedTokenServiceUnderTestFlow() {
        return Mockito.spy(new GatewayAuthTokenServiceMock(refreshTokenService, internalAuthTokenService));
    }

    @Override
    protected GatewayAuthTokenServiceMock getMockedTokenServiceUnderTestFlow() {
        return Mockito.mock(GatewayAuthTokenServiceMock.class, Mockito.withSettings().useConstructor(refreshTokenService, internalAuthTokenService));
    }

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateTokenWithEmptyAdditionalClaims(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        Mockito.when(internalAuthTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getInternalAuthToken());
        Mockito.when(refreshTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getRefreshToken());
        UserDTO user = tokenServiceTestPayload.getUserDTO();

        //TEST
        GatewayAuthToken actualGatewayAuthToken = getSpiedCurrentServiceUnderTest().generateTokenWithClaims(user, Collections.emptyMap());

        //VERIFY
        Mockito.verify(internalAuthTokenService, Mockito.atMostOnce())
                .generateTokenWithClaims(user, Collections.singletonMap(PeachyMindJwtClaim.TOKEN_ID, actualGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).orElse(StringUtils.EMPTY)));

        //ASSERT
        gatewayAuthTokenAssertions(actualGatewayAuthToken);
        Assertions.assertEquals(tokenServiceTestPayload.getUserDTO().getAuthority(), actualGatewayAuthToken.getAuthority(),
                String.format("Gateway token authority have to be %s, but is %s", tokenServiceTestPayload.getUserDTO().getAuthority(), actualGatewayAuthToken.getAuthority()));
    }

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateTokenWithAdditionalClaims(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        Mockito.when(internalAuthTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getInternalAuthToken());
        Mockito.when(refreshTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getRefreshToken());
        Pair<String, Object> testClaim = Pair.of("testClaimKey", "testClaimValue");
        UserDTO user = tokenServiceTestPayload.getUserDTO();

        //TEST
        GatewayAuthToken actualGatewayAuthToken =
                getSpiedCurrentServiceUnderTest().generateTokenWithClaims(user, Collections.singletonMap(testClaim.getLeft(), testClaim.getRight()));

        //VERIFY
        Mockito.verify(internalAuthTokenService, Mockito.atMostOnce())
                .generateTokenWithClaims(user, Collections.singletonMap(PeachyMindJwtClaim.TOKEN_ID, actualGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).orElse(StringUtils.EMPTY)));

        //ASSERT
        gatewayAuthTokenAssertions(actualGatewayAuthToken);
        Assertions.assertEquals(tokenServiceTestPayload.getUserDTO().getAuthority(), actualGatewayAuthToken.getAuthority(),
                String.format("Gateway token authority have to be %s, but is %s", tokenServiceTestPayload.getUserDTO().getAuthority(), actualGatewayAuthToken.getAuthority()));
        isAdditionalClaimMatches(actualGatewayAuthToken, testClaim);

    }

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateTokenWithAdditionalClaimsThatOverrideTokenSpecific(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        Mockito.when(internalAuthTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getInternalAuthToken());
        Mockito.when(refreshTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getRefreshToken());
        Pair<String, Object> testClaim = Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.NONE.name());
        UserDTO user = tokenServiceTestPayload.getUserDTO();

        //TEST
        GatewayAuthToken actualGatewayAuthToken =
                getSpiedCurrentServiceUnderTest().generateTokenWithClaims(user, Collections.singletonMap(testClaim.getLeft(), testClaim.getRight()));

        //VERIFY
        Mockito.verify(internalAuthTokenService, Mockito.atMostOnce())
                .generateTokenWithClaims(user, Collections.singletonMap(PeachyMindJwtClaim.TOKEN_ID, actualGatewayAuthToken.getAdditionalClaimForName(PeachyMindJwtClaim.TOKEN_ID, String.class).orElse(StringUtils.EMPTY)));

        //ASSERT
        jwtGeneratedTokenAssertions(actualGatewayAuthToken);
        Assertions.assertNotNull(actualGatewayAuthToken.getPrefixedRefreshToken(),
                "Gateway refresh token should not be null");
        Assertions.assertTrue(StringUtils.isNotBlank(actualGatewayAuthToken.getPrefixedRefreshToken()),
                "Gateway refresh token should not be empty");
        Assertions.assertTrue(isTokenPrefixed(actualGatewayAuthToken.getPrefixedRefreshToken(), TokenPrefix.REFRESH),
                String.format("Gateway refresh token should be prefixed with %s", TokenPrefix.REFRESH));
        Assertions.assertEquals((long) TestProperties.getGatewayTokenLifetime(), actualGatewayAuthToken.getExpiresAtUnix() - actualGatewayAuthToken.getIssuedAtUnix(),
                String.format("Gateway token lifetime should be %s, but is %s", TestProperties.getGatewayTokenLifetime(), actualGatewayAuthToken.getExpiresAtUnix() - actualGatewayAuthToken.getIssuedAtUnix()));
        Assertions.assertEquals(actualGatewayAuthToken.getPrefix(), TokenPrefix.BEARER,
                String.format("Gateway token prefix have to be %s, but is %s", TokenPrefix.BEARER.getStringyPrefix(), actualGatewayAuthToken.getStringyPrefix()));
        Assertions.assertTrue(isTokenPrefixed(actualGatewayAuthToken.getPrefixedToken(), TokenPrefix.BEARER));
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(actualGatewayAuthToken, PeachyMindJwtClaim.INTERNAL_TOKEN);
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(actualGatewayAuthToken, PeachyMindJwtClaim.TOKEN_ID);
        TokenServiceTestUtils.isAdditionalClaimNotEmpty(actualGatewayAuthToken, PeachyMindJwtClaim.SUBJECT);
        Assertions.assertEquals(tokenServiceTestPayload.getUserDTO().getAuthority(), actualGatewayAuthToken.getAuthority(),
                String.format("Gateway token authority have to be %s, but is %s", tokenServiceTestPayload.getUserDTO().getAuthority(), actualGatewayAuthToken.getAuthority()));
        isAdditionalClaimMatches(actualGatewayAuthToken, testClaim);
    }

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateToken(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        Mockito.doReturn(tokenServiceTestPayload.getGatewayAuthToken()).when(getSpiedCurrentServiceUnderTest()).generateTokenWithClaims(Mockito.any(), Mockito.anyMap());
        Mockito.when(refreshTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getRefreshToken());

        //TEST
        GatewayAuthToken actual = getSpiedCurrentServiceUnderTest().generateToken(tokenServiceTestPayload.getUserDTO());

        //VERIFY
        Mockito.verify(getSpiedCurrentServiceUnderTest(), Mockito.atMostOnce()).generateToken(tokenServiceTestPayload.getUserDTO());
        Mockito.verify(refreshTokenService, Mockito.atMostOnce())
                .generateTokenWithClaims(tokenServiceTestPayload.getUserDTO(), Collections.singletonMap(PeachyMindJwtClaim.TOKEN_ID, tokenServiceTestPayload.getTokenId()));
        //ASSERT
        Assertions.assertNotNull(
                tokenServiceTestPayload.getGatewayAuthToken().getPrefixedRefreshToken(),
                "Gateway token should carry refresh token within");
        Assertions.assertTrue(
                StringUtils.isNotBlank(actual.getPrefixedRefreshToken()),
                "Gateway token should carry refresh token within");
        Assertions.assertTrue(
                isTokenPrefixed(actual.getPrefixedRefreshToken(), tokenServiceTestPayload.getRefreshToken().getPrefix()),
                "Gateway token should carry prefixed refresh token within");

    }

    @SuppressWarnings("unchecked")
    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateTokenIfTokenIdIsEmptyThenThrowBadArgumentException(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        Mockito.doAnswer(answer -> Collections.emptyMap())
                .when(getSpiedCurrentServiceUnderTest()).getTokenSpecificClaims(Mockito.any(UserDTO.class));
        Mockito.when(refreshTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getRefreshToken());
        Mockito.when(internalAuthTokenService.generateTokenWithClaims(Mockito.any(UserDTO.class), Mockito.anyMap()))
                .then(answer -> tokenServiceTestPayload.getInternalAuthToken());


        //TEST AND ASSERT
        Assertions.assertThrows(
                BadArgumentException.class,
                () -> getSpiedCurrentServiceUnderTest().generateToken(tokenServiceTestPayload.getUserDTO()),
                String.format("Should throw %s due to tid claim absence", BadArgumentException.class.getSimpleName()));
    }
}