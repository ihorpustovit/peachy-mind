package unit.com.peachy.gateway.service.auth.token;

import argumentprovider.service.auth.token.TokenServiceTestArgumentProvider;
import argumentprovider.service.auth.token.TokenServiceTestPayload;
import com.peachy.gateway.dto.token.RefreshToken;
import com.peachy.gateway.service.auth.token.RefreshTokenService;
import com.peachy.security.PeachyMindJwtClaim;
import com.peachy.security.TokenType;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import testdata.TestProperties;

import java.util.Collections;

import static testdata.TokenServiceTestUtils.*;

@ExtendWith(MockitoExtension.class)
public class RefreshTokenServiceTest {

    private RefreshTokenService refreshTokenServiceSpy;

    @BeforeEach
    public void beforeEach() {
        refreshTokenServiceSpy = Mockito.spy(new RefreshTokenService());
        Mockito.when(refreshTokenServiceSpy.getIssuer()).then(answer -> TestProperties.getIssuer());
        Mockito.when(refreshTokenServiceSpy.getLifetime()).then(answer -> TestProperties.getRefreshTokenLifetime());
        Mockito.when(refreshTokenServiceSpy.getSignatureAlgorithmName()).then(answer -> TestProperties.getSignatureAlgorithmName());
        Mockito.when(refreshTokenServiceSpy.getSignature()).then(answer -> TestProperties.getGatewayAuthTokenSignature());
    }

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateTokenWithEmptyAdditionalClaims(TokenServiceTestPayload tokenServiceTestPayload) {
        //TEST
        RefreshToken actualRefreshToken = refreshTokenServiceSpy.generateTokenWithClaims(tokenServiceTestPayload.getUserDTO(), Collections.emptyMap());

        //ASSERT
        refreshTokenAssertions(actualRefreshToken);
        isAdditionalClaimNotEmpty(actualRefreshToken, PeachyMindJwtClaim.TOKEN_ID);
    }

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateTokenWithAdditionalClaims(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        Pair<String, Object> testClaim = Pair.of("testClaimKey", "testClaimValue");

        //TEST
        RefreshToken actualRefreshToken =
                refreshTokenServiceSpy.generateTokenWithClaims(tokenServiceTestPayload.getUserDTO(), Collections.singletonMap(testClaim.getLeft(), testClaim.getRight()));

        //ASSERT
        refreshTokenAssertions(actualRefreshToken);
        isAdditionalClaimMatches(actualRefreshToken, testClaim);
    }

    @ParameterizedTest
    @ArgumentsSource(TokenServiceTestArgumentProvider.class)
    public void testGenerateTokenWithAdditionalClaimsThatOverrideTokenSpecific(TokenServiceTestPayload tokenServiceTestPayload) {
        //SETUP
        Pair<String, Object> testClaim = Pair.of(PeachyMindJwtClaim.TOKEN_TYPE, TokenType.NONE.name());

        //TEST
        RefreshToken actualRefreshToken =
                refreshTokenServiceSpy.generateTokenWithClaims(tokenServiceTestPayload.getUserDTO(), Collections.singletonMap(testClaim.getLeft(), testClaim.getRight()));

        //ASSERT
        isAdditionalClaimMatches(actualRefreshToken, testClaim);
    }
}
