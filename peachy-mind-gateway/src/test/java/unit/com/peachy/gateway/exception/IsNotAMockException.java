package unit.com.peachy.gateway.exception;

import com.peachy.commons.exception.badrequest.BadArgumentException;

public class IsNotAMockException extends BadArgumentException {
    public IsNotAMockException(String message) {
        super(message);
    }
}
