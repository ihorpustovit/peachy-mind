Available profiles:
- dev (development profile. Ordinary is used when deploying application in cloud dev environment)
- local (profile for local development, that expects all microservices to be running locally with that same profile)
- standalone (profile for local development)